<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentTestProgress extends Model
{
    protected $table = 'student_test_progress';
}
