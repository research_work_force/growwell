<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuestionTransaction extends Model
{
    protected $table = 'question_transaction';

    protected $fillable = array('q_id','answer','batch_id','s_id','section_id','attempt_status');

   //   public static function insertData($data){

   //    $value=DB::table('questions')->get();
   //    if($value->count() == 0){
   //       DB::table('questions')->insert($data);
   //    }

   // }


}
