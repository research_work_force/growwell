<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionMapTransaction extends Model
{
    protected $table = 'question_map_transaction';
}
