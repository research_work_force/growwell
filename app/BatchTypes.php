<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchTypes extends Model
{
    protected $table = 'batch_type';
}
