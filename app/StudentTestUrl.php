<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentTestUrl extends Model
{
    protected $table = 'student_test_url';
}
