<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessorLogin extends Model
{
    protected $table = 'assessor_login';
}
