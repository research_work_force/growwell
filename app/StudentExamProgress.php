<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentExamProgress extends Model
{
    protected $table = 'student_exam_progress';
}
