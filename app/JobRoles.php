<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobRoles extends Model
{
    protected $table = 'job_role';
}
