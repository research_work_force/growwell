<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssessorHistory extends Model
{
    protected $table = 'assessor_history';
}
