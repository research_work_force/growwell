<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchHistory extends Model
{
    protected $table = 'batch_history';
}
