<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentLogin extends Model
{
    protected $table = 'student_login';
}
