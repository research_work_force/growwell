<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchReviewComment extends Model
{
    protected $table = 'batch_review_comments';
}
