<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assessor extends Model
{
    protected $table = 'assessor';
    protected $fillable = array('assessor_id','name','assessment_agency_aligned','linking_type','duration_fdt','duration_tdt','sectors','phone','email','domain_job_role','toa_status','state');
}
