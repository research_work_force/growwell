<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Question extends Model
{
    protected $table = 'questions';
    
    protected $fillable = array('id','question','option1','option2','option3','option4','right_answer','job_role','section','added_by','question_hindi','option1_hindi','option2_hindi','option3_hindi','option4_hindi','right_ans_hindi','question_teleugu','option1_teleugu','option2_teleugu','option3_teleugu','option4_teleugu','right_ans_teleugu','question_assam','option1_assam','option2_assam','option3_assam','option4_assam','right_ans_assam');



}
