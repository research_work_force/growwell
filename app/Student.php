<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    protected $fillable = array('name','username','address','email','batch_id','spoc','spoc_email','spoc_phone','added_by','dropout_status');
}
