<?php

namespace App\Imports;

use App\Student;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;
use App\Assessor;
use App\AssessorLogin;
use Illuminate\Support\Str;
use Mail;

class AssessorImport implements ToModel,WithStartRow //implements ToCollection 
{
   

    public function model(array $row)
    {
             $assessor_email_id_checking = Assessor::where('email',$row[8])->first();
             $assessor_assessor_id_checking = Assessor::where('assessor_id',$row[0])->first();

              if(empty($assessor_email_id_checking) && empty($assessor_assessor_id_checking)){
                      
                  

                        $pass = Str::random(9);
                        $assessor_login = new AssessorLogin();
                        $assessor_login->username = $row[0];
                        $assessor_login->password = Hash::make($pass);
                        $assessor_login->save();

                        //file save to folder
                        $supdoc_filename=null;

                         // if(!empty($row[12])){


                         //  $supdoc= $row[12];
                         //  $ext = $supdoc->getClientOriginalExtension();

                         //      if($ext == 'pdf' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){ //Extension checking


                         //                          $supdoc_filename = $supdoc->getClientOriginalName();
                         //                          $path =  "growwell/assessors/".$row[0]."/supdoc/";
                         //                          $supdoc->move(public_path($path), $supdoc_filename);
                         //          }
                         // }


                        //mail to employee
                          $to_name=$row[1];
                          $to_email=$row[8];
                          $passw=$pass;

                          $data=array('name' => "$to_name",'u_id'=>"$row[0]",'pass'=>"$passw");
                          Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                    $message->to($to_email)->subject('Growell Assessor Account');
                                  });

                                   
                       return Assessor::create([
                       'assessor_id'                              => $row[0], 
                       'name'                                     => $row[1],
                       'assessment_agency_aligned'                => $row[2], 
                       'linking_type'                             => $row[3],
                       'duration_fdt'                             => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[4])), 
                       'duration_tdt'                             => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5])),
                       'sectors'                                  => $row[6],
                       'phone'                                    => $row[7],
                       'email'                                    => $row[8],
                       'domain_job_role'                          => $row[9],
                       'toa_status'                               => $row[10],
                       'state'                                    => $row[11],
                       'added_by' => \Session::get('adminsession'),
                       
                        ]);
            
               }

    }
    
   /**
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        echo "eror"; die;
    }


    public function checkRecord($row)
    {
      $check_job_id = 1; //\DB::table('jobs')->where('job_id',$row[9])->first();

      return $check_job_id;
    }

    public function startRow(): int
    {
        return 2; //excel value starting from second row and first row is header
    }
}
