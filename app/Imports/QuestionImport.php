<?php

namespace App\Imports;

use App\Question;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;

class QuestionImport implements ToModel,WithStartRow //implements ToCollection 
{
     protected $section = 0;
     protected $jobRole = '';

    function __construct($jobRole,$section) {
        $this->section = $section;
        $this->jobRole = $jobRole;
     }

    public function model(array $row)
    {
      

           return Question::create([
           'question'     => $row[0], 
           'option1'     => $row[1],
           'option2'  =>  $row[2], 
           'option3'     => $row[3],
           'option4'    => $row[4], 
           'right_answer'     => $row[5],
           'job_role' =>    $this->jobRole,
           'section'  => $this->section,
           'added_by'     => "admin",
           
        ]);

       
    }



    public function checkRecord($row)
    {
      $check_job_id = 1; //\DB::table('jobs')->where('job_id',$row[9])->first();

      return $check_job_id;
    }

    public function startRow(): int
    {
        return 2; //excel value starting from second row and first row is header
    }
}
