<?php

namespace App\Imports;

use App\Student;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;
use App\StudentLogin;
use Illuminate\Support\Str;

class StudentImport implements ToModel,WithStartRow //implements ToCollection 
{ 
   protected $batch_id = 0;

    function __construct($bt_id) {
        $this->batch_id = $bt_id;
     }

    public function model(array $row)
    {
        $candidate_id_checking = Student::where('username',$row[0])->first();

            if(empty($candidate_id_checking)){

           return Student::create([
           'name'     => $row[1], 
           'username' => $row[0],
           'address'     => $row[2],
           'email' => $row[3],
           'batch_id'  => $this->batch_id, 
           'spoc'     => $row[4],
           'spoc_email'    => $row[5], 
           'spoc_phone'     => $row[6],
           'dropout_status'     => $row[7],
           'added_by' => \Session::get('adminsession'),
           
             ]);
         }

       
    }



    public function checkRecord($row)
    {
      $check_job_id = 1; //\DB::table('jobs')->where('job_id',$row[9])->first();

      return $check_job_id;
    }

    public function startRow(): int
    {
        return 2; //excel value starting from second row and first row is header
    }
}
