<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\AssessorLogin;
use Illuminate\Support\Str;
use App\Imports\AssessorImport;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Mail;
use App\BatchAssignProcess;
use Exporter;
use Importer;

class AssessorController extends Controller
{
     public function assessorDataInsert(Request $request)
    {

             $assessor_id        = $request->input('assessor_id');
             $name        = $request->input('name');
             $assessment_agency_aligned        = $request->input('assessment_agency_aligned');
             $linking_type        = $request->input('linking_type');
             $fdt        = $request->input('fdt');
             $tdt        = $request->input('tdt');
             $sectors        = $request->input('sectors');
             $phone        = $request->input('phone');
             $email        = $request->input('email');
             $domain_job_role        = $request->input('domain_job_role');
             $toa_status        = $request->input('toa_status');
             $state        = $request->input('state');
             $img = "default.png";




            //Save the support doc
               if($request->hasFile('supdoc')){


                          $supdoc= $request->file('supdoc');
                          $ext = $supdoc->getClientOriginalExtension();

                        if($ext == 'pdf' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){ //Extension checking

                            $assessor_id_checking = Assessor::where('assessor_id',$assessor_id)->first();


                                   if(empty($assessor_id_checking)){

                                      $assessor_emailid_checking = Assessor::where('email',$email)->first();

                                      if(empty($assessor_emailid_checking)){


                                            $supdoc_filename = $supdoc->getClientOriginalName();
                                            $path =  "growwell/assessors/".$assessor_id."/supdoc/";
                                            $supdoc->move(public_path($path), $supdoc_filename);


                                             $assessor_data = new Assessor();
                                             $assessor_data->assessor_id = $assessor_id;
                                             $assessor_data->name   =      $name;
                                             $assessor_data->assessment_agency_aligned   =  $assessment_agency_aligned;
                                             $assessor_data->linking_type    = $linking_type;
                                             $assessor_data->duration_fdt    = $fdt;
                                             $assessor_data->duration_tdt    = $tdt;
                                             $assessor_data->sectors   =  $sectors;
                                             $assessor_data->phone    = $phone;
                                             $assessor_data->email    = $email;
                                             $assessor_data->supdoc   = $supdoc_filename;
                                             $assessor_data->domain_job_role   =  $domain_job_role;
                                             $assessor_data->toa_status    = $toa_status;
                                             $assessor_data->state    = $state;
                                             $assessor_data->profile_img = $img;
                                             $assessor_data->added_by = \Session::get('adminsession');
                                             $assessor_data->save();

                                             $pass = Str::random(9);
                                             $assessor_password=Hash::make($pass);

                                             $assessorLogin = new AssessorLogin();
                                             $assessorLogin->username = $assessor_id;
                                             $assessorLogin->password = $assessor_password;
                                             $assessorLogin->save();


                                              //mail to assessor
                                                $to_name=$name;
                                                $to_email=$email;

                                              $data=array('name' => "$to_name",'u_id'=>"$assessor_id",'pass'=>$pass);
                                                Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                                  $message->to($to_email)->subject('Growell Assessor Account');
                                                });


                                                 \Session::flash('message', 'Assessor created successfully');
                                                  \Session::flash('status', 'success');

                                                  return redirect()->back();


                                                 }else{

                                                        \Session::flash('message', 'EmailID will be different');
                                                        \Session::flash('status', 'warning');

                                                        return redirect()->back();


                                                 }

                                         }else{

                                                  \Session::flash('message', 'Username will be different');
                                                  \Session::flash('status', 'warning');

                                                  return redirect()->back();
                                    }



                          }else{

                                        \Session::flash('message', 'File extension not supported');
                                        \Session::flash('status', 'danger');

                                        return redirect()->back();

                          }


               }else{

                 $assessor_id_checking = Assessor::where('assessor_id',$assessor_id)->first();

                                   if(empty($assessor_id_checking)){
                                  
                                  $assessor_emailid_checking = Assessor::where('email',$email)->first();
                                      
                                      if(empty($assessor_emailid_checking)){


                                             $assessor_data = new Assessor();
                                             $assessor_data->assessor_id = $assessor_id;
                                             $assessor_data->name   =      $name;
                                             $assessor_data->assessment_agency_aligned   =  $assessment_agency_aligned;
                                             $assessor_data->linking_type    = $linking_type;
                                             $assessor_data->duration_fdt    = $fdt;
                                             $assessor_data->duration_tdt    = $tdt;
                                             $assessor_data->sectors   =  $sectors;
                                             $assessor_data->phone    = $phone;
                                             $assessor_data->email    = $email;
                                             $assessor_data->supdoc   = 'default.png';
                                             $assessor_data->domain_job_role   =  $domain_job_role;
                                             $assessor_data->toa_status    = $toa_status;
                                             $assessor_data->state    = $state;
                                             $assessor_data->profile_img = $img;
                                             $assessor_data->added_by = \Session::get('adminsession');
                                             $assessor_data->save();

                                             $pass = Str::random(9);
                                             $assessor_password=Hash::make($pass);

                                             $assessorLogin = new AssessorLogin();
                                             $assessorLogin->username = $assessor_id;
                                             $assessorLogin->password = $assessor_password;
                                             $assessorLogin->save();


                                              //mail to assessor
                                                $to_name=$name;
                                                $to_email=$email;

                                              $data=array('name' => "$to_name",'u_id'=>"$assessor_id",'pass'=>$pass);
                                                Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                                  $message->to($to_email)->subject('Growell Assessor Account');
                                                });


                                                 \Session::flash('message', 'Assessor created successfully');
                                                  \Session::flash('status', 'success');

                                                  return redirect()->back();


                                                 }else{

                                                        \Session::flash('message', 'Email-ID will be different');
                                                        \Session::flash('status', 'warning');

                                                        return redirect()->back();


                                                 }

                                         }else{

                                                  \Session::flash('message', 'Username will be different');
                                                  \Session::flash('status', 'warning');

                                                  return redirect()->back();
                                    }


               }



    }

    public  function bulkAssessorUpload(Request $request)
    {
        $assessor_file = $request->file('assessor_file');

         // CSV to Database
            $filename = $assessor_file->getClientOriginalName();

            //Save the file temporarily
            $ext = $assessor_file->getClientOriginalExtension();

            if($ext == 'xlsx'){


                            $assessor_file->move(public_path('growwell/temp/'), $assessor_file->getClientOriginalName());

                            $url = public_path('growwell/temp/'.$filename);

                            // Excel::import(new AssessorImport, $url);

                            $excel = Importer::make('Excel');
                            $excel->load($url);
                            $collection = $excel->getCollection();

                            if(sizeof($collection[0])==12){
                                  for($row=1; $row< sizeof($collection); $row++) {

                                    try{

                                      $assessor_email_id_checking = Assessor::where('email',$collection[$row][8])->first();
                                      $assessor_assessor_id_checking = Assessor::where('assessor_id',$collection[$row][0])->first();

                                       if(empty($assessor_email_id_checking) && empty($assessor_assessor_id_checking)){
                                                    // Assessor login info for assessor
                                                      $pass = Str::random(9);
                                                      $assessor_login = new AssessorLogin();
                                                      $assessor_login->username = $collection[$row][0];
                                                      $assessor_login->password = Hash::make($pass);
                                                      $assessor_login->save();

                                                    // Email to every assessor
                                                    $to_name=$collection[$row][1];
                                                    $to_email=$collection[$row][8];
                                                    $assessor_id = $collection[$row][0];
                                                    $passw=$pass;

                                                    $data=array('name'=>"$to_name",'u_id'=>$assessor_id,'pass'=>"$passw");
                                                    Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                                              $message->to($to_email)->subject('Growell Assessor Account');
                                                            });

                                                   // Assessor Profile
// Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject())
                                                Assessor::create([
                                                   'assessor_id'                              => $collection[$row][0],
                                                   'name'                                     => $collection[$row][1],
                                                   'assessment_agency_aligned'                => $collection[$row][2],
                                                   'linking_type'                             => $collection[$row][3],
                                                   'duration_fdt'                             => $collection[$row][4],
                                                   'duration_tdt'                             => $collection[$row][5],
                                                   'sectors'                                  => $collection[$row][6],
                                                   'phone'                                    => $collection[$row][7],
                                                   'email'                                    => $collection[$row][8],
                                                   'domain_job_role'                          => $collection[$row][9],
                                                   'toa_status'                               => $collection[$row][10],
                                                   'state'                                    => $collection[$row][11],
                                                   'added_by' => \Session::get('adminsession'),

                                                    ]);



                                         }
                                         // else{

                                         //     \Session::flash('message', 'Assessor ID Sh');
                                         //      \Session::flash('status', 'danger');
                                         //     return redirect()->back();


                                         // }





                                    }catch(\Exception $e){

                                        \Session::flash('message', $e->getMessage());
                                        \Session::flash('status', 'danger');
                                       return redirect()->back();
                                    }





                                  }

                                  \Session::flash('message', 'Assessors Uploaded');
                                                      \Session::flash('status', 'success');

                                                      return redirect()->back();


                            }else{

                               \Session::flash('message', 'Please prefer our Excel format for Import');
                                \Session::flash('status', 'warning');

                                return redirect()->back();

                            }


            }

            else{
                            \Session::flash('message', 'Only Accept Xlsx');
                            \Session::flash('status', 'warning');

                            return redirect()->back();

            }

    }

    public function assessorList()
    {
      $assessorlist=Assessor::orderBy('created_at','desc')->paginate(10);
      return view('admin.pages.assessormanagement')->with('assessorlist',$assessorlist);

    }

       public  function assessorListSearch(request $request)
       {
              $name=$request->input('name');
              $id=$request->input('id');

             if(isset($name)==1){
              $assessorlist = Assessor::where('name','LIKE', '%' . $name . '%')->orderBy('created_at','desc')->paginate(10);
                  return view('admin.pages.assessormanagement',compact('assessorlist'));
             }


             if(isset($id)==1){
              $assessorlist = Assessor::where('id','LIKE', '%' . $id . '%')->orderBy('created_at','desc')->paginate(10);
                  return view('admin.pages.assessormanagement',compact('assessorlist'));
             }


             if(isset($name)==1 && isset($id)==1){
              $assessorlist = Assessor::where('name','LIKE', '%' . $name . '%')->where('name','LIKE', '%' . $id . '%')->orderBy('created_at','desc')->paginate(10);
                  return view('admin.pages.assessormanagement',compact('assessorlist'));
             }


             else{
                $assessorlist = Assessor::orderBy('created_at', 'desc')->paginate(10);
                return view('admin.pages.assessormanagement')
                                      ->with('assessorlist',$assessorlist);
                  }


       }


       public  function searchAssessor()
       {
          $search_assessor_result = Assessor::orderBy('created_at','desc')->paginate(10);
          return view('admin.pages.searchassessor')->with('search_result',$search_assessor_result);
       }

       public function searchAssessorByFilters(Request $request)
      {
        $states = $request->input('states');
        $assessor_id = $request->input('assessor_id');
        $assessorname = $request->input('assessorname');

        $domain_job_role = $request->input('domain_job_role');
        $toa_status = $request->input('toa_status');

        $batch_search_result = Assessor::orWhere('state',$states)
                                        ->orWhere('assessor_id',$assessor_id)
                                        ->orWhere('name',$assessorname)
                                        ->orWhere('domain_job_role',$domain_job_role)
                                        ->orWhere('toa_status',$toa_status)
                                        ->orderBy('created_at','desc')->paginate(10);


        return view('admin.pages.assessormanagement')->with('assessorlist', $batch_search_result);

      }

      public  function deleteAssessor(Request $request)
      {
         $id =$request->input('id');
         $username = $request->input('username');
         $assigned_assessor_status = BatchAssignProcess::where('assessor_id',$username)->first();

         if(empty($assigned_assessor_status)){

               $AssessorProfile = Assessor::find($id)->delete(); 
               $AssessorLoginTab = AssessorLogin::where('username', $username)->delete();

                \Session::flash('message', 'Assessor Deleted');
                \Session::flash('status', 'success');

               return redirect()->back();
         }else{

                \Session::flash('message', 'Assessor already assigned to a batch');
                \Session::flash('status', 'danger');

               return redirect()->back();
         }
        
      }

      public function updateAssessor(Request $request)
      {
        $name        = $request->input('name');
        $assessment_agency_aligned        = $request->input('assessment_agency_aligned');
        $linking_type        = $request->input('linking_type');
        $phone        = $request->input('phone');
        $fdt        = $request->input('fdt');
        $tdt        = $request->input('tdt');
        $email        = $request->input('email');
        $sectors = $request->input('sectors');
        $domain_job_role        = $request->input('domain_job_role');
        $toa_status        = $request->input('toa_status');
        $states = $request->input('state');
        $id = $request->input('id');

        //Save the support doc
               if($request->hasFile('supdoc')){


                          $supdoc= $request->file('supdoc');
                          $ext = $supdoc->getClientOriginalExtension();

                        if($ext == 'pdf' || $ext == 'jpg' || $ext == 'png' || $ext == 'jpeg'){ //Extension checking


                                      $assessor_id_checking = Assessor::where('id',$id)->first();

                                      // if(count($assessor_id_checking) == 1){

                                      $supdoc_filename = $supdoc->getClientOriginalName();
                                      $path =  "growwell/assessors/".$assessor_id_checking['assessor_id']."/supdoc/";
                                      $supdoc->move(public_path($path), $supdoc_filename);

                                       $admin = Assessor::find($id);
                                       $admin->name = $name;
                                       $admin->assessment_agency_aligned  = $assessment_agency_aligned;
                                       $admin->linking_type = $linking_type;
                                       $admin->phone = $phone;
                                       $admin->duration_fdt = $fdt;
                                       $admin->duration_tdt = $tdt;
                                       $admin->email = $email;
                                       $admin->sectors = $sectors;
                                       $admin->domain_job_role = $domain_job_role;
                                       $admin->toa_status = $toa_status;
                                       $admin->supdoc = $supdoc_filename;
                                       $admin->state = $states;
                                       $admin->added_by = \Session::get('adminsession');


                                       $admin->save();

                                       \Session::flash('message', 'Updated');
                                       \Session::flash('status', 'success');

                                       return redirect()->back();

                                      // }// EmailID checking
                                      //   else{
                                      //             \Session::flash('message', 'Email Id will be different');
                                      //             \Session::flash('status', 'warning');

                                      //             return redirect()->back();
                                      //   }

                          } // file extension checking
                            else{

                                                  \Session::flash('message', 'File extension is not allowed');
                                                  \Session::flash('status', 'warning');

                                                  return redirect()->back();
                            }
              }// file checking
                else{

                                      // $assessor_id_checking = Assessor::where('id',$id)->first();

                                      // if($assessor_id_checking['email'] != $email){

                                       $admin = Assessor::find($id);
                                       $admin->name = $name;
                                       $admin->assessment_agency_aligned  = $assessment_agency_aligned;
                                       $admin->linking_type = $linking_type;
                                       $admin->phone = $phone;
                                       $admin->duration_fdt = $fdt;
                                       $admin->duration_tdt = $tdt;
                                       $admin->email = $email;
                                       $admin->sectors = $sectors;
                                       $admin->domain_job_role = $domain_job_role;
                                       $admin->toa_status = $toa_status;
                                       $admin->state = $states;
                                       $admin->added_by = \Session::get('adminsession');


                                       $admin->save();

                                       \Session::flash('message', 'Updated');
                                       \Session::flash('status', 'success');
                                       return redirect()->back();

                                     // }// EmailID checking
                                     //    else{
                                     //              \Session::flash('message', 'Email Id will be different');
                                     //              \Session::flash('status', 'warning');

                                     //              return redirect()->back();
                                     //    }

                }
      }

      public function updateAssessorProfileData(Request $request)
      {

                         $name = $request->input('name');
                         $gender  = $request->input('gender');
                         $religion = $request->input('religion');
                         $date_of_birth = $request->input('date');
                         $language_known = $request->input('Language');
                         $phone = $request->input('phone');
                         $email = $request->input('email');
                         $address = $request->input('Address');
                         $landmark = $request->input('landmark');
                         $pincode = $request->input('pin');
                         $state = $request->input('state');
                         $education = $request->input('education');
                         $industrial_experience = $request->input('experience');
                         $city = $request->input('city');
                         $applicant_type = $request->input('applicant_type');

                         $assessment_agency_aligned = $request->input('assessment_agency_aligned');
                         $linking_type = $request->input('linking_type');
                         $availability = $request->input('availability');
                         $duration_fdt = $request->input('duration_fdt');
                         $duration_tdt = $request->input('duration_tdt');
                         $toa_status = $request->input('toa_status');
                         $domain_job_role = $request->input('domain_job_role');
                         $sectors = $request->input('sectors');

                         $added_by = \Session::get('assessorsession');
                         $profile_pic;

                         if($request->file('profile_img')){

                           $profile_img = $request->file('profile_img');


                           $filename = $profile_img->getClientOriginalName();

                           $profile_img->move(public_path('growwell/profile/'), $profile_img->getClientOriginalName());

                          $profile_pic = $filename;

                            $task =Assessor::where('assessor_id',\Session::get('assessorsession'))
                                         ->update(
                                            array(

                                                  'name'       => $name,
                                                  'gender'      =>$gender,
                                                  'religion'      =>$religion,
                                                  'date_of_birth'      =>$date_of_birth,
                                                  'language_known'      =>$language_known,
                                                  'phone'      =>$phone,
                                                  'email'      =>$email,
                                                  'address'      =>$address,
                                                  'landmark'       => $landmark,
                                                  'pincode'      =>$pincode,
                                                  'state'      =>$state,
                                                  'education'      =>$education,
                                                  'industrial_experience'      =>$industrial_experience,
                                                  'city'      =>$city,
                                                  'applicant_type'      =>$applicant_type,
                                                  'added_by'      =>$added_by,
                                                   'profile_img'   => $profile_pic,
                                                   'assessment_agency_aligned'      =>$assessment_agency_aligned,
                                                  'linking_type'      =>$linking_type,
                                                  'availability'      =>$availability,
                                                  'duration_fdt'      =>$duration_fdt,
                                                  'duration_tdt'      =>$duration_tdt,
                                                  'toa_status'      =>$toa_status,
                                                  'domain_job_role'      =>$domain_job_role,
                                                   'sectors'   => $sectors

                                                 ));


                        }else {

                                $task =Assessor::where('assessor_id',\Session::get('assessorsession'))
                                         ->update(
                                            array(

                                                  'name'       => $name,
                                                  'gender'      =>$gender,
                                                  'religion'      =>$religion,
                                                  'date_of_birth'      =>$date_of_birth,
                                                  'language_known'      =>$language_known,
                                                  'phone'      =>$phone,
                                                  'email'      =>$email,
                                                  'address'      =>$address,
                                                  'landmark'       => $landmark,
                                                  'pincode'      =>$pincode,
                                                  'state'      =>$state,
                                                  'education'      =>$education,
                                                  'industrial_experience'      =>$industrial_experience,
                                                  'city'      =>$city,
                                                  'applicant_type'      =>$applicant_type,
                                                  'added_by'      =>$added_by,
                                                  'assessment_agency_aligned'      =>$assessment_agency_aligned,
                                                  'linking_type'      =>$linking_type,
                                                  'availability'      =>$availability,
                                                  'duration_fdt'      =>$duration_fdt,
                                                  'duration_tdt'      =>$duration_tdt,
                                                  'toa_status'      =>$toa_status,
                                                  'domain_job_role'      =>$domain_job_role,
                                                   'sectors'   => $sectors

                                                 ));

                        }


                                       \Session::flash('message', 'Updated');
                                       \Session::flash('status', 'success');

                            return back();
      }


      public  function assessorProfilePage()
      {
         if(\Session::has('assessorsession')){

          $singleassessor_data = Assessor::where('assessor_id',\Session::get('assessorsession'))->first();


                // print_r(\Session::get('assessorsession')); die;
          return view('assessor.pages.profile')->with('singleassessor',$singleassessor_data);
         }
          else{
            return redirect(route('assessorlogin'));
          }


      }


      public  function assessorProfileAdmin($assessor_id)
      {
          $singleassessor_data = Assessor::where('assessor_id',$assessor_id)->first();

          return view('admin.pages.assessorprofileview')->with('singleassessor',$singleassessor_data);
      }







}
