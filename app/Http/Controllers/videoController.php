<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\AssessorLogin;
use Illuminate\Support\Str;
use App\Imports\AssessorImport;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Mail;
use App\Question;
use App\Batches;
use App\QuestionTransaction;
use Illuminate\Pagination\LengthAwarePaginator;
use App\StudentTestUrl;
use App\Student;
use App\BatchReviewComment;
use App\BatchAssignProcess;
use File;

class videoController extends Controller
{

     public function savevideo(Request $request)
     {
       //
       $data = $request->data;
       $filename = $request->fname;
      // echo($data);
      $imageName = \Session::get('studentsession').'.webm';

      $update_recorded_video_name = Student::where('username',\Session::get('studentsession'))
                                            ->update(array('recorded_video_name'=>$imageName));

          $data->move(public_path("growwell/student/"), $imageName);

          return true;

     }





}
