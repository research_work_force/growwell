<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Carbon\Carbon;
use App\StudentLogin;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Imports\StudentImport;
use App\StudentTestUrl;
use Mail;
use DateTime;
use App\Question;
use App\Batches;
use App\BatchAssignProcess;
use App\Assessor;

class StudentController extends Controller   
{
   public function studentDataInsert(Request $request)
   {	
            $candidate_id_checking = Student::where('username',$request->input('candidate_id'))->first();
            $batch_id = $request->input('batch_id');

            $batch_checking = BatchAssignProcess::where('batch_id', $batch_id)->first();


            if(empty($batch_checking)){



            if(empty($candidate_id_checking)){
   				
               $admin = new Student();				             
	             $admin->name = $request->input('name');
	             $admin->username = $request->input('candidate_id');
	             $admin->address = $request->input('address');
               $admin->email = $request->input('email');
	             $admin->batch_id = $request->input('batch_id');
	             $admin->spoc = $request->input('spoc');
	             $admin->spoc_email = $request->input('spoc_email');
	             $admin->spoc_phone = $request->input('spoc_phone');	
               $admin->dropout_status = $request->input('dropout_status');              
	             $admin->added_by = \Session::get('adminsession');
	             $admin->save();

	                       \Session::flash('message', 'Candidate created successfully'); 
                            \Session::flash('status', 'success'); 
                            return redirect()->back();
            }else{
                           \Session::flash('message', 'Candidate ID is already exists'); 
                            \Session::flash('status', 'danger'); 
                            return redirect()->back();

            } 

          }else{

                        \Session::flash('message', 'Candidate is not allowed in assigned batch'); 
                            \Session::flash('status', 'danger'); 
                            return redirect()->back();
          }

   }

   public function studentDetails()
   {

   	$stu_details=Student::orderBy('created_at','desc')->paginate(10);
   	return view('admin.pages.studentmanagement')->with('stu_details',$stu_details);
   }

   public function studentDelete(Request $request)
   {
 
   	$id=$request->input('id');
    $username=$request->input('username');

   	   $studentTab = Student::where('id',$id)->delete();
       $studentLoginTab = StudentLogin::where('username',$username)->delete();
   
   	return back();

   	
   }

    public function studentDetailsUpdate(Request $request)
          {                   
          		 $id = $request->input('id');
                 $name = $request->input('name');
                 $address = $request->input('address');
                 $batch_id = $request->input('batch_id');
                
                  $spoc_email = $request->input('spoc_email');
                 $spoc_phone = $request->input('spoc_phone');
                 $spoc = $request->input('spoc');
                 $dropout_status = $request->input('dropout_status');
            
                             
                
                  $task = Student::where('id',$id)
                          ->update(array('name' => $name,'batch_id'=>$batch_id,'address'=>$address,'spoc'=>$spoc,'spoc_email'=>$spoc_email,'spoc_phone'=>$spoc_phone,'dropout_status'=>$dropout_status));
                              

                  return redirect()->back();
          }


	    public function BulkImportStudent(Request $request)
	    {

	    	// CSV to db
	    	$batch_id = $request->input('batch_id');
            $studentDetails = $request->file('student_file');
            $filename = $studentDetails->getClientOriginalName();  

            //Save the file temporarily
            $ext = $studentDetails->getClientOriginalExtension(); 

            if($ext == 'xlsx'){
                    

                            $studentDetails->move(public_path('growell/docs/temp/'), $studentDetails->getClientOriginalName());

                            $url = public_path('growell/docs/temp/'.$filename);
                          
                       
                            Excel::import(new StudentImport($batch_id), $url);

                            \Session::flash('message', "Bulk Upload Succesfully Completed");
				            \Session::flash('alert-class', 'success');
				        	return back();

              }else{
							\Session::flash('message', "Bulk Upload Succesfully Completed");
				            \Session::flash('alert-class', 'danger');
	        				return back();

              }
	            
	    }


      public function urlSendToStudent(Request $request)
      { 
         $batch_id = $request->input('batch_id');
         $fdt = $request->input('fdt');
         $tdt = $request->input('tdt');
         $ftime = Carbon::parse($request->input('ftime'))->toTimeString();
         $ttime = Carbon::parse($request->input('ttime'))->toTimeString();
  
         
         $batch_jobrole = Batches::where('batch_id',$batch_id)->first();
          //Student Details fetching with respect to Batch Id
           $studentDetailsFromDB = Student::where('batch_id', $batch_id)->get();
           $studentEmailArray =  array();
           $studentUsernameArray =  array();
          
          foreach($studentDetailsFromDB as $studentemail){
                 $studentEmailArray[] =  $studentemail->email;
           }

          foreach($studentDetailsFromDB as $studentuname){
                 $studentUsernameArray[] =  $studentuname->username;
           }


         $url = url("/test/page/$batch_id");

          for($i = 0; $i < count($studentUsernameArray); $i++){


            $stdnt_scheduled_status = StudentTestUrl::where('student_username',$studentUsernameArray[$i])
                                                      ->where('batch_id',$batch_id)
                                                      ->first();
            $stdnt_login_scheduled_status = StudentLogin::where('username',$studentUsernameArray[$i])
                                                        ->first();
            if(empty($stdnt_scheduled_status) && empty($stdnt_login_scheduled_status)){

                           

                            $test_given_alrdy_by_assessor = Student::where('batch_id', $batch_id)
                                                                    ->where('username',$studentUsernameArray[$i])
                                                                    ->where('schedule_test_status','4')
                                                                    ->first();
                            if(empty($test_given_alrdy_by_assessor)){

                             $pass = Str::random(9);
                             $hashed_random_password = Hash::make($pass);

                           //Saving login Info of student to stdnt login table

                            $studentSaveLoginTable = new StudentLogin();
                            $studentSaveLoginTable->username = $studentUsernameArray[$i]; 
                            $studentSaveLoginTable->password = $hashed_random_password;
                            $studentSaveLoginTable->status = '1';
                            $studentSaveLoginTable->save();


                            //Mail URL and Login password to every student

                            $data=array(
                                       'name' => "$studentEmailArray[$i]",
                                       'username' =>  "$studentUsernameArray[$i]",
                                       'url'=>"$url",
                                       'password' => "$pass",
                                       'Link_valid'=>$fdt.'-'.$tdt,
                                       'Time_Limit' => $ftime.'-'.$ttime,
                                       'batch_id' => $batch_id,
                                       'job_role' => $batch_jobrole->jobRole
                                     );
                            
                            $email = $studentEmailArray[$i];
                            $name  = $studentUsernameArray[$i];

                            Mail::send('assessor.pages.mail',$data,function($message) use ($name,$email){
                                                    $message->to($email)->subject('Your Test URL is here');
                                                  });

                            // URLs and info save to studenttesturl db
                             $stdnt_data = new StudentTestUrl();
                             $stdnt_data->student_username =  $studentUsernameArray[$i];
                             $stdnt_data->batch_id = $batch_id;
                             $stdnt_data->url = $url;
                             $stdnt_data->fdt = $fdt;
                             $stdnt_data->tdt = $tdt;
                             $stdnt_data->ftime = $ftime;
                             $stdnt_data->ttime = $ttime;
                             $stdnt_data->student_mail = $studentEmailArray[$i];
                             $stdnt_data->save();


                             $update_status = Student::where('username', $studentUsernameArray[$i])
                                                          ->where('batch_id',$batch_id)
                                                          ->update(array('schedule_test_status'=>'2'));
                            }

                      }else{

                         $test_given_alrdy_by_assessor = Student::where('batch_id', $batch_id)
                                                                    ->where('username',$studentUsernameArray[$i])
                                                                    ->where('schedule_test_status','4')
                                                                    ->first();
                            if(empty($test_given_alrdy_by_assessor)){

                        // Updating schedule test data

                             $pass = Str::random(9);
                             $hashed_random_password = Hash::make($pass);

                           //Saving login Info of student to stdnt login table

                            $studentSaveLoginTable = StudentLogin::where('username',$studentUsernameArray[$i])
                                                                ->update(array('password'=>$hashed_random_password));
                            

                            //Mail URL and Login password to every student

                            $data=array(
                                       'name' => "$studentEmailArray[$i]",
                                       'username' =>  "$studentUsernameArray[$i]",
                                       'url'=>"$url",
                                       'password' => "$pass",
                                       'Link_valid'=>$fdt.'-'.$tdt,
                                       'Time_Limit' => $ftime.'-'.$ttime,
                                       'batch_id' => $batch_id,
                                       'job_role' => $batch_jobrole->jobRole
                                     );
                            
                            $email = $studentEmailArray[$i];
                            $name  = $studentUsernameArray[$i];

                            Mail::send('assessor.pages.mail',$data,function($message) use ($name,$email){
                                                    $message->to($email)->subject('Your Updated Test URL is here');
                                                  });

                            // URLs and info save to studenttesturl db
                             $stdnt_data = StudentTestUrl::where('student_username',$studentUsernameArray[$i])
                                                          ->where('batch_id',$batch_id)
                                                          ->update(array(
                                                            'fdt'=>$fdt,
                                                            'tdt'=>$tdt,
                                                            'ftime'=>$ftime,
                                                            'ttime'=>$ttime
                                                          ));

                             $update_status = Student::where('username', $studentUsernameArray[$i])
                                                          ->where('batch_id',$batch_id)
                                                          ->update(array('schedule_test_status'=>'2'));
                            }


                      }


          }

                      return redirect()->back();
                



      }

      public function studentListAssessedTabAdmin($batch_id)
      {
            // $stdnt_data_by_batchid = Student::where('batch_id', $batch_id)
            //                                   ->orderBy('created_at','desc')
            //                                   ->paginate(10);

            // $stdnt_test_progress = \DB::table('students')
            //                   ->join('student_exam_progress', 'students.username', '=', 'student_exam_progress.student_id')
            //                   ->select('students.*','student_exam_progress.*')
            //                   ->where('students.batch_id',$batch_id)
            //                   ->orderBy('student_exam_progress.created_at','desc')->paginate(10);


            // return view('admin.pages.studentlistassessed')
            //             ->with('batch_id', $batch_id)
            //             ->with('stu_details',$stdnt_data_by_batchid)
            //             ->with('stdnt_test_progress', $stdnt_test_progress);

           $assessor_batch_validation = BatchAssignProcess::where('batch_id',$batch_id)
                                                          // ->where('assessor_id',\Session::get('assessorsession'))
                                                          ->where('req_status','1')
                                                          ->first();
         if(empty($assessor_batch_validation)){

                \Session::flash('message', 'This batch is not belong to you'); 
                  \Session::flash('status', 'danger'); 

                  return redirect()->back();
          
          }else{

                $student_id = Student::where('batch_id', $batch_id)->get();

                      foreach($student_id as $student){


                                 $student_info_batch_wise[$student->username] = \DB::table('student_test_progress')
                                      ->select('student_name','section',\DB::raw("sum(marks) as mark"))
                                      ->where('student_id','=',$student->username)
                                      ->groupBy('section')
                                      ->get();
                           
                                     
                      }

              $section_wise_marks=array();
              $marks = 0;
              $job_role = Batches::where('batch_id', $batch_id)->first();
              $sections = \DB::table('questions')
                          ->select('section')
                          ->where('job_role','=', $job_role->jobRole)
                          ->groupBy('section')
                          ->get();

                $batch_info = Batches::where('batch_id', $batch_id)->first();

                $assessor_id = BatchAssignProcess::where('batch_id', $batch_id)->first();
                
                $assessor_info = Assessor::where('assessor_id',$assessor_id->assessor_id)->first();

                return view('admin.pages.reportmanagementstudentwise')
                         ->with('batch_id', $batch_id)
                         ->with('batch_info', $batch_info)
                         ->with('assessor_info', $assessor_info)
                         ->with('sections',$sections)
                         ->with('student_data', $student_info_batch_wise);
                 
          }
      } 



      



}
