<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\AssessorLogin;
use Illuminate\Support\Str;
use App\Imports\AssessorImport;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Mail;
use App\Question;
use App\Batches;
use App\QuestionTransaction;
use Illuminate\Pagination\LengthAwarePaginator;
use App\StudentTestUrl;
use App\Student;
use App\BatchReviewComment;
use App\BatchAssignProcess;
use App\StudentTestProgress;

class TestController extends Controller
{

     public function studentInfoCheckLoginPage($batch_id)
     {
            

            return view('login.pages.studentlogin')->with('batch_id', $batch_id);


     }
 
     public function studentWrongInfoCheckLoginPage()
     {

            return view('login.pages.wrongpage');


     }

     public function questionReloadWithBatch() //Questions reloading
     {
            $batchInfo = Batches::where('batch_id', \Session::get('batch_id'))->first();

            $question = Question::where('job_role',$batchInfo->jobRole)->get();

            $questionTransaction = QuestionTransaction::where('batch_id', \Session::get('batch_id'))->get();

            return view('student.pages.questiondashboard')
                        ->with('batchInfo', $batchInfo)
                        ->with('questionTransaction', $questionTransaction)
                        ->with('question', $question);
     }

     public function questions()
     {

     	$batch_id = \Session::get('batch_id');
      $student_id = \Session::get('studentsession');
       

     	$batch_details = Batches::where('batch_id', $batch_id)->where('batch_status',1)->first();

      $batch_test_id = Batches::where('batch_id',$batch_id)
                                ->update(array('test_status'=>'1'));

     	$question = QuestionTransaction::where('job_role',$batch_details->jobRole )->where('s_id',$student_id)
                            ->get();

      

      // Custom Paginate the result
          $items = array();
			foreach($question as $thread)
			{

			        array_push($items, $thread);

			}

      // $items = $items->shuffle();
			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$perPage = 1;

			// $items = array_reverse(array_sort($items , function ($value) {
			//     return $value['created_at'];
			// }));
			$currentItems = array_slice($items, $perPage * ($currentPage - 1), $perPage);

			$question_data = new LengthAwarePaginator($currentItems, count($items), $perPage, $currentPage);


      
			return json_encode($question_data);
     }

     public function questions_trans_data($qid)
     {


     	$q_id = $qid;
      $s_id= \Session::get('studentsession');
     	//$batch_details = Batches::where('batch_id', $batch_id)->where('batch_status',1)->first();


     	$answer = QuestionTransaction::where('q_id', $q_id)->where('s_id', $s_id)
     							->first();

         if(empty($answer)){

                return 1;
         }else{

                return json_encode($answer);
         }


     }


     public function questions_transaction_insert(Request $request) //option are updating from test end to db
     {
       $s_id=\Session::get('studentsession');

       // $answer = new QuestionTransaction();
       // $answer->answer = $request->input('answer');
       // $answer->batch_id= \Session::get('batch_id');
       // $answer->section_id = $request->input('section');
       // $answer->q_id= $request->input('q_id');
       // $answer->attempt_status= $request->input('attempt_status');
       // $answer->s_id= $s_id;

       // $q_data = QuestionTransaction::where('s_id',$s_id)
       //               ->where('q_id',$request->input('q_id'))->first();
      // if(!empty($q_data)){

        $task = QuestionTransaction::where('s_id',$s_id)
                      ->where('id',$request->input('q_id'))
                ->update(array('answer' => $request->input('answer')));
                return 1;

      // }else{
      //     $answer->save();
      //     return 0;
      // }

     }

     public function questions_transaction_insert_demodata()
     {


       for ($i=0; $i <50 ; $i++) {
          $answer = new QuestionTransaction();
         $answer->answer = "ans".$i;
         $answer->batch_id= "dassdasdsad";
         $answer->q_id= $i;
         $answer->attempt_status= 0;
          $answer->s_id= "545";
          echo ( "inserting data".$i);
          $answer->save();
       }


          return 0;


     }

     public function testWindow($username,$batch_id)
      {
                          $a=Carbon::now();
                          $val=StudentTestUrl::where('student_username',$username)
                                               ->where('batch_id',$batch_id)->first();
                          $time=$val->created_at;
                          $time_limit = $val->valid_time;


                          $datetime1 = new DateTime($a);
                          $datetime2 = new DateTime($time);
                          $interval = $datetime1->diff($datetime2);
                          $elapsed = $interval->format(' %i ');


                          /*format('%y years %m months %a days %h hours %i minutes %s seconds')*/


                          if($elapsed <= Intval($time_limit) ){
                            echo "Success";

                            $ques = Question::where('batch_id',$batch_id)->get();

                            return $ques;
                          }
                          else{
                            echo "Action timeout";
                          }
      }

      public function assessorTestURL($batch_id,$std_id)
      {

         return view('login.pages.studentLoginAuthenticate')
                      ->with('batch_id',$batch_id)
                      ->with('std_id',$std_id);
      }

      public function assessorTestURLValidate(Request $request)
      {
          $batch_id = $request->input('batch_id');
          $username = $request->input('username');

          $valid_url = Batches::where('batch_id', $batch_id)
                                      ->first();
          $valid_student = Student::where('username', $username)->where('batch_id',$batch_id)->first();

          if(!empty($valid_student)){

          $fdt = Carbon::parse($valid_url->assessment_date_fdt);
          // $tdt = Carbon::parse($valid_url->tdt);
           $today_date = Carbon::now();
           
          if(1){
              // echo $today_date->diffInDays($fdt); die;

              
                                
                          $stdnt_data = Student::where('username', $username)
                                                ->where('batch_id',$batch_id)
                                                ->where('schedule_test_status','4')->first();

                                  if(empty($stdnt_data)){

                                      $update_status = Student::where('username', $username)
                                          ->where('batch_id',$batch_id)
                                          ->update(array('schedule_test_status'=>'2'));


                                    \Session::put('batch_id',$batch_id);
                                     \Session::put('studentsession',$username);

                                     return redirect('/student/imageandgeocapture'); 
                                      
                                  }else{

                                    \Session::flash('message', 'You have already given the test'); 
                                      \Session::flash('status', 'warning');

                                    return redirect(route('assessor.test.url',[$batch_id]));
                                     
                                  }

               

          }else{

                \Session::flash('message', 'You are not authorized to take test now'); 
                 \Session::flash('status', 'danger'); 

               return redirect()->back();
          }

        }else{
              \Session::flash('message', 'Candidate is not elligible for exam'); 
                 \Session::flash('status', 'danger'); 

               return redirect()->back();
              
        }


      }

      
      public function reviewTestbyAssessor(Request $request)
      {
        $batch_id = $request->input('batch_id');
        $comment = $request->input('comment');


        $batch_assessed_status = BatchAssignProcess::where('batch_id', $batch_id)->where('assessed_req_status','1')->first();

           if(empty($batch_assessed_status)){

                
                \Session::flash('message', 'Wait for Assessor review call'); 
                 \Session::flash('status', 'danger'); 
                return redirect()->back();
               

           }else{

                    $comment_update = BatchReviewComment::where('batch_id', $batch_id)->first();
                  if(empty($comment_update)){

                      Batches::where('batch_id', $batch_id)->update(array('assessed_status'=>'1'));

                      $batch_review_comments = new BatchReviewComment();
                      $batch_review_comments->batch_id = $batch_id;
                      $batch_review_comments->comment = $comment;
                      $batch_review_comments->save();

                      BatchAssignProcess::where('batch_id', $batch_id)->update(array('assessed_req_status'=>'1'));

                      
                       \Session::flash('message', 'Review submitted'); 
                       \Session::flash('status', 'success'); 
                       return redirect()->back();

                  }else{


                      Batches::where('batch_id', $batch_id)->update(array('assessed_status'=>'1'));

                      $batch_review_comments = BatchReviewComment::where('batch_id', $batch_id)
                                                              ->update(array('comment'=>$comment));


                      BatchAssignProcess::where('batch_id', $batch_id)->update(array('assessed_req_status'=>'1'));

                      
                       \Session::flash('message', 'Review submitted again'); 
                       \Session::flash('status', 'success'); 
                       return redirect()->back();
                  }
                      
                
           }        
                                            
           
      }


      public function updateRightAnswerByAssessor(Request $request)
      {
         $batch_id = $request->input('batch_id');
         $student_id = $request->input('std_id');
         $q_id = $request->input('q_id');
         $right_answer = $request->input('right_answer');

         //Update right answer to question transaction table

         $update_ans = QuestionTransaction::where('q_id', $q_id)
                                            ->where('batch_id', $batch_id)
                                            ->where('s_id', $student_id)
                                            ->update(array('answer'=>$right_answer));

         $ques_ans_transaction =  QuestionTransaction::where('q_id', $q_id)
                                            ->where('batch_id', $batch_id)
                                            ->where('s_id', $student_id)
                                            ->first();
          // $count=0;
          //                   foreach($ques_ans_transaction as $data){

                            if(strcmp($ques_ans_transaction->answer,$ques_ans_transaction->right_answer) == 0){

                                            $update_marks = StudentTestProgress::where('student_id',$student_id)->where('batch_id', $batch_id)->where('question_id',$q_id)
                                                          ->update(array('marks'=>1));
                                                          $this->updateStudentMarks($student_id,$batch_id);
                                          

                                    }else{

                                            $update_marks = StudentTestProgress::where('student_id',$student_id)->where('batch_id', $batch_id)->where('question_id',$q_id)
                                                          ->update(array('marks'=>0));
                                                          $this->updateStudentMarks($student_id,$batch_id);


                                    }

                            //         $count++;
                            // }

                              


            return 1;
      }

      public function updateStudentMarks($student_id,$batch_id)
      {


                            $right_marks = StudentTestProgress::where('batch_id', $batch_id)
                                                         ->where('student_id', $student_id)
                                                         ->where('marks','1')->get();

                            $wrong_marks = StudentTestProgress::where('batch_id', $batch_id)
                                                         ->where('student_id', $student_id)
                                                         ->where('marks','0')->get();
                            $pass_fail_limit = 70;
                            $markscount = 0;
                            $pass=0;
                            $student_get_marks=0;

                            foreach ($right_marks as $m)
                              {
                                    $student_get_marks = $student_get_marks+ Intval($m->marks);
                              }//total marks student get

                          $stdnt_progress = Student::where('batch_id', $batch_id)
                                                    ->where('username', $student_id)
                                                    ->update(array('marks'=>$student_get_marks));

                          
      }




}
