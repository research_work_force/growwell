<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Question;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;
use Session;
use App\Sections;
use App\Imports\QuestionImport;
use Exporter;
use Importer;
use App\Student;
use App\Batches;
use App\QuestionTransaction;
use App\StudentTestProgress;
use App\StudentExamProgress;

class QuestionController extends Controller
{



		// public function questionDataInsert(Request $request)
  // 			 {

  //             $question        = $request->input('question');
  //             $option1        = $request->input('option1');
  //             $option2        = $request->input('option2');
  //             $option3        = $request->input('option3');
  //             $option4        = $request->input('option4');
  //             if($request->input('right_answer') == 'option1'){
  //                 $right_answer    = $request->input('option1');
  //             }elseif($request->input('right_answer') == 'option2'){
  //                 $right_answer = $request->input('option2');
  //             }elseif($request->input('right_answer') == 'option3'){
  //                 $right_answer    = $request->input('option3');
  //             }else{
  //                 $right_answer = $request->input('option4');
  //             }
              

  //             $job_role        = $request->input('job_role');
  //             $section        = $request->input('section');
  //             $ques_id        = $request->input('ques_id');
 
  //             $question_hindi        = $request->input('question_hindi');
  //             $option1_hindi        = $request->input('option1_hindi');
  //             $option2_hindi        = $request->input('option2_hindi');
  //             $option3_hindi        = $request->input('option3_hindi');
  //             $option4_hindi        = $request->input('option4_hindi');
  //             if($request->input('right_ans_hindi') == 'option1_hindi'){
  //                 $right_ans_hindi    = $request->input('option1_hindi');
  //             }elseif($request->input('right_ans_hindi') == 'option2_hindi'){
  //                 $right_ans_hindi = $request->input('option2_hindi');
  //             }elseif($request->input('right_ans_hindi') == 'option3_hindi'){
  //                 $right_ans_hindi    = $request->input('option3_hindi');
  //             }else{
  //                 $right_ans_hindi = $request->input('option4_hindi');
  //             }

  //             $question_teleugu        = $request->input('question_teleugu');
  //             $option1_teleugu        = $request->input('option1_teleugu');
  //             $option2_teleugu        = $request->input('option2_teleugu');
  //             $option3_teleugu        = $request->input('option3_teleugu');
  //             $option4_teleugu        = $request->input('option4_teleugu');
  //             if($request->input('right_ans_teleugu') == 'option1_teleugu'){
  //                 $right_ans_teleugu    = $request->input('option1_teleugu');
  //             }elseif($request->input('right_ans_teleugu') == 'option2_teleugu'){
  //                 $right_ans_teleugu = $request->input('option2_teleugu');
  //             }elseif($request->input('right_ans_teleugu') == 'option3_teleugu'){
  //                 $right_ans_teleugu    = $request->input('option3_teleugu');
  //             }else{
  //                 $right_ans_teleugu = $request->input('option4_teleugu');
  //             }


  //             $question_assam        = $request->input('question_assam');
  //             $option1_assam        = $request->input('option1_assam');
  //             $option2_assam        = $request->input('option2_assam');
  //             $option3_assam        = $request->input('option3_assam');
  //             $option4_assam        = $request->input('option4_assam');
  //             $right_ans_assam        = $request->input('right_ans_assam');
  //             if($request->input('right_ans_assam') == 'option1_assam'){
  //                 $right_ans_assam    = $request->input('option1_assam');
  //             }elseif($request->input('right_ans_assam') == 'option2_assam'){
  //                 $right_ans_assam = $request->input('option2_assam');
  //             }elseif($request->input('right_ans_assam') == 'option3_assam'){
  //                 $right_ans_assam    = $request->input('option3_assam');
  //             }else{
  //                 $right_ans_assam = $request->input('option4_assam');
  //             }


  //             // Question ID validation 

  //             $ques_data_id_checking  = Question::where('id',$ques_id)->first();

  //             if(empty($ques_data_id_checking)){

		// 		       $ques_add = new Question();
	 //             $ques_add->question = $question;
	 //             $ques_add->option1 = $option1;
	 //             $ques_add->option2 = $option2;
	 //             $ques_add->option3 = $option3;
	 //             $ques_add->option4 = $option4;
	 //             $ques_add->right_answer = $right_answer;

	 //             $ques_add->job_role = $job_role;
  //              $ques_add->section = strtolower($section);
  //              $ques_add->id = $ques_id;

  //              $ques_add->question_hindi  =  $question_hindi;
  //              $ques_add->option1_hindi  =  $option1_hindi;
  //              $ques_add->option2_hindi  =  $option2_hindi;
  //              $ques_add->option3_hindi  =  $option3_hindi;
  //              $ques_add->option4_hindi  =  $option4_hindi;
  //              $ques_add->right_ans_hindi  =  $right_ans_hindi;

  //              $ques_add->question_teleugu  =  $question_teleugu;
  //              $ques_add->option1_teleugu  =  $option1_teleugu;
  //              $ques_add->option2_teleugu  =  $option2_teleugu;
  //              $ques_add->option3_teleugu  =  $option3_teleugu;
  //              $ques_add->option4_teleugu  =  $option4_teleugu;
  //              $ques_add->right_ans_teleugu  =  $right_ans_teleugu;

  //              $ques_add->question_assam  =  $question_assam;
  //              $ques_add->option1_assam  =  $option1_assam;
  //              $ques_add->option2_assam  = $option2_assam;
  //              $ques_add->option3_assam  =  $option3_assam;
  //              $ques_add->option4_assam  =  $option4_assam;
  //              $ques_add->right_ans_assam  =  $right_ans_assam;

	 //             $ques_add->added_by = "admin";

	 //             $ques_add->save();

	             


  //               \Session::flash('message', 'Question created successfully');
  //                \Session::flash('status', 'success');

  //                return redirect()->back();
  //            }else{

  //                \Session::flash('message', 'Question Id should be unique');
  //                \Session::flash('status', 'danger');

  //                return redirect()->back();
  //            }



  //  			}


public function questionEnDataInsert(Request $request)  //English Question
         {

              $question        = $request->input('question');
              $option1        = $request->input('option1');
              $option2        = $request->input('option2');
              $option3        = $request->input('option3');
              $option4        = $request->input('option4');
              if($request->input('right_answer') == 'option1'){
                  $right_answer    = $request->input('option1');
              }elseif($request->input('right_answer') == 'option2'){
                  $right_answer = $request->input('option2');
              }elseif($request->input('right_answer') == 'option3'){
                  $right_answer    = $request->input('option3');
              }else{
                  $right_answer = $request->input('option4');
              }
              

              $job_role        = $request->input('job_role');
              $section        = $request->input('section');
              $ques_id        = $request->input('ques_id');

              // Question ID validation 

              $ques_data_id_checking  = Question::where('id',$ques_id)->first();
              if(empty($ques_data_id_checking)){

               $ques_add = new Question();
               $ques_add->question = $question;
               $ques_add->option1 = $option1;
               $ques_add->option2 = $option2;
               $ques_add->option3 = $option3;
               $ques_add->option4 = $option4;
               $ques_add->right_answer = $right_answer;

               $ques_add->job_role = $job_role;
               $ques_add->section = strtolower($section);
               $ques_add->id = $ques_id;

               $ques_add->added_by = "admin";

               $ques_add->save();


                \Session::flash('message', 'Question created successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }else{

                  $ques_add = Question::where('id', $ques_id)
                                ->update(array(
                                          'question'=>$question,
                                          'option1'=>$option1,
                                          'option2'=>$option2,
                                          'option3'=>$option3,
                                          'option4'=>$option4,
                                          'right_answer'=>$right_answer,
                                          ));


                 \Session::flash('message', 'Question updated successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }




        }

        public function questionHiDataInsert(Request $request)  // Hindi Question
         {

              $job_role        = $request->input('job_role');
              $section        = $request->input('section');
              $ques_id        = $request->input('ques_id');
 
              $question_hindi        = $request->input('question_hindi');
              $option1_hindi        = $request->input('option1_hindi');
              $option2_hindi        = $request->input('option2_hindi');
              $option3_hindi        = $request->input('option3_hindi');
              $option4_hindi        = $request->input('option4_hindi');
              if($request->input('right_ans_hindi') == 'option1_hindi'){
                  $right_ans_hindi    = $request->input('option1_hindi');
              }elseif($request->input('right_ans_hindi') == 'option2_hindi'){
                  $right_ans_hindi = $request->input('option2_hindi');
              }elseif($request->input('right_ans_hindi') == 'option3_hindi'){
                  $right_ans_hindi    = $request->input('option3_hindi');
              }else{
                  $right_ans_hindi = $request->input('option4_hindi');
              }

              // Question ID validation 

              $ques_data_id_checking  = Question::where('id',$ques_id)->first();

              
              if(empty($ques_data_id_checking)){

               $ques_add = new Question();
               $ques_add->question_hindi  =  $question_hindi;
               $ques_add->option1_hindi  =  $option1_hindi;
               $ques_add->option2_hindi  =  $option2_hindi;
               $ques_add->option3_hindi  =  $option3_hindi;
               $ques_add->option4_hindi  =  $option4_hindi;
               $ques_add->right_ans_hindi  =  $right_ans_hindi;

               $ques_add->job_role = $job_role;
               $ques_add->section = strtolower($section);
               $ques_add->id = $ques_id;

               $ques_add->added_by = "admin";

               $ques_add->save();


                \Session::flash('message', 'Question created successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }else{

                  $ques_add = Question::where('id', $ques_id)
                                ->update(array(
                                          'question_hindi'=>$question_hindi,
                                          'option1_hindi'=>$option1_hindi,
                                          'option2_hindi'=>$option2_hindi,
                                          'option3_hindi'=>$option3_hindi,
                                          'option4_hindi'=>$option4_hindi,
                                          'right_ans_hindi'=>$right_ans_hindi,
                                          ));


                 \Session::flash('message', 'Question updated successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }



        }

        public function questionTlDataInsert(Request $request)  // Teleugu Question
         {

              $job_role        = $request->input('job_role');
              $section        = $request->input('section');
              $ques_id        = $request->input('ques_id');
 
              $question_teleugu        = $request->input('question_teleugu');
              $option1_teleugu        = $request->input('option1_teleugu');
              $option2_teleugu        = $request->input('option2_teleugu');
              $option3_teleugu        = $request->input('option3_teleugu');
              $option4_teleugu        = $request->input('option4_teleugu');
              if($request->input('right_ans_teleugu') == 'option1_teleugu'){
                  $right_ans_teleugu    = $request->input('option1_teleugu');
              }elseif($request->input('right_ans_teleugu') == 'option2_teleugu'){
                  $right_ans_teleugu = $request->input('option2_teleugu');
              }elseif($request->input('right_ans_teleugu') == 'option3_teleugu'){
                  $right_ans_teleugu    = $request->input('option3_teleugu');
              }else{
                  $right_ans_teleugu = $request->input('option4_teleugu');
              }
              // Question ID validation 

              $ques_data_id_checking  = Question::where('id',$ques_id)->first();

              
              if(empty($ques_data_id_checking)){

               $ques_add = new Question();
               $ques_add->question_teleugu  =  $question_teleugu;
               $ques_add->option1_teleugu  =  $option1_teleugu;
               $ques_add->option2_teleugu  =  $option2_teleugu;
               $ques_add->option3_teleugu  =  $option3_teleugu;
               $ques_add->option4_teleugu  =  $option4_teleugu;
               $ques_add->right_ans_teleugu  =  $right_ans_teleugu;

               $ques_add->job_role = $job_role;
               $ques_add->section = strtolower($section);
               $ques_add->id = $ques_id;

               $ques_add->added_by = "admin";

               $ques_add->save();


                \Session::flash('message', 'Question created successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }else{

                  $ques_add = Question::where('id', $ques_id)
                                ->update(array(
                                          'question_teleugu'=>$question_teleugu,
                                          'option1_teleugu'=>$option1_teleugu,
                                          'option2_teleugu'=>$option2_teleugu,
                                          'option3_teleugu'=>$option3_teleugu,
                                          'option4_teleugu'=>$option4_teleugu,
                                          'right_ans_teleugu'=>$right_ans_teleugu,
                                          ));


                 \Session::flash('message', 'Question updated successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }



        }


        public function questionAsDataInsert(Request $request)  // Assamese Question
         {

              $job_role        = $request->input('job_role');
              $section        = $request->input('section');
              $ques_id        = $request->input('ques_id');
 
              $question_assam        = $request->input('question_assam');
              $option1_assam        = $request->input('option1_assam');
              $option2_assam        = $request->input('option2_assam');
              $option3_assam        = $request->input('option3_assam');
              $option4_assam        = $request->input('option4_assam');
              $right_ans_assam        = $request->input('right_ans_assam');
              if($request->input('right_ans_assam') == 'option1_assam'){
                  $right_ans_assam    = $request->input('option1_assam');
              }elseif($request->input('right_ans_assam') == 'option2_assam'){
                  $right_ans_assam = $request->input('option2_assam');
              }elseif($request->input('right_ans_assam') == 'option3_assam'){
                  $right_ans_assam    = $request->input('option3_assam');
              }else{
                  $right_ans_assam = $request->input('option4_assam');
              }
              // Question ID validation 

              $ques_data_id_checking  = Question::where('id',$ques_id)->first();

              
              if(empty($ques_data_id_checking)){

               $ques_add = new Question();
               $ques_add->question_assam  =  $question_assam;
               $ques_add->option1_assam  =  $option1_assam;
               $ques_add->option2_assam  = $option2_assam;
               $ques_add->option3_assam  =  $option3_assam;
               $ques_add->option4_assam  =  $option4_assam;
               $ques_add->right_ans_assam  =  $right_ans_assam;

               $ques_add->job_role = $job_role;
               $ques_add->section = strtolower($section);
               $ques_add->id = $ques_id;

               $ques_add->added_by = "admin";

               $ques_add->save();


                \Session::flash('message', 'Question created successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }else{

                  $ques_add = Question::where('id', $ques_id)
                                ->update(array(
                                          'question_assam'=>$question_assam,
                                          'option1_assam'=>$option1_assam,
                                          'option2_assam'=>$option2_assam,
                                          'option3_assam'=>$option3_assam,
                                          'option4_assam'=>$option4_assam,
                                          'right_ans_assam'=>$right_ans_assam,
                                          ));


                 \Session::flash('message', 'Question updated successfully');
                 \Session::flash('status', 'success');

                 return redirect()->back();
             }



        }



      public function createSection(Request $request)
      {
         $section_name = $request->input('section_name');

         $section_data_checking = Sections::where('name', $section_name)->first();

         if(empty($section_data_checking)){

             $section_data = new Sections();
             $section_data->name = strtolower($section_name);
             $section_data->save();

             \Session::flash('message', 'Section created');
             \Session::flash('status', 'success');

             return redirect()->back();
         }else{

              \Session::flash('message', 'This section already exists');
             \Session::flash('status', 'danger');

             return redirect()->back();
         }



      }

   			public function QuestionList()
   			{
   				$question_details= Question::orderBy('created_at','desc')->paginate(10);
   				return view('admin.pages.questionmanagement')->with('question_details',$question_details);
   			}


   			/*delete */
   			 public function questionDelete(Request $request)
			   {

			   	$id=$request->input('id');
			   	$admin = Question::where('id',$id)->delete();

			   	return back();


			   }

			   /*question edit*/
			   public function QuestionDetailsUpdate(Request $request)
          {
                $right_op = $request->input('right_answer');
                $ans;
          		 $id = $request->input('id');

               if(strcmp($right_op,'option1') == 0){
                    $ans = $request->input('option1');
              }

              elseif(strcmp($right_op,'option2') == 0){
                $ans = $request->input('option2');
              }

              elseif(strcmp($right_op,'option3') == 0){
                $ans = $request->input('option3');
              }

              elseif(strcmp($right_op,'option4') == 0){
                $ans = $request->input('option4');
              }

              else{
                $ans = 'NA';
              }


      			   $question = $request->input('question');
	             $option1 = $request->input('option1');
	             $option2 = $request->input('option2');
	             $option3 = $request->input('option3');
	             $option4 = $request->input('option4');
	             $right_answer = $ans;
	             $job_role = $request->input('job_role');
               $section = strtolower($request->input('section'));



                  $task = Question::where('id',$id)
                          ->update(array('question' => $question,'option1'=>$option1,'option2'=>$option2,'option3'=>$option3,'option4'=>$option4,'right_answer'=>$right_answer,'section'=>$section,'job_role'=>$job_role));


                  return redirect()->back();
          }


          /*bulk upload*/




	public function handleImportQuestion(Request $request)
    {


      // CSV to Database
            $csv_file = $request->file('qfile');
            $jobRole = $request->input('jobRole');

            $filename = $csv_file->getClientOriginalName();

            //Save the file temporarily
            $ext = $csv_file->getClientOriginalExtension();

            if($ext == 'xlsx'){


                            $csv_file->move(public_path('growwell/temp/'), $csv_file->getClientOriginalName());

                            $url = public_path('growwell/temp/'.$filename);


                            // Excel::import(new QuestionImport($jobRole,$section), $url);
                            $errormsg=array();
                            $excel = Importer::make('Excel');
                            $excel->load($url);
                            $collection = $excel->getCollection();

                            if(sizeof($collection[0])==26){
                                  for($row=1; $row< sizeof($collection); $row++) {

                                    try{

                                        $new_section = Sections::get();
                                        $count=0;

                                        foreach($new_section as $sec){

                                                if(strcmp(strtolower($collection[$row][6]),$sec->name) == 0){

                                                    $count = 1;
                                                    break;

                                                }
                                        }



                                      if($count){

                                        Question::create([
                                                       'question'     => $collection[$row][0],
                                                       'option1'     => $collection[$row][1],
                                                       'option2'  =>  $collection[$row][2],
                                                       'option3'     => $collection[$row][3],
                                                       'option4'    => $collection[$row][4],
                                                       'right_answer'     => $collection[$row][5],
                                                       'job_role' =>     $jobRole,
                                                       'section'  => strtolower($collection[$row][6]),
                                                       'question_hindi'=> $collection[$row][7],
                                                       'option1_hindi'=> $collection[$row][8],
                                                       'option2_hindi' => $collection[$row][9],
                                                       'option3_hindi' => $collection[$row][10],
                                                       'option4_hindi' => $collection[$row][11],
                                                       'right_ans_hindi'=> $collection[$row][12],
                                                       'question_teleugu'=> $collection[$row][13],
                                                       'option1_teleugu' => $collection[$row][14],
                                                       'option2_teleugu' => $collection[$row][15],
                                                       'option3_teleugu' => $collection[$row][16],
                                                       'option4_teleugu' => $collection[$row][17],
                                                       'right_ans_teleugu'=> $collection[$row][18],
                                                       'question_assam'=> $collection[$row][19],
                                                       'option1_assam' => $collection[$row][20],
                                                       'option2_assam' => $collection[$row][21],
                                                       'option3_assam' => $collection[$row][22],
                                                       'option4_assam' => $collection[$row][23],
                                                       'right_ans_assam'=> $collection[$row][24],
                                                       'id' => $collection[$row][25],
                                                       'added_by'     => \Session::get('adminsession'),

                                                    ]);


                                      }
                                      else{

                                          $errormsg[] = $collection[$row][25];
                                      //      Question::create([
                                      //                  'question'     => $collection[$row][0],
                                      //                  'option1'     => $collection[$row][1],
                                      //                  'option2'  =>  $collection[$row][2],
                                      //                  'option3'     => $collection[$row][3],
                                      //                  'option4'    => $collection[$row][4],
                                      //                  'right_answer'     => $collection[$row][5],
                                      //                  'job_role' =>     $jobRole,
                                      //                  'section'  => strtolower($collection[$row][6]),
                                      //                  'question_hindi'=> $collection[$row][7],
                                      //                  'option1_hindi'=> $collection[$row][8],
                                      //                  'option2_hindi' => $collection[$row][9],
                                      //                  'option3_hindi' => $collection[$row][10],
                                      //                  'option4_hindi' => $collection[$row][11],
                                      //                  'right_ans_hindi'=> $collection[$row][12],
                                      //                  'question_teleugu'=> $collection[$row][13],
                                      //                  'option1_teleugu' => $collection[$row][14],
                                      //                  'option2_teleugu' => $collection[$row][15],
                                      //                  'option3_teleugu' => $collection[$row][16],
                                      //                  'option4_teleugu' => $collection[$row][17],
                                      //                  'right_ans_teleugu'=> $collection[$row][18],
                                      //                  'question_assam'=> $collection[$row][19],
                                      //                  'option1_assam' => $collection[$row][20],
                                      //                  'option2_assam' => $collection[$row][21],
                                      //                  'option3_assam' => $collection[$row][22],
                                      //                  'option4_assam' => $collection[$row][23],
                                      //                  'right_ans_assam'=> $collection[$row][24],
                                      //                  'id' => $collection[$row][25],
                                      //                  'added_by'     => \Session::get('adminsession'),

                                      //               ]);
                                      //       Sections::create([
                                      //                 'name' => strtolower($collection[$row][6]),
                                      //                 ]);


                                          }
                                      }



                                    catch(\Exception $e){

                                      \Session::flash('message', $e->getMessage());
                                      \Session::flash('status', 'danger');

                                      return redirect()->back();
                                    }


                                  } //loop end

                                if(empty($errormsg)){

                                  \Session::flash('message', 'Questions uploaded');
                                  \Session::flash('status', 'success');

                                  return redirect()->back();

                                }else{

                                    // $str='';
                                    // for($i=0;$i<count($errormsg);$i++){
                                    //   $str = $errormsg[$i].',';
                                    // }

                                      \Session::flash('message','You have to create section first of these questions Id '.implode(",",$errormsg));

                                       \Session::flash('status', 'success');

                                   return redirect()->back();


                                }
                                  


                            }else{


                                 \Session::flash('message', 'Excel sheet is not similar to our sample excel');
                                  \Session::flash('status', 'danger');

                                  return redirect()->back();

                            }

                  }else{

                          \Session::flash('message', 'Wrong file format');
                            \Session::flash('status', 'danger');

                            return redirect()->back();
                  }


    }

    public function resultChecking(Request $request)
    {

        $batch_id = $request->input('batch_id');
        $studentsession = $request->input('studentsession');

        //checking student is associated with particular batch

        $batch_checking = Student::where('batch_id', $batch_id)->first();

               if(!empty($batch_checking)){

                      $batch_exam_checking = Batches::where('batch_id', $batch_id)->where('test_status','1')->first();

                        if(!empty($batch_exam_checking))
                        {

                              $stdnt_progress = Student::where('batch_id', $batch_id)
                                                         ->where('username', $studentsession)
                                                         ->update(array('schedule_test_status'=>'4'));


                            $stdnt_ans_array=array();
                            $ques_ans_transaction =  QuestionTransaction::where('s_id',$studentsession)
                                                                          ->where('batch_id', $batch_id)
                                                                          ->get();

                            // // Student given ans
                            // foreach ($ques_ans_transaction as $key => $ans){

                            //   $stdnt_ans_array[$ans->q_id] = $ans->answer;

                            // }




                            // $original_questions=array();
                            // // Original answer of that question
                            // foreach($stdnt_ans_array as $key => $ans){
                            // $original_questions[$key] = QuestionTransaction::where('id',$key)->first();
                            // }



                            // $right_answer_field=array();
                            // $sections=array();
                            // foreach ($original_questions as $key => $question){

                            //   $right_answer_field[$question->q_id] = $question->right_answer;
                            //   $sections[$question->q_id]    = $question->section;

                            // }



                            $marks_for_single_student = array();

                            $right_answer=array();

                            $count=0;
                            foreach($ques_ans_transaction as $data){

                                    if(strcmp($data->answer,$data->right_answer) == 0){

                                            $update_marks = StudentTestProgress::where('student_id',$studentsession)->where('batch_id', $batch_id)->where('question_id',$data->q_id)
                                                          ->update(array('marks'=>1));
                                          

                                    }else{

                                            $update_marks = StudentTestProgress::where('student_id',$studentsession)->where('batch_id', $batch_id)->where('question_id',$data->q_id)
                                                          ->update(array('marks'=>0));


                                    }

                                    $count++;
                            }

                             

 
                            $right_marks = StudentTestProgress::where('batch_id', $batch_id)
                                                         ->where('student_id', $studentsession)
                                                         ->where('marks','1')->get();

                            $wrong_marks = StudentTestProgress::where('batch_id', $batch_id)
                                                         ->where('student_id', $studentsession)
                                                         ->where('marks','0')->get();
                            $pass_fail_limit = 70;
                            $markscount = 0;
                            $pass=0;
                            $student_get_marks=0;

                            foreach ($right_marks as $m)
                              {
                                    $student_get_marks = $student_get_marks+ Intval($m->marks);
                              }//total marks student get



                              $stdnt_progress = Student::where('batch_id', $batch_id)
                                                         ->where('username', $studentsession)
                                                         ->update(array('schedule_test_status'=>'4',
                                                                    'marks'=>$student_get_marks));



                              \Session::flash('message', 'Exam Completed');
                              \Session::flash('status', 'success');

                              return view('student.pages.examend');



                        }else{

                                 \Session::flash('message', 'Exam is not active in the batch');
                                  \Session::flash('status', 'danger');
                                  return redirect()->back();
                        }


               }else{


                                  \Session::flash('message', 'Batch is wrong');
                                  \Session::flash('status', 'danger');
                                  return redirect()->back();

               }

    }


    /*create new question*/
       /**/

        public function questionCreateNew(Request $request)
        {

              $question        = $request->input('question');
              $option1        = $request->input('option1');
              $option2        = $request->input('option2');
              $option3        = $request->input('option3');
              $option4        = $request->input('option4');
              if($request->input('right_answer') == 'option1'){
                  $right_answer    = $request->input('option1');
              }elseif($request->input('right_answer') == 'option2'){
                  $right_answer = $request->input('option2');
              }elseif($request->input('right_answer') == 'option3'){
                  $right_answer    = $request->input('option3');
              }else{
                  $right_answer = $request->input('option4');
              }
              

              $job_role        = $request->input('job_role');
              $section        = $request->input('section');
              $ques_id        = $request->input('ques_id');
 
              $question_hindi        = $request->input('question_hindi');
              $option1_hindi        = $request->input('option1_hindi');
              $option2_hindi        = $request->input('option2_hindi');
              $option3_hindi        = $request->input('option3_hindi');
              $option4_hindi        = $request->input('option4_hindi');
              if($request->input('right_ans_hindi') == 'option1_hindi'){
                  $right_ans_hindi    = $request->input('option1_hindi');
              }elseif($request->input('right_ans_hindi') == 'option2_hindi'){
                  $right_ans_hindi = $request->input('option2_hindi');
              }elseif($request->input('right_ans_hindi') == 'option3_hindi'){
                  $right_ans_hindi    = $request->input('option3_hindi');
              }else{
                  $right_ans_hindi = $request->input('option4_hindi');
              }

              $question_teleugu        = $request->input('question_telugu');
              $option1_teleugu        = $request->input('option1_telugu');
              $option2_teleugu        = $request->input('option2_telugu');
              $option3_teleugu        = $request->input('option3_telugu');
              $option4_teleugu        = $request->input('option4_telugu');
              if($request->input('right_ans_telugu') == 'option1_telugu'){
                  $right_ans_teleugu    = $request->input('option1_telugu');
              }elseif($request->input('right_ans_telugu') == 'option2_telugu'){
                  $right_ans_teleugu = $request->input('option2_telugu');
              }elseif($request->input('right_ans_telugu') == 'option3_telugu'){
                  $right_ans_teleugu    = $request->input('option3_telugu');
              }else{
                  $right_ans_teleugu = $request->input('option4_telugu');
              }


              $question_assam        = $request->input('question_assam');
              $option1_assam        = $request->input('option1_assam');
              $option2_assam        = $request->input('option2_assam');
              $option3_assam        = $request->input('option3_assam');
              $option4_assam        = $request->input('option4_assam');
              $right_ans_assam        = $request->input('right_ans_assam');
              if($request->input('right_ans_assam') == 'option1_assam'){
                  $right_ans_assam    = $request->input('option1_assam');
              }elseif($request->input('right_ans_assam') == 'option2_assam'){
                  $right_ans_assam = $request->input('option2_assam');
              }elseif($request->input('right_ans_assam') == 'option3_assam'){
                  $right_ans_assam    = $request->input('option3_assam');
              }else{
                  $right_ans_assam = $request->input('option4_assam');
              }


              // Question ID validation 

              $ques_data_id_checking  = Question::where('id',$ques_id)->first();

              if(empty($ques_data_id_checking)){

              $ques_add = new Question();
               $ques_add->question = $question;
               $ques_add->option1 = $option1;
               $ques_add->option2 = $option2;
               $ques_add->option3 = $option3;
               $ques_add->option4 = $option4;
               $ques_add->right_answer = $right_answer;

               $ques_add->job_role = $job_role;
               $ques_add->section = strtolower($section);
               $ques_add->id = $ques_id;

               $ques_add->question_hindi  =  $question_hindi;
               $ques_add->option1_hindi  =  $option1_hindi;
               $ques_add->option2_hindi  =  $option2_hindi;
               $ques_add->option3_hindi  =  $option3_hindi;
               $ques_add->option4_hindi  =  $option4_hindi;
               $ques_add->right_ans_hindi  =  $right_ans_hindi;

               $ques_add->question_teleugu  =  $question_teleugu;
               $ques_add->option1_teleugu  =  $option1_teleugu;
               $ques_add->option2_teleugu  =  $option2_teleugu;
               $ques_add->option3_teleugu  =  $option3_teleugu;
               $ques_add->option4_teleugu  =  $option4_teleugu;
               $ques_add->right_ans_teleugu  =  $right_ans_teleugu;

               $ques_add->question_assam  =  $question_assam;
               $ques_add->option1_assam  =  $option1_assam;
               $ques_add->option2_assam  = $option2_assam;
               $ques_add->option3_assam  =  $option3_assam;
               $ques_add->option4_assam  =  $option4_assam;
               $ques_add->right_ans_assam  =  $right_ans_assam;

               $ques_add->added_by = "admin";

               $ques_add->save();

               


                \Session::flash('message', 'Question created successfully');
                 \Session::flash('status', 'success');

                 return redirect('/questionmanagement');
             }else{

                 \Session::flash('message', 'Question Id should be unique');
                 \Session::flash('status', 'danger');

                 return redirect()->back();
             }



         }


       /*save edit questions */
      public function questionDetailsEdit(Request $request)
        {

              $question   = $request->input('question');
              $option1    = $request->input('option1');
              $option2    = $request->input('option2');
              $option3    = $request->input('option3');
              $option4    = $request->input('option4');
              if($request->input('right_answer') == 'option1'){
                  $right_answer    = $request->input('option1');
              }elseif($request->input('right_answer') == 'option2'){
                  $right_answer = $request->input('option2');
              }elseif($request->input('right_answer') == 'option3'){
                  $right_answer    = $request->input('option3');
              }else{
                  $right_answer = $request->input('option4');
              }
              

              $job_role        = $request->input('job_role');
              $section        = $request->input('section');
              $ques_id        = $request->input('id');
 
              $question_hindi        = $request->input('question_hindi');
              $option1_hindi        = $request->input('option1_hindi');
              $option2_hindi        = $request->input('option2_hindi');
              $option3_hindi        = $request->input('option3_hindi');
              $option4_hindi        = $request->input('option4_hindi');
              if($request->input('right_ans_hindi') == 'option1_hindi'){
                  $right_ans_hindi    = $request->input('option1_hindi');
              }elseif($request->input('right_ans_hindi') == 'option2_hindi'){
                  $right_ans_hindi = $request->input('option2_hindi');
              }elseif($request->input('right_ans_hindi') == 'option3_hindi'){
                  $right_ans_hindi    = $request->input('option3_hindi');
              }else{
                  $right_ans_hindi = $request->input('option4_hindi');
              }

              $question_telugu        = $request->input('question_telugu');
              $option1_telugu        = $request->input('option1_telugu');
              $option2_telugu        = $request->input('option2_telugu');
              $option3_telugu       = $request->input('option3_telugu');
              $option4_telugu       = $request->input('option4_telugu');
              if($request->input('right_ans_telugu') == 'option1_telugu'){
                  $right_ans_telugu    = $request->input('option1_telugu');
              }elseif($request->input('right_ans_telugu') == 'option2_telugu'){
                  $right_ans_telugu = $request->input('option2_telugu');
              }elseif($request->input('right_ans_telugu') == 'option3_telugu'){
                  $right_ans_telugu    = $request->input('option3_telugu');
              }else{
                  $right_ans_telugu = $request->input('option4_telugu');
              }


              $question_assam        = $request->input('question_assam');
              $option1_assam        = $request->input('option1_assam');
              $option2_assam        = $request->input('option2_assam');
              $option3_assam        = $request->input('option3_assam');
              $option4_assam        = $request->input('option4_assam');
              $right_ans_assam        = $request->input('right_ans_assam');
              if($request->input('right_ans_assam') == 'option1_assam'){
                  $right_ans_assam    = $request->input('option1_assam');
              }elseif($request->input('right_ans_assam') == 'option2_assam'){
                  $right_ans_assam = $request->input('option2_assam');
              }elseif($request->input('right_ans_assam') == 'option3_assam'){
                  $right_ans_assam    = $request->input('option3_assam');
              }else{
                  $right_ans_assam = $request->input('option4_assam');
              }


              // Question ID validation 

              $ques_data_id_checking  = Question::where('id',$ques_id)->first();

              if(empty($ques_data_id_checking)){

                \Session::flash('message', 'Unable to edit question ');
                 \Session::flash('status', 'danger');

                 return redirect('/questionmanagement');

                }


                else{ 

                $questionDataUpdate = Question::where('id',$ques_id)->update(array('question' => $question,'option1'=>$option1,'option2'=>$option2,'option3'=>$option3,'option4'=>$option4,'right_answer'=>$right_answer,'job_role'=>$job_role,'question_hindi'=>$question_hindi,'option1_hindi'=>$option1_hindi,'option2_hindi'=>$option2_hindi,'option3_hindi'=>$option3_hindi,'option4_hindi'=>$option4_hindi,'right_ans_hindi'=>$right_ans_hindi,'question_teleugu'=>$question_telugu,'option1_teleugu'=>$option1_telugu,'option2_teleugu'=>$option2_telugu,'option3_teleugu'=>$option3_telugu,'option4_teleugu'=>$option4_telugu,'right_ans_teleugu'=>$right_ans_telugu,'question_assam'=>$question_assam,'option1_assam'=>$option1_assam,'option2_assam'=>$option2_assam,'option3_assam'=>$option3_assam,'option4_assam'=>$option4_assam,'right_ans_assam'=>$right_ans_assam)) ;             
            

             /* $ques_add = new Question();
               $ques_add->question = $question;
               $ques_add->option1 = $option1;
               $ques_add->option2 = $option2;
               $ques_add->option3 = $option3;
               $ques_add->option4 = $option4;
               $ques_add->right_answer = $right_answer;

               $ques_add->job_role = $job_role;
               $ques_add->section = strtolower($section);
               $ques_add->id = $ques_id;

               $ques_add->question_hindi  =  $question_hindi;
               $ques_add->option1_hindi  =  $option1_hindi;
               $ques_add->option2_hindi  =  $option2_hindi;
               $ques_add->option3_hindi  =  $option3_hindi;
               $ques_add->option4_hindi  =  $option4_hindi;
               $ques_add->right_ans_hindi  =  $right_ans_hindi;

               $ques_add->question_teleugu  =  $question_teleugu;
               $ques_add->option1_teleugu  =  $option1_teleugu;
               $ques_add->option2_teleugu  =  $option2_teleugu;
               $ques_add->option3_teleugu  =  $option3_teleugu;
               $ques_add->option4_teleugu  =  $option4_teleugu;
               $ques_add->right_ans_teleugu  =  $right_ans_teleugu;

               $ques_add->question_assam  =  $question_assam;
               $ques_add->option1_assam  =  $option1_assam;
               $ques_add->option2_assam  = $option2_assam;
               $ques_add->option3_assam  =  $option3_assam;
               $ques_add->option4_assam  =  $option4_assam;
               $ques_add->right_ans_assam  =  $right_ans_assam;

               /*$ques_add->added_by = "admin";*/

              /* $ques_add->save();*/

               


                \Session::flash('message', 'Question updated successfully');
                \Session::flash('status', 'success');

                 return redirect('/questionmanagement');
             


            



         }



       }


          /*question edit page*/
          public function questionEditPage($id)
          {
              $question_details=Question::where('id',$id)->get();
              return view('admin.pages.questionedit')->with('question_details',$question_details);
          }

        


public function sectionindex()
{
    

    $sections_details= sections::orderBy('created_at','desc')->paginate(10);
    return view('admin.pages.sectionindex')->with('sections_details',$sections_details);
}

public function editSection(Request $request)
{
    $data=$request->all();
    $section_id=$data['section_id'];
    $section_name=$data['section_name'];

    sections::where('id', $section_id)
          ->update(['name' => $section_name]);

          \Session::flash('message', 'Section editted');
          \Session::flash('status', 'success');
          return redirect()->back();


}


public function delete_section($name)
{
    $question_details_count= Question::where('section',$name)->count();

    if($question_details_count>0)
    {
        \Session::flash('message', 'Questions are assigned to this section. You cannot delete this section.');
          \Session::flash('status', 'danger');
          return redirect()->back();
    }
    else{
        \DB::table('sections')->where('name', $name)->delete();

        \Session::flash('message', 'Section deleted successfully.');
          \Session::flash('status', 'success');
          return redirect()->back();
    }
  
    
}


}
