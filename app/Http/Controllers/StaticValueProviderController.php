<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessor;
use Carbon\Carbon;
use App\BatchTypes;
use App\JobRoles;
use App\SchemeProgram;
use App\Sectors;
use App\Batches;
use App\Sections;
use App\Student;
use App\Question;
use App\BatchAssignProcess;
use App\AssessorImgTest;
use App\QuestionMapTransaction;

class StaticValueProviderController extends Controller
{
    
     public static function batchTypes(){

              $batchTypes = BatchTypes::get();

              return $batchTypes;
     }


     public static function jobRoles(){

              $jobRoles = JobRoles::get();

              return $jobRoles;
     }


     public static function schemeProgram(){

              $schemeProgram = SchemeProgram::get();

              return $schemeProgram;
     }


     public static function sectors(){

              $sectors = Sectors::get();

              return $sectors;
     }

     public static function getAssessors(){

              $assessor_data = Assessor::orderBy('created_at','desc')->paginate(10);

              return json_encode($assessor_data);
     }

     public static function getAssessorsList(){

              $assessor_data = Assessor::get();

              return $assessor_data;
     }

     public static function batcheids(){

              $batches = Batches::get();

              return $batches;
     }

     public static function sections(){

              $sections = Sections::get();

              return $sections;
     }


     public static function getBatchWiseStudentAttendencePercentage($batch_id){

        $student_testgiven_status = Student::where('schedule_test_status','4')->where('batch_id',$batch_id)->count();

         $total_no_of_stdnt_ingiven_batch = Student::where('batch_id',$batch_id)->count();

         if($total_no_of_stdnt_ingiven_batch !=0){
         $percentage_of_stdnt_attendance = (Intval($student_testgiven_status)/Intval($total_no_of_stdnt_ingiven_batch))*100;
         }else{

             $percentage_of_stdnt_attendance='0';
         }

         return $percentage_of_stdnt_attendance;
     }


     public static function getBatchWiseStudentPassedPercentage($batch_id)
     {
           $sec_wise_totalmarks = StaticValueProviderController::getTotalMarksSectionWise($batch_id);
           $total_ques_marks=0;
           for($i=0;$i<count($sec_wise_totalmarks);$i++){
              $total_ques_marks = $total_ques_marks + Intval($sec_wise_totalmarks[$i]);

           }

           $passed_percentage='';
           $no_stdnt_passed='';

           $student_testgiven_status = Student::where('schedule_test_status','4')->where('batch_id',$batch_id)->count();
        $student_marks = Student::where('schedule_test_status','4')->where('batch_id',$batch_id)->get();
           $total_no_of_stdnt_ingiven_batch = Student::where('batch_id',$batch_id)->count();
              if($total_no_of_stdnt_ingiven_batch !=0){

                $percentage_of_stdnt_attendance = (Intval($student_testgiven_status)/Intval($total_no_of_stdnt_ingiven_batch))*100;

                    $count_nof_stdnt_pass = 0;

                      foreach($student_marks as $sm){

                             $per = (Intval($sm->marks)/$total_ques_marks)*100;
                             $per_dec_pt = number_format($per, 2, '.', '');

                             if($per>=70){
                                 $count_nof_stdnt_pass = $count_nof_stdnt_pass+1;
                             }
                      }

                $passed_percentage = ($count_nof_stdnt_pass / Intval($total_no_of_stdnt_ingiven_batch))*100;


              }else{

                   $passed_percentage='0';
               }


          return $passed_percentage;
     } 


     public static function getBatchWiseStudentFailedPercentage($batch_id){

        $student_testgiven_status = Student::where('schedule_test_status','4')->where('batch_id',$batch_id)->count();

         $total_no_of_stdnt_ingiven_batch = Student::where('batch_id',$batch_id)->count();
    
    if($total_no_of_stdnt_ingiven_batch !=0){
         $percentage_of_stdnt_attendance = (Intval($student_testgiven_status)/Intval($total_no_of_stdnt_ingiven_batch))*100;
         $ques_job_role = Batches::where('batch_id', $batch_id)->first();

         $total_number_of_questions = Question::where('job_role',$ques_job_role->jobRole)->count();

         $marks_per_ques = 1;

         $total_marks =  $marks_per_ques * Intval($total_number_of_questions);

         $per_student_marks;

         $no_student_failed = Student::where('schedule_test_status','4')->where('marks','<','7')->count();

         $passed_percentage = (Intval($no_student_failed) / Intval($total_no_of_stdnt_ingiven_batch))*100;
       }else{
           $passed_percentage='0';
       }

         return $passed_percentage;
     }

     public static function getBatchSectionJobRoleWise($batch_id)
     {
        $job_role = Batches::where('batch_id', $batch_id)->first();

        $sections = \DB::table('question_transaction') 
            ->distinct()
            ->select('section')
            ->where('batch_id', $batch_id)
            ->where('job_role', $job_role->jobRole)
            ->groupBy('section')
            ->get();

        return $sections;
     }

      public static function getTotalMarksSectionWise($batch_id)
     {
        $job_role = Batches::where('batch_id', $batch_id)->first();

        $sections = \DB::table('question_map_transaction') 
            ->distinct()
            ->select('section')
            ->where('job_role', $job_role->jobRole)
            ->where('batch_id', $batch_id)
            ->groupBy('section')
            ->get();
       $marks_in_each_section=array();
       
        foreach ($sections as $sec){
        $marks_in_each_section[] = \DB::table('question_map_transaction')
                                      ->where('section','=',$sec->section)
                                      ->where('job_role', $job_role->jobRole)
                                      ->sum('marks');
        }

        return $marks_in_each_section; 
     }

     public static function studentAttendanceChecking($batch_id, $student_id)
     {
          $student_attdnc_status = Student::where('username',$student_id)
                                            ->select('schedule_test_status')
                                            ->where('batch_id', $batch_id)
                                            ->where('schedule_test_status','4')
                                            ->first();

        
          if(empty($student_attdnc_status)){
              return 0;
          }else{
              return $student_attdnc_status;
          }
          
     }

     public static function numberOfStudentPerBatch($batch_id)
     {
         $count_stdnt = Student::where('batch_id', $batch_id)->count();

         return $count_stdnt;
     }

     public static function nameOfStudentPerBatch($batch_id,$student_id)
     {
         $stdnt_name = Student::where('batch_id', $batch_id)
                                ->where('username', $student_id)
                                ->first();

         return $stdnt_name;
     }

     public static function totalMarksPerStudent($stdntmarks)
     {
        $totalmarks = 0;
        foreach($stdntmarks as $d)
        {
          if($d->mark == '-1'){
              return "NA";
              break;
          }
          $totalmarks += Intval($d->mark);
        }

        return $totalmarks;
     }

     public static function maxMarksPerstudent($stdntmarks)
     {
        $eachmarks = array();
        foreach($stdntmarks as $d)
        {
          if($d->mark == '-1'){
              return "NA";
              break;
          }
          $eachmarks[] = Intval($d->mark);
        }

        $max=0;

        for($i = 0; $i < count($eachmarks); $i++)
        {
            if($eachmarks[$i]>$max){
                $max = $eachmarks[$i];
            }
        }

        return $max;
     }

     public static function marksPercentagePerStudent($totalmarks,$batch_id)
     {
       $sec_wise_marks = StaticValueProviderController::getTotalMarksSectionWise($batch_id);

       $sec_wise_total_marks=0;
       $percentage_per_student="";
         for($i=0; $i < count($sec_wise_marks); $i++){

              if($sec_wise_marks[$i] == "NA"){
                  $percentage_per_student = "NA";
              }
               $sec_wise_total_marks = Intval($sec_wise_marks[$i])+$sec_wise_total_marks;
            
         }
         if($sec_wise_total_marks != '0'){
          $percentage_per_student = (Intval($totalmarks)/Intval($sec_wise_total_marks) ) * 100;   
         
         }
         else{
              $percentage_per_student=0;
         }            
          

          return $percentage_per_student;
     }

     public static function gradePerStudent($totalmarks)
     {
          if($totalmarks>=80){
            return 'A';
          }else if($totalmarks<80 && $totalmarks>70){
            return 'B';
          }else if($totalmarks<70){
            return 'C';
          }else if($totalmarks==0.00){
            return 'NA';
          }
          else{
            return 'D';
          }
     }

     public static function passOrFailPerStudent($totalmarks)
     {
          if($totalmarks>=70){
            return 1;
          }
          else{
            return 0;
          }
     }

     public static function candidatesImgVideoLink($batch_id,$key)
     {
          $stdnt_data = Student::where('batch_id',$batch_id)
                                ->where('username',$key)
                                ->first();
          return $stdnt_data;
     }

     public static function assessorImgLink($batch_id,$key)
     {
         $assessor_data = BatchAssignProcess::where('batch_id',$batch_id)->first();

         $assessor_img_data = AssessorImgTest::where('batch_id', $batch_id)
                                           ->where('assessor_id',$assessor_data->assessor_id)
                                           ->where('student_id',$key)
                                           ->first();
         
          return $assessor_img_data;
     }

}