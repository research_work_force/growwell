<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Question;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon; 
use Session;
use App\Imports\StudentImport;
use App\Batches;
use App\BatchAssignProcess;
use App\Assessor;
use Mail;
use App\Student;
use App\BatchHistory; 
use App\AssessorHistory; 
use App\StudentTestProgress;
use App\QuestionTransaction;
use PDF;

class ReportController extends Controller
{

	  public function batchReport()
	  {
	  	  
	  	  $batch_data = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessed_req_status','2')
                              ->orderBy('batches.created_at','desc')->paginate(10);
   
	  	  $batch_id = Batches::get();
          $batch_id_array =array();

          foreach ($batch_id as $batch){
            $batch_id_array[] = $batch->batch_id;
          }



          $enrolled_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->count();
              $enrolled_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $dropout_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
              $dropout_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $batch_process = BatchAssignProcess::where('req_status', '1')
                                               ->where('assessed_req_status','2')->get();

	  	  return view('admin.pages.reportmanagement')
	  	  			->with('batch_data', $batch_data)
	  	  			->with('enrolled_candidate_array', $enrolled_candidate_array)
              ->with('batch_process',$batch_process)
              ->with('dropout_candidate_array', $dropout_candidate_array);
	  }

    public function batchReportSearch(Request $request)
    {
        $batch_id = $request->input('batch_id');
        $jobRole     = $request->input('jobRole');
        $assessor_id     = $request->input('assessor_id');

        $batch_data = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessed_req_status','2')
                              ->where('batches.batch_id',$batch_id)
                              ->where('batches.jobRole',$jobRole)
                              ->where('batch_process.assessor_id',$assessor_id)
                              ->orderBy('batches.created_at','desc')->paginate(10);
   
        $batch_id = Batches::where('batch_id',$batch_id)->get();
          $batch_id_array =array();

          foreach ($batch_id as $batch){
            $batch_id_array[] = $batch->batch_id;
          }



          $enrolled_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->count();
              $enrolled_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $dropout_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
              $dropout_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $batch_process = BatchAssignProcess::where('req_status', '1')
                                               ->where('assessor_id',$assessor_id)
                                               ->where('assessed_req_status','2')->get();

        return view('admin.pages.reportmanagement')
              ->with('batch_data', $batch_data)
              ->with('enrolled_candidate_array', $enrolled_candidate_array)
              ->with('batch_process',$batch_process)
              ->with('dropout_candidate_array', $dropout_candidate_array);
    }
  
      public function studentwiseReportAdmin($batch_id)
      {	  

          $student_id = Student::where('batch_id', $batch_id)->get();

          foreach($student_id as $student){

               // if($student->schedule_test_status == '4'){

                     $student_info_batch_wise[$student->username] = \DB::table('student_test_progress')
                          ->select('student_name','section',\DB::raw("sum(marks) as mark"))
                          ->where('student_id','=',$student->username)
                          ->groupBy('section')
                          ->get();

               // }else{
 
               //      $student_info_batch_wise[$student->username] = QuestionTransaction::select('section_id')
               //                                                    ->where('s_id','=',$student->username)->groupBy('section_id')->get();
               // }
               
                         
          }

        $section_wise_marks=array();
        $marks = 0;
        $job_role = Batches::where('batch_id', $batch_id)->first();
        $sections = \DB::table('questions')
                    ->select('section')
                    ->where('job_role','=', $job_role->jobRole)
                    ->groupBy('section')
                    ->get();

          $batch_info = Batches::where('batch_id', $batch_id)->first();

          $assessor_id = BatchAssignProcess::where('batch_id', $batch_id)->first();
          
          $assessor_info = Assessor::where('assessor_id',$assessor_id->assessor_id)->first();

          return view('admin.pages.studentwisereport')
          		     ->with('batch_id', $batch_id)
                   ->with('batch_info', $batch_info)
                   ->with('assessor_info', $assessor_info)
          		     ->with('sections',$sections)
          		     ->with('student_data', $student_info_batch_wise);
      }

      public function assessorReport()
      { 

         $batch_search_result = \DB::table('batches')
                                    ->join('batch_process', function($join)
                                    {
                                        $join->on('batches.batch_id', '=', 'batch_process.batch_id')
                                             ->where('batch_process.assessor_id', '=',\Session::get('assessorsession'));
                                    })
                                    ->orderBy('batches.created_at','desc')->paginate(10);
   
          $batch_id = Batches::get();
          $batch_id_array =array();

          foreach ($batch_id as $batch){
            $batch_id_array[] = $batch->batch_id;
          }



          $enrolled_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->count();
              $enrolled_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $dropout_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
              $dropout_candidate_array[$batch_id_array[$i]] = $count;
          }
          

         return view('assessor.pages.assessorreportmanagement')
              ->with('batch_data', $batch_search_result)
              ->with('enrolled_candidate_array', $enrolled_candidate_array)
                      ->with('dropout_candidate_array', $dropout_candidate_array);
        
      }

      public function assessorStudentWiseReport($batch_id)
      {   
          $batch_assigned_assessor_check = BatchAssignProcess::where('batch_id', $batch_id)
                                            ->where('assessor_id',\Session::get('assessorsession'))
                                            ->first();

        if(empty($batch_assigned_assessor_check)){

              \Session::flash('message', 'This batch is not assigned to you');
             \Session::flash('status', 'danger');

             return redirect()->back();
        }
          $student_id = Student::where('batch_id', $batch_id)->get();

          foreach($student_id as $student){

               // if($student->schedule_test_status == '4'){

                     $student_info_batch_wise[$student->username] = \DB::table('student_test_progress')
                          ->select('student_name','section',\DB::raw("sum(marks) as mark"))
                          ->where('student_id','=',$student->username)
                          ->where('batch_id', $batch_id)
                          ->groupBy('section')
                          ->orderBy ('created_at','desc')->paginate(10);

               // }else{

               //      $student_info_batch_wise[$student->username] = QuestionTransaction::select('section_id')
               //                                                    ->where('s_id','=',$student->username)->groupBy('section_id')->get();
               // }
               
                         
          }

              $section_wise_marks=array();
              $marks = 0;
              $job_role = Batches::where('batch_id', $batch_id)->first();
              $sections = \DB::table('questions')
                          ->select('section')
                          ->where('job_role','=', $job_role->jobRole)
                          ->groupBy('section')
                          ->get();

              $batch_info = Batches::where('batch_id', $batch_id)->first();

              $assessor_info = Assessor::where('assessor_id', \Session::get('assessorsession'))->first();
            
              // print_r($assessor_info); die;
          return view('assessor.pages.assessorreportmanagementstudentwise')
                   ->with('batch_id', $batch_id)
                   ->with('batch_info', $batch_info)
                   ->with('assessor_info', $assessor_info)
                   ->with('sections',$sections)
                   ->with('student_data', $student_info_batch_wise);
      }

      public function gen_pdf($batch_id)
      {
        $student_id = Student::where('batch_id', $batch_id)->get();

        $job_role = Batches::where('batch_id', $batch_id)->first();
        $sections = \DB::table('questions')
                    ->select('section')
                    ->where('job_role','=', $job_role->jobRole)
                    ->groupBy('section')
                    ->get();

          $batch_info = Batches::where('batch_id', $batch_id)->first();

          $assessor_id = BatchAssignProcess::where('batch_id', $batch_id)->first();
          
          $assessor_info = Assessor::where('assessor_id',$assessor_id->assessor_id)->first();

        foreach($student_id as $student){

                  $student_data[$student->username] = \DB::table('student_test_progress')
                       ->select('student_name','section',\DB::raw("sum(marks) as mark"))
                       ->where('student_id','=',$student->username)
                       ->groupBy('section')
                       ->get();             
       }
 
        $pdf = PDF::loadView('admin.pages.student_report', compact('batch_id','student_data','sections','assessor_info','batch_info'))->setPaper('a4', 'landscape');;

  
        return $pdf->download('student_report.pdf');
      }

      public function assessorReportsStudentMarksWise($batch_id,$student_id)
      {
           $stdnt_in_batch_validation = Student::where('batch_id',$batch_id)
                                                ->where('username',$student_id)
                                                ->first();
           if(empty($stdnt_in_batch_validation)){

                   \Session::flash('message', 'This student is not assigned to this batch');
                   \Session::flash('status', 'danger');

                   return redirect()->back();
           }else{

                 $questions_student_test_wise = QuestionTransaction::where('batch_id', $batch_id)
                                                                    ->where('s_id', $student_id)
                                                                    ->orderBy('created_at','desc')->paginate(10);

                  return view('assessor.pages.assessorreportmanagementmarksview')
                          ->with('questions_student_test_wise', $questions_student_test_wise);
           }
      }

 
      public function adminReportsStudentMarksWise($batch_id,$student_id)
      {
          $stdnt_in_batch_validation = Student::where('batch_id',$batch_id)
                                                ->where('username',$student_id)
                                                ->first();
           if(empty($stdnt_in_batch_validation)){

                   \Session::flash('message', 'This student is not assigned to this batch');
                   \Session::flash('status', 'danger');

                   return redirect()->back();
           }else{

                 $questions_student_test_wise = QuestionTransaction::where('batch_id', $batch_id)
                                                                    ->where('s_id', $student_id)
                                                                    ->orderBy('created_at','desc')->paginate(10);

                  return view('admin.pages.reportmanagementmarksview')
                          ->with('questions_student_test_wise', $questions_student_test_wise);
           }
      }

}
