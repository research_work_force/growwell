<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminLogin;
use Illuminate\Support\Facades\Hash;
use App\StudentLogin;
use App\AssessorLogin;
use App\Assessor;
use App\Student;
use App\StudentTestUrl;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function adminReg($username,$password)
    {
           $adminData = new AdminLogin();
           $adminData->username = $username;
           $adminData->password = Hash::make($password);

           $adminData->save();

           return redirect()->back();
    }

    public function adminLoginService(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $adminAccChecking = AdminLogin::where('username', $username)->first();

        if(!strcmp($adminAccChecking['username'],$username)){
                     
                  if(Hash::check($password, $adminAccChecking['password'])){


                          \Session::put('adminsession',$adminAccChecking['id']);
                          \Session::put('admin_uname',$adminAccChecking['username']);

                           \Session::flash('message', 'Logged In'); 
                            \Session::flash('status', 'success'); 

                            return redirect("/dashboard"); 


                  }else{

                       \Session::flash('message', 'Your password is incorrect'); 
                            \Session::flash('status', 'danger'); 
                            return redirect()->back(); 
                  }


        }else{
               
               \Session::flash('message', 'You do not have any account'); 
                            \Session::flash('status', 'danger'); 
                            return redirect()->back(); 

        }
    }

    public function adminLogout()
    {
        \Session::flush('adminsession');
        \Session::flush('admin_uname');

            return redirect('/adminlogin');
    }

    public function changePasswordAdminPage()
    {
        return view('admin.pages.changePassword');
    }

    public function changePasswordAdmin(Request $request)
    {
        $opass = $request->input('opass');
        $npass = $request->input('npass');
        $cnpass = $request->input('cnpass');

          if($npass == $cnpass){

                    $admindata = AdminLogin::where('id',\Session::get('adminsession'))->first();

                               if(!empty($admindata)){

                                         if(Hash::check($opass, $admindata['password'])){

                                               $update_admin_password = AdminLogin::find(\Session::get('adminsession'));
                                               $update_admin_password->password = Hash::make($npass);
                                               $update_admin_password->save();

                                               \Session::flash('message', 'Password changed successfully'); 
                                                \Session::flash('status', 'success'); 

                                                return redirect()->back();

                                         }else{

                                            \Session::flash('message', 'Old password is incorrect'); 
                                            \Session::flash('status', 'warning'); 

                                            return redirect()->back();

                                         }

                               }else{

                                   \Session::flash('message', 'Something went wrong'); 
                                    \Session::flash('status', 'warning'); 

                                    return redirect()->back();

                               }


          }else{

                \Session::flash('message', 'New Password is not matching'); 
                \Session::flash('status', 'warning'); 

                return redirect()->back(); 

          }
    }


    public function assessorLoginService(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');

        $assessorAccChecking = AssessorLogin::where('username', $username)->first();
        $assessorData = Assessor::where('assessor_id', $username)->first();

        // print_r($assessorAccChecking); die;

        if(!strcmp($assessorAccChecking['username'],$username)){
                
                  if(Hash::check($password, $assessorAccChecking['password'])){


                          \Session::put('assessorsession',$assessorAccChecking['username']);
                          \Session::put('assessor_uname',$assessorAccChecking['username']);

                           \Session::flash('message', 'Logged In'); 
                            \Session::flash('status', 'success'); 

                            return redirect('/assessor/dashboard/0'); 


                  }else{

                       \Session::flash('message', 'Your password is incorrect'); 
                            \Session::flash('status', 'danger'); 
                            return redirect()->back(); 
                  }


        }else{
               
               \Session::flash('message', 'You do not have any account'); 
                            \Session::flash('status', 'danger'); 
                            return redirect()->back(); 

        }
    }

    public function assesorLogout()
    {
        \Session::flush('assessorsession');
        \Session::flush('assessor_uname');

            return redirect('/assessorlogin');
    }


    public function changePasswordAssessorPage()
    {
        return view('assessor.pages.changePassword');
    }

    public function changePasswordAssessor(Request $request)
    {
        $opass = $request->input('opass');
        $npass = $request->input('npass');
        $cnpass = $request->input('cnpass');

          if($npass == $cnpass){

                    $assessordata = AssessorLogin::where('username',\Session::get('assessorsession'))->first();

                               if(!empty($assessordata)){

                                         if(Hash::check($opass, $assessordata['password'])){

                                               $update_assessor_password = AssessorLogin::where('username',\Session::get('assessorsession'))
                                                  ->update(array('password'=> Hash::make($npass)));

                                               \Session::flash('message', 'Password changed successfully'); 
                                                \Session::flash('status', 'success'); 

                                                return redirect()->back();

                                         }else{

                                            \Session::flash('message', 'Old password is incorrect'); 
                                            \Session::flash('status', 'warning'); 

                                            return redirect()->back();

                                         }

                               }else{

                                   \Session::flash('message', 'Something went wrong'); 
                                    \Session::flash('status', 'warning'); 

                                    return redirect()->back();

                               }


          }else{

                \Session::flash('message', 'New Password is not matching'); 
                \Session::flash('status', 'warning'); 

                return redirect()->back(); 

          }
    }


    public function studentLoginCheck(Request $request)
    {
       $batch_id = $request->input('batch_id');
       $username = $request->input('username');
       $password = $request->input('password');

       $batchid_student_checking = StudentTestUrl::where('student_username', $username)
                                                   ->where('batch_id', $batch_id)
                                                   ->first();

          $fdt = Carbon::parse($batchid_student_checking->fdt);
          $tdt = Carbon::parse($batchid_student_checking->tdt);

          $ftime = $batchid_student_checking->ftime;
          $ttime = $batchid_student_checking->ttime;

           $today_date = Carbon::now();
           $today_time= Carbon::now()->toTimeString();

           // echo $ftime." ------- ".$ttime."<br>";
           // echo $today_time>$ftime;

           // echo $today_time<$ttime;
           //  die;

           if($today_date->greaterThanOrEqualTo($fdt) && $today_date->lessThanOrEqualTo($tdt)){

                if($today_time>$ftime && $today_time<$ttime){

                  if(empty($batchid_student_checking)){

                          \Session::flash('message', 'You are not authorized to login'); 
                          \Session::flash('status', 'warning');
                          return redirect()->back(); 
                  }else{

                      $studentLoginCheck = StudentLogin::where('username', $username)->first();

                             if(empty($studentLoginCheck)){

                                  \Session::flash('message', 'Account is not associated with us'); 
                                  \Session::flash('status', 'warning');
                                  return redirect()->back(); 

                             }else{

                                  if(Hash::check($password, $studentLoginCheck['password'])){


                                    \Session::put('studentsession',$studentLoginCheck['username']);
                                    \Session::put('student_uname',$studentLoginCheck['username']);
                                    \Session::put('batch_id',$batch_id);

                                     \Session::flash('message', 'Logged In'); 
                                      \Session::flash('status', 'success'); 

                                      return redirect('/student/imageandgeocapture'); 
                                    }else{

                                      \Session::flash('message', 'Wrong Username/Password'); 
                                      \Session::flash('status', 'warning');
                                      return redirect()->back(); 
                                    }
                             }

                  }
        }else{

                           \Session::flash('message', 'Your test time is not started yet. Come between '.$ftime.' - '.$ttime.'date'); 
                            \Session::flash('status', 'warning');
                            return redirect()->back(); 
        }

    }else{

                    \Session::flash('message', 'Your test date is not today. Come between '.$batchid_student_checking->fdt.' & '.$batchid_student_checking->tdt.' dates'); 
                            \Session::flash('status', 'warning');
                            return redirect()->back(); 
        }
      }

}
