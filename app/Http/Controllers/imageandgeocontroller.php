<?php

namespace App\Http\Controllers;

use Image;

use Illuminate\Http\Request;
use App\Student;
use Carbon\Carbon;
use App\StudentLogin;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Imports\StudentImport;
use App\StudentTestUrl;
use Mail;
use DateTime;
use File;
use App\Batches;
use App\Question;
use App\QuestionTransaction;
use App\Assessor;
use App\BatchAssignProcess;
use App\AssessorImgTest;


class imageandgeocontroller extends Controller
{
  public function makeimage(Request $Request)
   {

     // echo("hi");
     // die();

     $x=$Request->input('latitude');
     $y=$Request->input('longitude');
     $a=($x."\n".$y);

 

      $img = Image::make('growwell.jpeg');
      $img->text("$a", 100, 100, function($font) {
         $font->file(public_path('mitra.ttf'));
         $font->size(10);
         $font->color('#ff0000');
         $font->align('center');
         $font->valign('bottom');

     });
      // $img->save(public_path('avatar6.jpeg'));
      $imageName= "theme\images\studentexamimage\ newarijit.jpeg";
      $img->save(public_path($imageName));





      return back();


   }

   public function saveimage(Request $req)
    {

         // Cordinates code

               $x=$req->input('latitude');
               $y=$req->input('longitude');
               $a=('latitude:- '.$x."\n".'longitude:- '.$y);

               $addrs = $this->getLatLong($x,$y);
               $today_date = Carbon::now();

               $str = $a."\n".$addrs."\n".$today_date;
        $img=$req->input('image');

        $img_str = (string) $img;
        // echo($img_str);



        $image_parts = explode(";base64,", $img);

        // print_r($image_parts);

        $image_type_aux = explode("image/", $image_parts[0]);

          // print_r($image_parts);

          $image_type = $image_type_aux[1];
          // echo($image_type);

          $image_base64 = base64_decode($image_parts[1]);

          // print_r($image_base64);

          $imageName = 'arijit.'.'jpeg';

          File::put(public_path(). '/theme/images/studentexamimage/' . $imageName, $image_base64);


         

 
               $imagefullpath= "/theme/images/studentexamimage/arijit.jpeg";
                $img = Image::make(public_path($imagefullpath));
                $img->text("$str", 150,150, function($font) {
                   $font->file(public_path('Roboto-Bold.ttf'));
                   $font->size(8);
                   $font->color('#ffffff');
                   $font->align('center');
                   $font->valign('bottom');

               });
                // $img->save(public_path('avatar6.jpeg'));
                $imageName= "theme\images\studentexamimage\ ".\Session::get('studentsession').".jpeg";
                $img->resize(500, 500)->save(public_path($imageName));

                $img_name_lat_long = \Session::get('studentsession').".jpeg";

                $stdnt_data_update = Student::where('username', \Session::get('studentsession'))
                                              ->update(array('img_name'=>$img_name_lat_long));

                
                $batch_id = \Session::get('batch_id');
                $batch_test_id = Batches::where('batch_id',$batch_id)
                                      ->update(array('test_status'=>'1'));

          return redirect('/assessor/image/window');

    }

    public function imageWindowForAssessor()
    {
        return view('assessor.pages.imageandgeocapture');
    }

    public function assessorImgBeforeTest(Request $req) // Assessor Image before taking the test
      {
         $batch_id = \Session::get('batch_id');

         $img=$req->input('image');

        $img_str = (string) $img;
        


          // Cordinates code

               $x=$req->input('latitude');
               $y=$req->input('longitude');

          $a=('latitude:- '.$x."\n".'longitude:- '.$y);

               $addrs = $this->getLatLong($x,$y);
               $today_date = Carbon::now();

               $str = $a."\n".$addrs."\n".$today_date;

        $image_parts = explode(";base64,", $img);

        // print_r($image_parts);

        $image_type_aux = explode("image/", $image_parts[0]);

          // print_r($image_parts);

          $image_type = $image_type_aux[1];
          // echo($image_type);

          $image_base64 = base64_decode($image_parts[1]);

          // print_r($image_base64);

          $imageName = "temp".$batch_id."-".\Session::get('assessorsession').\Session::get('studentsession').'.jpeg';

          File::put(public_path(). '/theme/images/assessor/' . $imageName, $image_base64);



               $imagefullpath= "/theme/images/assessor/".$imageName;
                $img = Image::make(public_path($imagefullpath));
                $img->text("$str",150,150, function($font) {
                   $font->file(public_path('Roboto-Bold.ttf'));
                   $font->size(8);
                   $font->color('#ffffff');
                   $font->align('center');
                   $font->valign('bottom');

               });
                // $img->save(public_path('avatar6.jpeg'));
                $imageName= "theme\images\assessor\ ".$batch_id."-".\Session::get('assessorsession').\Session::get('studentsession').".jpeg";
                $img->resize(500, 500)->save(public_path($imageName));

                $img_name_lat_long = $batch_id."-".\Session::get('assessorsession').\Session::get('studentsession').".jpeg";

                $stdnt_data_update = Assessor::where('assessor_id', \Session::get('assessorsession'))
                                              ->update(array('image'=>$img_name_lat_long));
                
                
                $batch_test_id = Batches::where('batch_id',$batch_id)
                                      ->update(array('test_status'=>'1'));

                $set_assessor_img = new AssessorImgTest();
                $set_assessor_img->batch_id = $batch_id;
                $set_assessor_img->assessor_id = \Session::get('assessorsession');
                $set_assessor_img->student_id =  \Session::get('studentsession');
                $set_assessor_img->img = $img_name_lat_long;
                $set_assessor_img->save();

          return redirect('/student/questiondashboard'); 
      }

      public function getLatLong($x,$y)
      {
          $url ='https://maps.googleapis.com/maps/api/geocode/json?latlng='.$x.','.$y.'&key=AIzaSyBKXjlhXjRVQiQ6RpPW0jOh3jlhE1vGnEE';

          $request_url = $url;
            // echo $request_url; die; 
          $curl = curl_init($request_url);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

          $response = json_decode(curl_exec($curl));
          curl_close($curl);
          return $response->results[0]->formatted_address;

      }

}
