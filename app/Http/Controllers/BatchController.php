<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Question;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon; 
use Session;
use App\Imports\StudentImport;
use App\Batches;
use App\BatchAssignProcess;
use App\Assessor;
use Mail;
use App\Student;
use App\BatchHistory;
use App\AssessorHistory;
use App\QuestionTransaction;
use App\StudentTestProgress;
use App\QuestionMapTransaction;
use App\Http\Controllers\StaticValueProviderController;

class BatchController extends Controller
{
      public function saveBatchDetails(Request $request)
      {
 
                          $schemeProgram             = $request->input('schemeProgram');
                          $jobRole                   = $request->input('jobRole');
                          $sectors                   = $request->input('sectors');
                          $batchTypes                = $request->input('batchTypes');
                          $training_center_location  = $request->input('training_center_location');
                          $batch_size                = $request->input('batch_size');
                          $assessment_date_fdt       = $request->input('assessment_date_fdt');
                          // $assessment_date_tdt       = $request->input('assessment_date_tdt');
                          $states                    = $request->input('states');
                          $language                  = $request->input('language');
                          $action_date               = $request->input('action_date');
                          $received_from_ssc         = $request->input('received_from_ssc');
                          $batch_id                  = $request->input('batch_id');

                          $spoc_name               = $request->input('spoc_name');
                          $spoc_mobile_no         = $request->input('spoc_mobile_no');
                          $spoc_email                  = $request->input('spoc_email');

                          $batch_name   =   $request->input('batch_name');
                          $fduration    = $request->input('fduration');
                          $tduration    = $request->input('tduration');
         
                          $batch_id_checking = Batches::where('batch_id',$batch_id)->first();

                           if($request->hasFile('docfile')){

                                    $supdoc= $request->file('docfile');
                                    $ext = $supdoc->getClientOriginalExtension();

                                    $docfile_filename = $batch_id."-".$batch_name.'.'.$ext; 
                                    //$supdoc->getClientOriginalName();
                                    $path =  "growwell/batch/";
                                    $supdoc->move(public_path($path), $docfile_filename);

                                    if(empty($batch_id_checking)){
                                      $batch_data = new Batches();
                                      $batch_data->schemeProgram            = $schemeProgram;
                                      $batch_data->jobRole                  = $jobRole;
                                      $batch_data->sectors                  = $sectors; 
                                      $batch_data->batchTypes               = $batchTypes;
                                      $batch_data->language                 = $language;
                                      $batch_data->batch_id                 = $batch_id;
                                      $batch_data->training_center_location = $training_center_location;
                                      $batch_data->batch_size               = $batch_size;
                                      $batch_data->assessment_date_fdt      = $assessment_date_fdt;
                                      // $batch_data->assessment_date_tdt      = $assessment_date_tdt;
                                      $batch_data->action_date              = $action_date;
                                      $batch_data->received_from_ssc        = $received_from_ssc;
                                      $batch_data->states                   = $states;

                                      $batch_data->spoc_name                = $spoc_name;
                                      $batch_data->spoc_mobile_no           = $spoc_mobile_no;
                                      $batch_data->email               = $spoc_email;

                                      $batch_data->batch_name     =  $batch_name;
                                      $batch_data->fduration      =  $fduration;
                                      $batch_data->tduration      =  $tduration;
                                      $batch_data->docfile        =  $docfile_filename;
                                      $batch_data->added_by = \Session::get('adminsession');
                                      $batch_data->batch_status = 0;
                                      $batch_data->save();


                                      \Session::flash('message', 'Batch created successfully'); 
                                      \Session::flash('status', 'success'); 
                                      return redirect()->back();
                                    }else{

                                          \Session::flash('message', 'Batch Id should be different'); 
                                          \Session::flash('status', 'danger'); 
                                          return redirect()->back();
                                    }
                              }else{

                                  if(empty($batch_id_checking)){
                                      $batch_data = new Batches();
                                      $batch_data->schemeProgram            = $schemeProgram;
                                      $batch_data->jobRole                  = $jobRole;
                                      $batch_data->sectors                  = $sectors; 
                                      $batch_data->batchTypes               = $batchTypes;
                                      $batch_data->language                 = $language;
                                      $batch_data->batch_id                 = $batch_id;
                                      $batch_data->training_center_location = $training_center_location;
                                      $batch_data->batch_size               = $batch_size;
                                      $batch_data->assessment_date_fdt      = $assessment_date_fdt;
                                      // $batch_data->assessment_date_tdt      = $assessment_date_tdt;
                                      $batch_data->action_date              = $action_date;
                                      $batch_data->received_from_ssc        = $received_from_ssc;
                                      $batch_data->states                   = $states;

                                      $batch_data->spoc_name                = $spoc_name;
                                      $batch_data->spoc_mobile_no           = $spoc_mobile_no;
                                      $batch_data->email               = $spoc_email;

                                      $batch_data->batch_name     =  $batch_name;
                                      $batch_data->fduration      =  $fduration;
                                      $batch_data->tduration      =  $tduration;
                                      $batch_data->added_by = \Session::get('adminsession');
                                      $batch_data->batch_status = 0;
                                      $batch_data->save();


                                      \Session::flash('message', 'Batch created successfully'); 
                                      \Session::flash('status', 'success'); 
                                      return redirect()->back();
                                    }else{

                                          \Session::flash('message', 'Batch Id should be different'); 
                                          \Session::flash('status', 'danger'); 
                                          return redirect()->back();
                                    }
                              }


            
      }

      public function BatchDetails()
      {
       

          $batch_data = Batches::orderBy('created_at','desc')->paginate(10);

          $batch_id = Batches::get();
          $batch_id_array =array();

          foreach ($batch_id as $batch){
            $batch_id_array[] = $batch->batch_id;
          }



          $enrolled_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->count();
              $enrolled_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $dropout_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
              $dropout_candidate_array[$batch_id_array[$i]] = $count;
          }


          return view('admin.pages.batchmanagement')
                      ->with('batch_data', $batch_data)
                      ->with('enrolled_candidate_array', $enrolled_candidate_array)
                      ->with('dropout_candidate_array', $dropout_candidate_array);

      }

      public function BatchDetailsSinglePage($batch_id)
      { 
        $batch_details = Batches::where('batch_id', $batch_id)->first();

        $batch_history_details = BatchHistory::where('batch_id', $batch_id)->orderBy('created_at','desc')->paginate(10);

        $assessment_history_details = AssessorHistory::where('batch_id', $batch_id)->orderBy('created_at','desc')->paginate(10);



                                // foreach ($batched_assigned as $batch){
                                  $batch_id_array = $batch_details->batch_id;
                                // }



                                // $enrolled_candidate_array = array();

                                // for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array)->count();
                                    $enrolled_candidate_array[$batch_id_array] = $count;
                                // }
                                
                                // $dropout_candidate_array = array();

                                // for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array)->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array] = $count;
                                // }


        return view('admin.pages.viewbatchhistory')
                      ->with('batch_history_details', $batch_history_details)
                      ->with('assessment_history_details', $assessment_history_details)
                      ->with('batch_details', $batch_details)
                      ->with('enrolled_candidate_array', $enrolled_candidate_array)
                      ->with('dropout_candidate_array', $dropout_candidate_array);
      }

      public function batchAssignToAssessor(Request $request)
      {

          $batch_id = $request->input('batch_id');
          $assessor_id = $request->input('assessor_id');
          $req_status = '0';
          $assigned_date = $request->input('assigned_date');
          $test_duration = $request->input('test_duration');

          $batch_assigned_checking = BatchAssignProcess::where('batch_id', $batch_id)
                                                        ->where('assessor_id',$assessor_id)
                                                        ->first();

          $same_batch_assigned_to_other_checking = BatchAssignProcess::where('batch_id', $batch_id)
                                                        ->where('req_status','1')
                                                        ->first();

          $batch_reassigned_checking = BatchAssignProcess::where('batch_id', $batch_id)
                                                        ->where('req_status','0')
                                                        ->first();


          // $batch_assessed_pending_checking = BatchAssignProcess::where('batch_id', $batch_id)
          //                                               ->where('req_status','4')
          //                                               ->first();

          // $batch_assessed_sentback_checking = BatchAssignProcess::where('batch_id', $batch_id)
          //                                               ->where('req_status','5')
          //                                               ->first();

          // $batch_assessed_approve_checking = BatchAssignProcess::where('batch_id', $batch_id)
          //                                               ->where('req_status','6')
          //                                               ->first();

          $student_checking = Student::where('batch_id',$batch_id)->count();

          if($student_checking == 0){

               \Session::flash('message', 'Assign student first'); 
              \Session::flash('status', 'danger'); 

              return redirect()->back();
          }
          if(!empty($batch_assigned_checking)){

              \Session::flash('message', 'This batch is already assigned to this assessor'); 
              \Session::flash('status', 'danger'); 

              return redirect()->back();
          }  

          if(!empty($same_batch_assigned_to_other_checking)){

               \Session::flash('message', 'Accepted Batch could not be assigned to Assessor again'); 
              \Session::flash('status', 'danger'); 

              return redirect()->back();
          }

          if(!empty($batch_reassigned_checking)){
               \Session::flash('message', 'Batch is already Assigned to other Assessor'); 
              \Session::flash('status', 'danger'); 

              return redirect()->back();
          }


          
          $batch_assign_process = new BatchAssignProcess();
          $batch_assign_process->batch_id = $batch_id;
          $batch_assign_process->assessor_id = $assessor_id;
          $batch_assign_process->req_status = $req_status;
          $batch_assign_process->date = $assigned_date;
          $batch_assign_process->save();

          $assessor_email = Assessor::where('assessor_id', $assessor_id)->first();


           $batch_status_update = Batches::where('batch_id',$batch_id)
                          ->update(array('batch_status' => 1,'test_duration'=>$test_duration));

            //mail to employee
              $to_name=$assessor_email->name;
              $to_email=$assessor_email->email;

              $data=array('name' => "$to_name",'u_id'=>"$to_email",'pass'=>"One Batch has assigned to you");
              Mail::send('admin.pages.mailforassign',$data,function($message) use ($to_name,$to_email){
                                    $message->to($to_email)->subject('Growell Assessor Account');
                                  });

          \Session::flash('message', 'Assessor Assigned'); 
          \Session::flash('status', 'success'); 

          return redirect()->back();

      }

      public function batchReAssignAssessor(Request $request)
      {
          $batch_id = $request->input('batch_id');
          $assessor_id = $request->input('assessor_id');
          $req_status = '0';
          $id = $request->input('id');
          $assigned_date = $request->input('assigned_date');

          // AssessorHistory
          
           $same_batch_assigned_to_other_checking = BatchAssignProcess::where('batch_id', $batch_id)
                                                        ->where('req_status','1')
                                                        ->first();

           if(!empty($same_batch_assigned_to_other_checking)){

               \Session::flash('message', 'Accepted Batch could not be assigned to Assessor again'); 
              \Session::flash('status', 'danger'); 

              return redirect()->back();
          }
                          
          $old_assessor = BatchAssignProcess::where('batch_id',$batch_id)
                                              ->first();
          if(empty($old_assessor)){


                    \Session::flash('message', 'First assign a assessor'); 
                \Session::flash('status', 'danger'); 

                return redirect()->back();
           }
          $add_history_assessorchange = new AssessorHistory();
          $add_history_assessorchange->old_assessor_id = $old_assessor->assessor_id;
          $add_history_assessorchange->new_assessor_id = $assessor_id;
          $add_history_assessorchange->batch_id = $batch_id;
          $add_history_assessorchange->save();


          $batch_assign_process = BatchAssignProcess::where('batch_id',$batch_id)
                                      ->update(array('assessor_id'=>$assessor_id,'req_status'=>'0','date'=>$assigned_date));
         

          $assessor_email = Assessor::where('assessor_id', $assessor_id)->first();


           $batch_status_update = Batches::where('batch_id',$batch_id)
                          ->update(array('batch_status' => 1));

            //mail to employee
              $to_name=$assessor_email->name;
              $to_email=$assessor_email->email;

              $data=array('name' => "$to_name",'u_id'=>"$to_email",'pass'=>"One Batch has assigned to you");
              Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                    $message->to($to_email)->subject('Growell Assessor Account');
                                  });

          \Session::flash('message', 'Assessor Assigned'); 
          \Session::flash('status', 'success'); 

          return redirect()->back();
        
      }

      public function batchDelete($batch_id)
      {
        $batch_data_del = Batches::where('batch_id', $batch_id)->delete();

         return redirect()->back();
      }

      public function searchBatchByFilters(Request $request)
      {
        $schemeProgram = $request->input('schemeProgram');
        $jobRole = $request->input('jobRole');
        $sectors = $request->input('sectors');
        $batchTypes = $request->input('batchTypes');
        $states = $request->input('states');
        $batch_id = $request->input('batch_id');

       
        $batch_search_result = Batches::orWhere('schemeProgram',$schemeProgram)
                                        ->orWhere('jobRole',$jobRole)
                                        ->orWhere('sectors',$sectors)
                                        ->orWhere('batchTypes',$batchTypes)
                                        ->orWhere('states',$states)
                                        ->orWhere('batch_id',$batch_id)
                                        ->orderBy('created_at','desc')->paginate(10);

          $batch_id_array =array();

          foreach ($batch_search_result as $batch){
            $batch_id_array[] = $batch->batch_id;
          }



          $enrolled_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->count();
              $enrolled_candidate_array[$batch_id_array[$i]] = $count;
          }
          
          $dropout_candidate_array = array();

          for($i = 0; $i < count($batch_id_array); $i++){

              $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
              $dropout_candidate_array[$batch_id_array[$i]] = $count;
          }

                                        
        return view('admin.pages.batchmanagement')
                      ->with('batch_data', $batch_search_result)
                      ->with('enrolled_candidate_array', $enrolled_candidate_array)
                      ->with('dropout_candidate_array', $dropout_candidate_array);;

      }

      public function searchBatchAdmin(Request $request)
      {
          $schemeProgram = $request->input('schemeProgram');
        $jobRole = $request->input('jobRole');
        $sectors = $request->input('sectors');
        $batchTypes = $request->input('batchTypes');
        $states = $request->input('states');
        $batch_id = $request->input('batch_id');

    
            $batch_search_result = \DB::table('batches')
                                    ->join('batch_process', function($join)
                                    {
                                        $join->on('batches.batch_id', '=', 'batch_process.batch_id');

                                    })
                                    ->orderBy('batches.created_at','desc')->paginate(10);
           
                                        
        return view('admin.pages.assessmentreq')->with('batched_assigned', $batch_search_result);
      }

      public function searchBatchByFiltersAssessor(Request $request)
      {
        $schemeProgram = $request->input('schemeProgram');
        $jobRole = $request->input('jobRole');
        $sectors = $request->input('sectors');
        $batchTypes = $request->input('batchTypes');
        $states = $request->input('states');
        $batch_id = $request->input('batch_id');
        $status = $request->input('status');

       
        $batch_status_deadline = BatchAssignProcess::where('assessor_id',\Session::get('assessorsession'))->get();

        $batch_last_date=[];
        $batch_process_id=[];

        foreach($batch_status_deadline as $batch){

              if($batch->req_status != '1'){
                 $batch_last_date[] = Carbon::parse($batch->date);
              }
       

         }

         foreach($batch_status_deadline as $batch){

            $batch_process_id[] = $batch->id;

         }



        $today_date = Carbon::now();

        for($i = 0;$i <count($batch_last_date); $i++){

               if($today_date->greaterThan($batch_last_date[$i]) ){ //back dated
                  
                  $batch_process_status_update = BatchAssignProcess::find($batch_process_id[$i]);
                  // $batch_process_status_update->req_status = '-1';
                  $batch_process_status_update->delete();

 
               }else{                                               //coming date
                 


               }
        }
        

        if($status == null){
                             $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessor_id',\Session::get('assessorsession'))
                              ->where('batch_process.req_status','0')
                              ->orwhere('batches.schemeProgram',$schemeProgram)
                              ->orwhere('batches.jobRole',$jobRole)
                              ->orwhere('batches.sectors',$sectors)
                              ->orwhere('batches.batchTypes',$batchTypes)
                              ->orwhere('batches.training_center_location',$states)
                              ->orwhere('batches.batch_id',$batch_id)
                              ->orderBy('batches.created_at','desc')->paginate(10); 
                               $batch_id = Batches::get();
                              $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }

        }else{
                 $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessor_id',\Session::get('assessorsession'))
                              ->orwhere('batches.schemeProgram',$schemeProgram)
                              ->orwhere('batches.jobRole',$jobRole)
                              ->orwhere('batches.sectors',$sectors)
                              ->orwhere('batches.batchTypes',$batchTypes)
                              ->orwhere('batches.training_center_location',$states)
                              ->orwhere('batches.batch_id',$batch_id)
                              ->where('batch_process.req_status',$status)
                              ->orderBy('batches.created_at','desc')->paginate(10);

                               $batch_id = Batches::get();
                              $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }

        }
        

    

            // $batch_search_result = \DB::table('batches')
            //                         ->join('batch_process', function($join)
            //                         {
            //                             $join->on('batches.batch_id', '=', 'batch_process.batch_id')
            //                                  ->where('batch_process.assessor_id', '=',\Session::get('assessorsession'));
            //                         })
            //                         ->orderBy('batches.created_at','desc')->paginate(10);
           
                                        
        return view('assessor.pages.assessorassessmentreq')
                       ->with('batched_assigned',$batched_assigned)
                       ->with('enrolled_candidate_array', $enrolled_candidate_array)
                       ->with('dropout_candidate_array', $dropout_candidate_array);

      }

      public function getAssessmentBatchReq()
      {
         

         $batch_data = \DB::table('batches')
            ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
            ->select('batches.*', 'batch_process.*')
            ->orderBy('batch_process.created_at','desc')->paginate(10);

         return $batch_data;


      }
 
      public function getAssessorDashboard($status)
      {
        $batch_status_deadline = BatchAssignProcess::where('assessor_id',\Session::get('assessorsession'))->get();

        $batch_last_date=[];
        $batch_process_id=[];

        foreach($batch_status_deadline as $batch){

              if($batch->req_status != '1'){
                 $batch_last_date[] = Carbon::parse($batch->date);
              }
       

         }

         foreach($batch_status_deadline as $batch){

            $batch_process_id[] = $batch->id;

         }



        $today_date = Carbon::now();

        for($i = 0;$i <count($batch_last_date); $i++){

               if($today_date->greaterThan($batch_last_date[$i]) ){ //back dated
                  
                  $batch_process_status_update = BatchAssignProcess::find($batch_process_id[$i]);
                  // $batch_process_status_update->req_status = '-1';
                  $batch_process_status_update->delete();

 
               }else{                                               //coming date
                 


               }
        }
        

        if($status == null){
                             $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessor_id',\Session::get('assessorsession'))
                              ->where('batch_process.req_status','0')
                              ->orderBy('batches.created_at','desc')->paginate(10); 
                               $batch_id = Batches::get();
                              $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }

        }else{
                 $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessor_id',\Session::get('assessorsession'))
                              ->where('batch_process.req_status',$status)
                              ->orderBy('batches.created_at','desc')->paginate(10);

                               $batch_id = Batches::get();
                              $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }

        }
        

        return view('assessor.pages.assessorassessmentreq')
                      ->with('batched_assigned',$batched_assigned)
                       ->with('enrolled_candidate_array', $enrolled_candidate_array)
                      ->with('dropout_candidate_array', $dropout_candidate_array);
      }

      public function getAssessorAssignedBatchesAdmin($status=0)
      {
        if($status == null){
                             $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.req_status','0')
                              ->orderBy('batches.created_at','desc')->paginate(10); 

                               $batch_id = Batches::get();
                              $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }
                              


        }else{
                 $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.req_status',$status)
                              ->orderBy('batches.created_at','desc')->paginate(10);

                               $batch_id = Batches::get();
                              $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }

        }
        

        return view('admin.pages.assessmentreq')
                    ->with('batched_assigned',$batched_assigned)
                    ->with('enrolled_candidate_array', $enrolled_candidate_array)
                      ->with('dropout_candidate_array', $dropout_candidate_array);
      }

      public function getAssessorAssignedBatchs($status)
      {
          $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*')
                              ->where('batch_process.assessor_id',\Session::get('assessorsession'))
                              ->where('batch_process.req_status',$status)
                              ->get();    
      }

      public function assessorReqAccept(Request $request)
      {
         $id = $request->input('id');

         $find_batch_process = BatchAssignProcess::find($id);
         $find_batch_process->req_status = '1';
         $find_batch_process->save();

         $batch_id_data =BatchAssignProcess::where('id',$id)->first();
         //ques set into question transaction table
            $batch_id =  $batch_id_data->batch_id;

            $batch_details = Batches::where('batch_id', $batch_id)->first();

            $student_data = Student::where('batch_id',$batch_id)->get();

            $question = Question::where('job_role',$batch_details->jobRole )
                                  ->get();

            foreach ($question as $q)
            {
                foreach ($student_data as $student) { 
                    $qtransac = new QuestionTransaction();
                    $qtransac->batch_id = $batch_id;
                    $qtransac->q_id = $q->id;
                    $qtransac->section_id = $q->section;
                    $qtransac->attempt_status ='0';
                    $qtransac->s_id = $student->username;
                    $qtransac->question = $q->question;
                    $qtransac->option1 = $q->option1;
                    $qtransac->option2 = $q->option2;
                    $qtransac->option3 = $q->option3;
                    $qtransac->option4 = $q->option4;
                    $qtransac->right_answer = $q->right_answer;
                    $qtransac->question_hindi = $q->question_hindi;
                    $qtransac->option1_hindi = $q->option1_hindi;
                    $qtransac->option2_hindi = $q->option2_hindi;
                    $qtransac->option3_hindi = $q->option3_hindi;
                    $qtransac->option4_hindi = $q->option4_hindi;
                    $qtransac->right_ans_hindi = $q->right_ans_hindi;
                    $qtransac->question_teleugu = $q->question_teleugu;
                    $qtransac->option1_teleugu = $q->option1_teleugu;
                    $qtransac->option2_teleugu = $q->option2_teleugu;
                    $qtransac->option3_teleugu = $q->option3_teleugu;
                    $qtransac->option4_teleugu = $q->option4_teleugu;
                    $qtransac->right_ans_teleugu = $q->right_ans_teleugu;
                    $qtransac->question_assam = $q->question_assam;
                    $qtransac->option1_assam = $q->option1_assam;
                    $qtransac->option2_assam = $q->option2_assam;
                    $qtransac->option3_assam = $q->option3_assam;
                    $qtransac->option4_assam = $q->option4_assam;
                    $qtransac->right_ans_assam = $q->right_ans_assam;
                    $qtransac->job_role = $q->job_role;
                    $qtransac->section = $q->section;
                    $qtransac->ques_marks = $q->marks;                    

                    $qtransac->save();
                  }
                   
                  foreach ($student_data as $student) { 

                    $stdnt_test_progress = new StudentTestProgress();
                    $stdnt_test_progress->student_id = $student->username;
                    $stdnt_test_progress->student_name = $student->name;
                    $stdnt_test_progress->batch_id    = $batch_id;
                    $stdnt_test_progress->question_id = $q->id;
                    $stdnt_test_progress->section  = $q->section;
                    $stdnt_test_progress->marks    = "-1";
                    $stdnt_test_progress->save();
                  }

                  $ques_copy_to_map = new QuestionMapTransaction();
                  $ques_copy_to_map->q_id = $q->id;
                  $ques_copy_to_map->marks = $q->marks;
                  $ques_copy_to_map->section  = $q->section;
                  $ques_copy_to_map->job_role = $q->job_role;
                  $ques_copy_to_map->batch_id =  $batch_id;
                  $ques_copy_to_map->save();

            }

          //mail to employee
              $to_name="Admin";
              $to_email="sagarmondal0307@gmail.com";

              $data=array('name' => "$to_name",'u_id'=>"$to_email",'pass'=>"Assessor accepted the batch");
              Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                    $message->to($to_email)->subject('Growwell');
                                  });


         return redirect('/assessor/dashboard/0');
      }

      public function assessorReqReject(Request $request)
      {
         $id = $request->input('id');

         $find_batch_process = BatchAssignProcess::find($id);
         $find_batch_process->req_status = '2';
         $find_batch_process->save();

         //mail to employee
              $to_name="Admin";
              $to_email="sagarmondal0307@gmail.com";

              $data=array('name' => "$to_name",'u_id'=>"$to_email",'pass'=>"Assessor rejected the batch");
              Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                    $message->to($to_email)->subject('Growwell');
                                  });

         return redirect('/assessor/dashboard/0');
      }



      public function assessorAssessedReqAccept(Request $request)
      {
         $id = $request->input('id');

         $find_batch_process = BatchAssignProcess::where('batch_id',$id)
                                ->update(array('assessed_req_status'=>'2'));

         return redirect('assessor/assessed/1');
      }

      public function assessorAssessedReqReject(Request $request)
      {
         $id = $request->input('id');

         $find_batch_process = BatchAssignProcess::where('batch_id',$id)
                                ->update(array('assessed_req_status'=>'0'));

         return redirect('assessor/assessed/1');
      }




      public function assessorStudentList($batch_id)
      { 
         $assessor_batch_validation = BatchAssignProcess::where('batch_id',$batch_id)
                                                          ->where('assessor_id',\Session::get('assessorsession'))
                                                          ->where('req_status','1')
                                                          ->first();
         if(empty($assessor_batch_validation)){

                \Session::flash('message', 'This batch is not belong to you'); 
                  \Session::flash('status', 'danger'); 

                  return redirect()->back();
          
          }else{

                $student_id = Student::where('batch_id', $batch_id)->get();

                      foreach($student_id as $student){


                                 $student_info_batch_wise[$student->username] = \DB::table('student_test_progress')
                                      ->select('student_name','section',\DB::raw("sum(marks) as mark"))
                                      ->where('student_id','=',$student->username)
                                      ->groupBy('section')
                                      ->get();
                           
                                     
                      }

              $section_wise_marks=array();
              $marks = 0;
              $job_role = Batches::where('batch_id', $batch_id)->first();
              $sections = \DB::table('questions')
                          ->select('section')
                          ->where('job_role','=', $job_role->jobRole)
                          ->groupBy('section')
                          ->get();

                $batch_info = Batches::where('batch_id', $batch_id)->first();

                $assessor_id = BatchAssignProcess::where('batch_id', $batch_id)->first();
                
                $assessor_info = Assessor::where('assessor_id',$assessor_id->assessor_id)->first();

                return view('assessor.pages.assessorreportmanagementstudentwise')
                         ->with('batch_id', $batch_id)
                         ->with('batch_info', $batch_info)
                         ->with('assessor_info', $assessor_info)
                         ->with('sections',$sections)
                         ->with('student_data', $student_info_batch_wise);
                 
          }
          
      }

      public function changeAssessmentDate(Request $request)
      {
          $batch_id = $request->input('batch_id');
          $previous_date = $request->input('previous_date');
          $new_assessment_date = $request->input('new_assessment_date');
          $reason = $request->input('reason');

          $batch_history_data = new  BatchHistory();
          $batch_history_data->old_date = $previous_date;
          $batch_history_data->new_date = $new_assessment_date;
          $batch_history_data->reason = $reason;
          $batch_history_data->batch_id = $batch_id;
          $batch_history_data->change_by = \Session::get('adminsession');
          $batch_history_data->save();

          $batch_date_update = Batches::where('batch_id',$batch_id)
                          ->update(array('assessment_date_fdt' => $new_assessment_date));

          \Session::flash('message', 'Updated'); 
          \Session::flash('status', 'success'); 

          return redirect()->back();


      }

      public function getAssessorTestBatchesAdmin($status)
      {
          
          $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessed_req_status',$status)
                              ->orderBy('batches.created_at','desc')->paginate(10);

                              

          $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
          return view('admin.pages.assessmentreq')
                    ->with('batched_assigned',$batched_assigned)
                    ->with('enrolled_candidate_array', $enrolled_candidate_array)
                    ->with('dropout_candidate_array', $dropout_candidate_array);
      }


      public function assessedBatchListAssessor($status)
      {
           $batched_assigned = \DB::table('batches')
                              ->join('batch_process', 'batches.batch_id', '=', 'batch_process.batch_id')
                              // ->join('batch_review_comments', 'batches.batch_id', '=', 'batch_review_comments.batch_id')
                              ->select('batches.*','batch_process.*')
                              ->where('batch_process.assessed_req_status',$status)
                              ->orderBy('batches.created_at','desc')->paginate(10);

                              

          $batch_id_array =array();

                                foreach ($batched_assigned as $batch){
                                  $batch_id_array[] = $batch->batch_id;
                                }



                                $enrolled_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->count();
                                    $enrolled_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
                                $dropout_candidate_array = array();

                                for($i = 0; $i < count($batch_id_array); $i++){

                                    $count = Student::where('batch_id',$batch_id_array[$i])->where('dropout_status','1')->count();
                                    $dropout_candidate_array[$batch_id_array[$i]] = $count;
                                }
                                
          return view('assessor.pages.assessorassessmentreq')
                    ->with('batched_assigned',$batched_assigned)
                    ->with('enrolled_candidate_array', $enrolled_candidate_array)
                    ->with('dropout_candidate_array', $dropout_candidate_array);
      }


      public function reviewBatchToAdminByAssessor(Request $request)
      { 

             $batch_id = $request->input('batch_id');

             $batch_review_status =  BatchAssignProcess::where('batch_id', $batch_id)
                                                        ->where('assessed_req_status','1')
                                                        ->first();

            if(empty($batch_review_status)){

              $batch_review = BatchAssignProcess::where('batch_id', $batch_id)
                                                ->update(array('assessed_req_status'=>'1'));

                                                //mail to employee
              $to_name="Admin";
              $to_email="sagarmondal0307@gmail.com";

              $data=array('name' => "$to_name",'u_id'=>"$to_email",'pass'=>"Assessor submitted for review");
              Mail::send('admin.pages.mail',$data,function($message) use ($to_name,$to_email){
                                    $message->to($to_email)->subject('Growwell');
                                  });

              \Session::flash('message', 'Submitted for Admin Review'); 
                 \Session::flash('status', 'success'); 
                return redirect()->back();
            }else{

               \Session::flash('message', 'Already in review'); 
                 \Session::flash('status', 'danger'); 
                return redirect()->back();
            }
             

      }

      public function adminApproveBatch($batch_id)
      {

             $batch_review = BatchAssignProcess::where('batch_id', $batch_id)
                                                ->update(array('assessed_req_status'=>'2'));

             
             // $students_data = Student::where('batch_id',$batch_id)->get();


             // foreach($students_data as $sdata){

             //  $to_name=$sdata->name;
             //  $to_email=$sdata->email;

             //  $data=array('name' => "$to_name",'u_id'=>"$to_email");
             //  Mail::send('student.pages.resultmail',$data,function($message) use ($to_name,$to_email){
             //                        $message->to($to_email)->subject('Growwell');
             //                      });

             // }

           $sec_wise_totalmarks = StaticValueProviderController::getTotalMarksSectionWise($batch_id);
           $total_ques_marks=0;
           for($i=0;$i<count($sec_wise_totalmarks);$i++){
              $total_ques_marks = $total_ques_marks + Intval($sec_wise_totalmarks[$i]);

           }

             $student_id = Student::where('batch_id', $batch_id)->get();
             $pass_fail_status ="NA";
             $grade ="NA";
          foreach($student_id as $student){

                   if($total_ques_marks != 0){
                        $perstdnt = (Intval($student->marks)/$total_ques_marks)*100;

                              if($perstdnt>=80){
                                $pass_fail_status ="pass";
                                $grade ='A';
                              }else if($perstdnt<80 && $perstdnt>70){
                                $pass_fail_status ="pass";
                                $grade ='B';
                              }else if($perstdnt<70){
                                $pass_fail_status ="fail";
                                $grade ='C';
                              }else if($perstdnt==0.00){
                                $pass_fail_status ="fail";
                                $grade ='C';
                              }
                              else{
                                $pass_fail_status ="Fail";
                                $grade ='C';
                              }

                              $to_name=$student->name;
                              $to_email=$student->email;

                              $data=array('name' => "$to_name",'u_id'=>"$to_email",'grade' =>$grade,'pass_fail_status'=>$pass_fail_status);
                              Mail::send('student.pages.resultmail',$data,function($message) use ($to_name,$to_email){$message->to($to_email)->subject('Growwell Result Email');});


                   }else{

                         
                   }
                         
          }

             \Session::flash('message', 'One batch is approved');
             \Session::flash('status', 'success');

             return redirect()->back();
      }
} 
