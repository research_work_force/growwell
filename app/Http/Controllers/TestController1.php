<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Assessor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\AssessorLogin;
use Illuminate\Support\Str;
use App\Imports\AssessorImport;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Mail;
use App\Question;
use App\Batches;
use App\QuestionTransaction;
use Illuminate\Pagination\LengthAwarePaginator;

class TestController extends Controller
{

     public function questions()
     {

       // \Session::put('student_id','5454kkklkl');
     	$batch_id = "BI9736";

     	//$batch_details = Batches::where('batch_id', $batch_id)->where('batch_status',1)->first();


     	$question = Question::where('batch_id', $batch_id)
     							->get();

        // Custom Paginate the result
     	    $items = array();
			foreach($question as $thread)
			{

			        array_push($items, $thread);

			}

			$currentPage = LengthAwarePaginator::resolveCurrentPage();
			$perPage = 1;

			// $items = array_reverse(array_sort($items , function ($value) {
			//     return $value['created_at'];
			// }));
			$currentItems = array_slice($items, $perPage * ($currentPage - 1), $perPage);

			$question_data = new LengthAwarePaginator($currentItems, count($items), $perPage, $currentPage);



			return json_encode($question_data);
     }

     public function questions_transaction_insert(Request $request)
     {
       $answer = new QuestionTransaction();
       $answer->answer = $request->input('answer');
       $answer->batch_id= $request->input('batch_id');
       $answer->q_id= $request->input('q_id');
       $answer->attempt_status= $request->input('attempt_status');


       $answer->save();

       return back();
     }



}
