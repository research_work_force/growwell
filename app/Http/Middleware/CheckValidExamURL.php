<?php

namespace App\Http\Middleware;

use Closure;
use App\StudentTestUrl;
use Carbon\Carbon;
use App\Student;
class CheckValidExamURL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
                          
                      if($request->session()->exists('studentsession')){

                                  $student_username = $request->session()->get('studentsession');
                             
                                  $stdnt_data = Student::where('username', $student_username)
                                                         where('schedule_test_status','4')->first();

                                  if(empty($stdnt_data)){

                                      \Session::flash('message', 'You have already given the test'); 
                                      \Session::flash('status', 'warning');

                                      return redirect('/studentlogin');
                                  }else{
                                     return $next($request);
                                  }

                                       
                        }
        
    }
}
