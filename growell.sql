-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2020 at 08:09 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `growell`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `status` tinyint(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminlogin`
--

INSERT INTO `adminlogin` (`id`, `username`, `password`, `status`, `created_at`, `updated_at`) VALUES
(2, 'admin', '$2y$10$dYwUMsAfZ38SNmAxw5W5ietgZpjz0TDFrixUzaa2S6483r1nKVcs2', 0, '2020-06-01 14:00:27', '2020-06-03 20:04:39');

-- --------------------------------------------------------

--
-- Table structure for table `assessor`
--

CREATE TABLE `assessor` (
  `id` bigint(10) NOT NULL,
  `assessor_id` varchar(500) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `religion` varchar(500) DEFAULT NULL,
  `date_of_birth` varchar(500) DEFAULT NULL,
  `language_known` varchar(500) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `assessment_agency_aligned` varchar(100) DEFAULT NULL,
  `linking_type` varchar(100) DEFAULT NULL,
  `sectors` varchar(100) DEFAULT NULL,
  `duration_fdt` varchar(100) DEFAULT NULL,
  `duration_tdt` varchar(100) DEFAULT NULL,
  `supdoc` varchar(100) DEFAULT NULL,
  `domain_job_role` varchar(100) DEFAULT NULL,
  `toa_status` varchar(100) DEFAULT NULL,
  `landmark` varchar(500) DEFAULT NULL,
  `pincode` varchar(255) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `education` varchar(500) DEFAULT NULL,
  `industrial_experience` varchar(500) DEFAULT NULL,
  `applicant_type` varchar(100) DEFAULT NULL,
  `profile_img` varchar(150) DEFAULT NULL,
  `availability` varchar(100) DEFAULT NULL,
  `added_by` varchar(500) DEFAULT NULL,
  `created_at` varchar(500) NOT NULL,
  `updated_at` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessor`
--

INSERT INTO `assessor` (`id`, `assessor_id`, `name`, `image`, `gender`, `religion`, `date_of_birth`, `language_known`, `phone`, `email`, `address`, `assessment_agency_aligned`, `linking_type`, `sectors`, `duration_fdt`, `duration_tdt`, `supdoc`, `domain_job_role`, `toa_status`, `landmark`, `pincode`, `state`, `city`, `education`, `industrial_experience`, `applicant_type`, `profile_img`, `availability`, `added_by`, `created_at`, `updated_at`) VALUES
(130, '105', 'Test Assessor Name', '899998789798-10567676776688.jpeg', NULL, NULL, NULL, NULL, '7896541230', 'sayandeep.tal@gmail.com', NULL, 'Growell Skills Hub', 'On Role', 'AGRICULTURE SKILL COUNCIL OF INDIA', '2020-01-01', '2021-01-01', 'default.png', 'Science & Arts', 'Provisional', NULL, NULL, 'West Bengal', NULL, NULL, NULL, NULL, 'default.png', NULL, '2', '2020-06-27 12:20:12', '2020-07-10 22:34:47'),
(133, '404', 'say', NULL, NULL, NULL, NULL, NULL, '7896321456', 'habjsbh@dg.vom', NULL, 'Growell Skills Hub', 'Freelancer', 'AUTOMOTIVE SKILLS DEVELOPMENT COUNCIL', '2019-09-29', '2021-03-30', 'default.png', 'Science & Arts', 'Certified', NULL, NULL, 'West Bengal', NULL, NULL, NULL, NULL, 'default.png', NULL, '2', '2020-07-09 02:01:29', '2020-07-09 02:01:29');

-- --------------------------------------------------------

--
-- Table structure for table `assessor_history`
--

CREATE TABLE `assessor_history` (
  `id` int(11) NOT NULL,
  `old_assessor_id` varchar(100) NOT NULL,
  `new_assessor_id` varchar(100) NOT NULL,
  `batch_id` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assessor_img_test`
--

CREATE TABLE `assessor_img_test` (
  `id` int(11) NOT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `assessor_id` varchar(100) DEFAULT NULL,
  `student_id` varchar(100) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessor_img_test`
--

INSERT INTO `assessor_img_test` (`id`, `batch_id`, `assessor_id`, `student_id`, `img`, `updated_at`, `created_at`) VALUES
(1, '7', '105', '888888', '7-105.jpeg', '2020-07-06 18:02:17', '2020-07-06 18:02:17'),
(2, '7', '105', '151512000', '7-105.jpeg', '2020-07-06 18:04:21', '2020-07-06 18:04:21'),
(3, '9', '105', '999', '9-105999.jpeg', '2020-07-06 18:17:19', '2020-07-06 18:17:19'),
(4, '9', '105', '666', '9-105666.jpeg', '2020-07-06 18:25:14', '2020-07-06 18:25:14'),
(5, '666', '105', '199903', '666-105199903.jpeg', '2020-07-07 12:45:40', '2020-07-07 12:45:40'),
(6, '999', '105', '44444', '999-10544444.jpeg', '2020-07-07 14:34:44', '2020-07-07 14:34:44'),
(7, '666', '105', '199903', '666-105199903.jpeg', '2020-07-07 16:44:27', '2020-07-07 16:44:27'),
(8, '999', '105', '888888', '999-105888888.jpeg', '2020-07-08 17:15:32', '2020-07-08 17:15:32'),
(9, '999', '105', '888888', '999-105888888.jpeg', '2020-07-09 02:14:21', '2020-07-09 02:14:21'),
(10, '420', '105', '3520', '420-1053520.jpeg', '2020-07-09 11:54:30', '2020-07-09 11:54:30'),
(11, '999', '105', '888888', '999-105888888.jpeg', '2020-07-09 13:40:06', '2020-07-09 13:40:06'),
(12, '420', '105', '3520', '420-1053520.jpeg', '2020-07-09 14:14:44', '2020-07-09 14:14:44'),
(13, '999', '105', '151512000', '999-105151512000.jpeg', '2020-07-09 14:32:22', '2020-07-09 14:32:22'),
(14, '999', '105', '3', '999-1053.jpeg', '2020-07-09 14:43:25', '2020-07-09 14:43:25'),
(15, '999', '105', '3', '999-1053.jpeg', '2020-07-09 14:49:21', '2020-07-09 14:49:21'),
(16, '8787878', '105', '121212121212121212', '8787878-105121212121212121212.jpeg', '2020-07-09 15:07:01', '2020-07-09 15:07:01'),
(17, '12121212121', '105', '151041033020', '12121212121-105151041033020.jpeg', '2020-07-10 18:05:02', '2020-07-10 18:05:02'),
(18, '12121212121', '105', '151041033020', '12121212121-105151041033020.jpeg', '2020-07-10 18:22:18', '2020-07-10 18:22:18'),
(19, '12121212121', '105', '151041033020', '12121212121-105151041033020.jpeg', '2020-07-10 18:24:33', '2020-07-10 18:24:33'),
(20, '12121212121', '105', '151041033020', '12121212121-105151041033020.jpeg', '2020-07-10 18:29:34', '2020-07-10 18:29:34'),
(21, '12121212121', '105', '151041033020', '12121212121-105151041033020.jpeg', '2020-07-10 18:31:06', '2020-07-10 18:31:06'),
(22, '12121212121', '105', '151041033020', '12121212121-105151041033020.jpeg', '2020-07-10 18:31:47', '2020-07-10 18:31:47'),
(23, '899998789798', '105', '67676776688', '899998789798-10567676776688.jpeg', '2020-07-10 19:36:36', '2020-07-10 19:36:36'),
(24, '899998789798', '105', '67676776688', '899998789798-10567676776688.jpeg', '2020-07-10 19:39:43', '2020-07-10 19:39:43'),
(25, '899998789798', '105', '67676776688', '899998789798-10567676776688.jpeg', '2020-07-10 22:34:46', '2020-07-10 22:34:46'),
(26, '899998789798', '105', '67676776688', '899998789798-10567676776688.jpeg', '2020-07-10 22:34:47', '2020-07-10 22:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `assessor_login`
--

CREATE TABLE `assessor_login` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessor_login`
--

INSERT INTO `assessor_login` (`id`, `username`, `password`, `status`, `created_at`, `updated_at`) VALUES
(129, '105', '$2y$10$RAiwICICg4kpu7J9t73.GO89Y.eTOMhaheEdwJw2mTMpBdbHywEIy', 0, '2020-06-27 12:20:12', '2020-07-03 00:34:31'),
(132, '404', '$2y$10$1Q9h.JyKCWrfJVird4HCj..AlKpvbpYV006JihX/tsXEOD/bJtjUq', 0, '2020-07-09 02:01:29', '2020-07-09 02:01:29');

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `batch_id` varchar(150) NOT NULL,
  `batch_name` varchar(200) DEFAULT NULL,
  `schemeProgram` varchar(150) DEFAULT NULL,
  `jobRole` varchar(150) DEFAULT NULL,
  `sectors` varchar(150) DEFAULT NULL,
  `batchTypes` varchar(150) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `training_center_location` varchar(150) DEFAULT NULL,
  `batch_size` varchar(100) DEFAULT NULL,
  `spoc_name` varchar(100) DEFAULT NULL,
  `spoc_mobile_no` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `assessment_date_fdt` varchar(100) DEFAULT NULL,
  `assessment_date_tdt` varchar(100) DEFAULT NULL,
  `action_date` varchar(100) DEFAULT NULL,
  `fduration` varchar(100) DEFAULT NULL,
  `tduration` varchar(100) DEFAULT NULL,
  `test_duration` varchar(100) DEFAULT NULL,
  `received_from_ssc` varchar(100) DEFAULT NULL,
  `states` varchar(100) DEFAULT NULL,
  `student_attendance` varchar(100) DEFAULT NULL,
  `passed_percentage` varchar(100) DEFAULT NULL,
  `failed_percentage` varchar(100) DEFAULT NULL,
  `batch_status` tinyint(1) DEFAULT NULL,
  `test_status` varchar(15) DEFAULT NULL,
  `assessed_status` tinyint(1) NOT NULL DEFAULT 0,
  `docfile` varchar(200) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`batch_id`, `batch_name`, `schemeProgram`, `jobRole`, `sectors`, `batchTypes`, `language`, `training_center_location`, `batch_size`, `spoc_name`, `spoc_mobile_no`, `email`, `assessment_date_fdt`, `assessment_date_tdt`, `action_date`, `fduration`, `tduration`, `test_duration`, `received_from_ssc`, `states`, `student_attendance`, `passed_percentage`, `failed_percentage`, `batch_status`, `test_status`, `assessed_status`, `docfile`, `added_by`, `created_at`, `updated_at`) VALUES
('666', NULL, 'PMKVY SP CSCM', 'Textile Designer - Handloom Jacquard', 'FURNITURE AND FITTINGS SKILL COUNCIL', 'Regular', 'English,Bengali', 'Delhi', '10', 'SOME', '1593578520', 'sdsd@gmail.com', '2020-07-30', NULL, '2019-10-30', NULL, NULL, '75', '2021-03-03', 'Assam', NULL, NULL, NULL, 1, '1', 1, NULL, '2', '2020-07-07 11:55:49', '2020-07-07 17:59:34'),
('420', NULL, 'PMKVY SP CSCM', 'Textile Designer - Handloom Jacquard', 'CAPTIAL GOODS SKILL COUNCIL', 'Regular', 'English,Bengali', 'delhi', '15', 'SOME', '8017730774', 'smajumdar1993@gmail.com', '2020-07-30', NULL, '2020-07-21', NULL, NULL, '105', '2020-07-31', 'Delhi *', NULL, NULL, NULL, 1, '1', 0, NULL, '2', '2020-07-07 11:49:44', '2020-07-09 14:15:46'),
('153', NULL, 'PMKVY SP CSCM', 'Hank Dyer', 'Aerospace and Aviation Sector Skill Council', 'Regular', 'English,Bengali', 'Kolkata', '20', 'COMPany', '8017730775', 'smajumdar1993@gmail.com', '2020-07-09', NULL, '2020-07-21', NULL, NULL, '60', '2020-08-08', 'Kerala', NULL, NULL, NULL, 1, NULL, 0, NULL, '2', '2020-07-07 11:37:09', '2020-07-07 11:46:47'),
('999', NULL, 'PMKVY SP CSCM', 'Hank Dyer', 'Aerospace and Aviation Sector Skill Council', 'Regular', 'English,Bengali,Hindi', 'Kolkata', '15', 'njndksnd', '852985256', 'sdsd@gmail.com', '2020-07-15', NULL, '2020-07-27', NULL, NULL, '60', '2020-07-31', 'Gujarat', NULL, NULL, NULL, 1, '1', 0, NULL, '2', '2020-07-07 11:38:08', '2020-07-09 14:50:01'),
('153555555', 'New field', 'PMKVY SP CSCM', 'Two Shaft Handloom Weaver', 'Aerospace and Aviation Sector Skill Council', 'Regular', 'English,Bengali', 'Kolkata', '21', 'SOME COMPany', '8017730775', 'smajumdar1993@gmail.com', '2020-08-05', NULL, '2020-07-17', NULL, NULL, NULL, '2020-07-28', 'Andaman & Nicobar Islands *', NULL, NULL, NULL, 0, NULL, 0, NULL, '2', '2020-07-07 19:18:04', '2020-07-07 19:18:04'),
('12121212121', 'New', 'PMKVY SP CSCM', 'Jacquard Harness Builder', 'Aerospace and Aviation Sector Skill Council', 'Reassessment', 'English,Bengali,Hindi', 'Kolkata', '6', 'SOME COMPany', NULL, NULL, '2022-02-03', NULL, '2019-09-29', '2018-09-29', '2021-03-03', '15', '2021-03-03', 'Assam', NULL, NULL, NULL, 1, '1', 0, NULL, '2', '2020-07-09 12:26:17', '2020-07-10 18:31:51'),
('8787878', 'kajnsjk', 'PMKVY SP CSCM', 'warper', 'APPERAL MADE -UPS & HOME FURNISHING SECTOR SKILL COUNICL', 'Regular', 'English,Bengali,Hindi', 'Kolkata', '8', 'SOME COMPany', '0565656566', '1993msayandeep@gmail.com', '2020-07-24', NULL, '2018-10-28', '2023-03-04', '2027-01-03', '15', '2022-05-29', 'Assam', NULL, NULL, NULL, 1, '1', 0, NULL, '2', '2020-07-09 15:03:35', '2020-07-10 00:38:38'),
('15333351', 'polo', 'PMKVY SP CSCM', 'Two Shaft Handloom Weaver', 'BSFI SECTOR SKILL COUNCIL OF INDIA', 'Regular', 'English,Bengali,Hindi', 'bkbhjb', '6', 'M1', '13566223232', 'msayandeep@gmail.kol', '2021-03-01', NULL, '2021-02-04', '2021-10-29', '2021-03-03', NULL, '2023-03-03', 'West Bengal', NULL, NULL, NULL, 0, NULL, 0, NULL, '2', '2020-07-10 00:49:53', '2020-07-10 00:49:53'),
('973656565', 'kajnsjksjknskjnksd', 'PMKVY SP CSCM', 'Textile Designer - Handloom Jacquard', 'DOMESTIC WORKERS SECTOR SKILL COUNCIL', 'Regular', 'English,Bengali,Hindi', 'Kolkata', '7', 'SOME', '15935785622', 'sdsd@gmail.com', '2022-03-26', NULL, '2022-02-04', '2018-09-25', '2023-03-06', NULL, '2024-03-04', 'Jharkhand', NULL, NULL, NULL, 0, NULL, 0, NULL, '2', '2020-07-10 11:53:19', '2020-07-10 11:53:19'),
('55554545222222', 'sad', 'PMKVY SP CSCM', 'Jacquard weaver - Handloom', 'BSFI SECTOR SKILL COUNCIL OF INDIA', 'Reassessment', 'English,Bengali,Hindi', 'Kolkata', '7', 'SOME COMPany', '251565616165', 'habjsbh@dg.vom', '2025-03-05', NULL, '2017-08-26', '2021-10-29', '2021-03-30', NULL, '2021-03-03', 'Chandigarh *', NULL, NULL, NULL, 0, NULL, 0, '55554545222222-sad.docx', '2', '2020-07-10 11:54:14', '2020-07-10 11:54:14'),
('899998789798', 'jgjhhjbh hb', 'PMKVY SP CSCM', 'Hank Dyer', 'DOMESTIC WORKERS SECTOR SKILL COUNCIL', 'Regular', 'English,Bengali', 'Kolkata', '14', 'SOME', '646886454', 'habjsbh@dg.vom', '2022-04-03', NULL, '2020-11-29', '2021-10-28', '2022-03-04', '15', '2021-03-03', 'Chandigarh *', NULL, NULL, NULL, 1, '1', 0, NULL, '2', '2020-07-10 18:46:53', '2020-07-10 22:45:08');

-- --------------------------------------------------------

--
-- Table structure for table `batch_history`
--

CREATE TABLE `batch_history` (
  `id` int(11) NOT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `old_date` varchar(100) DEFAULT NULL,
  `new_date` varchar(100) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_history`
--

INSERT INTO `batch_history` (`id`, `batch_id`, `old_date`, `new_date`, `reason`, `change_by`, `created_at`, `updated_at`) VALUES
(4, '8787878', '2021-03-04', '2020-07-24', 'emni', '2', '2020-07-10 00:38:38', '2020-07-10 00:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `batch_process`
--

CREATE TABLE `batch_process` (
  `id` int(11) NOT NULL,
  `batch_id` varchar(100) NOT NULL,
  `assessor_id` varchar(100) NOT NULL,
  `assessor_img` varchar(100) DEFAULT NULL,
  `date` varchar(100) NOT NULL,
  `req_status` varchar(10) DEFAULT NULL,
  `assessed_req_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_process`
--

INSERT INTO `batch_process` (`id`, `batch_id`, `assessor_id`, `assessor_img`, `date`, `req_status`, `assessed_req_status`, `created_at`, `updated_at`) VALUES
(27, '8787878', '105', NULL, '2020-07-18', '1', 2, '2020-07-09 15:04:29', '2020-07-10 13:29:46'),
(26, '12121212121', '105', NULL, '2020-07-11', '1', 2, '2020-07-09 14:53:17', '2020-07-10 13:20:59'),
(25, '666', '105', NULL, '2020-07-31', '1', 2, '2020-07-07 11:57:11', '2020-07-09 11:48:51'),
(24, '420', '105', NULL, '2020-07-23', '1', 2, '2020-07-07 11:51:27', '2020-07-09 14:17:50'),
(23, '153', '105', NULL, '2020-07-22', '2', 0, '2020-07-07 11:46:47', '2020-07-07 11:47:43'),
(22, '999', '105', NULL, '2020-07-20', '1', 2, '2020-07-07 11:45:39', '2020-07-09 14:31:17'),
(28, '899998789798', '105', NULL, '2020-07-17', '1', 0, '2020-07-10 18:49:12', '2020-07-10 18:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `batch_review_comments`
--

CREATE TABLE `batch_review_comments` (
  `id` int(11) NOT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_review_comments`
--

INSERT INTO `batch_review_comments` (`id`, `batch_id`, `comment`, `created_at`, `updated_at`) VALUES
(7, '666', 'updated finalized', '2020-07-07 17:12:09', '2020-07-07 17:59:34');

-- --------------------------------------------------------

--
-- Table structure for table `batch_type`
--

CREATE TABLE `batch_type` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batch_type`
--

INSERT INTO `batch_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Regular', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Reassessment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `job_role`
--

CREATE TABLE `job_role` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_role`
--

INSERT INTO `job_role` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Hank Dyer', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Jacquard Harness Builder', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Jacquard weaver - Handloom', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Textile Designer - Handloom Jacquard', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Two Shaft Handloom Weaver', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'warper', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` varchar(100) NOT NULL,
  `question` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option1` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option2` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option3` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option4` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `right_answer` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `question_hindi` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option1_hindi` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option2_hindi` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option3_hindi` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option4_hindi` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `right_ans_hindi` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `question_teleugu` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option1_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option2_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option3_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option4_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `right_ans_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `question_assam` varchar(500) CHARACTER SET utf8 DEFAULT 'NA',
  `option1_assam` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option2_assam` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option3_assam` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `option4_assam` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `right_ans_assam` varchar(200) CHARACTER SET utf8 DEFAULT 'NA',
  `job_role` varchar(500) NOT NULL,
  `section` varchar(155) DEFAULT NULL,
  `marks` varchar(100) NOT NULL DEFAULT '1',
  `added_by` varchar(500) DEFAULT NULL,
  `created_at` varchar(500) NOT NULL,
  `updated_at` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `option1`, `option2`, `option3`, `option4`, `right_answer`, `question_hindi`, `option1_hindi`, `option2_hindi`, `option3_hindi`, `option4_hindi`, `right_ans_hindi`, `question_teleugu`, `option1_teleugu`, `option2_teleugu`, `option3_teleugu`, `option4_teleugu`, `right_ans_teleugu`, `question_assam`, `option1_assam`, `option2_assam`, `option3_assam`, `option4_assam`, `right_ans_assam`, `job_role`, `section`, `marks`, `added_by`, `created_at`, `updated_at`) VALUES
('onenjksnjkdn', 'kjn', 'nj', 'nn', 'nn', 'kjnj', 'nn', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Jacquard Harness Builder', 'math', '1', 'admin', '2020-07-09 15:02:10', '2020-07-09 15:02:10'),
('16', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'new sec', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('15', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'hola', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('45', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'statistic', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('44', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'programming', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('43', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'history', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('42', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'geography', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('41', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'english', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('40', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'math', '1', '2', '2020-07-08 11:47:31', '2020-07-08 11:47:31'),
('35', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1', '2', '2020-07-08 11:44:47', '2020-07-08 11:44:47'),
('34', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1', '2', '2020-07-08 11:44:47', '2020-07-08 11:44:47'),
('33', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1', '2', '2020-07-08 11:44:47', '2020-07-08 11:44:47'),
('30', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1', '2', '2020-07-08 11:44:47', '2020-07-08 11:44:47'),
('31', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1', '2', '2020-07-08 11:44:47', '2020-07-08 11:44:47'),
('32', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1', '2', '2020-07-08 11:44:47', '2020-07-08 11:44:47'),
('20', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Jacquard weaver - Handloom', 'history', '1', '2', '2020-07-08 11:40:55', '2020-07-08 11:40:55'),
('21', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Jacquard weaver - Handloom', 'programming', '1', '2', '2020-07-08 11:40:55', '2020-07-08 11:40:55'),
('22', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Jacquard weaver - Handloom', 'statistic', '1', '2', '2020-07-08 11:40:55', '2020-07-08 11:40:55'),
('14', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'statistic', '1', '2', '2020-07-08 11:36:53', '2020-07-08 11:36:53'),
('17', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Jacquard weaver - Handloom', 'math', '1', '2', '2020-07-08 11:40:55', '2020-07-08 11:40:55'),
('18', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Jacquard weaver - Handloom', 'english', '1', '2', '2020-07-08 11:40:55', '2020-07-08 11:40:55'),
('19', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Jacquard weaver - Handloom', 'geography', '1', '2', '2020-07-08 11:40:55', '2020-07-08 11:40:55'),
('12', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'history', '1', '2', '2020-07-08 11:36:53', '2020-07-08 11:36:53'),
('13', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'programming', '1', '2', '2020-07-08 11:36:53', '2020-07-08 11:36:53'),
('9', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'math', '1', '2', '2020-07-08 11:36:53', '2020-07-08 11:36:53'),
('10', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'english', '1', '2', '2020-07-08 11:36:53', '2020-07-08 11:36:53'),
('11', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'geography', '1', '2', '2020-07-08 11:36:53', '2020-07-08 11:36:53'),
('8', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'srk', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('7', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'helo', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('6', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'statistic', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('5', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'programming', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('4', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'history', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('3', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'geography', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('2', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'english', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('1', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Two Shaft Handloom Weaver', 'math', '1', '2', '2020-07-08 11:27:05', '2020-07-08 11:27:05'),
('3343441', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'srk', '1', '2', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
('99999', 'jksksbdkj', 'ram', 'as', 'nkjn', 'knk', 'knk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Textile Designer - Handloom Jacquard', 'programming', '1', 'admin', '2020-07-07 11:58:32', '2020-07-07 11:58:32'),
('353333333', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'helo', '1', '2', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
('24454546', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1', '2', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
('7451222', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1', '2', '2020-07-06 14:40:19', '2020-07-06 14:40:19'),
('1112212', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1', '2', '2020-07-06 14:40:19', '2020-07-06 14:40:19'),
('1212121', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1', '2', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
('23312323', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1', '2', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
('133434', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1', '2', '2020-07-06 14:40:20', '2020-07-06 14:40:20');

-- --------------------------------------------------------

--
-- Table structure for table `question_map_transaction`
--

CREATE TABLE `question_map_transaction` (
  `id` int(11) NOT NULL,
  `q_id` varchar(100) DEFAULT NULL,
  `marks` varchar(100) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `job_role` varchar(100) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_map_transaction`
--

INSERT INTO `question_map_transaction` (`id`, `q_id`, `marks`, `section`, `job_role`, `batch_id`, `created_at`, `updated_at`) VALUES
(48, '3343441', '1', 'srk', 'Hank Dyer', '999', '2020-07-07 11:47:21', '2020-07-07 11:47:21'),
(49, '353333333', '1', 'helo', 'Hank Dyer', '999', '2020-07-07 11:47:22', '2020-07-07 11:47:22'),
(50, '24454546', '1', 'statistic', 'Hank Dyer', '999', '2020-07-07 11:47:23', '2020-07-07 11:47:23'),
(51, '7451222', '1', 'math', 'Hank Dyer', '999', '2020-07-07 11:47:23', '2020-07-07 11:47:23'),
(52, '1112212', '1', 'english', 'Hank Dyer', '999', '2020-07-07 11:47:24', '2020-07-07 11:47:24'),
(53, '1212121', '1', 'geography', 'Hank Dyer', '999', '2020-07-07 11:47:25', '2020-07-07 11:47:25'),
(54, '23312323', '1', 'history', 'Hank Dyer', '999', '2020-07-07 11:47:25', '2020-07-07 11:47:25'),
(55, '133434', '1', 'programming', 'Hank Dyer', '999', '2020-07-07 11:47:26', '2020-07-07 11:47:26'),
(56, '99999', '1', 'programming', 'Textile Designer - Handloom Jacquard', '666', '2020-07-07 11:58:41', '2020-07-07 11:58:41'),
(57, '16', '1', 'new sec', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:13', '2020-07-09 11:46:13'),
(58, '15', '1', 'hola', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:13', '2020-07-09 11:46:13'),
(59, '45', '1', 'statistic', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:14', '2020-07-09 11:46:14'),
(60, '44', '1', 'programming', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:14', '2020-07-09 11:46:14'),
(61, '43', '1', 'history', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:15', '2020-07-09 11:46:15'),
(62, '42', '1', 'geography', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:16', '2020-07-09 11:46:16'),
(63, '41', '1', 'english', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:16', '2020-07-09 11:46:16'),
(64, '40', '1', 'math', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:16', '2020-07-09 11:46:16'),
(65, '99999', '1', 'programming', 'Textile Designer - Handloom Jacquard', '420', '2020-07-09 11:46:17', '2020-07-09 11:46:17'),
(66, '14', '1', 'statistic', 'warper', '8787878', '2020-07-09 15:04:59', '2020-07-09 15:04:59'),
(67, '12', '1', 'history', 'warper', '8787878', '2020-07-09 15:04:59', '2020-07-09 15:04:59'),
(68, '13', '1', 'programming', 'warper', '8787878', '2020-07-09 15:04:59', '2020-07-09 15:04:59'),
(69, '9', '1', 'math', 'warper', '8787878', '2020-07-09 15:05:00', '2020-07-09 15:05:00'),
(70, '10', '1', 'english', 'warper', '8787878', '2020-07-09 15:05:00', '2020-07-09 15:05:00'),
(71, '11', '1', 'geography', 'warper', '8787878', '2020-07-09 15:05:00', '2020-07-09 15:05:00'),
(72, '35', '1', 'statistic', 'Hank Dyer', '899998789798', '2020-07-10 18:49:39', '2020-07-10 18:49:39'),
(73, '34', '1', 'programming', 'Hank Dyer', '899998789798', '2020-07-10 18:49:39', '2020-07-10 18:49:39'),
(74, '33', '1', 'history', 'Hank Dyer', '899998789798', '2020-07-10 18:49:39', '2020-07-10 18:49:39'),
(75, '30', '1', 'math', 'Hank Dyer', '899998789798', '2020-07-10 18:49:39', '2020-07-10 18:49:39'),
(76, '31', '1', 'english', 'Hank Dyer', '899998789798', '2020-07-10 18:49:40', '2020-07-10 18:49:40'),
(77, '32', '1', 'geography', 'Hank Dyer', '899998789798', '2020-07-10 18:49:40', '2020-07-10 18:49:40'),
(78, '3343441', '1', 'srk', 'Hank Dyer', '899998789798', '2020-07-10 18:49:40', '2020-07-10 18:49:40'),
(79, '353333333', '1', 'helo', 'Hank Dyer', '899998789798', '2020-07-10 18:49:41', '2020-07-10 18:49:41'),
(80, '24454546', '1', 'statistic', 'Hank Dyer', '899998789798', '2020-07-10 18:49:41', '2020-07-10 18:49:41'),
(81, '7451222', '1', 'math', 'Hank Dyer', '899998789798', '2020-07-10 18:49:41', '2020-07-10 18:49:41'),
(82, '1112212', '1', 'english', 'Hank Dyer', '899998789798', '2020-07-10 18:49:41', '2020-07-10 18:49:41'),
(83, '1212121', '1', 'geography', 'Hank Dyer', '899998789798', '2020-07-10 18:49:41', '2020-07-10 18:49:41'),
(84, '23312323', '1', 'history', 'Hank Dyer', '899998789798', '2020-07-10 18:49:42', '2020-07-10 18:49:42'),
(85, '133434', '1', 'programming', 'Hank Dyer', '899998789798', '2020-07-10 18:49:42', '2020-07-10 18:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `question_transaction`
--

CREATE TABLE `question_transaction` (
  `id` int(11) NOT NULL,
  `q_id` bigint(10) DEFAULT NULL,
  `answer` varchar(100) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `s_id` varchar(100) DEFAULT NULL,
  `section_id` varchar(100) DEFAULT NULL,
  `attempt_status` int(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `question` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option1` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option2` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option3` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option4` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `right_answer` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `question_hindi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option1_hindi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option2_hindi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option3_hindi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option4_hindi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `right_ans_hindi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `question_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option1_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option2_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option3_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option4_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `right_ans_teleugu` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `question_assam` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option1_assam` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option2_assam` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option3_assam` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `option4_assam` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `right_ans_assam` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `job_role` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `section` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ques_marks` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `question_transaction`
--

INSERT INTO `question_transaction` (`id`, `q_id`, `answer`, `batch_id`, `s_id`, `section_id`, `attempt_status`, `created_at`, `updated_at`, `question`, `option1`, `option2`, `option3`, `option4`, `right_answer`, `question_hindi`, `option1_hindi`, `option2_hindi`, `option3_hindi`, `option4_hindi`, `right_ans_hindi`, `question_teleugu`, `option1_teleugu`, `option2_teleugu`, `option3_teleugu`, `option4_teleugu`, `right_ans_teleugu`, `question_assam`, `option1_assam`, `option2_assam`, `option3_assam`, `option4_assam`, `right_ans_assam`, `job_role`, `section`, `ques_marks`) VALUES
(292, 3343441, 'jkjk', '999', '44444', 'srk', 0, '2020-07-07 11:47:20', '2020-07-09 13:07:37', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'srk', '1'),
(293, 3343441, 'jkjk', '999', '888888', 'srk', 0, '2020-07-07 11:47:20', '2020-07-09 13:50:44', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'srk', '1'),
(294, 3343441, 'jkj', '999', '151512000', 'srk', 0, '2020-07-07 11:47:21', '2020-07-09 14:32:27', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'srk', '1'),
(295, 3343441, 'jkjkjk', '999', '3', 'srk', 0, '2020-07-07 11:47:21', '2020-07-09 14:49:25', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'srk', '1'),
(296, 353333333, 'jkjk', '999', '44444', 'helo', 0, '2020-07-07 11:47:21', '2020-07-07 14:34:57', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'helo', '1'),
(297, 353333333, 'jkjk', '999', '888888', 'helo', 0, '2020-07-07 11:47:21', '2020-07-09 13:50:38', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'helo', '1'),
(298, 353333333, 'jkj', '999', '151512000', 'helo', 0, '2020-07-07 11:47:21', '2020-07-09 14:32:33', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'helo', '1'),
(299, 353333333, 'jkjkjk', '999', '3', 'helo', 0, '2020-07-07 11:47:21', '2020-07-09 14:49:29', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'helo', '1'),
(300, 24454546, 'jkjk', '999', '44444', 'statistic', 0, '2020-07-07 11:47:22', '2020-07-09 13:27:57', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1'),
(301, 24454546, 'jkjk', '999', '888888', 'statistic', 0, '2020-07-07 11:47:22', '2020-07-09 13:45:04', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1'),
(302, 24454546, 'jkjk', '999', '151512000', 'statistic', 0, '2020-07-07 11:47:22', '2020-07-09 14:32:42', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1'),
(303, 24454546, 'jkjkjk', '999', '3', 'statistic', 0, '2020-07-07 11:47:23', '2020-07-09 14:49:35', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1'),
(304, 7451222, 'one', '999', '44444', 'math', 0, '2020-07-07 11:47:23', '2020-07-09 13:27:50', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1'),
(305, 7451222, 'one', '999', '888888', 'math', 0, '2020-07-07 11:47:23', '2020-07-09 13:50:29', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1'),
(306, 7451222, 'one', '999', '151512000', 'math', 0, '2020-07-07 11:47:23', '2020-07-09 14:33:58', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1'),
(307, 7451222, 'two', '999', '3', 'math', 0, '2020-07-07 11:47:23', '2020-07-09 14:49:38', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1'),
(308, 1112212, 'v', '999', '44444', 'english', 0, '2020-07-07 11:47:23', '2020-07-09 13:27:53', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1'),
(309, 1112212, NULL, '999', '888888', 'english', 0, '2020-07-07 11:47:24', '2020-07-07 11:47:24', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1'),
(310, 1112212, 'v', '999', '151512000', 'english', 0, '2020-07-07 11:47:24', '2020-07-09 14:33:48', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1'),
(311, 1112212, 'jkj', '999', '3', 'english', 0, '2020-07-07 11:47:24', '2020-07-09 14:49:42', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1'),
(312, 1212121, 'jkj', '999', '44444', 'geography', 0, '2020-07-07 11:47:24', '2020-07-09 13:27:48', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1'),
(313, 1212121, 'jkj', '999', '888888', 'geography', 0, '2020-07-07 11:47:24', '2020-07-09 13:50:23', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1'),
(314, 1212121, 'jkj', '999', '151512000', 'geography', 0, '2020-07-07 11:47:24', '2020-07-09 14:33:54', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1'),
(315, 1212121, 'jkjk', '999', '3', 'geography', 0, '2020-07-07 11:47:24', '2020-07-09 14:49:45', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1'),
(316, 23312323, 'jkjk', '999', '44444', 'history', 0, '2020-07-07 11:47:25', '2020-07-09 13:29:59', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1'),
(317, 23312323, 'jkjk', '999', '888888', 'history', 0, '2020-07-07 11:47:25', '2020-07-09 13:45:14', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1'),
(318, 23312323, 'jkjk', '999', '151512000', 'history', 0, '2020-07-07 11:47:25', '2020-07-09 14:33:36', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1'),
(319, 23312323, 'jkj', '999', '3', 'history', 0, '2020-07-07 11:47:25', '2020-07-09 14:49:49', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1'),
(320, 133434, 'jkjk', '999', '44444', 'programming', 0, '2020-07-07 11:47:25', '2020-07-09 13:30:02', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1'),
(321, 133434, NULL, '999', '888888', 'programming', 0, '2020-07-07 11:47:25', '2020-07-07 11:47:25', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1'),
(322, 133434, 'jkjk', '999', '151512000', 'programming', 0, '2020-07-07 11:47:25', '2020-07-09 14:33:42', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1'),
(323, 133434, 's', '999', '3', 'programming', 0, '2020-07-07 11:47:25', '2020-07-09 14:49:53', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1'),
(324, 99999, 'knk', '666', '199903', 'programming', 0, '2020-07-07 11:58:41', '2020-07-09 14:12:40', 'jksksbdkj', 'ram', 'as', 'nkjn', 'knk', 'knk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Textile Designer - Handloom Jacquard', 'programming', '1'),
(325, 16, 'jkjk', '420', '3520', 'new sec', 0, '2020-07-09 11:46:13', '2020-07-09 14:15:33', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'new sec', '1'),
(326, 15, 'jkjk', '420', '3520', 'hola', 0, '2020-07-09 11:46:13', '2020-07-09 14:17:05', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'hola', '1'),
(327, 45, 'jkjk', '420', '3520', 'statistic', 0, '2020-07-09 11:46:14', '2020-07-09 14:15:25', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'statistic', '1'),
(328, 44, 'jkjk', '420', '3520', 'programming', 0, '2020-07-09 11:46:14', '2020-07-09 14:16:57', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'programming', '1'),
(329, 43, 'jkjk', '420', '3520', 'history', 0, '2020-07-09 11:46:15', '2020-07-09 14:16:34', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'history', '1'),
(330, 42, 'jkj', '420', '3520', 'geography', 0, '2020-07-09 11:46:15', '2020-07-09 14:16:46', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'geography', '1'),
(331, 41, 'v', '420', '3520', 'english', 0, '2020-07-09 11:46:16', '2020-07-09 14:16:25', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'english', '1'),
(332, 40, 'one', '420', '3520', 'math', 0, '2020-07-09 11:46:16', '2020-07-09 14:15:14', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Textile Designer - Handloom Jacquard', 'math', '1'),
(333, 99999, 'knk', '420', '3520', 'programming', 0, '2020-07-09 11:46:17', '2020-07-09 14:16:19', 'jksksbdkj', 'ram', 'as', 'nkjn', 'knk', 'knk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Textile Designer - Handloom Jacquard', 'programming', '1'),
(334, 14, 'jkjkjk', '8787878', '121212121212121212', 'statistic', 0, '2020-07-09 15:04:59', '2020-07-09 16:01:50', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'statistic', '1'),
(335, 12, 'jkj', '8787878', '121212121212121212', 'history', 0, '2020-07-09 15:04:59', '2020-07-09 16:02:04', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'history', '1'),
(336, 13, 'jkj', '8787878', '121212121212121212', 'programming', 0, '2020-07-09 15:04:59', '2020-07-09 16:02:13', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'programming', '1'),
(337, 9, 'four', '8787878', '121212121212121212', 'math', 0, '2020-07-09 15:04:59', '2020-07-09 16:02:18', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'math', '1'),
(338, 10, 'jkjk', '8787878', '121212121212121212', 'english', 0, '2020-07-09 15:05:00', '2020-07-09 16:02:25', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'english', '1'),
(339, 11, 'jkjkjk', '8787878', '121212121212121212', 'geography', 0, '2020-07-09 15:05:00', '2020-07-09 16:02:29', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'warper', 'geography', '1'),
(340, 35, 'jkjkjk', '899998789798', '67676776688', 'statistic', 0, '2020-07-10 18:49:39', '2020-07-10 22:45:12', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1'),
(341, 34, 'jkj', '899998789798', '67676776688', 'programming', 0, '2020-07-10 18:49:39', '2020-07-10 22:42:46', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1'),
(342, 33, 'jkjkjk', '899998789798', '67676776688', 'history', 0, '2020-07-10 18:49:39', '2020-07-10 22:35:52', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1'),
(343, 30, 'one', '899998789798', '67676776688', 'math', 0, '2020-07-10 18:49:39', '2020-07-10 22:36:05', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1'),
(344, 31, 'jkjk', '899998789798', '67676776688', 'english', 0, '2020-07-10 18:49:40', '2020-07-10 22:35:38', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1'),
(345, 32, 'jkjkjk', '899998789798', '67676776688', 'geography', 0, '2020-07-10 18:49:40', '2020-07-10 22:35:25', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1'),
(346, 3343441, 'jkj', '899998789798', '67676776688', 'srk', 0, '2020-07-10 18:49:40', '2020-07-10 22:35:56', 'Demo Question 3', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'srk', '1'),
(347, 353333333, 'jkjkjk', '899998789798', '67676776688', 'helo', 0, '2020-07-10 18:49:40', '2020-07-10 22:36:01', 'Demo Question 2', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'helo', '1'),
(348, 24454546, 'jkjk', '899998789798', '67676776688', 'statistic', 0, '2020-07-10 18:49:41', '2020-07-10 22:35:31', 'c', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'statistic', '1'),
(349, 7451222, 'three', '899998789798', '67676776688', 'math', 0, '2020-07-10 18:49:41', '2020-07-10 22:35:42', 's', 'one', 'two', 'three', 'four', 'one', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'math', '1'),
(350, 1112212, 'jkj', '899998789798', '67676776688', 'english', 0, '2020-07-10 18:49:41', '2020-07-10 22:35:48', 'Next Question?', 'v', 'jkj', 'jkjk', 'jj', 'v', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'english', '1'),
(351, 1212121, 'jkj', '899998789798', '67676776688', 'geography', 0, '2020-07-10 18:49:41', '2020-07-10 22:36:08', 'what is in the Question?', 'jkjkjk', 'jkj', 'jkjk', 'jj', 'jkj', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'geography', '1'),
(352, 23312323, 'edit done', '899998789798', '67676776688', 'history', 0, '2020-07-10 18:49:41', '2020-07-10 22:35:45', 'Demo Question3e', 'jkjkjk', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'history', '1'),
(353, 133434, 's', '899998789798', '67676776688', 'programming', 0, '2020-07-10 18:49:42', '2020-07-10 22:35:22', 'Demo Question12', 's', 'jkj', 'jkjk', 'edit done', 'jkjk', ' जन गण जन गणजन गणज', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Hank Dyer', 'programming', '1');

-- --------------------------------------------------------

--
-- Table structure for table `schema_model`
--

CREATE TABLE `schema_model` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schema_model`
--

INSERT INTO `schema_model` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'PMKVY SP CSCM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'PMKVY STT CSSM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'PMKVY SP CSCM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'PMKVY STT CSSM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'PMKVY RPL', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `created_at`, `updated_at`) VALUES
(36, 'math', '2020-07-06 14:38:14', '2020-07-06 14:38:14'),
(37, 'english', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(38, 'geography', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(39, 'history', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(40, 'programming', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(41, 'statistic', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(42, 'helo', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(43, 'srk', '2020-07-06 14:40:20', '2020-07-06 14:40:20'),
(44, 'hola', '2020-07-08 11:45:37', '2020-07-08 11:45:37'),
(45, 'new sec', '2020-07-08 11:45:45', '2020-07-08 11:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `section_wise_marks`
--

CREATE TABLE `section_wise_marks` (
  `id` int(11) NOT NULL,
  `section` varchar(100) DEFAULT NULL,
  `student_id` varchar(100) DEFAULT NULL,
  `marks` varchar(100) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sectors`
--

CREATE TABLE `sectors` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sectors`
--

INSERT INTO `sectors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Aerospace and Aviation Sector Skill Council', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'AGRICULTURE SKILL COUNCIL OF INDIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'APPERAL MADE -UPS & HOME FURNISHING SECTOR SKILL COUNICL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'AUTOMOTIVE SKILLS DEVELOPMENT COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'BEAUTY & WELLNESS SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'BSFI SECTOR SKILL COUNCIL OF INDIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'CAPTIAL GOODS SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'CONSTRUCTION SKILL DEVELOPMENT COUNCIL OF INDIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'DOMESTIC WORKERS SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'ELECTRONICS SECTOR SKILL COUNCIL OF INDIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'FOOD INDUSTRY CAPACITY & SKILL INTIATIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'FURNITURE AND FITTINGS SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'GEM & JEWELERY SKILL COUNCIL OF INDIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'HANDICRAFTS AND CARPET SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'HEALTH CARE SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'HYDROCARBON SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'INDIAN IRON AND STEEL SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'INDIAN PLUMBING SKILLS COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'INFRASTRUCTURE EQUIPMENT SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'IT -ITES SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'LEATHER SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'LIFE SCIENCE SECTOR SKILL DEVELOPMENT', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'LOGISTIC SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'MANAGEMENT & ENTREPENEURSHIP AND PROFFESSIONAL SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'MEDIA & ENTERTAINEMENT SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'PAINT AND COATING SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'POWER SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'RETAILERS ASSOCIATIONS SKILL COUNCIL OF INDIA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'RUBBER SKILL DEVELOPMENT COIUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'SKILL COUNCIL FOR GREEN JOBS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'SKILL COUNCIL FOR MINING SECTOR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'SKILL COUNCIL FOR PERSON WITH DISABILITY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'SPORTS ,PHYSICAL EDUCATION', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'FITNESS & LEASURE SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'STRATEGIC MANUFACTURING SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'TELECOM SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'TEXTILE SECTOR SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'TOURISM AND HOSPITALITY SKILL COUNCIL', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(10) NOT NULL,
  `username` varchar(150) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `batch_id` varchar(500) DEFAULT NULL,
  `dropout_status` tinyint(1) DEFAULT NULL,
  `spoc` varchar(500) DEFAULT NULL,
  `spoc_email` varchar(500) DEFAULT NULL,
  `spoc_phone` bigint(10) DEFAULT NULL,
  `recorded_video_name` varchar(100) DEFAULT NULL,
  `img_name` varchar(150) DEFAULT NULL,
  `schedule_test_status` tinyint(1) NOT NULL DEFAULT 0,
  `test_progress_status` tinyint(1) NOT NULL DEFAULT 0,
  `pass_fail_status` tinyint(1) NOT NULL DEFAULT 0,
  `marks` varchar(100) DEFAULT NULL,
  `added_by` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `username`, `email`, `name`, `address`, `batch_id`, `dropout_status`, `spoc`, `spoc_email`, `spoc_phone`, `recorded_video_name`, `img_name`, `schedule_test_status`, `test_progress_status`, `pass_fail_status`, `marks`, `added_by`, `created_at`, `updated_at`) VALUES
(56, '67676776688', 'habjsbh@dg.vom', 'Sayandeep Majumdar', 'B/13, Bapujinagar, Sulekha', '899998789798', 1, 'Majumdar', 'sayandeep.tal@gmail.com', 7896541230, '67676776688.webm', '67676776688.jpeg', 4, 0, 0, '3', '2', '2020-07-10 18:48:26', '2020-07-10 22:36:16'),
(55, '1202020202', 'sayandeepmajumdar.official@gmail.com', 'Sayandeep Majumdar', 'B/13, Bapujinagar, Sulekha', '15333351', 1, 'Majumdar', 'sdsd@gmail.com', 852985256, NULL, NULL, 0, 0, 0, NULL, '2', '2020-07-10 00:51:47', '2020-07-10 00:51:47'),
(54, '121212121212121212', 'sayandeep.tal@gmail.com', 'Sayandeep NA Majumdar', 'Garia', '8787878', 0, 'Majumdar', 'smajumdar1993@gmail.com', 8017730776, '121212121212121212.webm', '121212121212121212.jpeg', 4, 0, 0, '0', '2', '2020-07-09 15:04:11', '2020-07-10 13:14:41'),
(46, '44444', 'as2@sd.com', 'Deep', 'Garia', '999', 1, 'sdj', 'doc@gmail.com', 78965233, '44444.webm', '44444.jpeg', 4, 0, 0, '8', '2', '2020-07-07 11:42:19', '2020-07-09 13:30:02'),
(47, '888888', 'sdd@mg.cl', 'Rahul', 'Pune', '999', 0, 'jj', 'fol@gmail.com', 4528222, '888888.webm', '888888.jpeg', 4, 0, 0, '6', '2', '2020-07-07 11:42:19', '2020-07-09 13:50:44'),
(48, '151512000', 'sd@df.com', 'Sagar', 'Garia', '999', 0, 'Some', 'some@gmail.com', 455454545, '151512000.webm', '151512000.jpeg', 4, 0, 0, '6', '2', '2020-07-07 11:42:19', '2020-07-09 14:33:58'),
(49, '3', '1993msayandeep@gmail.com', 'Sayandeep NA Majumdar', 'Garia', '999', 0, 'Majumdar', 'habjsbh@dg.vom', 8017730775, '3.webm', '3.jpeg', 4, 0, 0, '0', '2', '2020-07-07 11:42:42', '2020-07-09 14:49:59'),
(50, '55', 'habjsbh@dg.vom', 'Test Assessor Name', 'Kolkata', '153', 1, 'Name', 'msayandeep1993@gmail.com', 4445454545, NULL, NULL, 0, 0, 0, NULL, '2', '2020-07-07 11:43:38', '2020-07-07 11:43:38'),
(51, '3520', 'stdnt@gmail.com', 'student one', 'Kolkata', '420', 0, 'Name', 'msayandeep1993@gmail.com', 8017730775, '3520.webm', '3520.jpeg', 4, 0, 0, '9', '2', '2020-07-07 11:50:33', '2020-07-09 14:17:05'),
(52, '199903', 'sayandeep.tal@gmail.com', 'Sayandeep Majumdar', 'B/13, Bapujinagar, Sulekha', '666', 1, 'Majumdar', 'msayandeep1993@gmail.com', 8017730775, '199903.webm', '199903.jpeg', 4, 0, 0, '1', '2', '2020-07-07 11:56:47', '2020-07-09 14:12:40'),
(53, '151041033020', 'aksas@gmial.com', 'snjdknsdj', 'skndksjdn', '12121212121', 1, 'Majumdar', 'habjsbh@dg.vom', 8017730775, NULL, '151041033020.jpeg', 2, 0, 0, NULL, '2', '2020-07-09 14:52:46', '2020-07-10 18:20:44');

-- --------------------------------------------------------

--
-- Table structure for table `student_exam_progress`
--

CREATE TABLE `student_exam_progress` (
  `id` int(11) NOT NULL,
  `student_id` varchar(100) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `exam_status` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_login`
--

CREATE TABLE `student_login` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_login`
--

INSERT INTO `student_login` (`id`, `username`, `password`, `status`, `created_at`, `updated_at`) VALUES
(2, '199903', '$2y$10$vpcvvXXHWp8nKABtSwm7R.8vl5ofCPZ1yG1cdFhMhP6W.e154ea.a', 1, '2020-07-07 13:58:23', '2020-07-07 14:31:46'),
(4, '151041033020', '$2y$10$NqWbPNFCJeUmk689.zT4r.nW/Z6238FKIjI5UzMFphI3azALJTwpe', 1, '2020-07-09 17:12:37', '2020-07-09 17:12:37');

-- --------------------------------------------------------

--
-- Table structure for table `student_test_progress`
--

CREATE TABLE `student_test_progress` (
  `id` int(11) NOT NULL,
  `student_id` varchar(100) DEFAULT NULL,
  `student_name` varchar(100) DEFAULT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `question_id` varchar(100) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `marks` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_test_progress`
--

INSERT INTO `student_test_progress` (`id`, `student_id`, `student_name`, `batch_id`, `question_id`, `section`, `marks`, `created_at`, `updated_at`) VALUES
(245, '44444', 'Deep', '999', '3343441', 'srk', '1', '2020-07-07 11:47:21', '2020-07-09 13:07:37'),
(246, '888888', 'Rahul', '999', '3343441', 'srk', '1', '2020-07-07 11:47:21', '2020-07-09 13:50:44'),
(247, '151512000', 'Sagar', '999', '3343441', 'srk', '0', '2020-07-07 11:47:21', '2020-07-09 14:33:14'),
(248, '3', 'Sayandeep NA Majumdar', '999', '3343441', 'srk', '0', '2020-07-07 11:47:21', '2020-07-09 14:49:59'),
(249, '44444', 'Deep', '999', '353333333', 'helo', '1', '2020-07-07 11:47:22', '2020-07-07 14:35:35'),
(250, '888888', 'Rahul', '999', '353333333', 'helo', '1', '2020-07-07 11:47:22', '2020-07-09 13:50:38'),
(251, '151512000', 'Sagar', '999', '353333333', 'helo', '0', '2020-07-07 11:47:22', '2020-07-09 14:33:14'),
(252, '3', 'Sayandeep NA Majumdar', '999', '353333333', 'helo', '0', '2020-07-07 11:47:22', '2020-07-09 14:49:59'),
(253, '44444', 'Deep', '999', '24454546', 'statistic', '1', '2020-07-07 11:47:23', '2020-07-09 13:27:57'),
(254, '888888', 'Rahul', '999', '24454546', 'statistic', '1', '2020-07-07 11:47:23', '2020-07-09 13:49:19'),
(255, '151512000', 'Sagar', '999', '24454546', 'statistic', '1', '2020-07-07 11:47:23', '2020-07-09 14:33:14'),
(256, '3', 'Sayandeep NA Majumdar', '999', '24454546', 'statistic', '0', '2020-07-07 11:47:23', '2020-07-09 14:49:59'),
(257, '44444', 'Deep', '999', '7451222', 'math', '1', '2020-07-07 11:47:23', '2020-07-09 13:27:50'),
(258, '888888', 'Rahul', '999', '7451222', 'math', '1', '2020-07-07 11:47:23', '2020-07-09 13:50:29'),
(259, '151512000', 'Sagar', '999', '7451222', 'math', '1', '2020-07-07 11:47:23', '2020-07-09 14:33:58'),
(260, '3', 'Sayandeep NA Majumdar', '999', '7451222', 'math', '0', '2020-07-07 11:47:23', '2020-07-09 14:49:59'),
(261, '44444', 'Deep', '999', '1112212', 'english', '1', '2020-07-07 11:47:24', '2020-07-09 13:27:53'),
(262, '888888', 'Rahul', '999', '1112212', 'english', '0', '2020-07-07 11:47:24', '2020-07-09 13:49:19'),
(263, '151512000', 'Sagar', '999', '1112212', 'english', '1', '2020-07-07 11:47:24', '2020-07-09 14:33:48'),
(264, '3', 'Sayandeep NA Majumdar', '999', '1112212', 'english', '0', '2020-07-07 11:47:24', '2020-07-09 14:49:59'),
(265, '44444', 'Deep', '999', '1212121', 'geography', '1', '2020-07-07 11:47:24', '2020-07-09 13:27:48'),
(266, '888888', 'Rahul', '999', '1212121', 'geography', '1', '2020-07-07 11:47:24', '2020-07-09 13:50:23'),
(267, '151512000', 'Sagar', '999', '1212121', 'geography', '1', '2020-07-07 11:47:24', '2020-07-09 14:33:54'),
(268, '3', 'Sayandeep NA Majumdar', '999', '1212121', 'geography', '0', '2020-07-07 11:47:25', '2020-07-09 14:49:59'),
(269, '44444', 'Deep', '999', '23312323', 'history', '1', '2020-07-07 11:47:25', '2020-07-09 13:29:59'),
(270, '888888', 'Rahul', '999', '23312323', 'history', '1', '2020-07-07 11:47:25', '2020-07-09 13:49:19'),
(271, '151512000', 'Sagar', '999', '23312323', 'history', '1', '2020-07-07 11:47:25', '2020-07-09 14:33:36'),
(272, '3', 'Sayandeep NA Majumdar', '999', '23312323', 'history', '0', '2020-07-07 11:47:25', '2020-07-09 14:49:59'),
(273, '44444', 'Deep', '999', '133434', 'programming', '1', '2020-07-07 11:47:26', '2020-07-09 13:30:02'),
(274, '888888', 'Rahul', '999', '133434', 'programming', '0', '2020-07-07 11:47:26', '2020-07-09 13:49:19'),
(275, '151512000', 'Sagar', '999', '133434', 'programming', '1', '2020-07-07 11:47:26', '2020-07-09 14:33:42'),
(276, '3', 'Sayandeep NA Majumdar', '999', '133434', 'programming', '0', '2020-07-07 11:47:26', '2020-07-09 14:49:59'),
(277, '199903', 'Sayandeep Majumdar', '666', '99999', 'programming', '1', '2020-07-07 11:58:41', '2020-07-09 14:12:40'),
(278, '3520', 'student one', '420', '16', 'new sec', '1', '2020-07-09 11:46:13', '2020-07-09 14:15:44'),
(279, '3520', 'student one', '420', '15', 'hola', '1', '2020-07-09 11:46:13', '2020-07-09 14:17:05'),
(280, '3520', 'student one', '420', '45', 'statistic', '1', '2020-07-09 11:46:14', '2020-07-09 14:15:44'),
(281, '3520', 'student one', '420', '44', 'programming', '1', '2020-07-09 11:46:14', '2020-07-09 14:16:57'),
(282, '3520', 'student one', '420', '43', 'history', '1', '2020-07-09 11:46:15', '2020-07-09 14:16:34'),
(283, '3520', 'student one', '420', '42', 'geography', '1', '2020-07-09 11:46:15', '2020-07-09 14:16:46'),
(284, '3520', 'student one', '420', '41', 'english', '1', '2020-07-09 11:46:16', '2020-07-09 14:16:25'),
(285, '3520', 'student one', '420', '40', 'math', '1', '2020-07-09 11:46:16', '2020-07-09 14:15:45'),
(286, '3520', 'student one', '420', '99999', 'programming', '1', '2020-07-09 11:46:17', '2020-07-09 14:16:19'),
(287, '121212121212121212', 'Sayandeep NA Majumdar', '8787878', '14', 'statistic', '0', '2020-07-09 15:04:59', '2020-07-09 16:04:40'),
(288, '121212121212121212', 'Sayandeep NA Majumdar', '8787878', '12', 'history', '0', '2020-07-09 15:04:59', '2020-07-09 16:04:40'),
(289, '121212121212121212', 'Sayandeep NA Majumdar', '8787878', '13', 'programming', '0', '2020-07-09 15:04:59', '2020-07-09 16:04:40'),
(290, '121212121212121212', 'Sayandeep NA Majumdar', '8787878', '9', 'math', '0', '2020-07-09 15:04:59', '2020-07-09 16:04:40'),
(291, '121212121212121212', 'Sayandeep NA Majumdar', '8787878', '10', 'english', '0', '2020-07-09 15:05:00', '2020-07-09 16:04:40'),
(292, '121212121212121212', 'Sayandeep NA Majumdar', '8787878', '11', 'geography', '0', '2020-07-09 15:05:00', '2020-07-09 16:04:40'),
(293, '67676776688', 'Sayandeep Majumdar', '899998789798', '35', 'statistic', '0', '2020-07-10 18:49:39', '2020-07-10 22:36:15'),
(294, '67676776688', 'Sayandeep Majumdar', '899998789798', '34', 'programming', '0', '2020-07-10 18:49:39', '2020-07-10 22:36:15'),
(295, '67676776688', 'Sayandeep Majumdar', '899998789798', '33', 'history', '0', '2020-07-10 18:49:39', '2020-07-10 22:36:15'),
(296, '67676776688', 'Sayandeep Majumdar', '899998789798', '30', 'math', '1', '2020-07-10 18:49:39', '2020-07-10 22:36:15'),
(297, '67676776688', 'Sayandeep Majumdar', '899998789798', '31', 'english', '0', '2020-07-10 18:49:40', '2020-07-10 22:36:15'),
(298, '67676776688', 'Sayandeep Majumdar', '899998789798', '32', 'geography', '0', '2020-07-10 18:49:40', '2020-07-10 22:36:15'),
(299, '67676776688', 'Sayandeep Majumdar', '899998789798', '3343441', 'srk', '0', '2020-07-10 18:49:40', '2020-07-10 22:36:15'),
(300, '67676776688', 'Sayandeep Majumdar', '899998789798', '353333333', 'helo', '0', '2020-07-10 18:49:40', '2020-07-10 22:36:15'),
(301, '67676776688', 'Sayandeep Majumdar', '899998789798', '24454546', 'statistic', '1', '2020-07-10 18:49:41', '2020-07-10 22:36:15'),
(302, '67676776688', 'Sayandeep Majumdar', '899998789798', '7451222', 'math', '0', '2020-07-10 18:49:41', '2020-07-10 22:36:15'),
(303, '67676776688', 'Sayandeep Majumdar', '899998789798', '1112212', 'english', '0', '2020-07-10 18:49:41', '2020-07-10 22:36:15'),
(304, '67676776688', 'Sayandeep Majumdar', '899998789798', '1212121', 'geography', '1', '2020-07-10 18:49:41', '2020-07-10 22:36:15'),
(305, '67676776688', 'Sayandeep Majumdar', '899998789798', '23312323', 'history', '0', '2020-07-10 18:49:41', '2020-07-10 22:36:15'),
(306, '67676776688', 'Sayandeep Majumdar', '899998789798', '133434', 'programming', '0', '2020-07-10 18:49:42', '2020-07-10 22:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `student_test_url`
--

CREATE TABLE `student_test_url` (
  `id` int(11) NOT NULL,
  `student_username` varchar(150) NOT NULL,
  `batch_id` varchar(150) NOT NULL,
  `url` varchar(100) NOT NULL,
  `fdt` varchar(100) DEFAULT NULL,
  `tdt` varchar(100) DEFAULT NULL,
  `ftime` varchar(100) DEFAULT NULL,
  `ttime` varchar(100) DEFAULT NULL,
  `student_mail` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_test_url`
--

INSERT INTO `student_test_url` (`id`, `student_username`, `batch_id`, `url`, `fdt`, `tdt`, `ftime`, `ttime`, `student_mail`, `created_at`, `updated_at`) VALUES
(2, '199903', '666', 'http://localhost:8002/test/page/666', '2020-08-01', '2020-08-31', '06:30:00', '15:30:00', 'sayandeep.tal@gmail.com', '2020-07-07 13:58:31', '2020-07-07 14:31:59'),
(4, '151041033020', '12121212121', 'http://localhost:8002/test/page/12121212121', '2020-07-23', '2020-07-24', '03:12:00', '17:12:00', 'aksas@gmial.com', '2020-07-09 17:12:47', '2020-07-09 17:12:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor`
--
ALTER TABLE `assessor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor_history`
--
ALTER TABLE `assessor_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor_img_test`
--
ALTER TABLE `assessor_img_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor_login`
--
ALTER TABLE `assessor_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`batch_id`);

--
-- Indexes for table `batch_history`
--
ALTER TABLE `batch_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_process`
--
ALTER TABLE `batch_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_review_comments`
--
ALTER TABLE `batch_review_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_type`
--
ALTER TABLE `batch_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_role`
--
ALTER TABLE `job_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_map_transaction`
--
ALTER TABLE `question_map_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_transaction`
--
ALTER TABLE `question_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schema_model`
--
ALTER TABLE `schema_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_wise_marks`
--
ALTER TABLE `section_wise_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sectors`
--
ALTER TABLE `sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_exam_progress`
--
ALTER TABLE `student_exam_progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_login`
--
ALTER TABLE `student_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_test_progress`
--
ALTER TABLE `student_test_progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_test_url`
--
ALTER TABLE `student_test_url`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assessor`
--
ALTER TABLE `assessor`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `assessor_history`
--
ALTER TABLE `assessor_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `assessor_img_test`
--
ALTER TABLE `assessor_img_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `assessor_login`
--
ALTER TABLE `assessor_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `batch_history`
--
ALTER TABLE `batch_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `batch_process`
--
ALTER TABLE `batch_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `batch_review_comments`
--
ALTER TABLE `batch_review_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `batch_type`
--
ALTER TABLE `batch_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_role`
--
ALTER TABLE `job_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `question_map_transaction`
--
ALTER TABLE `question_map_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `question_transaction`
--
ALTER TABLE `question_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT for table `schema_model`
--
ALTER TABLE `schema_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `section_wise_marks`
--
ALTER TABLE `section_wise_marks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sectors`
--
ALTER TABLE `sectors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `student_exam_progress`
--
ALTER TABLE `student_exam_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_login`
--
ALTER TABLE `student_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_test_progress`
--
ALTER TABLE `student_test_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=307;

--
-- AUTO_INCREMENT for table `student_test_url`
--
ALTER TABLE `student_test_url`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
