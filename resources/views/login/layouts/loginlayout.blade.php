<!DOCTYPE html>
<html lang="en">
     @include('login.include.headerlinks')

     <style>

     .growweltextbox{
       border-bottom: 2px solid #464dee!important;
       border-top: none !important;
       border-left: none !important;
       border-right: none !important;
       padding-left: 0px !important;

     }

     .growweltextbox:focus {
       border-bottom: 2px solid #000 !important;
       border-top: none !important;
       border-left: none !important;
       border-right: none !important;
       padding-left: 0px !important;

     }


     .growwellinputfield{
       position:relative !important;
       margin-bottom:25px !important;
     }
     .growwellinputfield label{
       position:absolute;
       top:0px;
       left:0px;
       font-size:16px;
       color:#464dee;
       font-weight: bold;
         pointer-event:none;
       transition: all 0.5s ease-in-out;
     }
     .growwellinputfield input{
       border:0;
       border-bottom:1.5px solid #8E93F0;
       background:transparent;
       width:100%;
       padding:20px 0 15px 0;
       font-size:16px;
       color:#464dee;
     }
     .growwellinputfield input:focus{
      border:none !important;
      outline:none !important;
      border-bottom:2px solid #464dee !important;
     }
     </style>
 <body>
   <div class="container-scroller">


   @yield('content')

   </div>
   <!-- container-scroller -->
     @include('login.include.footerlinks')
 </body>
</html>
