<!-- base:js -->
   <script src="{{asset('theme/vendors/base/vendor.bundle.base.js')}}"></script>
   <!-- endinject -->
   <!-- Plugin js for this page-->
   <!-- End plugin js for this page-->
   <!-- inject:js -->
   <script src="{{asset('theme/js/template.js')}}"></script>
   <!-- endinject -->
   <!-- plugin js for this page -->
   <!-- End plugin js for this page -->
   <script src="{{asset('theme/vendors/chart.js/Chart.min.js')}}"></script>
   <script src="{{asset('theme/vendors/progressbar.js/progressbar.min.js')}}"></script>
   <script src="{{asset('theme/vendors/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js')}}"></script>
   <script src="{{asset('theme/vendors/justgage/raphael-2.1.4.min.js')}}"></script>
   <script src="{{asset('theme/vendors/justgage/justgage.js')}}"></script>
   <!-- Custom js for this page-->
   <script src="{{asset('theme/js/dashboard.js')}}"></script>
   <!-- End custom js for this page-->

   <!-- dropzone file upload -->
   <script src="{{asset('theme/dropzone/dist/dropzone.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

   <script type="text/javascript">

       Dropzone.options.dropzoneForm = {

           autoprocessQueue : false,

           init:function(){
               var submitButton = document.querySelector("#submit-all");
               myDropzone = this;

               submitButton.addEventListener('click',function(){
                   myDropzone.processQueue();
               });

               this.on("compleate",function(){

                  /* if(this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0)
                   {
                       var _this= this;
                       _this.removeAllFiles();
                   }*/

               });
           }
       };
   </script>
