@extends('login.layouts.loginlayout')

@section('content')

<div class="container-scroller">
<div class="container-fluid page-body-wrapper full-page-wrapper">
  <div class="main-panel">
        @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
    <div class="content-wrapper d-flex align-items-center auth px-0">
      <div class="row w-100 mx-0">
        <div class="col-lg-4 mx-auto">
          <div class="auth-form-light text-left py-5 px-4 px-sm-5">
            <div class="brand-logo">
              <img src="{{asset('theme/images/colorlogo.png')}}" alt="logo">
            </div>
            <h4>Hello <span style="color: #464dee;">Student</span>! let's get started</h4>
            <form class="pt-3" action="{{route('assessor.test.auth')}}" method="POST">
              {{csrf_field()}}
              <input type="hidden" name="batch_id" value={{$batch_id}}>
              <div class="form-group">
                <input type="text" name="username" class="form-control form-control-lg growweltextbox" id="exampleInputEmail1" value="{{$std_id}}" placeholder="Username" readonly>
              </div>
              <div class="mt-3">
                <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">Authenticate</button>
              </div>
              <div class="my-2 d-flex justify-content-between align-items-center">
                <div class="form-check">
                  
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
@endsection
