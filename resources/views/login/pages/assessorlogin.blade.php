@extends('login.layouts.loginlayout')

@section('content')

<div class="container-scroller">
<div class="container-fluid page-body-wrapper full-page-wrapper">
  <div class="main-panel">
    @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
    <div class="content-wrapper d-flex align-items-center auth px-0">
      <div class="row w-100 mx-0">
        <div class="col-lg-4 mx-auto">
          <div class="auth-form-light text-left py-5 px-4 px-sm-5">
            <div class="brand-logo">
              <img src="{{asset('theme/images/colorlogo.png')}}" alt="logo">
            </div>
            <h4>Hello <span style="color: #464dee;">Assessor</span>! let's get started</h4>
            <h6 class="font-weight-light">Sign in to continue.</h6>
            <form class="pt-3" action="{{route('assessorlogin.service')}}" method="POST">
              {{csrf_field()}}
              <div class="growwellinputfield">
                <input type="text" name="username" class="form-control form-control-lg " id="exampleInputEmail1" style="border-bottom:2px solid blue !important;border-left: none !important;border-right: none !important;border-top: none !important; " >
                <label>Username</label>
              </div>
              <div class="growwellinputfield">
                <input type="password" name="password" class="form-control form-control-lg " style="border-bottom:2px solid blue !important;border-left: none !important;border-right: none !important;border-top: none !important; " id="exampleInputPassword1"  >
                 <label>Password</label>
              </div>
              <div class="mt-3">
                <button type="submit"class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" style="background-color: #27313b;">SIGN IN</button>
              </div>
              <div class="my-2 d-flex justify-content-between align-items-center">
                <div class="form-check">

                </div>
                
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
@endsection
