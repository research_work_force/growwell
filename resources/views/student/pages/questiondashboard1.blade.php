@extends('student.layouts.home')

@section('content')

<style>
input[type="radio"] {
    /* remove standard background appearance */
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    /* create custom radiobutton appearance */
    display: inline-block;
    width: 25px;
    height: 25px;
    padding: 6px;
    margin: 20px;
    /* background-color only for content */
    background-clip: content-box;
    border: 2px solid #4a9cdf;
    background-color: #4a9cdf;
    border-radius: 50%;
  }

  /* appearance for checked radiobutton */
  input[type="radio"]:checked {
    background-color: #93e026;
    width: 35px;
    height: 35px;
  }

  /* optional styles, I'm using this for centering radiobuttons */
  .flex {
    display: flex;
    align-items: center;
  }
</style>

<!-- Navbar start -->
 @include('student.include.navbarstudent')
<!-- Navbar end -->


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Question Dashboard</h2> -->

          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-8 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <script src="{{asset('theme/js/countdowntimer.js')}}"></script>
                  <div  class="row "style="float: right;display:block;background-color:#464dee;padding: 1% 2% 1% 2%;color: white;"> Time left : <span id="demo"> </span></div>
                    <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;">Q. <span style="font-size: 20px !important;Color:#4a9cdf;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec eros aliquet, faucibus ligula malesuada, luctus felis. Aliquam erat volutpat. </span> </h2>

                    <form>
                      <div class="flex">
                        <input type="radio" name="radio" id="radio1" />
                        <label for="radio1">sit amet, consectetur adipiscing elit. Curabitur nec eros aliquet,</label>
                      </div>
                      <div class="flex">
                        <input type="radio" name="radio" id="radio2" />
                        <label for="radio2">sit amet, consectetur adipiscing elit. Curabitur nec eros aliquet,</label>
                      </div>
                      <div class="flex">
                        <input type="radio" name="radio" id="radio3" />
                        <label for="radio3">sit amet, consectetur adipiscing elit. Curabitur nec eros aliquet,</label>
                      </div>

                      <div class="flex">
                        <input type="radio" name="radio" id="radio3" />
                        <label for="radio3">sit amet, consectetur adipiscing elit. Curabitur nec eros aliquet,</label>
                      </div>

                    </form>

                    <div class="template-demo" align="center" style="margin-top: 10%;">
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-primary">PREVIOUS</button>
                        <button type="button" class="btn btn-outline-primary" disabled>You are attending x out of y questions</button>
                        <button type="button" class="btn btn-primary">NEXT</button>
                      </div>

                    </div>

                    <div class="col-md-12" align="center" style="margin-top:2%;">
                      <p><span> <a href="#">English</a> |</span><span> <a href="#">Hindi</a> |</span><span> <a href="#">Telegu</a> |</span><span> <a href="#">Assam</a></span> </p>

                    </div>
                    <div class="row" align="center" style="margin-top:2%;background: #464dee;padding:2px;color:white;">
                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Batch ID :</span> <span>JASKL444</span></p>
                      </div>
                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Job Role :</span> <span>job role example</span></p>
                      </div>
                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Sector :</span> <span>sector name example</span></p>
                      </div>

                    </div>


                  </div>
                </div>
              </div>

              <div class="col-lg-4 grid-margin stretch-card" align="center">
                <div class="card" >
                  <div class="card-body">

                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Answer Status </h2>
                      <h5  style="Color:#4a9cdf;">  <button type="button" class="btn btn-success btn-rounded btn-icon">

                      </button> Attempted  <button type="button" class="btn btn-primary btn-rounded btn-icon" style="margin-left: 10%;">

                      </button> Non Attempted </h5>
                      <div class="template-demo d-flex justify-content-between flex-nowrap">
                      <button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button>
                      <button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                        1
                      </button>

                    </div>
                    <div class="template-demo d-flex justify-content-between flex-nowrap">
                    <button type="button" class="btn btn-primary btn-rounded btn-icon">
                      1
                    </button>
                    <button type="button" class="btn btn-primary btn-rounded btn-icon">
                      1
                    </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                      1
                    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                      1
                    </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                      1
                    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                      1
                    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                      1
                    </button>

                  </div>
                  <div class="template-demo d-flex justify-content-between flex-nowrap">
                  <button type="button" class="btn btn-primary btn-rounded btn-icon">
                    1
                  </button>
                  <button type="button" class="btn btn-primary btn-rounded btn-icon">
                    1
                  </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                    1
                  </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                    1
                  </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                    1
                  </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                    1
                  </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                    1
                  </button>

                </div>
                <div class="template-demo d-flex justify-content-between flex-nowrap">
                <button type="button" class="btn btn-primary btn-rounded btn-icon">
                  1
                </button>
                <button type="button" class="btn btn-primary btn-rounded btn-icon">
                  1
                </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                  1
                </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                  1
                </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                  1
                </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                  1
                </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                  1
                </button>

              </div>
              <div class="template-demo d-flex justify-content-between flex-nowrap">
              <button type="button" class="btn btn-primary btn-rounded btn-icon">
                1
              </button>
              <button type="button" class="btn btn-primary btn-rounded btn-icon">
                1
              </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                1
              </button><button type="button" class="btn btn-success btn-rounded btn-icon">
                1
              </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                1
              </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                1
              </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
                1
              </button>

            </div>
            <div class="template-demo d-flex justify-content-between flex-nowrap">
            <button type="button" class="btn btn-primary btn-rounded btn-icon">
              1
            </button>
            <button type="button" class="btn btn-primary btn-rounded btn-icon">
              1
            </button><button type="button" class="btn btn-success btn-rounded btn-icon">
              1
            </button><button type="button" class="btn btn-success btn-rounded btn-icon">
              1
            </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
              1
            </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
              1
            </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
              1
            </button>

          </div>
          <div class="template-demo d-flex justify-content-between flex-nowrap">
          <button type="button" class="btn btn-primary btn-rounded btn-icon">
            1
          </button>
          <button type="button" class="btn btn-primary btn-rounded btn-icon">
            1
          </button><button type="button" class="btn btn-success btn-rounded btn-icon">
            1
          </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
            1
          </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
            1
          </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
            1
          </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
            1
          </button>

        </div>
        <div class="template-demo d-flex justify-content-between flex-nowrap">
        <button type="button" class="btn btn-primary btn-rounded btn-icon">
          1
        </button>
        <button type="button" class="btn btn-primary btn-rounded btn-icon">
          1
        </button><button type="button" class="btn btn-success btn-rounded btn-icon">
          1
        </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
          1
        </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
          1
        </button><button type="button" class="btn btn-success btn-rounded btn-icon">
          1
        </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
          1
        </button>

      </div>
      <div class="template-demo d-flex justify-content-between flex-nowrap">
      <button type="button" class="btn btn-primary btn-rounded btn-icon">
        1
      </button>
      <button type="button" class="btn btn-primary btn-rounded btn-icon">
        1
      </button><button type="button" class="btn btn-success btn-rounded btn-icon">
        1
      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
        1
      </button><button type="button" class="btn btn-success btn-rounded btn-icon">
        1
      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
        1
      </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
        1
      </button>

    </div>
    <div class="template-demo d-flex justify-content-between flex-nowrap">
    <button type="button" class="btn btn-primary btn-rounded btn-icon">
      1
    </button>
    <button type="button" class="btn btn-primary btn-rounded btn-icon">
      1
    </button><button type="button" class="btn btn-success btn-rounded btn-icon">
      1
    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
      1
    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
      1
    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
      1
    </button><button type="button" class="btn btn-primary btn-rounded btn-icon">
      1
    </button>

  </div>


                    </div>
                  </div>
                </div>

              <!-- <div class="col-lg-3 grid-margin stretch-card" align="center">
                <div class="card" >
                  <div class="card-body">

                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Captured Image </h2>
                      <img src="{{asset('theme/images/userimage.png')}}" width=300 height= 300/>
                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;">  </h2>
                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Tracked Location </h2>
                      <h4 class="card-title"> Assam ( Approx 26.2006° N, 92.9376° E)   </h4>

                    </div>
                  </div>
                </div> -->
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
