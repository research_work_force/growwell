@extends('student.layouts.questionhome')

@section('content')

<script>
document.addEventListener('contextmenu', event => event.preventDefault());

var today = new Date();
var year = today.getFullYear();
var m = today.getMonth();

switch(m)
{
  case 0:
  m= "Jan";
  break;

  case 1:
  m= "Feb";
  break;

  case 2:
  m= "Mar";
  break;

  case 3:
  m= "Apr";
  break;

  case 4:
  m= "May";
  break;

  case 5:
  m= "June";
  break;

  case 6:
  m= "July";
  break;
}

 // alert("hi");

var d = today.getDate();
var x = today.getHours();
 var y = today.getMinutes();
 var z = today.getSeconds();

 var users = {!! json_encode($batchInfo->test_duration) !!};

 // alert(users);

 y= parseInt(y) + parseInt(users);

y= parseInt(y);
 // alert("the value"+ y);

 if(y>59)
 {
   // alert(x);
   x=parseInt(x);
   x=x+1;
   // alert(x);
   y=y-60;
 }


 // alert(m+" "+d+" "+year+" "+x+":"+y+":"+z);



var k= m+" "+d+" "+year+" "+x+":"+y+":"+z ;
var d = "Jun"+" 16 " + "2020 " + "5"+":"+"00"+":"+"00";
// alert(k);
var countDownDate = new Date(k).getTime();

// Set the date we're counting down to
// var countDownDate = new Date("30:10").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";



  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "Exam Over";
  }
}, 1000);




</script>






<!-- video css -->
<style >
video {
margin-top: 2px;
border: 1px solid black;
}

.button {
cursor: pointer;
display: block;
width: 160px;
border: 1px solid black;
font-size: 16px;
text-align: center;
padding-top: 2px;
padding-bottom: 4px;
color: white;
background-color: darkgreen;
text-decoration: none;
}

h2 {
margin-bottom: 4px;
}

.left {
margin-right: 10px;
float: left;
width: 160px;
padding: 0px;
}

.right {
margin-left: 10px;
float: left;
width: 160px;
padding: 0px;
}

.bottom {
clear: both;
padding-top: 10px;
}

</style>
<!-- video css end -->

<style>
input[type="radio"] {
    /* remove standard background appearance */
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    /* create custom radiobutton appearance */
    display: inline-block;
    width: 25px;
    height: 25px;
    padding: 6px;
    margin: 20px;
    /* background-color only for content */
    background-clip: content-box;
    border: 2px solid #4a9cdf;
    background-color: #27313b;
    border-radius: 50%;
  }

  /* appearance for checked radiobutton */
  input[type="radio"]:checked {
    background-color: #93e026;
    width: 35px;
    height: 35px;
  }

  /* optional styles, I'm using this for centering radiobuttons */
  .flex {
    display: flex;
    align-items: center;
  }
</style>

<!-- Navbar start -->
 @include('student.include.navbarstudent')
<!-- Navbar end -->


@include('student.include.endexammodal',['batch_id' => $batchInfo->batch_id])

    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper" >

          <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Question Dashboard</h2>

          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-8 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <!-- <script src="{{asset('theme/js/countdowntimer.js')}}"></script> -->
                  <a href="#" class="btn btn-primary btn-growwell" style="margin-left: 5%;" onclick="stoptherecording();"> End Exam</a>

                  <div  class="row "style="float: right;display:block;background-color:#27313b;padding: 1% 2% 1% 2%;color: white;">Time left : <span id="demo"> </span></div>
                    <h2 id="ques" class="card-title" style="font-size: 30px !important;Color:#27313b;">Q. <span style="font-size: 20px !important;Color: #4a9cdf;">


                    </span> </h2>

                    <form >
                      {{csrf_field()}}
                      <div id="radiodiv">
                      <!-- <div class="flex">
                        <div class="radio1"></div>
                        <input type="radio" name="radio" id="radio1" />
                        <label id="op1" for="radio1"> </label>
                      </div>
                      <div class="flex">
                        <div class="radio2"></div>
                        <input type="radio" name="radio" id="radio2" />
                        <label id="op2" for="radio2"> </label>
                      </div>
                      <div class="flex">
                        <div class="radio3"></div>
                        <input type="radio" name="radio" id="radio3" />
                        <label id="op3" for="radio3"> </label>
                      </div>

                      <div class="flex">
                        <div class="radio4"></div>
                        <input type="radio" name="radio" id="radio3" />
                        <label id="op4" for="radio3"> </label>
                      </div> -->

                    </div>

                    </form>

                    <div class="row" style="float:right">

                      <div class="left">
                        <div id="startButton" class="button">
                          Start
                        </div>  <h2>Preview</h2>
                        <video id="preview" width="160" height="120" autoplay muted></video>
                      </div>
                      <div class="right">
                        <div id="stopButton" class="button">
                          Stop
                        </div>





                        <h2>Recording</h2>
                        <video id="recording" width="160" height="120" controls></video>
                        <a id="downloadButton" class="button">
                          Download
                        </a>
                      </div>
                      <div class="bottom">
                        <pre id="log"></pre>
                      </div>

                    </div>

                    <div class="template-demo" align="center" style="margin-top: 10%;">
                      <div id="pagination" class="btn-group" role="group" aria-label="Basic example">
                        <a id="prelink" class="btn btn-primary btn-growwell" style="color:white;" onclick="prePage()">PREVIOUS</a>

                       </div>
                       <div id="pagination" class="btn-group" role="group" aria-label="Basic example">
                        <a id="nextlink" class="btn btn-primary btn-growwell" style="color:white;" onclick="nextPage()">NEXT</a>
                      </div>

                    </div>

                      <div class="row" align="center" style="margin-top:2%;padding:2px;color:#27313b;text-align: center;">
                        <div class="col-md-12">
                            <b> Question Section </b> - <span id="sectionidspan"></span>
                        </div>

                      </div>



                    <!-- <div class="col-md-12" align="center" style="margin-top:2%;">
                      <p><span> <a href="#">English</a> |</span><span> <a href="#">Hindi</a> |</span><span> <a href="#">Telegu</a> |</span><span> <a href="#">Assam</a></span> </p>

                    </div> -->
                    <div class="row" align="center" style="margin-top:2%;background: #27313b;padding:2px;color:white;">
                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Batch ID :</span> <span>{{\Session::get('batch_id')}}</span></p>
                      </div>
                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Job Role :</span> <span>{{$batchInfo->jobRole}}</span></p>
                      </div>
                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Sector :</span> <span>{{$batchInfo->sectors}}</span></p>
                      </div>

                      <div class="col-md-4">
                        <p><span style="font-weight:bolder;">Test Duration:</span> <span> {{$batchInfo->test_duration}}</span></p>
                      </div>

                    </div>


                  </div>
                </div>
              </div>

              <div class="col-lg-4 grid-margin stretch-card" align="center">
                <div class="card" >
                  <div class="card-body">

                      <h2 class="card-title" style="font-size: 30px !important;Color:#27313b;"> Answer Status </h2>
                      <h5  style="Color:#4a9cdf;">  <button type="button" class="btn btn-success btn-rounded btn-icon">

                      </button> Attempted  <button type="button" class="btn btn-primary btn-rounded btn-icon" style="margin-left: 10%;background:#27313b">

                      </button> Non Attempted </h5>

                      <div id="qnos">

                        <div class="row">
                      @php $count=1; @endphp
                      @foreach($question as $ques)






                        <div class="col-md-2" style="margin-bottom: 5%;">

                          @if(1)
                          <button type="button" onclick="getdata({{$count}})" class="btn btn-success  btn-rounded btn-icon">
                            {{$count}}
                          </button>

                          @else
                          <button type="button" onclick="getdata({{$count}})" class="btn btn-primary btn-growwell btn-rounded btn-icon">
                            {{$count}}
                          </button>
                          @endif



                      </div>


                        @php $count++; @endphp
                        @endforeach

                        </div>
                    </div>



                  </div>

                     </div>


                    </div>
                  </div>
                </div>

              <!-- <div class="col-lg-3 grid-margin stretch-card" align="center">
                <div class="card" >
                  <div class="card-body">

                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Captured Image </h2>
                      <img src="{{asset('theme/images/userimage.png')}}" width=300 height= 300/>
                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;">  </h2>
                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Tracked Location </h2>
                      <h4 class="card-title"> Assam ( Approx 26.2006° N, 92.9376° E)   </h4>

                    </div>
                  </div>
                </div> -->
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->
<script type="text/javascript">
    window.onbeforeunload = function() {
        alert("ji");
    }
</script>

<script type="text/javascript">

let preview = document.getElementById("preview");
let recording = document.getElementById("recording");
let startButton = document.getElementById("startButton");
let stopButton = document.getElementById("stopButton");
let downloadButton = document.getElementById("downloadButton");
let logElement = document.getElementById("log");

let recordingTimeMS = 60000;

function log(msg) {
console.log(msg);
}

function wait(delayInMS) {
return new Promise(resolve => setTimeout(resolve, delayInMS));
}

function startRecording(stream, lengthInMS) {
let recorder = new MediaRecorder(stream);
let data = [];

recorder.ondataavailable = event => data.push(event.data);
recorder.start();
console.log(recorder.state + " for " + (recordingTimeMS/1000) + " seconds...");

let stopped = new Promise((resolve, reject) => {
  recorder.onstop = resolve;
  recorder.onerror = event => reject(event.name);
});

  let recorded = wait(recordingTimeMS).then(
  () => recorder.state == "recording" && recorder.stop()
  );

return Promise.all([
  stopped,
  recorded
])
.then(() => data);
}
function stop(stream) {
stream.getTracks().forEach(track => track.stop());
}
startButton.addEventListener("click", function() {
navigator.mediaDevices.getUserMedia({
  video: true,
  audio: true
}).then(stream => {
  preview.srcObject = stream;
  downloadButton.href = stream;
  preview.captureStream = preview.captureStream || preview.mozCaptureStream;
  return new Promise(resolve => preview.onplaying = resolve);
}).then(() => startRecording(preview.captureStream(), recordingTimeMS))
.then (recordedChunks => {
  let recordedBlob = new Blob(recordedChunks, { type: "video/webm" });
  recording.src = URL.createObjectURL(recordedBlob);


  var fd = new FormData();
        fd.append('fname', 'arijit.webm');
        fd.append('data', recordedBlob);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/camrecorder3/save',
            data: fd,
            processData: false,
            contentType: false
        }).done(function(data) {
               console.log(data);
        });



  downloadButton.href = recording.src;
  downloadButton.download = "RecordedVideo.webm";

  log("Successfully recorded " + recordedBlob.size + " bytes of " +
      recordedBlob.type + " media.");
})
.catch(log);
}, false);

stopButton.addEventListener("click", function() {
stop(preview.srcObject);
}, false);

function startvideorecording(){
  navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
  }).then(stream => {
    preview.srcObject = stream;
    downloadButton.href = stream;
    preview.captureStream = preview.captureStream || preview.mozCaptureStream;
    return new Promise(resolve => preview.onplaying = resolve);
  }).then(() => startRecording(preview.captureStream(), recordingTimeMS))
  .then (recordedChunks => {
    let recordedBlob = new Blob(recordedChunks, { type: "video/webm" });
    recording.src = URL.createObjectURL(recordedBlob);


    var fd = new FormData();
          fd.append('fname', 'arijit.webm');
          fd.append('data', recordedBlob);

          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
              type: 'POST',
              url: '/camrecorder3/save',
              data: fd,
              processData: false,
              contentType: false
          }).done(function(data) {
                 console.log(data);
          });



    downloadButton.href = recording.src;
    downloadButton.download = "RecordedVideo.webm";

    // window.location.href = "/student/examend";

    log("Successfully recorded " + recordedBlob.size + " bytes of " +
        recordedBlob.type + " media.");
  })
  .catch(log);
}

function stoptherecording(){

 // var r = confirm("Press a button!");
 // if (r == true) {

   stop(preview.srcObject);

   $("#endexam").modal();

 // } else {

 // }

}


</script>
    <script src="{{asset('theme/js/qtest.js')}}"></script>
    <script src="{{asset('theme/js/vanilla.js')}}"></script>
				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
