@extends('student.layouts.home')

@section('content')



<!-- Navbar start -->
 @include('student.include.navbarstudent')
<!-- Navbar end -->




    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Question Dashboard</h2> -->

          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body" style="text-align: center;">

                  <h1>Thank you so much for completing the exam.</h1>
                  <h1>All the best for your future endeavours</h1>

                  </div>
                </div>
              </div>


                  </div>
                </div>

              <!-- <div class="col-lg-3 grid-margin stretch-card" align="center">
                <div class="card" >
                  <div class="card-body">

                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Captured Image </h2>
                      <img src="{{asset('theme/images/userimage.png')}}" width=300 height= 300/>
                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;">  </h2>
                      <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"> Tracked Location </h2>
                      <h4 class="card-title"> Assam ( Approx 26.2006° N, 92.9376° E)   </h4>

                    </div>
                  </div>
                </div> -->
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

    <script src="{{asset('theme/js/qtest.js')}}"></script>
    <script src="{{asset('theme/js/vanilla.js')}}"></script>
				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
