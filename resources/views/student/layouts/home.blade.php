 <!DOCTYPE html>
<html lang="en">
      @include('student.include.headerLinks')
  <body >
    <div class="container-scroller">


    @yield('content')

    </div>
		<!-- container-scroller -->
      @include('student.include.footerLinks')
  </body>
</html>
