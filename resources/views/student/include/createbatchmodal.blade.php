<div class="modal fade" id="createbatchmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#4a9cdf;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create Batch</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Scheme/ Program/ Model</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>Scheme/ Program/ Model 1</option>
                              <option>Scheme/ Program/ Model 2</option>
                              <option>Scheme/ Program/ Model 3</option>
                              <option>Scheme/ Program/ Model 4</option>
                              <option>Scheme/ Program/ Model 5</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>Job Role 1</option>
                              <option>Job Role 2</option>
                              <option>Job Role 3</option>
                              <option>Job Role 4</option>
                              <option>Job Role 5</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sector</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>sector 1</option>
                              <option>sector 2</option>
                              <option>sector 3</option>
                              <option>sector 4</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Type</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>Batch Type 1</option>
                              <option>Batch Type 2</option>
                              <option>Batch Type 3</option>
                              <option>Batch Type 4</option>
                            </select>
                          </div>
                        </div>

                        <!-- <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Membership</label>
                          <div class="col-sm-4">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios1" value="" checked>
                                Free
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-5">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios2" value="option2">
                                Professional
                              </label>
                            </div>
                          </div>
                        </div> -->
                      </div>
                    </div>
                    <!-- <p class="card-description">
                      Address
                    </p> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Languauge</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Training Center Location</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Size</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Mobile No</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" />
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email ID</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessment Date</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" />
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>state 1</option>
                              <option>state 2</option>
                              <option>state 3</option>
                              <option>state 4</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Upload Student Details</label>
                          <div class="col-sm-9">
                            <input type="file" class="form-control" accept=".csv,.xlsx" />
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">

                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Create</button>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
