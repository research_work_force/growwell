

       <div class="modal fade" id="endexam">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="{{route('exam.result.checking')}}" id="examendform" method="post">
              {{csrf_field()}}
            <div class="modal-body">
              <p>Are you sure?</p>
            </div>
            <div class="modal-footer">

              <input type="hidden" name="batch_id" value="{{$batch_id}}">
              <input type="hidden" name="studentsession" value="{{\Session::get('studentsession')}}">
              <button type="submit" class="btn btn-primary btn-growwell" style="color:white;" onclick="stoptherecording();">End</button>
              <a  class="btn btn-danger btn-growwell" style="color:white;" data-dismiss="modal">Close</a>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
