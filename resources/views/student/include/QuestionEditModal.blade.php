

       <div class="modal fade" id="editQuestionmodal{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="/question/edit" method="post">
              {{csrf_field()}}

              <div class="modal-header">
                <p>Edit Question</p>
              </div>
            <div class="modal-body">
              

              <input type="hidden" name="id" value="{{$item->id}}">

               <div class="form-group">         
              <label >Question</label>
              <input type="text" class="form-control" name="question" value="{{$item->question}}" required="">
              </div> 

               <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1" value="{{$item->option1}}" required="">
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2" value="{{$item->option2}}" required="">
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3" value="{{$item->option3}}" required="">
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4" value="{{$item->option4}}" required="">
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_answer" class="form-control" >

                   <option value="option 1" {{ old('right_answer',$item->right_answer)=='option 1' ? 'selected' : ''  }}>option 1</option>
                  <option value="option 2"  {{ old('right_answer',$item->right_answer)=='option 2' ? 'selected' : '' }}>option 2</option>
                  <option value="option 3" {{ old('right_answer',$item->right_answer)=='option 3' ? 'selected' : ''  }}>option 3</option>
                   <option value="option 4" {{ old('right_answer',$item->right_answer)=='option 4' ? 'selected' : ''  }}>option 4</option>
   

                </select>

              
            </div>


              <div class="form-group">
               <label>Job role</label>
               <select  name="job_role" class="form-control" >

                   <option value="Hank Dyer" {{ old('job_role',$item->job_role)=='Hank Dyer' ? 'selected' : ''  }}>Hank Dyer</option>
                  <option value="Jacquard Harness builder"  {{ old('job_role',$item->job_role)=='Jacquard Harness builder' ? 'selected' : '' }}>Jacquard Harness builder</option>
                  <option value="Jacquard Weaver - Handloom" {{ old('job_role',$item->job_role)=='Jacquard Weaver - Handloom' ? 'selected' : ''  }}>Jacquard Weaver - Handloom</option>
                   <option value="Textile Designer - Handloom Jacquard" {{ old('job_role',$item->job_role)=='Textile Designer - Handloom Jacquard' ? 'selected' : ''  }}>Textile Designer - Handloom Jacquard</option>
                    <option value="two Shaft Handloom Weaver" {{ old('job_role',$item->job_role)=='two Shaft Handloom Weaver' ? 'selected' : ''  }}>two Shaft Handloom Weaver</option>
                   <option value="Warper" {{ old('job_role',$item->job_role)=='Warper' ? 'selected' : ''  }}>Warper</option>
   

                </select>

             
            </div>

            </div>
            <div class="modal-footer">            

             <button type="submit" class="btn btn-primary">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->