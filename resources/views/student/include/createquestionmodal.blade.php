<div class="modal fade" id="createquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#4a9cdf;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create a Question</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>  
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="/question/save" method="POST">
          {{csrf_field()}}
                    <div class="row">
                    
                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Question</label>
                          <div class="col-sm-10">
                           <textarea name="question" class="form-control" required=""></textarea>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 1</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option1" required="">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 2</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option2">
                          </div>
                        </div>
                      </div>
                    
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 3</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option3" required="">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 4</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option4" required="">
                          </div>
                        </div>
                      </div>
                    
                    </div>


                    <div class="row">
                      <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Right Answer</label>
                          <div class="col-sm-9">
                           <select class="form-control" name="right_answer">
                            <option value="option 1">Option 1</option>
                            <option value="option 2">Option 2</option>
                            <option value="option 3">Option 3</option>
                            <option value="option 4">Option 4</option>
                          </select>
                          </div>
                        </div>
                         
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                          <select class="form-control" name="job_role">
                            <option value="Hank Dyer">Hank Dyer</option>
                            <option value="Jacquard Harness builder">Jacquard Harness builder</option>
                            <option value="Jacquard Weaver - Handloom">Jacquard Weaver - Handloom</option>
                            <option value="Textile Designer - Handloom Jacquard">Textile Designer - Handloom Jacquard</option>
                             <option value="two Shaft Handloom Weaver">two Shaft Handloom Weaver</option>
                            <option value="Warper">Warper</option>
                          </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  

                  
    </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
       <button type="Submit" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Submit</button>
     </form>
              
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
