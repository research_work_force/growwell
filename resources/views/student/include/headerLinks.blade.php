  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Growwell Skills Hub</title>
    <!-- base:css -->
    <link rel="stylesheet" href="/theme/vendors/mdi/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="/theme/vendors/base/vendor.bundle.base.css">
    <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="/theme/css/style.css">
    <!-- endinject -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src = "//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    </script>
    <link rel="shortcut icon" href="/theme/images/favicon.ico" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
  </head>