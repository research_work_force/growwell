

       <div class="modal fade" id="viewassessor{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
           
            <div class="modal-body">                     

              <div class="form-group">
             <label ><b>Assessor ID</b>:&nbsp{{$item->assessor_id}}</label>
           </div>

              <div class="form-group">
              <label><b>Name</b>: &nbsp{{$item->name}}</label>
              </div>

              <div class="form-group">
              <label><b>Gender</b>: &nbsp{{$item->	gender}}</label>
            </div>

              <div class="form-group">
              <label><b>Address</b>: &nbsp {{$item->address}}</label>
              </div>
              <div class="form-group">              
              <label><b>Religion</b>: &nbsp {{$item->	religion}}</label>
              </div>

              <div class="form-group">             
              <label ><b>Date of birth</b>:&nbsp{{$item->date_of_birth}}</label>
              </div>

              <div class="form-group"> 
              <label><b>Languages known</b>: &nbsp {{$item->language_known}}</label>
              </div>

              <div class="form-group">              
              <label><b>phone</b>	: &nbsp {{$item->phone}}</label>
              </div>

              <div class="form-group">          
              <label ><b>Email</b>:&nbsp{{$item->email}}</label>
              </div>

              <div class="form-group"> 
               <label><b>landmark</b>: &nbsp {{$item->landmark}}</label>
              </div>

               <div class="form-group">            
              <label><b>Pincode</b>: &nbsp {{$item->pincode}}</label>

            </div>

            <div class="form-group">           
              <label ><b>State</b>:&nbsp{{$item->state}}</label>
              </div>
              <div class="form-group"> 
              <label><b>Education</b>	: &nbsp {{$item->education}}</label>
              </div>
              <div class="form-group">           
              <label ><b>Industrial experience</b>:&nbsp{{$item->industrial_experience}}</label>
              </div>

          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->