

       <div class="modal fade" id="deletemodal{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="/student/delete" method="post">
              {{csrf_field()}}
            <div class="modal-body">
              <p>Are you sure?</p>
            </div>
            <div class="modal-footer">

              <input type="hidden" name="id" value="{{$item->id}}">
              <button type="submit" class="btn btn-danger">Delete</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->