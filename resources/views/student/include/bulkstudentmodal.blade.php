<div class="modal fade" id="bulkstudentmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#4a9cdf;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Bulk Students Upload</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample">
                    
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Upload Student Details</label>
                          <div class="col-sm-8">
                            <input type="file" class="form-control">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-4">
                        <button type="button" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Upload</button>
                      </div>
                    
                    </div>


                  
                  </form>

                
    </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
       

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
