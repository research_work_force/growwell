@extends('assessor.layouts.home')

@section('content')


<!-- Navbar start -->
 @include('assessor.include.navbar')
   <!-- Navbar end -->


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper">
			<div class="main-panel">
				<div class="content-wrapper">
           <!-- row 1 -->
          <div class="row">
						<div class="col-sm-6 mb-4 mb-xl-0">
							<div class="d-lg-flex align-items-center">
								<div>
									<h2 class="text-dark font-weight-bold mb-2">Hi Assessor, Welcome back !</h2>
									<!-- <h6 class="font-weight-normal mb-2">Last login was 23 hours ago. View details</h6> -->
								</div>
							</div>
						</div>
						<div class="col-sm-6">

						</div>
					</div>
					<!-- row 1 ends -->
					<br>


					<div class="row">
						<div class="col-sm-12 flex-column d-flex stretch-card">
							<div class="row">




								<div class="col-lg-4 d-flex grid-margin stretch-card">
									<a href="/assessormanagement" class="card bg-primary talhref">
										<div class="card-body text-white">
											<div class="row">
												<div class="col-md-4" style="font-size: 63px;">
													<img src="{{asset('theme/images/assessor.png')}}" width="100" alt="Assessor"/>
												</div>

												<div class="col-md-8 talletter">
													<h2 class="pb-0 mb-0">Batch Management</h2>
												</div>

											</div>
										</div>
									</a>
								</div>




							</div>

						</div>

					</div>





				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
