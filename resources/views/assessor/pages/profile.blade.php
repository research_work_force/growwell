@extends('assessor.layouts.home')

@section('content')


 <!-- Navbar start -->
  @include('assessor.include.navbar')
    <!-- Navbar end -->


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Profile Management</h2> -->

          <!-- Row 1 -->

          <div class="row">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title" style="font-size: 30px !important;">Profile Details <span class="headingbtnspan" style="float: right;"><a href=""  data-toggle="modal" data-target="#editassessorprofile">  <button type="button" class="btn btn-primary btn-growwell" style="float: right;font-family:lato !important;font-weight: bold;">Edit Profile</button></a></span></h2>
                     @include('assessor.include.editassessorprofilemodal',['assessor_data' => $singleassessor])



                <div class="row">
                  <div class="col-md-12">
                    <h3 class="card-description" style="color: #212121;font-weight: bold;">
                    Personal Inforation
                    </h3>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <h3 class="card-description" style="font-weight: bold;">

                    </h3>
                  </div>
                </div>


                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Name of the applicant</label>
                          <div class="col-sm-6">
                                {{ $singleassessor->name }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Gender</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->gender }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Religion</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->religion }}
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Date of Birth</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->date_of_birth }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Languages Known</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->language_known }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Phone </label>
                          <div class="col-sm-6">
                            {{ $singleassessor->phone }}
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-12 " style="font-weight:bold;"></label>
                          <img src="{{ asset('growwell/profile/'.$singleassessor->profile_img) }}" width=150 height= 150/>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style=" color: #212121;font-weight: bold;">
                        Contact & Address Details
                        </h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style="font-weight: bold;">

                        </h3>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Email-Id</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->email }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">State/ Union Territory</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->email }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Nearby Landmark</label>
                          <div class="col-sm-6">
                            {{ $singleassessor->landmark }}
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Pincode</label>
                          <div class="col-sm-6">
                            {{ $singleassessor->pincode }}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Applicant District/City</label>
                          <div class="col-sm-6">
                            {{ $singleassessor->city }}
                          </div>
                        </div>


                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Applicant Address</label>
                          <div class="col-sm-6">
                            {{ $singleassessor->address }}
                          </div>
                        </div>


                      </div>

                       </div>

                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style="color: #212121;font-weight: bold;">
                        Educational details
                        </h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style="font-weight: bold;">

                        </h3>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Education</label>
                          <div class="col-sm-6">
                            {{ $singleassessor->education }}
                          </div>
                        </div>



                      </div>

                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;"></label>
                          <div class="col-sm-6">

                          </div>
                        </div>



                      </div>


                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style="color: #212121;font-weight: bold;">
                        Added Industrial Experience Details
                        </h3>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style="font-weight: bold;">

                        </h3>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;"> Industrial Experience</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->industrial_experience }}
                          </div>
                        </div>



                      </div>




                    </div>


                    <div class="row">
                      <div class="row">
                        <div class="col-md-4">
                          <div class="form-group row">
                            <label class="col-sm-6 " style="font-weight:bold;"></label>
                            <div class="col-sm-6">

                            </div>
                          </div>


                        </div>




                      </div>




                    </div>

                 <!--    <div class="row">
                      <div class="col-md-12">
                        <h3 class="card-description" style="color: #212121; font-weight: bold;">

                        </h3>
                      </div>
                    </div>
 -->
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Domain Job Role</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->domain_job_role }}
                          </div>
                        </div>


                      </div>




                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Linking Type</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->linking_type }}
                          </div>
                        </div>


                      </div>




                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Assessment Agency Aligned</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->assessment_agency_aligned }}
                          </div>
                        </div>


                      </div>




                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Duration</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->duration_fdt }} to {{ $singleassessor->duration_tdt }}
                          </div>
                        </div>


                      </div>




                    </div>

                     <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Sector</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->sectors }}
                          </div>
                        </div>


                      </div>




                    </div>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">TOA Status</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->toa_status }}
                          </div>
                        </div>


                      </div>




                    </div>



                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Applicant Type</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->applicant_type }}
                          </div>
                        </div>


                      </div>




                    </div>

                       <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Availability</label>
                          <div class="col-sm-6">
                              {{ $singleassessor->availability }}
                          </div>
                        </div>


                      </div>




                    </div>



                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Support Document</label>
                          <div class="col-sm-6">
                              <a href="{{asset('growwell/assessors/')}}/{{$singleassessor->assessor_id}}/supdoc/{{$singleassessor->supdoc}}" target="_blank">{{$singleassessor->supdoc}}</a>
                          </div>
                        </div>


                      </div>




                    </div>










                </div>
              </div>
            </div>
          </div>

          <!-- Row 1 end -->


				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
