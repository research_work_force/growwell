@extends('assessor.layouts.home')

@section('content')



<!-- Navbar start -->
 @include('assessor.include.navbar')
<!-- Navbar end -->

<!-- CSS -->
  <style>
  #my_camera{
      width: 320px;
      height: 240px;
      /* border: 1px solid black; */
  }
</style>


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Assessor Image</h2>

          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">


            <div class="col-lg-3 grid-margin stretch-card">

            </div>


              <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body" style="text-align: left;">

                    <div class="row">

                      <div class="col-md-6" style="border: 5px solid #27313b;text-align: center;padding:2%;">

                        <div id="my_camera" > See your frame here </div>


                      </div>

                      <div class="col-md-6" style="border: 5px solid #27313b;text-align: center;padding:2%;">
                        <div id="results"  >See Your captured Image here</div>
                      </div>

                    </div>
                    <div class="row" style="margin-top:2%;">

                      <div class="col-md-2">

                      </div>
                      <div class="col-md-8 text-center" style="text-align: center;float:none;">
                        <input id="startcamera" class="btn btn-primary btn-growwell-center" type=button value="Start Camera" onClick="configure()">
                        <input id="takesnapshot" class="btn btn-primary btn-growwell-center" type=button value="Take Snapshot" onClick="take_snapshot()">
                        <!-- <input  class="btn btn-primary btn-growwell-center" type=button value="Save Snapshot" onClick="saveSnap()"> -->
                      </div>
                      <div class="col-md-2">

                      </div>

                    </div>

                    <div  class="row" style="margin-top:2%;">

                      <div class="col-md-1">

                      </div>
                      <div class="col-md-10 text-center">
                        <form id="lastrow" method="POST" action="{{ route('assessor.img.save') }}">

                          @csrf


                                  <div class="col-md-12 text-center">
                                      <br/>
                                      <input type="hidden" name="image" class="image-tag">
                                      <input type="hidden" name="latitude" value="" id="t1">
                                      <input type="hidden" name="longitude" value="" id="t2">

                                      <div class="row" >
                                        <div class="col-md-6">
                                          <button id="retake" class="btn btn-primary btn-growwell-center">Retake</button>
                                        </div>

                                        <div class="col-md-6">
                                          <button id="finalbtn" class="btn btn-primary btn-growwell-center">Submit</button>
                                        </div>


                                      </div>

                                  </div>

                          </form>
                      </div>
                      <div class="col-md-1">

                      </div>

                    </div>









                    </div>
                  </div>
                </div>



              <div class="col-lg-3 grid-margin stretch-card">

              </div>
            </div>
        </div>


            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

        <script>
        // var x = document.getElementById("demo");
        var t1 = document.getElementById("t1");
        var t2 = document.getElementById("t2");

        // var finalbtn = document.getElementById("finalbtn");
        // var retake = document.getElementById("retake");
        //
        // var startcamera = document.getElementById("startcamera");
        // var takesnapshot = document.getElementById("takesnapshot");
        //
        // finalbtn.style.display="none";
        // retake.style.display="none";

        var lastrow = document.getElementById("lastrow");
         lastrow.style.display="none";


        function getLocation() {

          // alert("getlocation");
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {

          // alert("showPosition");
            // x.innerHTML = "Latitude: " + position.coords.latitude +
            // "<br>Longitude: " + position.coords.longitude;

            t1.value = position.coords.latitude;
            t2.value = position.coords.longitude;

        }
        </script>

        <!-- Webcam.min.js -->
        
          <script src="/theme/js/webcamjs/webcam.min.js"></script>


        <!-- Code to handle taking the snapshot and displaying it locally -->
        <script language="JavaScript">

		// Configure a few settings and attach camera
		function configure(){
			Webcam.set({
				width: 320,
				height: 240,
				image_format: 'jpeg',
				jpeg_quality: 90
			});
			Webcam.attach( '#my_camera' );
		}


		function take_snapshot() {


			// take snapshot and get image data
			Webcam.snap( function(data_uri) {
				// display results in page

        $(".image-tag").val(data_uri);


				document.getElementById('results').innerHTML =
					'<img id="imageprev" name="imageprevtag" type="file" src="'+data_uri+'"/>';


          lastrow.style.display="inline-block";

          startcamera.style.display="none";
          takesnapshot.style.display="none";




			} );

      getLocation();

			Webcam.reset();
		}

    function retake(){
      lastrow.style.display="none";

      startcamera.style.display="inline-block";
      takesnapshot.style.display="inline-block";

    }



		function saveSnap(){

      
			// Get base64 value from <img id='imageprev'> source
			var base64image =  document.getElementById("imageprev").src;
      // alert(base64image);

			 Webcam.upload( base64image, '/assessor/image/save', function(code, text) {
				 console.log('Save successfully');
				 console.log(code);
            });



  }
	</script>

    <!-- <script src="{{asset('theme/js/qtest.js')}}"></script>
    <script src="{{asset('theme/js/vanilla.js')}}"></script> -->
				<!-- partial:partials/_footer.html -->
				  @include('assessor.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
