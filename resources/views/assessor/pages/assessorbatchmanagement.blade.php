@extends('assessor.layouts.home')

@section('content')


<!-- Navbar start -->
 @include('assessor.include.navbar')
   <!-- Navbar end -->


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">ToT/ ToA dashboard<span style="float: right;"><a href=""  data-toggle="modal" data-target="#">  <button type="button" class="btn btn-primary" style="float: right;background: #4a9cdf;font-family:lato !important;font-weight: bold;">Go Back</button></a></span></h2>

          <!-- Row 1 -->

          <div class="row">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;">My Batches</h2>
                  <form class="form-sample">






                     <div class="row">
                      <div class="col-md-3">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Batch Type</label>
                          <div class="col-sm-6">
                            <select class="form-control">
                              <option>Batch Type 1</option>
                              <option>Batch Type 2</option>
                              <option>Batch Type 3</option>
                              <option>Batch Type 4</option>
                              <option>Batch Type 5</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">State</label>
                          <div class="col-sm-6">
                            <select class="form-control">
                              <option>State 1</option>
                              <option>State 2</option>
                              <option>State 3</option>
                              <option>State 4</option>
                              <option>State 5</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">District</label>
                          <div class="col-sm-6">
                            <select class="form-control">
                              <option>District 1</option>
                              <option>District 2</option>
                              <option>District 3</option>
                              <option>District 4</option>
                              <option>District 5</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Batch ID</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-md-6">
                          <div class="col-md-6">
                            <div class="template-demo">
                                <button type="button" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Apply</button>
                                <button type="button" class="btn btn-secondary">Reset</button>

                            </div>
                          </div>
                          <div class="col-md-6">

                          </div>
                      </div>
                      <div class="col-md-6">
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                    <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;">Batch Details</h2>




                    <!-- Search Form -->



                    <!-- Search form end -->
                      <!-- <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">Launch
                          modal Subscription</a>
                      </div> -->

                      @include('admin.include.createassessormodal')

                  <!-- <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p> -->
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>

                          <th>
                            Batch ID
                          </th>
                          <th>
                            Batch Type
                          </th>
                          <th>
                            Job Role Name ( QP Code)
                          </th>
                          <th>
                            Current Status
                          </th>
                          <th>
                          Status Action Date
                          </th>
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>

                          <td>
                            ashjd4565
                          </td>
                          <td>
                            Batch Type example
                          </td>

                          <td>
                            JOb45126
                          </td>
                          <td>
                            yes
                          </td>
                          <td>
                            5/5/2020
                          </td>
                          <td>
                              <a href="#" ><button type="button" class="btn btn-success" style="color:white;">View</button></a>
                          </td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
