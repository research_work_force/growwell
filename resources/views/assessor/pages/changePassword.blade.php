@extends('assessor.layouts.home')

@section('content')

 @include('assessor.include.navbar')


    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
         @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
        <div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Change Password</h2> -->

          <!-- Row 1 -->

          <div class="row">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <!-- <h2 class="card-title" style="font-size: 30px !important;Color:#4a9cdf;"></h2> -->
                  <form class="form-sample" action="{{route('assessor.changepassword.save')}}" method="post">
                    {{csrf_field()}}

                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Old Password</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="opass">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">New Password</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="npass">
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Confirm New Password</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="cnpass">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-md-6">
                          <div class="col-md-6">
                            <div class="template-demo">
                                <button type="submit" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Change</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>

                            </div>
                          </div>
                          <div class="col-md-6">

                          </div>
                      </div>
                      <div class="col-md-6">
                      </div>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">

          <!-- row 1 ends -->

        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('assessor.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->



@endsection
