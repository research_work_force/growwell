@extends('admin.layouts.home')

@section('content')

<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

<!-- Navbar start -->
 @include('assessor.include.navbar')
   <!-- Navbar end -->

    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
        <div class="content-wrapper">

          <!-- Row 1 -->

            <div class="col-12 grid-margin ">
              <div class="card collapse" id="demo">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >View Report<a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a></h2>




                  <form class="form-sample " >

                    <div class="row" style="margin-bottom:2%;">
                      <div class="col-md-6">

                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-3">Batch Id </div>
                          <div class="col-sm-9">
                            <select class="form-control" >
                              <option>Batch ID 1</option>
                              <option>Batch ID 2</option>
                              <option>Batch ID 3</option>

                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4 ">Job Role</div>
                          <div class="col-sm-8">
                            <select class="form-control" >
                              <option>Job Role 1</option>
                              <option>Job Role 2</option>


                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4"><label class=" col-form-label">Start Date</label></div>
                          <div class="col-sm-8">
                            <input type="date" style="padding:5%;color:grey;">
                          </div>


                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4"><label class=" col-form-label">End Date</label></div>
                          <div class="col-sm-8">
                            <input type="date" style="padding:5%;color:grey;">
                          </div>


                        </div>
                      </div>

                    </div>







                    <div class="row">

                      <div class="col-md-4">
                        <button type="button" class="btn btn-primary btn-growwell-left">Search Report</button>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" > Report Details<a href="#demo" class="btn btn-primary btn-growwell" data-toggle="collapse" >Search Report </a></h2>





                    <div class="table-responsive">
                       <table class="table table-growwell">
                        <thead>
                          <tr>
                            <th>
                              Scheme/ Program/ Model
                            </th>
                            <th>
                              Sector
                            </th>
                            <th>
                              Batch Type
                            </th>
                            <th>
                              Batch ID
                            </th>
                            <th>
                              Job Role
                            </th>
                            <th>
                              Language
                            </th>
                            <th>
                              Batch Size
                            </th>
                            <th>
                              Training center location
                            </th>
                            <th>
                              Enrolled Canditates
                            </th>
                            <th>
                              Dropout
                            </th>
                            <th>
                              Action Date
                            </th>
                            <th>
                              Received From SSC On
                            </th>
                            <th>
                              Assessment Date(s)
                            </th>
                            <th>
                              Student Attendance(%)
                            </th>
                            <th>
                              Passed(%)
                            </th>
                            <th>
                              Failed(%)
                            </th>
                            <th>
                              Status
                            </th>
                            <th>
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>

                          @if(!$batch_data->isEmpty())

                          @foreach($batch_data as $data)
                          <tr>
                            <td >
                              {{ $data->schemeProgram }}
                            </td>
                            <td>
                             {{ $data->sectors }}
                            </td>
                            <td>
                              {{ $data->batchTypes }}
                            </td>
                            <td>
                              {{ $data->batch_id }}
                            </td>
                            <td>
                              {{ $data->jobRole }}
                            </td>
                            <td>
                              {{ $data->language }}
                            </td>
                            <td>
                              {{ $data->batch_size }}
                            </td>
                            <td>
                              {{ $data->training_center_location }}
                            </td>


                            
                          @foreach($enrolled_candidate_array as $key => $enroll)
                          @if($key == $data->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif

                          @endforeach
                          @foreach($dropout_candidate_array as $key => $enroll)
                          @if($key == $data->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif
                          @endforeach



                            <td>
                              {{ $data->action_date }}

                            </td>
                            <td>
                              {{ $data->received_from_ssc }}
                            </td>
                            <td>
                              {{ $data->assessment_date_fdt }} - {{ $data->assessment_date_fdt }}
                            </td>
                            <td>
                               @php
                               $attendence_percentage = App\Http\Controllers\StaticValueProviderController::getBatchWiseStudentAttendencePercentage($data->batch_id);
                               $formated_attendence_percentage = number_format($attendence_percentage, 2, '.', '')

                               @endphp

                               {{$formated_attendence_percentage}}
                            </td>
                            <td>
                               @php
                               $passed_percentage = App\Http\Controllers\StaticValueProviderController::getBatchWiseStudentPassedPercentage($data->batch_id);
                                $formated_passed_percentage = number_format($passed_percentage, 2, '.', '')
                               @endphp

                               {{$formated_passed_percentage}}
                            </td>
                            <td>
                               @php
                               $failed_percentage = App\Http\Controllers\StaticValueProviderController::getBatchWiseStudentFailedPercentage($data->batch_id);
                                $formated_failed_percentage = number_format($failed_percentage, 2, '.', '')
                               @endphp

                               {{$formated_failed_percentage}}
                            </td>
                            <td>
                              @if($data->assessed_status == 0)
                                 Not Sent for review
                              @else
                                 Sent for review
                              @endif
                            </td>
                            <td>
                              <div>
                                <div class="dropdown">
                                    <a href="{{route('assessor.student.report',[$data->batch_id])}}" type="button" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;">View</a>

                                </div>

                              </div>











                            </td>
                          </tr>
                          @endforeach

                          @endif
                        </tbody>
                      </table>
                      <nav aria-label="Page navigation example" >
                        {{ $batch_data->render("pagination::bootstrap-4") }}
                    </nav>
                    </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->
        </div>
        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>


@endsection
