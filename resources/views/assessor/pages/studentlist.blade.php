@extends('assessor.layouts.home')

@section('content')


<!-- Navbar start -->
 @include('assessor.include.navbar')
   <!-- Navbar end -->


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Assessment Request Management<span style="float: right;"><a href=""  data-toggle="modal" data-target="#">  <button type="button" class="btn btn-primary" style="float: right;background: #4a9cdf;font-family:lato !important;font-weight: bold;">Go Back</button></a></span></h2> -->

          <!-- Row 1 -->

        
          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card" >
                <div class="card-body ">

                    <h2 class="card-title growwell-card-title" >Student List</h2>

                  <div class="table-responsive">
                    <table id="assessortablereq" class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                            Username
                          </th>
                          <th>
                            Name 
                          </th>
                          <th>
                            Address
                          </th>
                          <th>
                            SPOC
                          </th>
                          <th>
                             SPOC Email
                          </th>
                          <th>
                            SPOC Phone
                          </th>
                          <th>
                            Batch Id
                          </th>

                          <th>
                            Action
                          </th> 
                        </tr>
                      </thead>

                      <tbody>
                     

                        @foreach($student_data as $batch)
                        <tr>
                          <td >
                            {{ $batch->username }}
                          </td>
                          <td>
                            {{ $batch->name }}
                          </td>
                          <td>
                            {{ $batch->address }}
                          </td>
                          <td>
                            {{ $batch->spoc }}
                          </td>
                          <td>
                           {{ $batch->spoc_email }}
                          </td>
                          <td>
                            {{ $batch->spoc_phone }}
                          </td>
                          <td>
                            {{ $batch->batch_id }}
                          </td>
                          
                          <td>

                            
                            <div class="template-demo">

                                <a href="/test/page/{{$batch->username}}/{{$batch->batch_id}}" class="btn btn-primary">Test</a>

                            </div>

                            <div class="template-demo">

                                <button type="button" data-toggle="modal" data-target="#sendtesturl{{$batch->batch_id}}" class="btn btn-danger" style="COLOR:WHITE;" >Send Link</button>
                                @include('assessor.include.sendtesturlmodal',['batch_id' => $batch->batch_id,'stdnt_uname'=>$batch->username])

                            </div>
                            
                            
                          </td>
                        </tr>
                        @endforeach
                        

                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
