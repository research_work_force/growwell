@extends('assessor.layouts.home')

@section('content')

<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>



<!-- Navbar start -->
 @include('assessor.include.navbar')
   <!-- Navbar end -->

    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
        <div class="content-wrapper">

          <!-- Row 1 -->

            <div class="col-12 grid-margin ">
              <div class="card collapse" id="demo">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >View Report<a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a></h2>




                  <form class="form-sample " >

                    <div class="row" style="margin-bottom:2%;">
                      <div class="col-md-6">

                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-3">Batch Id </div>
                          <div class="col-sm-9">
                            <select class="form-control" >
                              <option>Batch ID 1</option>
                              <option>Batch ID 2</option>
                              <option>Batch ID 3</option>

                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4 ">Job Role</div>
                          <div class="col-sm-8">
                            <select class="form-control" >
                              <option>Job Role 1</option>
                              <option>Job Role 2</option>


                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4">Assessor Id </div>
                          <div class="col-sm-8">
                            <select class="form-control" >
                              <option>Assessor 1</option>
                              <option>Assessor 2</option>
                              <option>Assessor 3</option>

                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-5">Assessor Name </div>
                          <div class="col-sm-7">
                            <input type="text" style="padding:4%;color:grey;">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">

                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4"><label class=" col-form-label">Start Date</label></div>
                          <div class="col-sm-8">
                            <input type="date" style="padding:5%;color:grey;">
                          </div>


                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4"><label class=" col-form-label">End Date</label></div>
                          <div class="col-sm-8">
                            <input type="date" style="padding:5%;color:grey;">
                          </div>


                        </div>
                      </div>
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">

                      </div>

                    </div>





                    <div class="row">

                      <div class="col-md-4">
                        <button type="button" class="btn btn-primary btn-growwell-left">Search Report</button>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" > Marks Details</h2>

                    <div id="here" class="table-responsive">
                      <table class="table table-growwell">
                        <thead>
                          <tr>
                            <th>
                              Quetion ID
                            </th>
                            <th>
                              Section
                            </th>
                            <th>
                              Chosen Option
                            </th>
                            <th>
                              Right Option
                            </th>
                            <th>
                              Action
                            </th>
                          </tr>
                        </thead>
                        <tbody>

                         @foreach($questions_student_test_wise as $ques)
                          <tr>
                            <td >
                              {{$ques->q_id}}
                            </td>
                            <td>
                              {{$ques->section_id}}
                            </td>
                            <td>
                              <select class="form-control" id="choosenoptionselect{{$ques->q_id}}" style="display:none;">
                              @if($ques->answer == $ques->option1)
                                <option value="{{$ques->option1}}" selected>Option 1</option>
                                <option value="{{$ques->option2}}">Option 2</option>
                                <option value="{{$ques->option3}}">Option 3</option>
                                <option value="{{$ques->option4}}">Option 4</option>
                              @elseif($ques->answer == $ques->option2)
                                <option value="{{$ques->option1}}">Option 1</option>
                                <option value="{{$ques->option2}}" selected>Option 2</option>
                                <option value="{{$ques->option3}}">Option 3</option>
                                <option value="{{$ques->option4}}">Option 4</option>
                               @elseif($ques->answer == $ques->option3)
                                <option value="{{$ques->option1}}">Option 1</option>
                                <option value="{{$ques->option2}}">Option 2</option>
                                <option value="{{$ques->option3}}" selected>Option 3</option>
                                <option value="{{$ques->option4}}">Option 4</option>
                              @elseif($ques->answer == $ques->option4)
                                <option value="{{$ques->option1}}">Option 1</option>
                                <option value="{{$ques->option2}}">Option 2</option>
                                <option value="{{$ques->option3}}">Option 3</option>
                                <option value="{{$ques->option4}}" selected>Option 4</option>
                              @else
                                <option value="{{$ques->option1}}">Option 1</option>
                                <option value="{{$ques->option2}}">Option 2</option>
                                <option value="{{$ques->option3}}">Option 3</option>
                                <option value="{{$ques->option4}}">Option 4</option>
                              @endif


                              </select>
                              @if($ques->answer == $ques->option1)
                                <span id="choosenoptionspan{{$ques->q_id}}">{{$ques->option1}}</span>
                              @elseif($ques->answer == $ques->option2)
                                <span id="choosenoptionspan{{$ques->q_id}}">{{$ques->option2}}</span>
                               @elseif($ques->answer == $ques->option3)
                               <span id="choosenoptionspan{{$ques->q_id}}">{{$ques->option3}}</span>
                              @elseif($ques->answer == $ques->option4)
                              <span id="choosenoptionspan{{$ques->q_id}}">{{$ques->option4}}</span>
                              @else
                              <!-- <span id="choosenoptionspan">b</span> -->
                              @endif
                              
                            </td>
                            <td>

                              @if($ques->right_answer == $ques->option1)
                                    Option 1
                              @elseif($ques->right_answer == $ques->option2)
                                   Option 2
                               @elseif($ques->right_answer == $ques->option3)
                                   Option 3
                              @elseif($ques->right_answer == $ques->option4)
                                 Option 4
                              @else
                              NA
                              @endif
                            </td>

                            <td>
                              <div>
                                <div class="dropdown">
                                    <a href="#" id="editoption{{$ques->q_id}}" type="button" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;" onclick="edit({!! json_encode($ques->q_id) !!})">Edit</a>

                                    <a href="#" id="saveoption{{$ques->q_id}}" type="button" class="btn btn-success btn-growwell-success" style="COLOR:WHITE; display:none;" onclick="save({!! json_encode($ques->q_id) !!})">Save</a>

                                </div>

                              </div>

                            </td>

                            <script>

function edit(q_id) {

  var x = document.getElementById("choosenoptionselect"+q_id);   // Get the element with id="demo"
  x.style.display = "block";
  var y = document.getElementById("choosenoptionspan"+q_id);   // Get the element with id="demo"
  y.style.display = "none";

  var z = document.getElementById("editoption"+q_id);   // Get the element with id="demo"
  z.style.display = "none";

  var a = document.getElementById("saveoption"+q_id);   // Get the element with id="demo"
  a.style.display = "block";


}

function save(q_id) {

  std_id = {!! json_encode($ques->s_id) !!}
  batch_id = {!! json_encode($ques->batch_id) !!}
  
  var x = document.getElementById("choosenoptionselect"+q_id).selectedIndex;
  var y = document.getElementById("choosenoptionselect"+q_id).options;
  var right_answer = y[x].value;


  var x = document.getElementById("choosenoptionselect"+q_id);   // Get the element with id="demo"
  x.style.display = "none";
  var y = document.getElementById("choosenoptionspan"+q_id);   // Get the element with id="demo"
  y.style.display = "block";

  var z = document.getElementById("editoption"+q_id);   // Get the element with id="demo"
  z.style.display = "block";

  var a = document.getElementById("saveoption"+q_id);   // Get the element with id="demo"
  a.style.display = "none";

  var datastr = 'std_id='+std_id + '&batch_id='+batch_id + '&q_id='+q_id+ '&right_answer='+right_answer;

   $.ajax({
     headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
              type:'POST',
              url:'/assessor/updated/ques/ans',
              data:datastr,

              success:function(data) {
                console.log(data);
                 $( "#here" ).load(window.location.href + " #here" );
              },
              error: function(errMsg) {
                console.log(errMsg);
        }
           });

}
</script>
                          </tr>
                     
                          @endforeach
                        </tbody>
                      </table>
                      <nav aria-label="Page navigation example" >

                    </nav>
                    </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->
        </div>
        </div>
 
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>


@endsection
