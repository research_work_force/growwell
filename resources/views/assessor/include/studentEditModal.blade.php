

       <div class="modal fade" id="editmodal{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="/student/edit" method="post">
              {{csrf_field()}}
              <div class="modal-header">
                <p>Edit student details</p>
                
              </div>
            <div class="modal-body">
              
               <input type="hidden" name="id" value="{{$item->id}}">

              <div class="form-group">                                
              <label for="exampleInputEmail1">Name</label>          
              <input type="text" class="form-control" name="name" value="{{$item->name}}" required="">
            </div>


              <div class="form-group"> 
              <label >Address</label>
              <input type="text" class="form-control" name="address" value="{{$item->address}}" required="">
              </div>

             

                 <div class="form-group">
               <label >SPOC</label>
              <input type="text" class="form-control" name="spoc" value="{{$item->spoc}}" required="">
            </div>

            <div class="form-group">
              <label>SPOC Email</label>
              <input type="text" class="form-control" name="spoc_email" value="{{$item->spoc_email}}" required="">
              </div>

                <div class="form-group">
              <label >SPOC phone</label>
              <input type="text" class="form-control" name="spoc_phone" value="{{$item->spoc_phone}}" required="">
            </div>

            </div>
            <div class="modal-footer">      
             
           
             <button type="submit" class="btn btn-primary">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->