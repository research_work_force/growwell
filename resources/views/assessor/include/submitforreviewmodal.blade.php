

       <div class="modal fade" id="submitforreviewmodal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Submit for Review</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
            <form action="{{route('assessor.review')}}" method="post">
              {{csrf_field()}}
            <div class="modal-body">
               
              <input type="hidden" name="batch_id" value="{{$batch_id}}">
            </div>
            <div class="modal-footer">
 
              
              <button type="submit" class="btn btn-danger">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->