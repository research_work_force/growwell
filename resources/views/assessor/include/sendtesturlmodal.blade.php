<div class="modal fade" id="sendtesturl{{$batch_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Send Test URL to Student</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div> 

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="{{route('student.test.url')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="batch_id" value="{{$batch_id}}">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">From Date</label>
                          <div class="col-sm-9">
                              <input type="date" class="form-control" name="fdt" required>
                          </div>
                        </div>
                      </div>
 
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">To Date</label>
                          <div class="col-sm-9">
                             <input type="date" class="form-control" name="tdt" required>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">From Time</label>
                          <div class="col-sm-9">
                              <input type="time" class="form-control" name="ftime" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">To Time</label>
                          <div class="col-sm-9">
                             <input type="time" class="form-control" name="ttime" required>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">


                       <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-growwell" style="COLOR:WHITE;font-family:lato !important;font-weight: bold;">Send</button>
                      </div>
                     

                    </div>


                    <div class="row">

                      <div class="col-md-4">

                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>

                  

      <!--Footer-->
      <div class="modal-footer justify-content-center">


      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
</div>
