<div class="modal fade" id="createstudentmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#4a9cdf;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create Student</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>    

      <!--Body-->
      <div class="modal-body"> 
        <form class="form-sample" action="/student/insert" method="POST">
          {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          
                            
                         
                          <label class="col-sm-3 col-form-label">Name</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="name" required="">
                            
                          </div>
                        </div>
                      </div>

                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Address</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" required="">
                          </div>
                        </div>
                      </div>
                      
                    </div>
                    
                    <!-- <p class="card-description">
                      Address
                    </p> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="spoc" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Email</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="spoc_email" required="">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Phone</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="spoc_phone" required="">
                          </div>
                        </div>
                      </div>


                    </div>


                    <div class="row">

                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
        

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Create</button>

         </form>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
