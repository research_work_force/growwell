

       <div class="modal fade" id="reqrejectmodal{{$id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="{{route('assessor.req.reject')}}" method="post">
              {{csrf_field()}}
            <div class="modal-body">
              <p>Are you sure?</p>
            </div>
            <div class="modal-footer">

              <input type="hidden" name="id" value="{{$id}}">
              <button type="submit" class="btn btn-danger">Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->