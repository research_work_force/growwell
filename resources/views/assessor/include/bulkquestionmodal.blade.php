<div class="modal fade" id="bulkquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#4a9cdf;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Bulk Question Upload</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">


          <!-- id="dropzoneForm" class="dropzone" -->
             


        <form  action="/question/bulkupload" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}   
                 
              
                <input type="file" name="file" class="form-control">  

                               
                                  
        
         


      </div>      
                   

      <!--Footer-->
      <div class="modal-footer">

        <button type="submit" id="submit-all" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Upload</button> 
        </form>      

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
