<div class="modal fade" id="assessorassignmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#4a9cdf;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Assign to Assessor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor ID</label>
                          <div class="col-sm-9">
                             <select class="form-control">
                              <option>1</option>
                              <option>2</option>
                              <option>3</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Date</label>
                          <div class="col-sm-9">
                           <input type="date" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">


                       <div class="col-md-6">
                        <button type="button" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Search Assessor</button>
                      </div>
                      <div class="col-md-6">

                     </div>

                    </div>


                    <div class="row">

                      <div class="col-md-4">

                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>

                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>
                            Name
                          </th>
                          <th>
                            Type
                          </th>
                          <th>
                           Action
                          </th>

                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          </td>
                          <td>
                            Agriculture skill council of India
                          </td>

                          <td>
                              <a href=""  data-toggle="modal" data-target="#createbatchmodal"><button type="button" class="btn btn-success" style="color:white;">Assign</button></a>
                          </td>
                        </tr>
                         <tr>
                          <td >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          </td>
                          <td>
                            Agriculture skill council of India
                          </td>

                          <td>
                              <a href=""  data-toggle="modal" data-target="#createbatchmodal"><button type="button" class="btn btn-success" style="color:white;">Assign</button></a>
                          </td>
                        </tr>
                         <tr>
                          <td >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          </td>
                          <td>
                            Agriculture skill council of India
                          </td>

                          <td>
                              <a href=""  data-toggle="modal" data-target="#createbatchmodal"><button type="button" class="btn btn-success" style="color:white;">Assign</button></a>
                          </td>
                        </tr>
                         <tr>
                          <td >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          </td>
                          <td>
                            Agriculture skill council of India
                          </td>

                          <td>
                              <a href=""  data-toggle="modal" data-target="#createbatchmodal"><button type="button" class="btn btn-success" style="color:white;">Assign</button></a>
                          </td>
                        </tr>
                         <tr>
                          <td >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          </td>
                          <td>
                            Agriculture skill council of India
                          </td>

                          <td>
                              <a href=""  data-toggle="modal" data-target="#createbatchmodal"><button type="button" class="btn btn-success" style="color:white;">Assign</button></a>
                          </td>
                        </tr>
                         <tr>
                          <td >
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          </td>
                          <td>
                            Agriculture skill council of India
                          </td>

                          <td>
                              <a href=""  data-toggle="modal" data-target="#createbatchmodal"><button type="button" class="btn btn-success" style="color:white;">Assign</button></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">


      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
</div>
