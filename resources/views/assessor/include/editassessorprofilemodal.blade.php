<div class="modal fade" id="editassessorprofile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Edit your profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="{{ route('assessor.profile.update')}}" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Name of Assessor</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="name" value="{{$assessor_data->name}}" required="">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Gender</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="gender">
                              @if($assessor_data->gender == "Male")
                              <option value="Male" selected>Male</option>
                              <option value="Female">Female</option>
                              <option value="Other">Other</option>
                              @elseif($assessor_data->gender == "Female")
                              <option value="Male">Male</option>
                              <option value="Female" selected>Female</option>
                              <option value="Other">Other</option>
                              @else
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                              <option value="Other" selected>Other</option>
                              @endif
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Religion</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="religion" value="{{$assessor_data->religion}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Date of Birth</label>
                          <div class="col-sm-9">
                           <input type="date" class="form-control" name="date" value="{{$assessor_data->date_of_birth}}" required="">
                          </div>
                        </div>


                      </div>
                    </div>
                    <!-- <p class="card-description">
                      Address
                    </p> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Language Known</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="Language" value="{{$assessor_data->language_known}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Mobile no</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" value="{{$assessor_data->phone}}" name="phone">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email Address</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" value="{{$assessor_data->email}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor Address</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="Address" value="{{$assessor_data->address}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Nearby Landmark</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="landmark" value="{{$assessor_data->landmark}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Pincode</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="pin" value="{{$assessor_data->pincode}}" required="">
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State/Union Territory</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="state" required="" value="{{$assessor_data->state}}">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">District/City</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="city" value="{{$assessor_data->city}}" required="">
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Education Details</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="education" value="{{$assessor_data->education}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Added Industrial Experience Details</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="experience" value="{{$assessor_data->industrial_experience}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Applicant Type</label>
                          <div class="col-sm-9">
                              <select class="form-control" name="applicant_type">
                              @if($assessor_data->applicant_type == "Freelancer")
                              <option value="Freelancer" selected>Freelancer</option>
                              <option value="Permanent">Permanent</option>
                              @elseif($assessor_data->applicant_type == "Permanent")
                              <option value="Freelancer" >Freelancer</option>
                              <option value="Permanent" selected>Permanent</option>
                              @else
                              <option value="Freelancer" >Freelancer</option>
                              <option value="Permanent" >Permanent</option>
                              @endif
                            </select>

                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Profile Image</label>
                          <div class="col-sm-9">
                             <input type="file" class="form-control" name="profile_img" accept=".jpg,.png">
                          </div>
                        </div>
                      </div>
                    </div>



                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sectors</label>
                          <div class="col-sm-9">
                             @php
                               $sectors = App\Http\Controllers\StaticValueProviderController::sectors();

                            @endphp
                            <select class="form-control" name="sectors">
                             @foreach($sectors as $sector)
                               @if($sector == $assessor_data->industrial_experience)
                              <option value="{{ $sector->name }}" selected>{{ $sector->name }}</option>
                              @else
                              <option value="{{ $sector->name }}">{{ $sector->name }}</option>
                              @endif
                             @endforeach
                            </select>

                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessment Agency Aligned</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="assessment_agency_aligned" value="{{$assessor_data->assessment_agency_aligned}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Linking Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="linking_type">
                              @if($assessor_data->linking_type == "On Role")
                              <option value="On Role" selected>On Role</option>
                              <option value="Freelancer">Freelancer</option>
                              @else
                              <option value="On Role">On Role</option>
                              <option value="Freelancer" selected>Freelancer</option>
                              @endif
                            </select>

                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Availability</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="availability" value="{{$assessor_data->availability}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>



                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Duration</label>
                          <div class="col-sm-9">
                            <input type="date" class="form-control" name="duration_fdt" value="{{$assessor_data->duration_fdt}}" required="">

                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-9">
                            <input type="date" class="form-control" name="duration_tdt" value="{{$assessor_data->duration_tdt}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">TOA Status</label>
                          <div class="col-sm-9">
                              <select class="form-control" name="toa_status" required>
                              @if($assessor_data->toa_status == "Certified")
                                <option value="Certified" selected>Certified</option>
                              <option value="Provisional">Provisional</option>
                              @else
                                <option value="Certified">Certified</option>
                              <option value="Provisional" selected>Provisional</option>
                              @endif
                            </select>

                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Domain Job Role</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="domain_job_role" value="{{$assessor_data->domain_job_role}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>


      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="Submit" class="btn btn-primary" style="background: #4a9cdf;font-family:lato !important;font-weight: bold;">Submit</button>
        </form>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
