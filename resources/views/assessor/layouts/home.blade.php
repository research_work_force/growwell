 <!DOCTYPE html> 
<html lang="en">
      @include('assessor.include.headerLinks')
  <body>
    <div class="container-scroller">

    
    @yield('content')

    </div>
		<!-- container-scroller -->
      @include('assessor.include.footerLinks')
  </body>
</html>