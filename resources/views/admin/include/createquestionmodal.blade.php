<div class="modal fade" id="createquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create a Question</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
<style>
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #27313b;
  font-family: 'Lato';
  color: White; 
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: White;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: White;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border-top: none;
}
</style>
      <!--Body-->
      <div class="modal-body">
         <form class="form-sample" action="{{route('question.en.save')}}" method="POST">
          {{csrf_field()}}
       <!-- Tab links -->
        <div class="tab">
          <button class="tablinks" onclick="openCity(event, 'English')" id="defaultOpen">English</button>
          <button class="tablinks" onclick="openCity(event, 'Hindi')">Hindi</button>
          <button class="tablinks" onclick="openCity(event, 'Teleugu')">Teleugu</button>
          <button class="tablinks" onclick="openCity(event, 'Assamese')">Assamese</button>
        </div>

        <!-- Tab content -->
        <div id="English" class="tabcontent">
           <div class="row">

                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Question</label>
                          <div class="col-sm-10">
                           <textarea name="question" class="form-control" ></textarea>
                          </div>

                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 1</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option1" >
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 2</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option2">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 3</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option3" >
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 4</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option4" >
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Right Answer</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="right_answer">
                              <option value="option1">Option 1</option>
                              <option value="option2">Option 2</option>
                              <option value="option3">Option 3</option>
                              <option value="option4">Option 4</option>
                             </select>
                            
                          </div>
                        </div>

                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                         @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="job_role" required>
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sections</label>
                          <div class="col-sm-9">
                            @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                              <option value="{{ $section->name }}">{{ $section->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                      </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Question ID</label>
                          <div class="col-sm-9">
                             <input type="number" class="form-control" name="ques_id" required="">
                          </div>
                        </div>

                      </div>
                    </div>
                   

                       <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
                    </form>

    </div>
        

        <div id="Hindi" class="tabcontent">
             <form class="form-sample" action="{{route('question.hi.save')}}" method="POST">
          {{csrf_field()}}
                <div class="row">

                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Question-Hindi</label>
                          <div class="col-sm-10">
                           <textarea name="question_hindi" class="form-control"></textarea>
                          </div>

                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 1-Hindi</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option1_hindi">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 2-Hindi</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option2_hindi">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 3-Hindi</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option3_hindi">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 4-Hindi</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option4_hindi">
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Right Answer-Hindi</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="right_ans_hindi">
                              <option value="option1_hindi">Option 1</option>
                              <option value="option2_hindi">Option 2</option>
                              <option value="option3_hindi">Option 3</option>
                              <option value="option4_hindi">Option 4</option>
                             </select>
                          </div>
                        </div>

                      </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                         @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="job_role" required>
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sections</label>
                          <div class="col-sm-9">
                            @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                              <option value="{{ $section->name }}">{{ $section->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                      </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Question ID</label>
                          <div class="col-sm-9">
                             <input type="number" class="form-control" name="ques_id" required="">
                          </div>
                        </div>

                      </div>
                    </div>
                   

                       <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
                    </form>

        </div>
        
        <div id="Teleugu" class="tabcontent">
             <form class="form-sample" action="{{route('question.tl.save')}}" method="POST">
          {{csrf_field()}}
          <div class="row">

                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Question-Teleugu</label>
                          <div class="col-sm-10">
                           <textarea name="question_teleugu" class="form-control"></textarea>
                          </div>

                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 1-Teleugu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option1_teleugu">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 2-Teleugu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option2_teleugu">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 3-Teleugu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option3_teleugu">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 4-Teleugu</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option4_teleugu">
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Right Answer-Teleugu</label>
                          <div class="col-sm-9">
                              <select class="form-control" name="right_ans_teleugu">
                              <option value="option1_teleugu">Option 1</option>
                              <option value="option2_teleugu">Option 2</option>
                              <option value="option3_teleugu">Option 3</option>
                              <option value="option4_teleugu">Option 4</option>
                             </select>
                          </div>
                        </div>

                      </div>
                    
                   <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                         @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="job_role" required>
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sections</label>
                          <div class="col-sm-9">
                            @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                              <option value="{{ $section->name }}">{{ $section->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                      </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Question ID</label>
                          <div class="col-sm-9">
                             <input type="number" class="form-control" name="ques_id" required="">
                          </div>
                        </div>

                      </div>
                    </div>
                   

                       <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
                    </form>
        </div>

         <div id="Assamese" class="tabcontent">
             <form class="form-sample" action="{{route('question.as.save')}}" method="POST">
          {{csrf_field()}}
          <div class="row">

                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Question-Assamese</label>
                          <div class="col-sm-10">
                           <textarea name="question_assam" class="form-control"></textarea>
                          </div>

                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 1-Assamese</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option1_assam">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 2-Assamese</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option2_assam">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 3-Assamese</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option3_assam">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-6">
                            <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Option 4-Assamese</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="option4_assam">
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Right Answer-Assamese</label>
                          <div class="col-sm-9">
                              <select class="form-control" name="right_ans_assam">
                              <option value="option1_assam">Option 1</option>
                              <option value="option2_assam">Option 2</option>
                              <option value="option3_assam">Option 3</option>
                              <option value="option4_assam">Option 4</option>
                             </select>
                          </div>
                        </div>

                      </div>
                    
                <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                         @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="job_role" required>
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sections</label>
                          <div class="col-sm-9">
                            @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                              <option value="{{ $section->name }}">{{ $section->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                      </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Question ID</label>
                          <div class="col-sm-9">
                             <input type="number" class="form-control" name="ques_id" required="">
                          </div>
                        </div>

                      </div>
                    </div>
                   

                       <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
                    </form>
        </div>

      </div>
        <!--Footer-->
      <div class="modal-footer justify-content-center">
      
   </div>
    </div>
    <!--/.Content-->
  </div>
</div>

<script>
  document.getElementById("defaultOpen").click();

  function openCity(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>