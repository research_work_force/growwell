<div class="modal fade" id="changeassessorassign{{$bdata->batch_id}}modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Assign to Assessor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button> 
      </div>

      <div class="modal-body">
        <form class="form-sample" action="{{ route('assessor.batch.assign')}}" method="post">
          {{csrf_field()}}
            <input type="hidden" name="batch_id" value="{{$bdata->batch_id}}"/>
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor ID</label>
                          <div class="col-sm-9">
                                @php
                               $assessor = App\Http\Controllers\StaticValueProviderController::getAssessorsList();

                                  @endphp
                                  <select class="form-control" name="assessor_id" required>
                                   @foreach($assessor as $sector)
                                    <option value="{{ $sector->assessor_id }}">{{ $sector->name }} [{{ $sector->assessor_id }}]</option>
                                   @endforeach

                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Date</label>
                          <div class="col-sm-9">
                           <input type="date" id="txtDateAssign{{$bdata->batch_id}}" class="form-control"  name="assigned_date" required  />
                          </div>
                        </div> 
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Test Duration (In hours)</label>
                          <div class="col-sm-6">

                           <select class="form-control" name="test_duration" required >
                              <option value="15">15 mnts</option>
                              <option value="30">30 mnts</option>
                              <option value="45">45 mnts</option>
                              <option value="60">60 mnts</option>
                              <option value="75">75 mnts</option>
                              <option value="90">90 mnts</option>
                              <option value="105">105 mnts</option>
                           </select>
                          </div>
                        </div>
                      </div>
                    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

    $(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;
    $('#txtDateAssign{{$bdata->batch_id}}').attr('min', maxDate);
});
</script>
                      <div class="row">
                       <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Assign</button>
                      </div>
                      <div class="col-md-6">

                     </div>

                    </div>


                    <div class="row">

                      <div class="col-md-4">

                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>


    </div>
    <!--/.Content-->
  </div>
</div>
</div>
