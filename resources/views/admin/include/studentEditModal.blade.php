       <div class="modal fade" id="editmodal{{$item->id}}">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="/student/edit" method="post">
              {{csrf_field()}}
              <div class="modal-header">
                <p>Edit student details</p>

              </div>
            <div class="modal-body">

               <input type="hidden" name="id" value="{{$item->id}}">

              <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" class="form-control" name="name" value="{{$item->name}}" required="">
            </div>


              <div class="form-group">
              <label >Address</label>
              <input type="text" class="form-control" name="address" value="{{$item->address}}" required="">
              </div>



                 <div class="form-group">
               <label >SPOC</label>
              <input type="text" class="form-control" name="spoc" value="{{$item->spoc}}" >
            </div>

            <div class="form-group">
              <label>SPOC Email</label>
              <input type="text" class="form-control" name="spoc_email" value="{{$item->spoc_email}}" >
              </div>

             <div class="form-group">
              <label >SPOC phone</label>
              <input type="text" class="form-control" maxlength="10" minlength="10" name="spoc_phone" value="{{$item->spoc_phone}}" >
             </div>

             <div class="form-group">
              <label >Batch ID</label>
                            @php

                               $bats = App\Http\Controllers\StaticValueProviderController::batcheids();

                            @endphp
                            <select class="form-control" name="batch_id" required>
                             @foreach($bats as $sprog)
                                 @if($sprog->batch_id == $item->batch_id)
                                   <option value="{{ $sprog->batch_id }}" selected>{{ $sprog->batch_id }}</option>
                                 @else
                                   <option value="{{ $sprog->batch_id }}">{{ $sprog->batch_id }}</option>
                                 @endif
                             @endforeach
                            </select>
              <!-- //editing end -->
                     </div>
                            <div class="form-group">
                              <label >Dropout</label>
                              <select class="form-control" name="dropout_status" required>
                                     @if('1' == $item->dropout_status)
                                           <option value="1" selected>Yes</option>
                                           <option value="0" >No</option>
                                         @else
                                          <option value="1" >Yes</option>
                                           <option value="0" selected >No</option>
                                         @endif
                                         </select>
                             </div>

            </div>
            <div class="modal-footer">


             <button type="submit" class="btn btn-primary btn-growwell">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
