<div class="modal fade" id="editsectionmodal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Edit Section</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">




        <form  action="question/sections/edit" method="POST" >
          @csrf

<input name="section_id" type="hidden" value="{{ $id }}">
                 <div class="row">
                      <div class="col-md-8">
                        <div class="form-group row">
                          <div class="col-sm-8">
                            <input type="text" name="section_name" value="{{ $name }}" class="form-control" required>
                          </div>
                        </div>
                      </div>

                    </div>





      </div>


      <!--Footer-->
      <div class="modal-footer">

        <button type="submit" id="submit-all" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Edit</button> 
        </form>


      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
