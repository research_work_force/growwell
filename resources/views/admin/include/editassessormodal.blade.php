<div class="modal fade" id="editAssessormodal{{$assessor_data->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Edit your profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="{{ route('admin.assessor.update')}}" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
              <input type="hidden" name="id" value="{{$assessor_data->id}}" required/>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Name of Assessor</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" name="name" value="{{$assessor_data->name}}" required="">
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Assessment Agency Aligned</label>
                          <div class="col-sm-6">
                           <input type="text" class="form-control" name="assessment_agency_aligned" value="{{$assessor_data->assessment_agency_aligned}}" required="">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Linking Type</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="linking_type">
                              @if($assessor_data->linking_type == "On Role")
                              <option value="On Role" selected>On Role</option>
                              <option value="Freelancer">Freelancer</option>
                              @elseif($assessor_data->linking_type == "Freelancer")
                              <option value="On Role" >On Role</option>
                              <option value="Freelancer" selected>Freelancer</option>
                              @else
                              <option value="On Role" >On Role</option>
                              <option value="Freelancer" >Freelancer</option>
                              @endif
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Mobile No.</label>
                          <div class="col-sm-9">
                           <input type="number" maxlength="10" minlength="10" class="form-control" name="phone" value="{{$assessor_data->phone}}" required="">
                          </div>
                        </div>


                      </div>
                    </div>
                    <!-- <p class="card-description">
                      Address
                    </p> -->
                   <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Duration</label>
                          <div class="col-sm-9">
                           <input type="date" class="form-control" name="fdt" value="{{$assessor_data->duration_fdt}}"  required>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-9">
                            <input type="date" class="form-control" name="tdt" value="{{$assessor_data->duration_tdt}}" required>
                          </div>
                        </div>


                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email Address</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" value="{{$assessor_data->email}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sectors</label>
                          <div class="col-sm-9">
                            @php
                               $sectors = App\Http\Controllers\StaticValueProviderController::sectors();

                            @endphp
                            <select class="form-control" name="sectors" required>
                             @foreach($sectors as $sector)
                              @if($sector->name == $assessor_data->sectors)
                              <option value="{{ $sector->name }}" selected>{{ $sector->name }}</option>
                              @else
                              <option value="{{ $sector->name }}">{{ $sector->name }}</option>
                              @endif
                             @endforeach
                           </select>
                          </div>
                        </div>
                      </div>
                    </div>


                 <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Domain Job Role</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="domain_job_role" value="{{$assessor_data->domain_job_role}}" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">TOA status</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="toa_status">
                              @if($assessor_data->toa_status == "Certified")
                              <option value="Certified" selected>Certified</option>
                              <option value="Provisional">Provisional</option>
                              @elseif($assessor_data->toa_status == "Provisional")
                              <option value="Certified" >Certified</option>
                              <option value="Provisional" selected>Provisional</option>
                              @else
                              <option value="Certified" >Certified</option>
                              <option value="Provisional" >Provisional</option>
                              @endif
                            </select>
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">State/Union Territory</label>
                          <div class="col-sm-6">
                           <select class="form-control" id="stateinassessoredit" name="state" required="">
                                <option value="{{$assessor_data->state}}">{{$assessor_data->state}}</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Supported Document</label>
                          <div class="col-sm-6">
                             <input type="file" class="form-control" name="supdoc" accept=".jpg,.jpeg,.png,.pdf">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label"></label>
                          <div class="col-sm-9">
                             <img src="{{asset('growwell/assessors')}}/{{$assessor_data->assessor_id}}/supdoc/{{$assessor_data->supdoc}}" height="50" width="50">
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">

                      <div class="col-md-6">

                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>


      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
        </form>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
 <script>

    //Create the XHR Object
    let xhrassessor1edit = new XMLHttpRequest;
    //Call the open function, GET-type of request, url, true-asynchronous
    xhrassessor1edit.open('GET', 'https://indian-cities-api-nocbegfhqg.now.sh/cities', true)
    //call the onload
    xhrassessor1edit.onload = function()
        {
            //check if the status is 200(means everything is okay)
            if (this.status === 200)
                {
                    //return server response as an object with JSON.parse
                    let values = JSON.parse(this.responseText);
                    let stateDups=[];
                    let states=[];

                    for(var i = 0; i < values.length; i++){

                               stateDups[i] = values[i].State;

                    }

                    states = removeDups(stateDups).sort();

                    var stateDiv = document.getElementById("stateinassessoredit");

                    for(var i = 0; i < states.length; i++){

                      var option = document.createElement("OPTION");

                      //Set Customer Name in Text part.
                      option.innerHTML = states[i]

                      //Set CustomerId in Value part.
                      option.value = states[i]

                      //Add the Option element to DropDownList.
                      stateDiv.options.add(option);

                    }

        }
                }
    //call send
    xhrassessor1edit.send();


  </script>
