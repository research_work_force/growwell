<!-- Navbar start -->
 <div class="horizontal-menu">
     <nav class="navbar top-navbar col-lg-12 col-12 p-0" style="padding-left: 2% !important; padding-right: 2% !important; background: #27313b !important; color:white !important; border:none;">
       <div class="container-fluid">
         <div class="navbar-menu-wrapper d-flex align-items-center justify-content-between">
           <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
               <a class="navbar-brand brand-logo" href="/batchmanagement"><img src="{{asset('theme/images/logo.png')}}" /></a>
               <a class="navbar-brand brand-logo-mini" href="/batchmanagement"><img src="{{asset('theme/images/logo.png')}}" alt="logo"/></a>
           </div>
           <ul class="navbar-nav navbar-nav-right">

               <li class="nav-item nav-profile dropdown">
                 <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                   <span class="nav-profile-name" style="color:white;">{{\Session::get('admin_uname')}}</span>
                   <span class="online-status"></span>
                   <img src="{{asset('theme/images/faces/face28.png')}}" alt="profile" style="border-radius:50%;"/>
                 </a>
                      <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                      <a href="{{route('admin.changepassword')}}" class="dropdown-item">
                        <i  class="mdi mdi-settings text-primary"></i>
                        Change Password
                      </a>
                      <a href="{{route('admin.logout')}}" class="dropdown-item">

                        <i class="mdi mdi-logout text-primary"></i> Logout
                      </a>
                  </div>
               </li>
           </ul>
           <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
             <span class="mdi mdi-menu"></span>
           </button>
         </div>
       </div>
     </nav>
     <nav class="bottom-navbar" style="float:left !important; background: #27313b !important; color:white !important;box-shadow: 0px 3px rgba(0, 0, 20, 0.4);">
     <div class="container col-md-12" >
         <ul class="nav page-navigation">


           <li class="nav-item" >
             <a class="nav-link" href="/assessmentreq/0" style="color:white;">
               <i class="mdi mdi-clipboard-text menu-icon" style="color:white;"></i>
               <span class="menu-title" style="color:white;">Assessment Request</span>
             </a>
           </li>

            <li class="nav-item">
             <a class="nav-link" href="/assessormanagement" style="color:white;">
               <i class="mdi mdi-account-box menu-icon " style="color:white;"></i>
               <span class="menu-title">View Assessors</span>
             </a>
           </li>


             <li class="nav-item">
             <a class="nav-link" href="/batchmanagement" style="color:white;">
               <i class="mdi mdi-animation menu-icon " style="color:white;"></i>
               <span class="menu-title">Create Batches</span>
             </a>
           </li>

            <li class="nav-item">
             <a class="nav-link" href="/studentmanagement" style="color:white;">
               <i class="mdi mdi-school menu-icon " style="color:white;"></i>
               <span class="menu-title">Upload Candidates</span>
             </a>
           </li>

            <li class="nav-item">
             <a class="nav-link" href="/questionmanagement" style="color:white;">
               <i class="mdi mdi-checkbox-marked-circle-outline menu-icon " style="color:white;"></i>
               <span class="menu-title">Create/Upload Tests</span>
             </a>
           </li>

          


          <!--  <li class="nav-item">
             <a class="nav-link" href="/searchassessor" style="color:white;">
               <i class="mdi mdi-account-search menu-icon " style="color:white;"></i>
               <span class="menu-title">Search Assessors</span>
             </a>
           </li> -->



           <li class="nav-item">
             <a class="nav-link" href="{{route('admin.batch.report')}}" style="color:white;">
               <i class="mdi mdi-trending-up menu-icon " style="color:white;"></i>
               <span class="menu-title">View Reports</span>
             </a>
           </li>


         </ul>
     </div>
   </nav>

   </div>
   <!-- Navbar end -->
