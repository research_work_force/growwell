<div class="modal fade" id="changeassessmentdate{{$bdata->batch_id}}modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Change Assessment Date</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="{{ route('admin.change.assessmentdate.save')}}" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
              <input type="hidden" name="batch_id" value="{{$bdata->batch_id}}" required/>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Previous Assessment Date(s)</label>
                          <div class="col-sm-6">
                             <input type="date" class="form-control"  value="{{$bdata->assessment_date_fdt}}" disabled>
                              <input type="hidden" class="form-control" name="previous_date" value="{{$bdata->assessment_date_fdt}}" >
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">New Assessment Date(s)</label>
                          <div class="col-sm-6">
                           <input type="date" id="txtDateAssessment{{$bdata->batch_id}}" class="form-control" name="new_assessment_date"  required>
                          </div>
                        </div>
                      </div>
                    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

    $(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;
    $('#txtDateAssessment{{$bdata->batch_id}}').attr('min', maxDate);
});
</script>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Reason</label>
                          <div class="col-sm-9">
                             <textarea class="form-control" name="reason" required></textarea>
                          </div>
                        </div>
                      </div>


                    </div>









                    <div class="row">

                      <div class="col-md-6">

                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>


      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
        </form>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
