 <!-- base:js -->
    <script src="/theme/vendors/base/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="/theme/js/template.js"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <!-- End plugin js for this page -->
    <script src="/theme/vendors/chart.js/Chart.min.js"></script>
    <script src="/theme/vendors/progressbar.js/progressbar.min.js"></script>

    <script src="/theme/js/pagination.js"></script>
    <script src="/theme/js/pagination.min.js"></script>

		<script src="/theme/vendors/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js"></script>
		<script src="/theme/vendors/justgage/raphael-2.1.4.min.js"></script>
		<script src="/theme/vendors/justgage/justgage.js"></script>
    <!-- Custom js for this page-->
    <script src="/theme/js/dashboard.js"></script>
    <!-- End custom js for this page-->

    <!-- dropzone file upload -->
    <script src="/theme/dropzone/dist/dropzone.js"></script>
    