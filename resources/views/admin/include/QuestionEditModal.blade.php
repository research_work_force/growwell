<div class="modal fade" id="editQuestionmodal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" >
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-family: Lato; font-weight: 900;color: White;">Edit Question</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

            <div class="modal-body">

               <form action="/question/edit" method="post">
              {{csrf_field()}}

              <input type="hidden" name="id" value="{{$item->id}}">
              <div id="myGroup">
              <a href="#eng" class="btn btn-info" data-parent="#myGroup" data-toggle="collapse">English</a>
               <a href="#hin" class="btn btn-info"data-parent="#myGroup"  data-toggle="collapse">Hindi</a>
               <a href="#tl" class="btn btn-info"data-parent="#myGroup"  data-toggle="collapse">Telugu</a>



              <div id="eng" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question" value="{{$item->question}}" required="">
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1" value="{{$item->option1}}" required="">
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2" value="{{$item->option2}}" required="">
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3" value="{{$item->option3}}" required="">
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4" value="{{$item->option4}}" required="">
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_answer" class="form-control" >

                   <option value="option1" {{ old('right_answer',$item->right_answer)=='option1' ? 'selected' : ''  }}>option 1</option>
                  <option value="option2"  {{ old('right_answer',$item->right_answer)=='option2' ? 'selected' : '' }}>option 2</option>
                  <option value="option3" {{ old('right_answer',$item->right_answer)=='option3' ? 'selected' : ''  }}>option 3</option>
                   <option value="option4" {{ old('right_answer',$item->right_answer)=='option4' ? 'selected' : ''  }}>option 4</option>


                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="option4" value="{{$item->right_answer}}" disabled>

              </div>


              <div class="form-group">
               <label>Job role</label>
               <select  name="job_role" class="form-control" >

                   <option value="Hank Dyer" {{ old('job_role',$item->job_role)=='Hank Dyer' ? 'selected' : ''  }}>Hank Dyer</option>
                  <option value="Jacquard Harness builder"  {{ old('job_role',$item->job_role)=='Jacquard Harness builder' ? 'selected' : '' }}>Jacquard Harness builder</option>
                  <option value="Jacquard Weaver - Handloom" {{ old('job_role',$item->job_role)=='Jacquard Weaver - Handloom' ? 'selected' : ''  }}>Jacquard Weaver - Handloom</option>
                   <option value="Textile Designer - Handloom Jacquard" {{ old('job_role',$item->job_role)=='Textile Designer - Handloom Jacquard' ? 'selected' : ''  }}>Textile Designer - Handloom Jacquard</option>
                    <option value="two Shaft Handloom Weaver" {{ old('job_role',$item->job_role)=='two Shaft Handloom Weaver' ? 'selected' : ''  }}>two Shaft Handloom Weaver</option>
                   <option value="Warper" {{ old('job_role',$item->job_role)=='Warper' ? 'selected' : ''  }}>Warper</option>


                </select>


            </div>

             <div class="form-group">
              <label >Section</label>


                               @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                               @if(strcmp($item->section,$section->name) == 0)
                              <option value="{{ $section->name }}" selected>{{ $section->name }}</option>
                                @else
                                    <option value="{{ $section->name }}">{{ $section->name }}</option>
                                 @endif
                             @endforeach
                            </select>


            </div>


              </div>


             
              <div id="hin" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question" value="{{$item->question_hindi}}" required="">
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1" value="{{$item->option1_hindi}}" required="">
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2" value="{{$item->option2_hindi}}" required="">
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3" value="{{$item->option3_hindi}}" required="">
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4" value="{{$item->option4_hindi}}" required="">
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_hindi" class="form-control" >

                   <option value="option1_hindi" {{ old('right_ans_hindi',$item->right_ans_hindi)=='option1_hindi' ? 'selected' : ''  }}>option 1</option>
                  <option value="option2_hindi"  {{ old('right_ans_hindi',$item->right_ans_hindi)=='option2_hindi' ? 'selected' : '' }}>option 2</option>
                  <option value="option3_hindi" {{ old('right_ans_hindi',$item->right_ans_hindi)=='option3_hindi' ? 'selected' : ''  }}>option 3</option>
                   <option value="option4_hindi" {{ old('right_ans_hindi',$item->right_ans_hindi)=='option4_hindi' ? 'selected' : ''  }}>option 4</option>


                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="option4" value="{{$item->right_ans_hindi}}" disabled>

              </div>


              <div class="form-group">
               <label>Job role</label>
               <select  name="job_role" class="form-control" >

                   <option value="Hank Dyer" {{ old('job_role',$item->job_role)=='Hank Dyer' ? 'selected' : ''  }}>Hank Dyer</option>
                  <option value="Jacquard Harness builder"  {{ old('job_role',$item->job_role)=='Jacquard Harness builder' ? 'selected' : '' }}>Jacquard Harness builder</option>
                  <option value="Jacquard Weaver - Handloom" {{ old('job_role',$item->job_role)=='Jacquard Weaver - Handloom' ? 'selected' : ''  }}>Jacquard Weaver - Handloom</option>
                   <option value="Textile Designer - Handloom Jacquard" {{ old('job_role',$item->job_role)=='Textile Designer - Handloom Jacquard' ? 'selected' : ''  }}>Textile Designer - Handloom Jacquard</option>
                    <option value="two Shaft Handloom Weaver" {{ old('job_role',$item->job_role)=='two Shaft Handloom Weaver' ? 'selected' : ''  }}>two Shaft Handloom Weaver</option>
                   <option value="Warper" {{ old('job_role',$item->job_role)=='Warper' ? 'selected' : ''  }}>Warper</option>


                </select>


            </div>

             <div class="form-group">
              <label >Section</label>


                               @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                               @if(strcmp($item->section,$section->name) == 0)
                              <option value="{{ $section->name }}" selected>{{ $section->name }}</option>
                                @else
                                    <option value="{{ $section->name }}">{{ $section->name }}</option>
                                 @endif
                             @endforeach
                            </select>


            </div>


              </div>

               </div>


                <div id="tl" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question" value="{{$item->question_teleugu}}" required="">
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1" value="{{$item->option1_hindi}}" required="">
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2" value="{{$item->option2_hindi}}" required="">
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3" value="{{$item->option3_hindi}}" required="">
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4" value="{{$item->option4_hindi}}" required="">
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_hindi" class="form-control" >

                   <option value="option1_hindi" {{ old('right_ans_hindi',$item->right_ans_hindi)=='option1_hindi' ? 'selected' : ''  }}>option 1</option>
                  <option value="option2_hindi"  {{ old('right_ans_hindi',$item->right_ans_hindi)=='option2_hindi' ? 'selected' : '' }}>option 2</option>
                  <option value="option3_hindi" {{ old('right_ans_hindi',$item->right_ans_hindi)=='option3_hindi' ? 'selected' : ''  }}>option 3</option>
                   <option value="option4_hindi" {{ old('right_ans_hindi',$item->right_ans_hindi)=='option4_hindi' ? 'selected' : ''  }}>option 4</option>


                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="option4" value="{{$item->right_ans_hindi}}" disabled>

              </div>


              <div class="form-group">
               <label>Job role</label>
               <select  name="job_role" class="form-control" >

                   <option value="Hank Dyer" {{ old('job_role',$item->job_role)=='Hank Dyer' ? 'selected' : ''  }}>Hank Dyer</option>
                  <option value="Jacquard Harness builder"  {{ old('job_role',$item->job_role)=='Jacquard Harness builder' ? 'selected' : '' }}>Jacquard Harness builder</option>
                  <option value="Jacquard Weaver - Handloom" {{ old('job_role',$item->job_role)=='Jacquard Weaver - Handloom' ? 'selected' : ''  }}>Jacquard Weaver - Handloom</option>
                   <option value="Textile Designer - Handloom Jacquard" {{ old('job_role',$item->job_role)=='Textile Designer - Handloom Jacquard' ? 'selected' : ''  }}>Textile Designer - Handloom Jacquard</option>
                    <option value="two Shaft Handloom Weaver" {{ old('job_role',$item->job_role)=='two Shaft Handloom Weaver' ? 'selected' : ''  }}>two Shaft Handloom Weaver</option>
                   <option value="Warper" {{ old('job_role',$item->job_role)=='Warper' ? 'selected' : ''  }}>Warper</option>


                </select>


            </div>

             <div class="form-group">
              <label >Section</label>


                               @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                               @if(strcmp($item->section,$section->name) == 0)
                              <option value="{{ $section->name }}" selected>{{ $section->name }}</option>
                                @else
                                    <option value="{{ $section->name }}">{{ $section->name }}</option>
                                 @endif
                             @endforeach
                            </select>


            </div>


              </div>

            </div>

            <div class="modal-footer">

             <button type="submit" class="btn btn-primary btn-growwell">Save</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
<script>
  var $myGroup = $('#myGroup');
$myGroup.on('show.bs.collapse','.collapse', function() {
    $myGroup.find('.collapse.in').collapse('hide');
});
</script>