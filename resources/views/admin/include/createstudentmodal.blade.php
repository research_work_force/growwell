<div class="modal fade" id="createstudentmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create Student</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="/student/insert" method="POST">
          {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Candidate ID</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="candidate_id" required="">
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Email-ID</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" required="">
                          </div>
                         </div>
                      </div>

                    </div>

                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Name</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="name" required="">
                          </div>
                        </div>
                      </div>

                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Address</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="address" required="">
                          </div>
                        </div>
                      </div>



                    </div>

                    <!-- <p class="card-description">
                      Address
                    </p> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="spoc" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Email</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" name="spoc_email" >
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Phone</label>
                          <div class="col-sm-9">
                            <input type="number" maxlength="10" minlength="10" class="form-control" name="spoc_phone" ">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch ID</label>
                          <div class="col-sm-9">
                            @php
                               $bats = App\Http\Controllers\StaticValueProviderController::batcheids();

                            @endphp
                            <select class="form-control" name="batch_id" required>
                             @foreach($bats as $sprog)
                              <option value="{{ $sprog->batch_id }}">{{ $sprog->batch_id }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                      </div>


                    </div>


                    <div class="row">

                      <div class="col-md-6">
                         <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Drop-out</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="dropout_status" required>
                              <option value="1">Yes</option>
                              <option value="0">No</option>
                            </select>
                          </div>
                         </div>
                      </div>
                    </div>


      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Create</button>

         </form>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
