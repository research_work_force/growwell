<div class="modal fade" id="bulkquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Bulk Question Upload</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">


           

        <form  action="/question/bulkupload" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}

                 <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Upload Questions</label>
                          <div class="col-sm-8">
                            <input type="file" name="qfile" class="form-control" required>
                          </div>
                        </div>
                      </div>

                          <div class="col-md-6">
                         
                             <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Job Role</label>
                          <div class="col-sm-8">
                            @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="jobRole">
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                      </div>
                    </div>
                    </div>
      </div>


      <!--Footer-->
      <div class="modal-footer">

        <button type="submit" id="submit-all" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Upload</button>
        </form>

        <span style="float: right;margin-left: 2%;"> <a href="/growwell/questions_sample.xlsx" download>  
          <button type="button" class="btn btn-primary btn-growwell" >Download Sample</button></a></span>


      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
