<div class="modal fade" id="bulkassessormodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Bulk Assessor Upload</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">

            <span style="float: right;margin-left: 2%;"> <a href="/growwell/assessor_sample.xlsx" download>  <button type="button" class="btn btn-primary btn-growwell" >Download Sample</button></a></span>

        <form class="form-sample" action="{{ route('assessor.bulkupload')}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Upload Assessor Details</label>
                          <div class="col-sm-8">
                            <input type="file" name="assessor_file" class="form-control" accept=".xlsx">
                          </div>
                        </div>
                      </div>

                       <div class="col-md-4">
                        <button type="submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Upload</button>
                      </div>

                    </div>



                  </form>


    </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">


      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
