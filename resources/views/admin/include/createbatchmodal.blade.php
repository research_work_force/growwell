<div class="modal fade" id="createbatchmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create Batch</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
    <form class="form-sample" action="{{ route('admin.batch.save') }}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Scheme/ Program/ Model</label>
                          <div class="col-sm-6">
                           @php
                               $schemeProgram = App\Http\Controllers\StaticValueProviderController::schemeProgram();

                          @endphp
                            <select class="form-control" name="schemeProgram" required>
                             @foreach($schemeProgram as $sprog)
                              <option value="{{ $sprog->name }}">{{ $sprog->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Name</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="batch_name" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sector</label>
                          <div class="col-sm-9">
                            @php
                               $sectors = App\Http\Controllers\StaticValueProviderController::sectors();

                            @endphp
                            <select class="form-control" name="sectors" required>
                             @foreach($sectors as $sector)
                              <option value="{{ $sector->name }}">{{ $sector->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Type</label>
                          <div class="col-sm-9">
                            @php
                               $batchtype = App\Http\Controllers\StaticValueProviderController::batchTypes();

                             @endphp
                            <select class="form-control" name="batchTypes" required>
                             @foreach($batchtype as $bat)
                              <option value="{{ $bat->name }}">{{ $bat->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>


                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Training Center Location</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control" name="training_center_location" required/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Size</label>
                          <div class="col-sm-9">
                            <input type="number" class="form-control" name="batch_size" required/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Assessment Date(s)</label>
                          <div class="col-sm-6">
                            <input type="date" id="txtDate" class="form-control" name="assessment_date_fdt" required/>
                          </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

    $(function(){
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;
    $('#txtDate').attr('min', maxDate);
});
</script>

                        </div>
                      </div>
                     <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="batch_id" required/>
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="statesModal" name="states" required>

                            </select>
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Languauge</label>
                          <div class="col-sm-9">
                           <input type="text" class="form-control" name="language" required/>
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Action Date</label>
                          <div class="col-sm-9">
                            <input type="date" class="form-control" name="action_date"/>
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Received From SSC On</label>
                          <div class="col-sm-6">
                          <input type="date" class="form-control" name="received_from_ssc"/>
                          </div>
                        </div>
                      </div>

                    </div>



                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="spoc_name" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">SPOC Mobile No.</label>
                          <div class="col-sm-6">
                          <input type="number" maxlength="10" minlength="10"class="form-control" name="spoc_mobile_no" />
                          </div>
                        </div>
                      </div>

                    </div>



                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">SPOC Email</label>
                          <div class="col-sm-9">
                            <input type="email" class="form-control" name="spoc_email" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                            @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="jobRole" required>
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Duration</label>
                          <div class="col-sm-9">
                            <input type="date" class="form-control" name="fduration" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">to</label>
                          <div class="col-sm-6">
                          <input type="date" class="form-control" name="tduration" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Documents</label>
                          <div class="col-sm-9">
                            <input type="file" class="form-control" name="docfile" />
                          </div>
                        </div>
                      </div>
                       <div class="col-md-6">
                        
                      </div>
                    </div>


      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Create</button>
 </form>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
