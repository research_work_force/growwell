<div class="modal fade" id="createassessormodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify modal-warning" role="document" style="max-width: 999px;">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header text-center" style="background:#27313b;">
        <h4 class="modal-title white-text w-100 font-weight-bold py-2" style="font-size: 30px; font-family: Lato; font-weight: 900;color: White;">Create Assessor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <form class="form-sample" action="/assessor/create" method="POST" enctype="multipart/form-data">
          {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor ID</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="assessor_id" required>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Name of Assessor</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" name="name" required="">
                          </div>
                        </div>
                      </div>
                    </div>
                      <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Assessment Agency Aligned</label>
                          <div class="col-sm-6">
                           <input type="text" class="form-control" name="assessment_agency_aligned" required="">
                          </div>
                        </div>
                      </div>
                        <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Linking Type</label>
                          <div class="col-sm-9">
                            <select class="form-control" name="linking_type">
                              <option value="On Role">On Role</option>
                              <option value="Freelancer">Freelancer</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Duration</label>
                          <div class="col-sm-9">
                           <input type="date" class="form-control" name="fdt" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">to</label>
                          <div class="col-sm-9">
                            <input type="date" class="form-control" name="tdt" required>
                          </div>
                        </div>


                      </div>
                    </div>
                    <!-- <p class="card-description">
                      Address
                    </p> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sectors</label>
                          <div class="col-sm-9">
                           @php
                               $sectors = App\Http\Controllers\StaticValueProviderController::sectors();

                            @endphp
                            <select class="form-control" name="sectors" required>
                             @foreach($sectors as $sector)
                              <option value="{{ $sector->name }}">{{ $sector->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Mobile no</label>
                          <div class="col-sm-9">
                            <input type="number" maxlength="10" minlength="10" class="form-control" name="phone">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Email Address</label>
                          <div class="col-sm-6">
                            <input type="email" class="form-control" name="email" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Supported Document</label>
                          <div class="col-sm-6">
                            <input type="file" class="form-control" name="supdoc" >
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">Domain Job Role</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control" name="domain_job_role" required="">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">TOA status</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="toa_status" required>
                              <option value="Certified">Certified</option>
                              <option value="Provisional">Provisional</option>
                            </select>
                          </div>


                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-6 col-form-label">State/Union Territory</label>
                          <div class="col-sm-6">
                            <select class="form-control" id="stateinassessorcreate" name="state" required="">

                            </select>
                          </div>
                        </div>
                      </div>

                    </div>
  <script>

    //Create the XHR Object
    let xhrassessor1 = new XMLHttpRequest;
    //Call the open function, GET-type of request, url, true-asynchronous
    xhrassessor1.open('GET', 'https://indian-cities-api-nocbegfhqg.now.sh/cities', true)
    //call the onload
    xhrassessor1.onload = function()
        {
            //check if the status is 200(means everything is okay)
            if (this.status === 200)
                {
                    //return server response as an object with JSON.parse
                    let values = JSON.parse(this.responseText);
                    let stateDups=[];
                    let states=[];

                    for(var i = 0; i < values.length; i++){

                               stateDups[i] = values[i].State;

                    }

                    states = removeDups(stateDups).sort();

                    var stateDiv = document.getElementById("stateinassessorcreate");

                    for(var i = 0; i < states.length; i++){

                      var option = document.createElement("OPTION");

                      //Set Customer Name in Text part.
                      option.innerHTML = states[i]

                      //Set CustomerId in Value part.
                      option.value = states[i]

                      //Add the Option element to DropDownList.
                      stateDiv.options.add(option);

                    }

        }
                }
    //call send
    xhrassessor1.send();


  </script>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="Submit" class="btn btn-primary btn-growwell" style="font-family:lato !important;font-weight: bold;">Submit</button>
        </form>

      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
