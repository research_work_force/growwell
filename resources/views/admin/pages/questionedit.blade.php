@extends('admin.layouts.home')

@section('content') 



<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->
    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
            @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
				<div class="content-wrapper">

	<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card " >
                <div class="card-body">

                    <h2 class="card-title growwell-card-title">Edit question</h2>

                    @foreach($question_details as $item)

                    <form action="/questionsedit" method="POST">
              {{csrf_field()}}

              <input type="hidden" name="id" value="{{$item->id}}">
              <div id="myGroup">
              <a href="#eng" class="btn btn-info" id="engdiv" data-parent="#myGroup" data-toggle="collapse" onclick="show_english();">English</a>

               <a href="#hin" class="btn btn-info" data-parent="#myGroup"  data-toggle="collapse" onclick="show_hindi();">Hindi</a>

               <a href="#tl" class="btn btn-info" data-parent="#myGroup"  data-toggle="collapse" onclick="show_telugu();">Telugu</a>

               <a href="#ass" class="btn btn-info" data-parent="#myGroup"  data-toggle="collapse" onclick="show_assamese();">Assamese</a>

          

              <div id="eng" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question" value="{{$item->question}}" required="">
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1" value="{{$item->option1}}" required="">
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2" value="{{$item->option2}}" required="">
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3" value="{{$item->option3}}" required="">
              </div>
 
               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4" value="{{$item->option4}}" required="">
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_answer" class="form-control" >
               		
                   <option value="option1" @if($item->right_answer == $item->option1) selected='selected' @endif > Option 1</option>
                  
                   <option value="option2" @if($item->right_answer == $item->option2) selected='selected' @endif >Option 2</option>

                   <option value="option3" @if($item->right_answer == $item->option3) selected='selected' @endif >Option 3</option>

                   <option value="option4" @if($item->right_answer == $item->option4) selected='selected' @endif >Option 4</option>                   


                </select>

              </div>
             


              <div class="form-group">
               <label>Job role</label>
               <select  name="job_role" class="form-control" >

                   <option value="Hank Dyer" {{ old('job_role',$item->job_role)=='Hank Dyer' ? 'selected' : ''  }}>Hank Dyer</option>
                  <option value="Jacquard Harness builder"  {{ old('job_role',$item->job_role)=='Jacquard Harness builder' ? 'selected' : '' }}>Jacquard Harness builder</option>
                  <option value="Jacquard Weaver - Handloom" {{ old('job_role',$item->job_role)=='Jacquard Weaver - Handloom' ? 'selected' : ''  }}>Jacquard Weaver - Handloom</option>
                   <option value="Textile Designer - Handloom Jacquard" {{ old('job_role',$item->job_role)=='Textile Designer - Handloom Jacquard' ? 'selected' : ''  }}>Textile Designer - Handloom Jacquard</option>
                    <option value="two Shaft Handloom Weaver" {{ old('job_role',$item->job_role)=='two Shaft Handloom Weaver' ? 'selected' : ''  }}>two Shaft Handloom Weaver</option>
                   <option value="Warper" {{ old('job_role',$item->job_role)=='Warper' ? 'selected' : ''  }}>Warper</option>


                </select>


            </div>

             <div class="form-group">
              <label >Section</label>


                               @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                               @if(strcmp($item->section,$section->name) == 0)
                              <option value="{{ $section->name }}" selected>{{ $section->name }}</option>
                                @else
                                    <option value="{{ $section->name }}">{{ $section->name }}</option>
                                 @endif
                             @endforeach
                            </select>


            </div>


              </div>


             
              <div id="hin" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question_hindi" value="{{$item->question_hindi}}" >
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1_hindi" value="{{$item->option1_hindi}}" >
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2_hindi" value="{{$item->option2_hindi}}" >
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3_hindi" value="{{$item->option3_hindi}}" >
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4_hindi" value="{{$item->option4_hindi}}" >
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_hindi" class="form-control" >


               	 <option value="option1_hindi" @if($item->right_ans_hindi == $item->option1_hindi) selected='selected' @endif > Option 1</option>
                  
                   <option value="option2_hindi" @if($item->right_ans_hindi == $item->option2_hindi) selected='selected' @endif >Option 2</option>

                   <option value="option3_hindi" @if($item->right_ans_hindi == $item->option3_hindi) selected='selected' @endif >Option 3</option>

                   <option value="option4_hindi" @if($item->right_ans_hindi == $item->option4_hindi) selected='selected' @endif >Option 4</option> 




                </select>
              </div>
              


              

             


              </div>

               </div>


                <div id="tl"  class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question_telugu" value="{{$item->question_teleugu}}" >
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1_telugu" value="{{$item->option1_teleugu}}" >
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2_telugu" value="{{$item->option2_teleugu}}" >
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3_telugu" value="{{$item->option3_teleugu}}" >
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4_telugu" value="{{$item->option4_teleugu}}" >
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_telugu" class="form-control" >



               	<option value="option1_telugu" @if($item->right_ans_teleugu == $item->option1_teleugu) selected='selected' @endif > Option 1</option>
                  
                <option value="option2_telugu" @if($item->right_ans_teleugu == $item->option2_teleugu) selected='selected' @endif >Option 2</option>

                <option value="option3_telugu" @if($item->right_ans_teleugu == $item->option3_teleugu) selected='selected' @endif >Option 3</option>

                <option value="option4_telugu" @if($item->right_ans_teleugu == $item->option4_teleugu) selected='selected' @endif >Option 4</option>


                </select>
              </div>
             


              

            


              </div>

              <!-- assamase div start -->



               <div id="ass" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question_assam" value="{{$item->question_assam}}" >
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1_assam" value="{{$item->option1_assam}}" >
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2_assam" value="{{$item->option2_assam}}" >
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3_assam" value="{{$item->option3_assam}}" >
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4_assam" value="{{$item->option4_assam}}" >
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_assam" class="form-control" >

                <option value="option1_assam" @if($item->right_ans_assam == $item->option1_assam) selected='selected' @endif > Option 1</option>
                  
                <option value="option2_assam" @if($item->right_ans_assam == $item->option2_assam) selected='selected' @endif >Option 2</option>

                <option value="option3_assam" @if($item->right_ans_assam == $item->option3_assam) selected='selected' @endif >Option 3</option>

                <option value="option4_assam" @if($item->right_ans_assam == $item->option4_assam) selected='selected' @endif >Option 4</option>


                </select>
              </div>

               
              


             

           


            


              </div>

            </div>

            <div class="modal-footer">

             <button type="submit" class="btn btn-primary btn-growwell">Save</button>
              <a href="/questionmanagement" class="btn btn-danger" >Cancel</a>
              </form>

              @endforeach



<script>
   document.getElementById("engdiv").click();
</script>

                </div>
            </div>
        </div>
    </div>




				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection

<script>

  function show_english() {
 var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    hin.style.display = "none";
    tel.style.display = "none";
    ass.style.display = "none";
    eng.style.display="block";
  
}

function show_hindi() {
  var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    eng.style.display = "none";
    tel.style.display = "none";
    ass.style.display = "none";
    hin.style.display="block";
  
}

function show_telugu() {
  var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    hin.style.display = "none";
    eng.style.display = "none";
    ass.style.display = "none";
    tel.style.display = "block";
  
}

function show_assamese() {
  var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    hin.style.display = "none";
    eng.style.display = "none";
    tel.style.display = "none";
    ass.style.display = "block";
  
}


  var $myGroup = $('#myGroup');
$myGroup.on('show.bs.collapse','.collapse', function() {
    $myGroup.find('.collapse.in').collapse('hide');
});
</script>

