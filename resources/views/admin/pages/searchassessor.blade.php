@extends('admin.layouts.home')

@section('content')



<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->



    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Assessor Advance Search</h2> -->

          <!-- Row 1 -->

          <div class="row">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >Search Assessor</h2>
                   <form class="form-sample" action="{{ route('search.assessor.filters')}}" method="get">
                     {{csrf_field()}}
                    <!-- <p class="card-description">
                      Personal info
                    </p> -->
                    <!-- <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">First Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Last Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="stateinassessor" name="states">

                            </select>
<script>

function removeDups(names) {
  let unique = {};
  names.forEach(function(i) {
    if(!unique[i]) {
      unique[i] = true;
    }
  });
  return Object.keys(unique);
}

  //Create the XHR Object
    let xhr = new XMLHttpRequest;
    //Call the open function, GET-type of request, url, true-asynchronous
    xhr.open('GET', 'https://indian-cities-api-nocbegfhqg.now.sh/cities', true)
    //call the onload
    xhr.onload = function()
        {
            //check if the status is 200(means everything is okay)
            if (this.status === 200)
                {
                    //return server response as an object with JSON.parse
                    let values = JSON.parse(this.responseText);
                    let stateDups=[];
                    let states=[];

                    for(var i = 0; i < values.length; i++){

                               stateDups[i] = values[i].State;

                    }

                    states = removeDups(stateDups).sort();

                    var stateDiv = document.getElementById("stateinassessor");

                    for(var i = 0; i < states.length; i++){

                      var option = document.createElement("OPTION");

                      //Set Customer Name in Text part.
                      option.innerHTML = states[i]

                      //Set CustomerId in Value part.
                      option.value = states[i]

                      //Add the Option element to DropDownList.
                      stateDiv.options.add(option);

                    }






        }
                }
    //call send
    xhr.send();

</script>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor Name</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="assessorname"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="assessor_id"/>
                          </div>
                        </div>
                      </div>
                     <!--  <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>

                      </div> -->
                    </div>
                    <!-- <p class="card-description">
                      Address
                    </p> -->
                   <!--  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor Name</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">TOA Status</label>
                          <div class="col-sm-9">


                            <select class="form-control">
                              <option>TOA Status 1</option>
                              <option>TOA Status 2</option>
                              <option>TOA Status 3</option>
                              <option>TOA Status 4</option>
                              <option>TOA Status 5</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div> -->


                    <div class="row">

                      <div class="col-md-6">
                        <div class="template-demo">
                            <button type="submit" class="btn btn-primary btn-growwell-left" >Search Assessor</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card" >
                <div class="card-body">

                    <h2 class="card-title growwell-card-title" >Assessor Search Result </h2>
                      <!-- <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">Launch
                          modal Subscription</a>
                      </div> -->

                      @include('admin.include.createbatchmodal')

                  <!-- <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p> -->
                  <div class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                            Name
                          </th>
                          <th>
                            Gender
                          </th>
                          <th>
                            Religion
                          </th>
                          <th>
                            Date of birth
                          </th>
                          <th>
                            Language Known
                          </th>
                          <th>
                            Phone
                          </th>
                          <th>
                            Email-ID/Username
                          </th>

                          <th>
                            Address
                          </th>
                          <th>
                            Landmark
                          </th>

                          <th>
                            Pincode
                          </th>
                           <th>
                            State
                          </th>

                           <th>
                            Education
                          </th>
                          <th>
                            Industrial Experience
                          </th>

                        </tr>
                      </thead>
                      <tbody>
                        @foreach($search_result as $res)
                        <tr>
                          <td >
                            {{ $res->name }}
                          </td>
                          <td>
                            {{ $res->gender }}
                          </td>
                          <td>
                            {{ $res->religion }}
                          </td>

                          <td>
                            {{ $res->date_of_birth }}
                          </td>
                          <td>
                            {{ $res->language_known }}
                          </td>
                          <td>
                            {{ $res->phone }}
                          </td>
                          <td>
                            {{ $res->email }}
                          </td>
                          <td>
                            {{ $res->address }}
                          </td>
                          <td>
                            {{ $res->landmark }}
                          </td>
                          <td>
                            {{ $res->pincode }}
                          </td>
                          <td>
                            {{ $res->state }}
                          </td>
                          <td>
                            {{ $res->education }}
                          </td>
                           <td>
                            {{ $res->industrial_experience }}
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $search_result->render("pagination::bootstrap-4") }}
                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
