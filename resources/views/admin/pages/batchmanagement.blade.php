@extends('admin.layouts.home')

@section('content')
<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute !important;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
  float: left !important;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->

    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
         @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Batch Management<span style="float: right;"><a href=""  data-toggle="modal" data-target="#createbatchmodal">  <button type="button" class="btn btn-primary" style="float: right;background: #4a9cdf;font-family:lato !important;font-weight: bold;">Create Batch</button></a></span></h2> -->

          <!-- Row 1 -->

          <div class="row collapse" id="demo">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >Search Batch<span style="float: right;"><a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a></span> </h2>
                  <form class="form-sample" action="{{ route('admin.batch.search') }}" method="get">
                    {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row"> 
                          <label class="col-sm-3 col-form-label">Scheme/ Program/ Model</label>
                          <div class="col-sm-9">
                          @php
                               $schemeProgram = App\Http\Controllers\StaticValueProviderController::schemeProgram();

                          @endphp
                            <select class="form-control" name="schemeProgram">
                             @foreach($schemeProgram as $sprog)
                              <option value="{{ $sprog->name }}">{{ $sprog->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                            @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="jobRole">
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sector</label>
                          <div class="col-sm-9">
                            @php
                               $sectors = App\Http\Controllers\StaticValueProviderController::sectors();

                            @endphp
                            <select class="form-control" name="sectors">
                             @foreach($sectors as $sector)
                              <option value="{{ $sector->name }}">{{ $sector->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Type</label>
                          <div class="col-sm-9">
                              @php
                               $batchtype = App\Http\Controllers\StaticValueProviderController::batchTypes();

                             @endphp
                            <select class="form-control" name="batchTypes">
                             @foreach($batchtype as $bat)
                              <option value="{{ $bat->name }}">{{ $bat->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                        <!-- <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Membership</label>
                          <div class="col-sm-4">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios1" value="" checked>
                                Free
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-5">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios2" value="option2">
                                Professional
                              </label>
                            </div>
                          </div>
                        </div> -->
                      </div>
                    </div>
                    <!-- <p class="card-description">
                      Address
                    </p> -->


                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="states" name="states">

                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="batch_id"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Address 2</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Postcode</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> -->


                    <!-- <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">City</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Country</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>America</option>
                              <option>Italy</option>
                              <option>Russia</option>
                              <option>Britain</option>
                            </select>
                          </div>


                        </div>
                      </div>
                    </div> -->

                    <div class="row">

                      <div class="col-md-4">
                        <button type="submit" class="btn btn-primary btn-growwell-left" >Search Batch</button>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12" style="padding:0px !important;">
              <div class="card growwell-table-card ">
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" >Assign Batch 

                       <a href=""  data-toggle="modal" data-target="#createbatchmodal">  <button type="button" class="btn btn-primary btn-growwell" >Create Batch</button></a><a href="#demo" class="btn btn-primary btn-growwell" data-toggle="collapse" style="margin-right: 2%;">Search Batch </a></h2>





                      <!-- <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">Launch
                          modal Subscription</a>
                      </div> -->

                      @include('admin.include.createbatchmodal')



                  <!-- <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p> -->
                  <div class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                            Scheme/ Program/ Model
                          </th>
                          <th>
                            Sector
                          </th>
                          <th>
                            Batch Type
                          </th>
                          <th>
                            Batch ID
                          </th>
                          <th>
                            Job Role
                          </th>
                          <th>
                            Language
                          </th>
                          <th>
                            Batch Size
                          </th>
                          <th>
                            Training center location
                          </th>
                          <th>
                            Enrolled Canditates
                          </th>
                          <th>
                            Dropout
                          </th>
                          <th>
                            Action Date
                          </th>
                          <th>
                            Received From SSC On
                          </th>
                          <th>
                            Assessment Date(s)
                          </th>
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>

                        @if(!$batch_data->isEmpty())
                        @foreach($batch_data as $bdata)
                        <tr>
                          <td >
                            {{ $bdata->schemeProgram  }}
                          </td>
                          <td>
                             {{ $bdata->sectors  }}
                          </td>
                          <td>
                            {{ $bdata->batchTypes  }}
                          </td>
                          <td>
                             {{ $bdata->batch_id  }}
                          </td>
                          <td>
                             {{ $bdata->jobRole  }}
                          </td>
                          <td>
                             {{ $bdata->language  }}
                          </td>
                          <td>
                             {{ $bdata->batch_size  }}
                          </td>
                          <td>
                             {{ $bdata->training_center_location  }}
                          </td>

                          @foreach($enrolled_candidate_array as $key => $enroll)
                          @if($key == $bdata->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif

                          @endforeach
                          @foreach($dropout_candidate_array as $key => $enroll)
                          @if($key == $bdata->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif
                          @endforeach


                          <td>

                             {{ $bdata->action_date  }}
                          </td>
                          <td>
                             {{ $bdata->received_from_ssc  }}
                          </td>
                          <td>
                             {{ $bdata->assessment_date_fdt  }}-{{ $bdata->assessment_date_fdt  }}
                          </td>
                          <td> 
                            <div>

                              <div class="btn-group dropup">
                                  <button type="button" class="btn btn-primary btn-growwell dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    </button>
                                    <div class="dropdown-menu" style="background-color: #27313b;">
                                      <!-- Dropdown menu links -->
                                      <a class="dropdown-item " style="color: #fff;" href=""  data-toggle="modal" data-target="#changeassessorassign{{$bdata->batch_id}}modal">Assign Assessor</a>

                                      <a class="dropdown-item " style="color: #fff;" href=""  data-toggle="modal" data-target="#reassignassessor{{$bdata->batch_id}}modal">Re-Assign Assessor</a>

                                      <a class="dropdown-item" style="color: #fff;" href=""  data-toggle="modal" data-target="#changeassessmentdate{{$bdata->batch_id}}modal">Edit Assessment Date</a>

                                      <a class="dropdown-item" style="color: #fff;" href="\viewbatchhistory\{{$bdata->batch_id}}" >Batch Details</a>
                                      
                                    @if($bdata->batch_status != 1)
                                      <a class="dropdown-item" style="color: #fff;" href="{{route('admin.batch.delete', $bdata->batch_id)}}" >Delete</a>
                                    @else
                                    @endif
                                     
                                    </div>
                              </div>


                              <!-- <div class="dropdown">
                                  <button type="button" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;" data-toggle="modal" data-target=""><i class="mdi mdi-debug-step-into menu-icon " style=""></i></button>
                                  <div class="dropdown-content">

                                    <a href=""  data-toggle="modal" data-target="#changeassessorassign{{$bdata->batch_id}}modal">Assign Assessor</a>



                                    <a href=""  data-toggle="modal" data-target="#changeassessmentdate{{$bdata->batch_id}}modal">Edit Assessment Date</a>

                                    <a href="\viewbatchhistory\{{$bdata->batch_id}}" >Batch Details</a>


                                   <a href="{{route('admin.batch.delete', $bdata->batch_id)}}" >Delete</a>
                                  </div>
                              </div> -->

                            </div>

                            @include('admin.include.changeassessorassignmodal',['bdata'=>$bdata])
                            @include('admin.include.assessorassignmodal',['bdata'=>$bdata])
                            @include('admin.include.changeassessmentdatemodal',['bdata'=>$bdata])









                          </td>
                        </tr>
                        @endforeach
                         @else

                          <td >
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                          <td>

                          </td>
                          <td>
                           
                          </td>
                          <td>
                           
                          </td>
                          <td>
                             No Data
                          </td>
                          <td>
                          </td>

                          <td></td>
                           <td>
                          </td>
                          <td>
                           
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>

                          <td></td>


                        @endif
                      </tbody>
                    </table>
                    <nav aria-label="Page navigation example" >
                  {{ $batch_data->render("pagination::bootstrap-4") }}
                  </nav>
                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
      </div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
