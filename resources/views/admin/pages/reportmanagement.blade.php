@extends('admin.layouts.home')

@section('content')

<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}
 
.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

 <!-- Navbar start -->

  @include('admin.include.navbaradmin')
 <!-- Navbar end -->

    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
        <div class="content-wrapper">

          <!-- Row 1 -->

            <div class="col-12 grid-margin ">
              <div class="card collapse" id="demo">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >View Report<a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a></h2>




                  <form class="form-sample " action="{{route('admin.report.search')}}" method="post" >
                     {{csrf_field()}}
                    <div class="row" style="margin-bottom:2%;">
                      <div class="col-md-6">

                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-3">Batch Id </div>
                          <div class="col-sm-9">
                            <input type="text" id="search1" name="batch_id" style="padding:4%;color:grey;">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4 ">Job Role</div>
                          <div class="col-sm-8">
                           
                            <input type="text" id="search2" name="batch_id" style="padding:4%;color:grey;">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-4">Assessor Id </div>
                          <div class="col-sm-8">
                           <input type="text" id="search3" name="assessor_id" style="padding:4%;color:grey;">
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row">

                      <div class="col-md-4">
                        <!-- <button type="submit" class="btn btn-primary btn-growwell-left">Search Report</button> -->
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" > Report Details
                      <a href="#demo" class="btn btn-primary btn-growwell" data-toggle="collapse" >Search Report </a>

                     <!--  <a href="#" class="btn btn-primary btn-growwell"  style="margin-right:2%;">Export PDF </a><a href="#" class="btn btn-primary btn-growwell"  style="margin-right:2%;">Export Excel </a></h2>
 -->




                    <div class="table-responsive">
                      <table class="table table-growwell" id="table">
                        <thead>
                          
                            <th>
                              Scheme/ Program/ Model
                            </th>
                            <th>
                              Sector
                            </th>
                            <th>
                              Batch Type
                            </th>
                            <th>
                              Batch ID
                            </th>
                            <th>
                              Assessor ID
                            </th>
                            <th>
                              Job Role
                            </th>
                            <th>
                              Language
                            </th>
                            <th>
                              Batch Size
                            </th>
                            <th>
                              Training center location
                            </th>
                            <th>
                              Enrolled Canditates
                            </th>
                            <th>
                              Dropout
                            </th>
                            <th>
                              Action Date
                            </th>
                            <th>
                              Received From SSC On
                            </th>
                            <th>
                              Assessment Date(s)
                            </th>
                            <th>
                              Student Attendance(%)
                            </th>
                            <th>
                              Passed(%)
                            </th>
                            <th>
                              Failed(%)
                            </th>
                            <th>
                              Status
                            </th>
                            <th>
                              Action
                            </th>
                          
                        </thead>
                        <tbody>

                          @if(!$batch_data->isEmpty())

                          @foreach($batch_data as $data)
                          <tr>
                            <td >
                              {{ $data->schemeProgram }}
                            </td>
                            <td>
                             {{ $data->sectors }}
                            </td>
                            <td>
                              {{ $data->batchTypes }}
                            </td>
                            <td>
                              {{ $data->batch_id }}
                            </td>
                            <td>
                              {{ $data->assessor_id }}
                            </td>
                            <td>
                              {{ $data->jobRole }}
                            </td>
                            <td>
                              {{ $data->language }}
                            </td>
                            <td>
                              {{ $data->batch_size }}
                            </td>
                            <td>
                              {{ $data->training_center_location }}
                            </td>


                            
                          @foreach($enrolled_candidate_array as $key => $enroll)
                          @if($key == $data->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif

                          @endforeach
                          @foreach($dropout_candidate_array as $key => $enroll)
                          @if($key == $data->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif
                          @endforeach



                            <td>
                              {{ $data->action_date }}

                            </td>
                            <td>
                              {{ $data->received_from_ssc }}
                            </td>
                            <td>
                              {{ $data->assessment_date_fdt }} - {{ $data->assessment_date_fdt }}
                            </td>
                            <td>
                               @php
                               $attendence_percentage = App\Http\Controllers\StaticValueProviderController::getBatchWiseStudentAttendencePercentage($data->batch_id);
                               $formated_attendence_percentage = number_format($attendence_percentage, 2, '.', '')

                               @endphp

                               {{$formated_attendence_percentage}}
                            </td>
                            <td> 
                               @php
                               $passed_percentage = App\Http\Controllers\StaticValueProviderController::getBatchWiseStudentPassedPercentage($data->batch_id);
                                $formated_passed_percentage = number_format($passed_percentage, 2, '.', '')
                               @endphp

                               {{$formated_passed_percentage}}
                            </td>
                            <td>
                               @php
                               $failed_percentage = App\Http\Controllers\StaticValueProviderController::getBatchWiseStudentPassedPercentage($data->batch_id);
                                $formated_failed_percentage = 100 - number_format($failed_percentage, 2, '.', '');
                               @endphp

                               {{$formated_failed_percentage}}
                            </td>
                            <td>
                              @if($data->assessed_req_status == '0')
                                 Not Sent for review
                              @else
                                 Reviewed
                              @endif
                            </td>
                            <td>
                              <div>
                                <div class="dropdown">
                                   @foreach($batch_process as $btp)
                                     @if($data->batch_id == $btp->batch_id)
                                    <a href="{{route('admin.student.report',[$data->batch_id])}}" type="button" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;">View</a>
                                     @endif
                                  @endforeach
                                </div>

                              </div>











                            </td>
                          </tr>
                          @endforeach

                          @endif
                        </tbody>
                      </table>
                      <nav aria-label="Page navigation example" >
                        {{ $batch_data->render("pagination::bootstrap-4") }}
                    </nav>
                    </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->
        </div>
        </div>
        <!-- content-wrapper ends -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="./javascript.js"></script>


<script>

var $rows = $('#table tr');
$('#search3').keyup(function() {
    
    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
        reg = RegExp(val, 'i'),
        text;
    
    $rows.show().filter(function() {
        text = $(this).text().replace(/\s+/g, ' ');
        return !reg.test(text);
    }).hide();
});
</script>

<script>

var $rows = $('#table tr');
$('#search1').keyup(function() {
    
    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
        reg = RegExp(val, 'i'),
        text;
    
    $rows.show().filter(function() {
        text = $(this).text().replace(/\s+/g, ' ');
        return !reg.test(text);
    }).hide();
});
</script>

<script>

var $rows = $('#table tr');
$('#search2').keyup(function() {
    
    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
        reg = RegExp(val, 'i'),
        text;
    
    $rows.show().filter(function() {
        text = $(this).text().replace(/\s+/g, ' ');
        return !reg.test(text);
    }).hide();
});
</script>
        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>


@endsection
