@extends('admin.layouts.home')

@section('content')


<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->

    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
        @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Student Management<span style="float: right;"><a href=""  data-toggle="modal" data-target="#bulkstudentmodal">  <button type="button" class="btn btn-primary" style="float: right;background: #4a9cdf;font-family:lato !important;font-weight: bold;">Bulk Upload</button></a></span></h2> -->


          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body">

                    <h2 class="card-title growwell-card-title" >Student List<a href=""  data-toggle="modal" data-target="#createstudentmodal">  <button type="button" class="btn btn-primary btn-growwell" style="margin-left: 2%;">Create Student</button></a>
                      <span class="headingbtnspan" style="float: right;"><a href=""  data-toggle="modal" data-target="#bulkstudentmodal">  <button type="button" class="btn btn-primary btn-growwell" >Bulk Upload</button></a></span>
                    </h2>





                      <!-- <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">Launch
                          modal Subscription</a>
                      </div> -->

                      @include('admin.include.bulkstudentmodal')
                      @include('admin.include.createstudentmodal')



                  <!-- <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p> -->
                  <div class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                           Email-ID
                          </th>
                          <th>
                            Name
                          </th>
                          <th>
                            Username
                          </th>
                          <th>
                            Address
                          </th>
                          <th>
                            Batch ID
                          </th>
                          <th>
                            SPOC
                          </th>

                          <th>
                            SPOC Email
                          </th>
                          <th>
                            SPOC Phone
                          </th>
                          <th>
                            Dropout
                          </th>

                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!$stu_details->isEmpty())

                        @foreach($stu_details as $item)
                        <tr>

                          <td>
                            {{$item->email}}
                          </td>

                          <td>
                            {{$item->name}}
                          </td>

                          <td>
                            {{$item->username }}
                          </td>
                          <td>
                            {{$item->address}}
                          </td>
                          <td>
                            {{$item->batch_id}}
                          </td>
                          <td>
                            {{$item->spoc}}
                          </td>
                          <td>
                            {{$item->spoc_email}}
                          </td>
                          <td>
                            {{$item->spoc_phone}}
                          </td>
                           <td>
                             @if($item->dropout_status==1)
                                 Yes
                             @else
                                 No
                             @endif
                          </td>
                          <td>

                                <button type="button" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;margin-right:4%;" data-toggle="modal" data-target="#editmodal{{$item->id}}">EDIT</button>
                                @include('admin.include.studentEditModal')


                                <button type="button" class="btn btn-danger btn-growwell-danger" data-toggle="modal" data-target="#deletemodal{{$item->id}}">DELETE</button>
                                @include('admin.include.deleteStudentModal',['items'=>$item])

                          </td>
                        </tr>
                        @endforeach

                        @else


                          <td >
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                          <td>

                          </td>
                          <td>
                            No Data
                          </td>
                          <td>

                          </td>
                          <td>
                          </td>
                          <td>
                          </td>

                          <td></td>
                          <td></td>


                        @endif
                      </tbody>

                    </table>

                    <nav aria-label="Page navigation example" >
                  {{ $stu_details->render("pagination::bootstrap-4") }}
                  </nav>
                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
