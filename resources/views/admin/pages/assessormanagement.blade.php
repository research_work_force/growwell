@extends('admin.layouts.home')

@section('content')
 

<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->
<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
         @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Assessor Management<span style="float: right;"><a href=""  data-toggle="modal" data-target="#bulkassessormodal">  <button type="button" class="btn btn-primary" style="float: right;background: #4a9cdf;font-family:lato !important;font-weight: bold;">Bulk Upload</button></a></span></h2> -->

          <!-- Row 1 -->

          <div class="row collapse" id="demo">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >Search Assessor <span style="float: right;"><a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a></span> </h2>
                  <form class="form-sample" action="{{ route('search.assessor.filters')}}" method="get">
                     {{csrf_field()}}
                    <!-- <p class="card-description">
                      Personal info
                    </p> -->
                    <!-- <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">First Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Last Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="stateinassessor" name="states">

                            </select>
<script>

function removeDups(names) {
  let unique = {};
  names.forEach(function(i) {
    if(!unique[i]) {
      unique[i] = true;
    }
  });
  return Object.keys(unique);
}

  //Create the XHR Object
    let xhr = new XMLHttpRequest;
    //Call the open function, GET-type of request, url, true-asynchronous
    xhr.open('GET', 'https://indian-cities-api-nocbegfhqg.now.sh/cities', true)
    //call the onload
    xhr.onload = function()
        {
            //check if the status is 200(means everything is okay)
            if (this.status === 200)
                {
                    //return server response as an object with JSON.parse
                    let values = JSON.parse(this.responseText);
                    let stateDups=[];
                    let states=[];

                    for(var i = 0; i < values.length; i++){

                               stateDups[i] = values[i].State;

                    }

                    states = removeDups(stateDups).sort();

                    var stateDiv = document.getElementById("stateinassessor");

                    for(var i = 0; i < states.length; i++){

                      var option = document.createElement("OPTION");

                      //Set Customer Name in Text part.
                      option.innerHTML = states[i]

                      //Set CustomerId in Value part.
                      option.value = states[i]

                      //Add the Option element to DropDownList.
                      stateDiv.options.add(option);

                    }






        }
                }
    //call send
    xhr.send();

</script>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor Name</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="assessorname"/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Assessor ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="assessor_id"/>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Domain Job Role</label>
                          <div class="col-sm-9">
                             <input type="text" class="form-control" name="domain_job_role" >
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">TOA Status</label>
                          <div class="col-sm-9">
                             <select class="form-control" name="toa_status" >
                              <option value="Certified">Certified</option>
                              <option value="Provisional">Provisional</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">

                      <div class="col-md-6">
                        <div class="template-demo">
                            <button type="submit" class="btn btn-primary btn-growwell-left" >Search Assessor</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12  " style="padding:0px !important;">
              <div class="card growwell-table-card" >
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title">Assessor Details<span class="headingbtnspan" style="float: right;margin-left: 2%;"> <a href=""  data-toggle="modal" data-target="#createassessormodal">  <button type="button" class="btn btn-primary btn-growwell" >Link Assessor</button></a></span><span class="headingbtnspan" style="float: right;">


                      <a href=""  data-toggle="modal" data-target="#bulkassessormodal" >  <button type="button" class="btn btn-primary btn-growwell" >Bulk Upload</button></a>
                    </span>
                    <a href="#demo" class="btn btn-primary btn-growwell" data-toggle="collapse" style="margin-right: 2%;">Search Assessor </a></h2>




                    <!-- Search Form -->



                    <!-- Search form end -->
                      <!-- <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">Launch
                          modal Subscription</a>
                      </div> -->

                      @include('admin.include.createassessormodal')
                      @include('admin.include.bulkassessormodal')

                  <!-- <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p> -->
                  <div class="table-responsive">
                    <table class="table table-growwell">


                      <thead>
                        <tr>
                          <th >
                            Name
                          </th>
                          <th >
                            Username
                          </th>
                          <th>
                            Phone
                          </th>
                          <th>
                            Availability
                          </th>
                          <th>
                            State
                          </th>
                          <th>
                            Sector
                          </th>
                          <th>
                            Domain Job Role
                          </th>
                          <th>
                            TOA Status
                          </th>
                          <th>
                            Action
                          </th>

                        </tr>
                      </thead>
                      <tbody>

                        @if(!$assessorlist->isEmpty())
                        @foreach($assessorlist as $res)
                        <tr>
                          <td >
                            {{ $res->name }}
                          </td>
                          <td>
                            {{ $res->assessor_id }}
                          </td>
                          <td>
                            {{ $res->phone }}
                          </td>

                          <td>
                            {{ $res->availability }}
                          </td>
                          <td>
                            {{ $res->state }}
                          </td>
                          <td>
                            {{ $res->sectors }}
                          </td>
                          <td>
                            {{ $res->domain_job_role }}
                          </td>
                          <td>
                            {{ $res->toa_status }}
                          </td>

                          <td>

                             @include('admin.include.deleteAssessorModal',['item'=>$res])
                                @include('admin.include.editassessormodal',['assessor_data'=>$res])



                                <div class="btn-group dropup">
                                    <button type="button" class="btn btn-primary btn-growwell dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                      </button>
                                      <div class="dropdown-menu" style="background-color: #27313b;">
                                        <!-- Dropdown menu links -->

                                        <a class="dropdown-item" style="color: #fff;" href="" data-toggle="modal" data-target="#editAssessormodal{{$res->id}}">Edit</a>

                                       <a class="dropdown-item" style="color: #fff;" href="{{route('admin.assessor.profile',[$res->assessor_id])}}" >Assessor Details</a>



 
                                       <a class="dropdown-item" style="color: #fff;" href="" data-toggle="modal" data-target="#deleteAssessorModal{{$res->id}}">Delete</a>

                                      </div>
                                </div>






                          </td>

                        </tr>
                        @endforeach
                        @else

                          <td >
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                          <td>

                          </td>
                          <td>
                            No Data
                          </td>
                          <td>

                          </td>
                          <td>
                          </td>
                          <td>
                          </td>

                          <td></td>


                        @endif
                      </tbody>
                    </table>
                    {{ $assessorlist->render("pagination::bootstrap-4") }}



                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
