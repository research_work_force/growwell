@extends('admin.layouts.home')

@section('content')
 
<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none; 
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>

<!-- Navbar start -->
 @include('assessor.include.navbar')
   <!-- Navbar end -->

    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
        @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
        <div class="content-wrapper">

          <!-- Row 1 -->

            <div class="col-12 grid-margin ">
              <div class="card ">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >View Report</h2>




                  <form class="form-sample " >

                    <div class="row" style="margin-bottom:2%;">
                      <div class="col-md-6">

                      </div>
                      <div class="col-md-6">

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <div class="col-sm-6"><b style="color:#27331b;">Name of the Assessment Agency</b></div>
                          <div class="col-sm-6">
                            {{$assessor_info->assessment_agency_aligned}}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-6"><b style="color:#27331b;">Location</b></div>
                          <div class="col-sm-6">
                            {{$batch_info->training_center_location}}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-8"><b style="color:#27331b;">Number of candidates</b></div>
                          <div class="col-sm-4">
                            @php
                          $count_stdnt = App\Http\Controllers\StaticValueProviderController::numberOfStudentPerBatch($batch_info->batch_id);

                            @endphp

                            {{$count_stdnt}}
                          </div>
                        </div>
                      </div>


                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <div class="col-sm-6"><b style="color:#27331b;">Sector Skill Council</b></div>
                          <div class="col-sm-6">
                            {{$batch_info->sectors}}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-6"><b style="color:#27331b;">Date of Assessment</b></div>
                          <div class="col-sm-6">
                            {{$batch_info->assessment_date_fdt}}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-8"><b style="color:#27331b;">Job Role</b></div>
                          <div class="col-sm-4">
                            {{$batch_info->jobRole}}
                          </div>
                        </div>
                      </div>



                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <div class="col-sm-6"><b style="color:#27331b;">Assessor Name</b></div>
                          <div class="col-sm-6">
                            {{$assessor_info->name}}
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group row">
                          <div class="col-sm-6"><b style="color:#27331b;">Batch ID</b></div>
                          <div class="col-sm-6">
                            {{$batch_info->batch_id}}
                          </div>
                        </div>
                      </div>



                    </div>

                    <div class="row collapse" id="demo">

                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">
                          <a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a>
                      </div>

                    </div>

                    <div class="row collapse" id="demo">

                      <div class="col-md-3">
                        <div class="form-group row">
                          <!-- <div class="col-sm-4"><label class=" col-form-label"><b style="color: #27313b">Student ID</b></label></div> -->
                          <div class="col-sm-8">
                            <input type="text" id="search" style="padding:5%;color:grey;" placeholder="Student ID">
                          </div>
                          <div class="col-sm-4" >
                            <button type="button" class="btn btn-primary btn-growwell-left"><i class="mdi mdi-magnify menu-icon " style="color:white;"></i></button>
                          </div>


                        </div>
                      </div>
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">

                      </div>

                    </div>





                    <!-- <div class="row collapse" id="demo">

                      <div class="col-md-4">
                        <button type="button" class="btn btn-primary btn-growwell-left">Search Report</button>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div> -->
                  </form>
                </div>
              </div>
            </div>
  
          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" >Report Details <a href="#demo" class="btn btn-primary btn-growwell" data-toggle="collapse" >Search Report </a>
                       <a href="{{ url('/gen_pdf',$batch_id) }}" class="btn btn-primary btn-growwell"  style="margin-right:2%;">Export PDF </a>
                    <a href="javascript:void(0);" id="btnExport" class="btn btn-primary btn-growwell"  style="margin-right:2%;">Export Excel </a>

                    </h2>
                  
                  @include('admin.include.reviewtoassessormodal',['batch_id' => $batch_id])

                  


 



                                       <div class="table-responsive" id="export_to_excel">


                                        <table id="table" class="table table-growwell" style="text-align: center;">
                                              <col>
                                              <colgroup span="2"></colgroup>
                                              <colgroup span="2"></colgroup>

                                              <tr style="background: #27313b;COLOR: white;">
                                                <th rowspan="1" colspan="3"></th>


                                                 @php
                                                   $sections = App\Http\Controllers\StaticValueProviderController::getBatchSectionJobRoleWise($batch_id);

                                                  @endphp
                                                   <th id="add_col" style="display:none;"></th>
                                               <th colspan="1" scope="colgroup">Candidate Image Link</th>
                                               <th colspan="1" scope="colgroup">Candidate Video Link</th> 
                                               <th colspan="1" scope="colgroup">Assessor Image Link</th>
                                                 
                                                    @foreach($sections as $sec)
                                                       <th colspan="1" scope="colgroup">{{ $sec->section }}</th>
                                                    @endforeach
                                                <th colspan="1" scope="colgroup" >Total</th>
                                                <th colspan="1" scope="colgroup" >Percentage</th>
                                                <th colspan="1" scope="colgroup" >Grade</th>
                                                <th colspan="1" scope="colgroup" >Result</th>
                                                <th colspan="1" scope="colgroup" >Attendance Status</th>
                                                <th colspan="1" scope="colgroup" id="remove_col">Action</th>
                                              </tr>
                                                 @php
                                                   $secmarks = App\Http\Controllers\StaticValueProviderController::getTotalMarksSectionWise($batch_id);

                                                  @endphp
                                
                                             <tr>
                                               <th >Candidate Id</th>
                                               <th >Candidate Name</th>
                                               <th ></th>
                                               <th ></th>
                                               <th ></th>

                                               
                            
                                                <th></th>
                                             @for($i=0; $i < count($secmarks); $i++)
                                                  <th scope="col">{{$secmarks[$i]}}</th>
                                                 @endfor
                                               </tr>
                                              <tr>
                                                 


                                              @foreach($student_data as $key => $data)

                                                  @php

                                                   $stdnt_name = App\Http\Controllers\StaticValueProviderController::nameOfStudentPerBatch($batch_id,$key);

                                                  @endphp

                                                <td>{{$key}}</td>
                                                <td>{{$stdnt_name->name}}</td>
                                                <td></td>
                                                  @php

                                                   $candidatesImgVidLink = App\Http\Controllers\StaticValueProviderController::candidatesImgVideoLink($batch_id,$key);

                                                  @endphp

                                                 <td>
                                                  @if($candidatesImgVidLink->recorded_video_name == null)
                            
                                                  @else
                                                  <a href="{{ asset('growwell/student/') }}/{{$candidatesImgVidLink->recorded_video_name}}" target="_blank">Video Link</a>
                                                  @endif
                                                 </td>
                                                 <td>
                                            @if($candidatesImgVidLink->img_name == null)
                            
                                          @else
                                          <a href="{{ asset('theme/images/studentexamimage') }}/ {{$candidatesImgVidLink->img_name}}" target="_blank">Image Link</a>
                                          @endif
                                          </td>


 
                                                @php

                                                   $assessorimgLink = App\Http\Controllers\StaticValueProviderController::assessorImgLink($batch_id,$key);

                                                  @endphp


                                                 <td>
                                                  @if($assessorimgLink['img']==null)

                                                  @else
                                                  <a href="{{ asset('theme/images/assessor') }}/ {{$assessorimgLink['img']}}" target="_blank">Image Link</a>
                                                  @endif

                                                </td>                                                  
                              

                                             @foreach($student_data[$key] as $d)

                                              @if(Intval($d->mark) < 0)
                                                <td>NA</td>
                                              @else
                                                <td>{{$d->mark}}</td>
                                              @endif
                                              @endforeach



                                            


                                                 @php

                                                   $totalmarks = App\Http\Controllers\StaticValueProviderController::totalMarksPerStudent($student_data[$key]);

                                                  @endphp


                                                                                    
                                                   <td>{{$totalmarks}}</td>


                                                
                                                @php

                                                   $percentage_marks = App\Http\Controllers\StaticValueProviderController::marksPercentagePerStudent($totalmarks,$batch_id);
                                                   $per_marks_modified = number_format($percentage_marks, 2, '.', '')
                                                  @endphp


                                                                                    
                                                   <td>{{$per_marks_modified}}</td>



                                                @php

                                                   $grade = App\Http\Controllers\StaticValueProviderController::gradePerStudent($per_marks_modified);
                                                  @endphp


                                                                                    
                                                   <td>{{$grade}}</td>


                                               @php

                                                  $passorfail = App\Http\Controllers\StaticValueProviderController::passOrFailPerStudent($per_marks_modified);
                                                  @endphp


                                                  @if($passorfail==1)                   
                                                    <td style="color:green;font-weight: Bold;">Pass</td>
                                                  @else
                                                    <td style="color:red;font-weight: Bold;">Fail</td>
                                                  @endif
                                                
                                       @php
                                         $attdnc = App\Http\Controllers\StaticValueProviderController::studentAttendanceChecking($batch_id,$key);

                                         @endphp

                                       @if(empty($attdnc))
                                
                                         <td>Absent</td>
                                       @else
                                        @if($attdnc->schedule_test_status == '4')
                                         <td>Present</td>
                                         @elseif($attdnc->schedule_test_status == '2')
                                            <td>Present</td>
                                         @else
                                            <td>Absent</td>
                                            @endif
                                           @endif


                                                @php
                                         $attdnc = App\Http\Controllers\StaticValueProviderController::studentAttendanceChecking($batch_id,$key);

                                         @endphp

                                         @if(empty($attdnc))
                                
                                         <td id="remove_col" >NA</td>
                                       @else
                                        @if($attdnc->schedule_test_status == '4')
                                          <td id="remove_col" ><a href="{{route('admin.markswise.report',[$batch_id,$key])}}" type="button" class="btn btn-primary btn-growwell-left">View Marks</a></td>
                                         @elseif($attdnc->schedule_test_status == '2')
                                             <td> id="remove_col" <a href="{{route('admin.markswise.report',[$batch_id,$key])}}" type="button" class="btn btn-primary btn-growwell-left">View Marks</a></td>
                                         @else
                                            <td id="remove_col" >NA</td>
                                            @endif
                                           @endif

                                               




                                              </tr>
                                              @endforeach
                                            </table>
                                              



                                      </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->

        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
     <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript" src="./javascript.js"></script>


<script>

var $rows = $('#table tr');
$('#search').keyup(function() {
    
    var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
        reg = RegExp(val, 'i'),
        text;
    
    $rows.show().filter(function() {
        text = $(this).text().replace(/\s+/g, ' ');
        return !reg.test(text);
    }).hide();
});
</script>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script type="text/javascript">


    $(function () {
        $("#btnExport").click(function () {
            $("#export_to_excel").table2excel({
                exclude: "#remove_col",
                include: "#add_col",
                filename: "student_report.xls"
            });
           
        });
    });

</script>

@endsection
