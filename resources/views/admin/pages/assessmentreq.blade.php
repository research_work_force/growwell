@extends('admin.layouts.home')

@section('content')


 <!-- Navbar start -->

  @include('admin.include.navbaradmin')
 <!-- Navbar end -->
 
    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
        @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
        <div class="content-wrapper">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title growwell-card-title" >View All Batches</h2>



                  <!-- Tab start -->


                  <div class="c-assess-main">
                  <ul class="nav nav-pills pills-dark mb-3 nav-justified" id="pills-tab" role="tablist" style="margin-bottom: 40px; width: 45%;border:none;">
                      <li class="nav-item" style="margin-right: 16px;">
                          <a class="nav-link {{ \Request::is('assessmentreq/*') ? 'active' : '' }}" id="pills-First-tab" data-toggle="pill" href="#First" role="tab" aria-controls="pills-First" aria-selected="true">Assessment Batch Request</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link {{ \Request::is('assessed/q*') ? 'active' : '' }}" id="pills-Second-tab" data-toggle="pill" href="#Second" role="tab" aria-controls="pills-Second" aria-selected="false">Assessed Batch Request</a>
                      </li>
                  </ul>



                        <div class="tab-content" id="pills-tabContent" style="border: none !important;">
                            <div class="tab-pane {{ \Request::is('assessmentreq/*') ? 'active' : '' }} show fade  " id="First" role="tabpanel" aria-labelledby="pills-First-tab">
                                <ul class="nav nav-pills pills-dark mb-3 nav-justified" id="pills-tab" role="tablist" style="border: none;">
                                    <li class="nav-item" style="margin-right: 16px;">
                                        <a class="nav-link {{ \Request::is('assessmentreq/0') ? 'active' : '' }}" id="pills-pending-tab" href="/assessmentreq/0" >Pending Request</a>
                                    </li>
                                    <li class="nav-item" style="margin-right: 16px;">
                                        <a class="nav-link {{ \Request::is('assessmentreq/1') ? 'active' : '' }}" id="pills-accepted-tab" href="/assessmentreq/1">Accepted</a>
                                    </li>
                                    <li class="nav-item" style="margin-right: 16px;">
                                        <a class="nav-link {{ \Request::is('assessmentreq/2') ? 'active' : '' }}" id="pills-Rejected-tab" href="/assessmentreq/2" >Rejected</a>
                                    </li>
                                    <li class="nav-item" >
                                        <a class="nav-link {{ \Request::is('assessmentreq/-1') ? 'active' : '' }}" id="pills-Cancelled-tab" href="/assessmentreq/-1">Cancelled</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane {{ \Request::is('assessed/q/*') ? 'active' : '' }} face" id="Second" role="tabpanel" aria-labelledby="pills-Second-tab">
                                <ul class="nav nav-pills pills-dark mb-3 nav-justified" id="pills-tab" role="tablist">
                                    <li class="nav-item" style="margin-right: 16px;">
                                        <a class="nav-link {{ \Request::is('assessed/q/1') ? 'active' : '' }}" id="pills-Cancelled-tab" href="/assessed/q/1">Pending</a>
                                    </li>
                                    <li class="nav-item" style="margin-right: 16px;">
                                       <a class="nav-link {{ \Request::is('assessed/q/2') ? 'active' : '' }}" id="pills-Cancelled-tab" href="/assessed/q/2">Approved</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                      </div>

                  <!-- Tab End -->
                  <form class="form-sample collapse" id="demo" action="{{ route('admin.batchreq.search') }}" method="get">
                    {{csrf_field()}}

                    <div class="row" style="margin-bottom:2%;">
                      <div class="col-md-6">
                       <!--  <h2 > <span style="background-color: #f7f7f7;padding: 2%;color:#464dee;border-radius:2%;">Batch Count</span> <span style=" background-color: #f7f7f7;padding: 2%;color: blue;"> 102</span></h2> -->
                      </div>
                      <div class="col-md-6">
                        <a href="#demo" class="btn btn-danger " data-toggle="collapse" style="float:right;">X </a>
                      </div>
                    </div>

                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Scheme/ Program/ Model</label>
                          <div class="col-sm-9">
                          @php
                               $schemeProgram = App\Http\Controllers\StaticValueProviderController::schemeProgram();

                          @endphp
                            <select class="form-control" name="schemeProgram">
                             @foreach($schemeProgram as $sprog)
                              <option value="{{ $sprog->name }}">{{ $sprog->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                            @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="jobRole">
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">State</label>
                          <div class="col-sm-9">
                            <select class="form-control" id="states" name="states">

                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch ID</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" name="batch_id"/>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- <p class="card-description">
                      Address
                    </p> -->
                   <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sector</label>
                          <div class="col-sm-9">
                            @php
                               $sectors = App\Http\Controllers\StaticValueProviderController::sectors();

                            @endphp
                            <select class="form-control" name="sectors">
                             @foreach($sectors as $sector)
                              <option value="{{ $sector->name }}">{{ $sector->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">

                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Batch Type</label>
                          <div class="col-sm-9">
                              @php
                               $batchtype = App\Http\Controllers\StaticValueProviderController::batchTypes();

                             @endphp
                            <select class="form-control" name="batchTypes">
                             @foreach($batchtype as $bat)
                              <option value="{{ $bat->name }}">{{ $bat->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                        <!-- <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Membership</label>
                          <div class="col-sm-4">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios1" value="" checked>
                                Free
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-5">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="membershipRadios" id="membershipRadios2" value="option2">
                                Professional
                              </label>
                            </div>
                          </div>
                        </div> -->
                      </div>
                    </div>
                    <!-- <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Address 2</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Postcode</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                    </div> -->


                    <!-- <div class="row">
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">City</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" />
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Country</label>
                          <div class="col-sm-9">
                            <select class="form-control">
                              <option>America</option>
                              <option>Italy</option>
                              <option>Russia</option>
                              <option>Britain</option>
                            </select>
                          </div>


                        </div>
                      </div>
                    </div> -->

                    <div class="row">

                      <div class="col-md-4">
                        <button type="submit" class="btn btn-primary btn-growwell-left">Search Batch</button>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>


          <!-- Row 1 end -->
           <!-- row 2 -->
           <div class="row">
             <div class="col-lg-12  " style="padding:24px !important;">
               <div class="card growwell-table-card" >
                 <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" >Batch Details <a href="#demo" class="btn btn-primary btn-growwell" data-toggle="collapse" >Search Batches </a></h2>

                  <div id="assessmenttab" class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                         <table id="assessortablereq" class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                            Assessor ID
                          </th>
                          <th>
                            Scheme/ Program/ Model
                          </th>
                          <th>
                            Job Role
                          </th>
                          <th>
                            Sector
                          </th>
                          <th>
                            Batch Type
                          </th>
                          <th>
                             Preffered Assessment Language
                          </th>
                          <th>
                            Location
                          </th>
                          <th>
                            Batch Size
                          </th>
                          <th>
                            Enrolled Canditates
                          </th>
                          <th>
                            Dropout
                          </th>
                          <th>
                            Batch ID
                          </th>
                          <th>
                            Assessment Date
                          </th>
                          <th>
                            State
                          </th>
                          <th>
                            Status
                          </th>
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>

                      <tbody>
                        @if(!$batched_assigned->isEmpty())
                        @foreach($batched_assigned as $batch)
                        <tr>
                          <td >
                            {{ $batch->assessor_id }}
                          </td>
                          <td >
                            {{ $batch->schemeProgram }}
                          </td>
                          <td>
                            {{ $batch->jobRole }}
                          </td>
                          <td>
                            {{ $batch->sectors }}
                          </td>
                          <td>
                            {{ $batch->batchTypes }}
                          </td>
                          <td>
                           {{ $batch->language }}
                          </td>
                          <td>
                            {{ $batch->training_center_location }}
                          </td>
                          <td>
                            {{ $batch->batch_size }}
                          </td>
                           @foreach($enrolled_candidate_array as $key => $enroll)
                          @if($key == $batch->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif

                          @endforeach
                          @foreach($dropout_candidate_array as $key => $enroll)
                          @if($key == $batch->batch_id)
                          <td>
                             {{ $enroll  }}
                          </td>
                          @endif
                          @endforeach

                          <td>
                            {{ $batch->batch_id  }}
                          </td>
                          <td>
                            {{ $batch->assessment_date_fdt }}
                          </td>
                          <td>
                            {{ $batch->states }}
                          </td>
                          <td>
                             @if ($batch->req_status == '0')


                                 Action Pending


                              @elseif($batch->req_status == '1')

                                  @if($batch->assessed_req_status == '0')

                                     Accepted

                                    @elseif($batch->assessed_req_status == '1')

                                    Review Pending

                                    @elseif($batch->assessed_req_status == '2')

                                    Approved

                                    @elseif($batch->assessed_req_status == '3')

                                    In Review

                                    @endif

                              @elseif($batch->req_status == '2')

                                 Rejected

                              @else

                              Cancelled

                              @endif


                          </td>
                          <td>
                             @if ($batch->req_status == '0' && $batch->assessed_req_status == '0')


                                 <button type="button" class="btn btn-danger btn-growwell-danger">Pending</button>


                              @elseif($batch->req_status == '1' && $batch->assessed_req_status == '0')

                               
                                
                              @elseif($batch->req_status == '2' && $batch->assessed_req_status == '0')

                                     <button type="button" data-toggle="modal" data-target="#assessorassignbatch{{$batch->batch_id}}modal" class="btn btn-succes btn-growwell-success" style="COLOR:WHITE;" >Reassign</button>
                                  @include('admin.include.assessorassignbatchmodal',['batch_id'=>$batch->batch_id,'id'=>$batch->id])
                                 @endif


                          
    
                          <!-- Assessed Batch Condition -->

                             @if($batch->req_status == '1' && $batch->assessed_req_status == '1')

                                 <a href="{{route('admin.assessreq.student.list',[$batch->batch_id])}}" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;">Student List</a>


                                 <a href="{{route('admin.assessreq.student.approve',[$batch->batch_id])}}" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;">Approve</a>
                                @elseif($batch->assessed_req_status == '2')

                                @elseif($batch->assessed_req_status == '3')
                                    
                                <a href="{{route('admin.assessreq.student.approve',[$batch->batch_id])}}" class="btn btn-success btn-growwell-success" style="COLOR:WHITE;">Approve</a>                                   

                                 @endif

                          </td>
                        </tr>
                        @endforeach
                           @else

                              <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                 <td></td>

                                <td>No Data</td>

                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>


                              </tr>
                            @endif


                      </tbody>
                    </table>
                     {{ $batched_assigned->render("pagination::bootstrap-4") }}
                  </div>
                </div>
              </div>
            </div>

          <!-- row 1 ends -->

        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->



@endsection
