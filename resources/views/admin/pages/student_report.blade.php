<style>
        table, th,tr,td {
          {{-- border: 1px solid black;
          border-collapse:collapse;
          border-left:1px solid black;
          border-right:1px solid black; --}}
          padding:4px;
         
        }
        </style>
          
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body growwell-table-cardbody">

                    <h2 class="card-title growwell-card-title" >Report Details  </h2>
                  
                   <div class="table-responsive" >


                   <table class="table table-growwell" style="text-align: center;">
                                              <col>
                                              <colgroup span="2"></colgroup>
                                              <colgroup span="2"></colgroup>

                                              <tr style="background: #27313b; COLOR: white;">
                                                <th rowspan="1" colspan="3"></th>


                                                 @php
                                                   $sections = App\Http\Controllers\StaticValueProviderController::getBatchSectionJobRoleWise($batch_id);

                                                  @endphp
                                                   <th id="add_col" style="display:none;"></th>
                                               <th colspan="1" scope="colgroup">Candidate Image Link</th>
                                               <th colspan="1" scope="colgroup">Candidate Video Link</th> 
                                               <th colspan="1" scope="colgroup">Assessor Image Link</th>
                                                 
                                                    @foreach($sections as $sec)
                                                       <th colspan="1" scope="colgroup">{{ $sec->section }}</th>
                                                    @endforeach
                                                <th colspan="1" scope="colgroup" >Total</th>
                                                <th colspan="1" scope="colgroup" >Percentage</th>
                                                <th colspan="1" scope="colgroup" >Grade</th>
                                                <th colspan="1" scope="colgroup" >Result</th>
                                                <th colspan="1" scope="colgroup" >Attendance Status</th>
                                                <th colspan="1" scope="colgroup" id="remove_col">Action</th>
                                              </tr>
                                                 @php
                                                   $secmarks = App\Http\Controllers\StaticValueProviderController::getTotalMarksSectionWise($batch_id);

                                                  @endphp
                                
                                             <tr>
                                               <th >Candidate Id</th>
                                               <th >Candidate Name</th>
                                               <th ></th>
                                               <th ></th>
                                               <th ></th>

                                               
                            
                                                <th></th>
                                             @for($i=0; $i < count($secmarks); $i++)
                                                  <th scope="col">{{$secmarks[$i]}}</th>
                                                 @endfor
                                               </tr>
                                              <tr>
                                                 


                                              @foreach($student_data as $key => $data)

                                                  @php

                                                   $stdnt_name = App\Http\Controllers\StaticValueProviderController::nameOfStudentPerBatch($batch_id,$key);

                                                  @endphp

                                                <td>{{$key}}</td>
                                                <td>{{$stdnt_name->name}}</td>
                                                 <td></td>

                                                  @php

                                                   $candidatesImgVidLink = App\Http\Controllers\StaticValueProviderController::candidatesImgVideoLink($batch_id,$key);

                                                  @endphp

                                                 <td>
                                                  @if($candidatesImgVidLink->recorded_video_name == null)
                            
                                                  @else
                                                  <a href="{{ asset('growwell/student/') }}/{{$candidatesImgVidLink->recorded_video_name}}" target="_blank">Video Link</a>
                                                  @endif
                                                 </td>
                                                 <td>
                                            @if($candidatesImgVidLink->img_name == null)
                            
                                          @else
                                          <a href="{{ asset('theme/images/studentexamimage') }}/ {{$candidatesImgVidLink->img_name}}" target="_blank">Image Link</a>
                                          @endif
                                          </td>



                                                @php

                                                   $assessorimgLink = App\Http\Controllers\StaticValueProviderController::assessorImgLink($batch_id,$key);

                                                  @endphp

 
                                                 <td>
                                                  @if($assessorimgLink['assessor_img'] == null)
                                                   NA
                                                  @else
                                                  <a href="{{ asset('theme/images/assessor') }}/ {{$assessorimgLink->assessor_img}}" target="_blank">Image Link</a>
                                                  @endif
                                                </td>                                                  
                              

                                             @foreach($student_data[$key] as $d)

                                              @if(Intval($d->mark) < 0)
                                                <td>NA</td>
                                              @else
                                                <td>{{$d->mark}}</td>
                                              @endif
                                              @endforeach




                                                 @php

                                                   $totalmarks = App\Http\Controllers\StaticValueProviderController::totalMarksPerStudent($student_data[$key]);

                                                  @endphp


                                                                                    
                                                   <td>{{$totalmarks}}</td>


                                                
                                                @php

                                                   $percentage_marks = App\Http\Controllers\StaticValueProviderController::marksPercentagePerStudent($totalmarks,$batch_id);
                                                   $per_marks_modified = number_format($percentage_marks, 2, '.', '')
                                                  @endphp


                                                                                    
                                                   <td>{{$per_marks_modified}}</td>



                                                @php

                                                   $grade = App\Http\Controllers\StaticValueProviderController::gradePerStudent($per_marks_modified);
                                                  @endphp


                                                                                    
                                                   <td>{{$grade}}</td>


                                               @php

                                                  $passorfail = App\Http\Controllers\StaticValueProviderController::passOrFailPerStudent($per_marks_modified);
                                                  @endphp


                                                  @if($passorfail==1)                   
                                                    <td style="color:green;font-weight: Bold;">Pass</td>
                                                  @else
                                                    <td style="color:red;font-weight: Bold;">Fail</td>
                                                  @endif
                                                
                                       @php
                                         $attdnc = App\Http\Controllers\StaticValueProviderController::studentAttendanceChecking($batch_id,$key);

                                         @endphp

                                       @if(empty($attdnc))
                                
                                         <td>Absent</td>
                                       @else
                                        @if($attdnc->schedule_test_status == '4')
                                         <td>Present</td>
                                         @elseif($attdnc->schedule_test_status == '2')
                                            <td>Present</td>
                                         @else
                                            <td>Absent</td>
                                            @endif
                                           @endif


                                                @php
                                         $attdnc = App\Http\Controllers\StaticValueProviderController::studentAttendanceChecking($batch_id,$key);

                                         @endphp

                                         @if(empty($attdnc))
                                
                                         <td id="remove_col" >NA</td>
                                       @else
                                        @if($attdnc->schedule_test_status == '4')
                                          <td id="remove_col" ><a href="{{route('admin.markswise.report',[$batch_id,$key])}}" type="button" class="btn btn-primary btn-growwell-left">View Marks</a></td>
                                         @elseif($attdnc->schedule_test_status == '2')
                                             <td> id="remove_col" <a href="{{route('admin.markswise.report',[$batch_id,$key])}}" type="button" class="btn btn-primary btn-growwell-left">View Marks</a></td>
                                         @else
                                            <td id="remove_col" >NA</td>
                                            @endif
                                           @endif


                                              </tr>
                                              @endforeach
                                            </table>



                  </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->

        </div>