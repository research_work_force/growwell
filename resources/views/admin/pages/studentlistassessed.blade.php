@extends('admin.layouts.home')

@section('content')

 
<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->

    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
        @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
        <div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Student Management<span style="float: right;"><a href=""  data-toggle="modal" data-target="#bulkstudentmodal">  <button type="button" class="btn btn-primary" style="float: right;background: #4a9cdf;font-family:lato !important;font-weight: bold;">Bulk Upload</button></a></span></h2> -->


          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card">
                <div class="card-body">

                    <h2 class="card-title growwell-card-title" >Student List(Batch wise)

                      <a href=""  data-toggle="modal" data-target="#reviewtoassessor">  <button type="button" class="btn btn-primary btn-growwell" style="margin-left: 2%;">Review</button></a>
                      @include('admin.include.reviewtoassessormodal',['batch_id'=>$batch_id])
                    </h2>


                  <div class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                           Email-ID
                          </th>
                          <th>
                            Name
                          </th>
                          <th>
                            Username
                          </th>
                          <th>
                            Address
                          </th>
                          <th>
                            Batch ID
                          </th>
                          <th>
                            Recorded Video
                          </th>
                          <th>
                            Image Link
                          </th>

                          <th>
                            Status
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @if(!$stu_details->isEmpty())

                        @foreach($stu_details as $item)
                        <tr>

                          <td>
                            {{$item->email}}
                          </td>

                          <td>
                            {{$item->name}}
                          </td>

                          <td>
                            {{$item->username }}
                          </td>
                          <td>
                            {{$item->address}}
                          </td>
                          <td>
                            {{$item->batch_id}}
                          </td>
                          <td>
                            @if($item->recorded_video_name == null)
                            
                            @else
                            <a href="{{ asset('growwell/student/') }}/{{$item->recorded_video_name}}" target="_blank">Video Link</a>
                            @endif
                          </td>
                          <td>
                            @if($item->recorded_video_name == null)
                            
                            @else
                            <a href="{{ asset('theme/images/studentexamimage') }}/ {{$item->img_name}}" target="_blank">Image Link</a>
                            @endif
                          </td>
                           <td>
                                @if($item->schedule_test_status == 0)
                                  <span class="badge"> Pending for Action</span>
                                @elseif($item->schedule_test_status == 1)
                                   <span class="badge">  Test Scheduled </span>
                                @elseif($item->schedule_test_status == 2)
                                   <span class="badge">  Inprogress </span>
                                @elseif($item->schedule_test_status == 3)
                                   <span class="badge">  Incomplete </span>
                                @elseif($item->schedule_test_status == 4)
                                   <span class="badge">  Completed </span>
                                @else
                                   <span class="badge">  NA </span>
                                @endif
                          </td>
                        </tr>
                        @endforeach

                        @else


                          <td >
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>
                          <td>

                          </td>
                          <td>
                            No Data
                          </td>
                          <td>
                           
                          </td>
                          <td>
                          </td>
                          <td>
                          </td>

                          <td></td>


                        @endif
                      </tbody>

                    </table>

                    <nav aria-label="Page navigation example" >
                  {{ $stu_details->render("pagination::bootstrap-4") }}
                  </nav>
                  </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->

        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->



@endsection
