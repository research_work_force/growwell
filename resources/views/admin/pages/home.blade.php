@extends('admin.layouts.home')

@section('content')



<!-- Navbar start -->

 @include('admin.include.navbaradmindashboard')
<!-- Navbar end -->


    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper">
			<div class="main-panel">
        @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
          <div class="content-wrapper" style="background-color:  white;" >

            <div class="row" align="center">
              <div class="col-md-2">

              </div>
              <div class="col-md-8">
                <img class="dashboardimage" src="{{asset('theme/images/dashboardbanner1.jpg')}}" width="716" height="293" alt="dashboardbanner" />
              </div>
              <div class="col-md-2">

              </div>


            </div>

             <!-- row 1 -->
            <div class="row">
              <div class="col-md-2">
              </div>
  						<div class="col-md-8" style="text-align: center;">


  									<h2 class="text-dark font-weight-bold mb-2 menucardtitle" id="dashboardheading" style="margin-top: 2%; margin-bottom: 5% !important; ">Hey Admin, Let's upskill the youth! </h2>
  									<!-- <h6 class="font-weight-normal mb-2">Last login was 23 hours ago. View details</h6> -->


  						</div>
              <div class="col-md-2">
              </div>

  					</div>
  					<!-- row 1 ends -->
  					<br>


  					<div class="row">
  						<div class="col-sm-12 flex-column d-flex stretch-card">
  							<div class="row ">

                  <div class="col-lg-3 d-flex grid-margin stretch-card ">
  									<a href="/assessmentreq/0" class="card growellmenucardpink talhref">
  										<div class="card-body text-white">
  											<div class="row">
  												<div class="col-md-12" style="font-size: 100px;" align="center">
  													<i class="mdi mdi-clipboard-text menu-icon" style="color:#fff;"></i>
  												</div>

  												<div class="col-md-12">
  													<h2 class="menucardtitle">Assessment Request</h2>
  												</div>

  											</div>


  										</div>
  									</a>
  								</div>


  								<div class="col-lg-3 d-flex grid-margin stretch-card">
  									<a href="/assessormanagement" class="card growellmenucardpurple talhref">
  										<div class="card-body text-white">
  											<div class="row">
  												<div class="col-md-12" style="font-size: 100px;" align="center">
  												<i class="mdi mdi-account-box menu-icon " style="color:#fff;"></i>
  												</div>

  												<div class="col-md-12 talletter">
  													<h2 class="menucardtitle" >View <br> Assessors</h2>
  												</div>

  											</div>
  										</div>
  									</a>
  								</div>


  								<!-- <div class="col-lg-3 d-flex grid-margin stretch-card">
  									<a href="/searchassessor" class="card growellmenucardgreen talhref">
  										<div class="card-body text-white">
  											<div class="row">
  												<div class="col-md-12" style="font-size: 100px;" align="center">
  													<i class="mdi mdi-account-search menu-icon " style="color:#fff;"></i>
  												</div>

  												<div class="col-md-12 talletter">
  													<h2 class="menucardtitle">Search <br> Assessors</h2>
  												</div>

  											</div>
  										</div>
  									</a>
  								</div> -->


                   <div class="col-lg-3 d-flex grid-margin stretch-card">

                      <a href="/batchmanagement" class="card growellmenucardcyan talhref " >
                      <div class="card-body text-white">
                        <div class="row">
                          <div class="col-md-12" style="font-size: 100px;" align="center">
                            <i class="mdi mdi-animation menu-icon " style="color: #fff;"></i>
                          </div>

                          <div class="col-md-12 ">
                            <h2 class="menucardtitle">Create <br> Batches</h2>
                          </div>

                        </div>
                      </div>
                      </a>

                  </div>

                <div class="col-lg-3 d-flex grid-margin stretch-card" style="">

                      <a href="/studentmanagement" class="card growellmenucardblue  talhref" >
                      <div class="card-body text-white">
                        <div class="row">
                          <div class="col-md-12" style="font-size: 100px;" align="center">
                            <i class="mdi mdi-school menu-icon " style="color:#fff;"></i>

                          </div>



                          <div class="col-md-12 ">
                            <h2 class="menucardtitle">Upload <br>Candidates</h2>
                          </div>

                        </div>
                      </div>
                      </a>

                  </div>

  							</div>

  						</div>

  					</div>

  					<div class="row">
  						<div class="col-sm-12 flex-column d-flex stretch-card">
  							<div class="row">

                    <div class="col-lg-3 d-flex grid-margin stretch-card">
                    <a href="/questionmanagement" class="card growellmenucardred talhref" >
                      <div class="card-body text-white">
                        <div class="row">
                          <div class="col-md-12" style="font-size: 100px;" align="center">
                            <i class="mdi mdi-checkbox-marked-circle-outline menu-icon " style="color:#fff;"></i>
                          </div>

                          <div class="col-md-12 ">
                            <h2 class="menucardtitle">Create/Upload <br>Tests</h2>
                          </div>

                        </div>


                      </div>
                    </a>
                  </div>


                  <div class="col-lg-3 d-flex grid-margin stretch-card">

                      <a href="/assessorreport" class="card growellmenucardyellow talhref" >
                      <div class="card-body text-white">
                        <div class="row">
                          <div class="col-md-12" style="font-size: 100px;" align="center">
                            <i class="mdi mdi-trending-up menu-icon " style="color:#fff;"></i>
                          </div>

                          <div class="col-md-12 ">
                            <h2 class="menucardtitle">View <br> Reports</h2>
                          </div>

                        </div>
                      </div>
                      </a>

                  </div>

  							</div>

  						</div>

  					</div>




  				</div>
  				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
