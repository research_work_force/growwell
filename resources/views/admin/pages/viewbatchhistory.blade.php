@extends('admin.layouts.home')

@section('content')

 
<!-- Navbar start -->

 @include('admin.include.navbaradmin')
<!-- Navbar end -->

 

    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Profile Management</h2> -->

          <!-- Row 1 -->

          <div class="row">

            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h2 class="card-title" style="font-size: 30px !important;">Batch ID : {{$batch_details->batch_id}} <span style="float: right;"><a href="/batchmanagement"  >  <button type="button" class="btn btn-primary btn-growwell" style="float: right;font-family:lato !important;font-weight: bold;">Go Back</button></a></span></h2>



                <div class="row">
                  <div class="col-md-12">
                    <h3 class="card-description" style="color: #212121;font-weight: bold;">
                    Batch Details
                    </h3>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <h3 class="card-description" style="font-weight: bold;">

                    </h3>
                  </div>
                </div>


                    <div class="row">
                      <div class="col-md-6">
                        

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Batch Name</label>
                          <div class="col-sm-6">
                            {{$batch_details->batch_name}}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Batch Size</label>
                          <div class="col-sm-6">
                            {{$batch_details->batch_size}}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Enrolled Candidates</label>
                          <div class="col-sm-6">
                            {{ $enrolled_candidate_array[$batch_details->batch_id] }}

                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">SPOC Name</label>
                          <div class="col-sm-6">
                              {{ $batch_details->spoc_name }}
                          </div>
                        </div>

                     <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Duration </label>
                          <div class="col-sm-6">
                            {{ $batch_details->fduration }} to {{ $batch_details->tduration }}
                          </div>
                        </div>

                       
                      </div>

                      <div class="col-md-6">


                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Dropout Candidates </label>
                          <div class="col-sm-6">
                            {{ $dropout_candidate_array[$batch_details->batch_id] }}
                          </div>
                        </div>


                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">Assessment Date(s) </label>
                          <div class="col-sm-6">
                            {{$batch_details->assessment_date_fdt}} to {{$batch_details->assessment_date_fdt}}
                          </div>
                        </div>

                        <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">SPOC Mail </label>
                          <div class="col-sm-6">
                            {{ $batch_details->email }}
                          </div>
                        </div>
                         <div class="form-group row">
                          <label class="col-sm-6 " style="font-weight:bold;">SPOC Mobile No.</label>
                          <div class="col-sm-6">
                            {{ $batch_details->spoc_mobile_no }}
                          </div>
                        </div>

                        
                        


                      </div>


                    </div>


                </div>
              </div>
            </div>
          </div>


          <!-- row 2 -->
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
             <div class="card growwell-table-card" >
               <div class="card-body">

                   <h2 class="card-title growwell-card-title">Batch History </h2>


                 <div class="table-responsive">
                   <table class="table table-growwell">
                     <thead>
                       <tr>

                         <th>
                           Old Date
                         </th>
                         <th>
                           New Date
                         </th>
                         <th>
                           Reason
                         </th>
                         <th>
                           Date-Time
                         </th>


                       </tr>
                     </thead>
                     <tbody>

                       @if(!$batch_history_details->isEmpty())
                       @foreach($batch_history_details as $history)
                       <tr>

                         <td>
                           {{ $history->old_date }}
                         </td>

                         <td>
                            {{ $history->old_date }}

                         </td>
                         <td>
                          {{ $history->reason }}
                         </td>
                         <td>
                            {{ $history->created_at }}
                         </td>
                       

                       </tr>
                       @endforeach
                       @else 

                        <td>
                          
                         </td>

                         <td>
                           
                         </td>
                         <td>
                           No Data
                         </td>
                         <td>
                           
                         </td>

                       @endif

                     </tbody>

                   </table>
                   <nav aria-label="Page navigation example" >
                    {{ $batch_history_details->render("pagination::bootstrap-4") }}
                 </nav>
                 </div>
               </div>
             </div>
           </div>
         <!-- row 1 ends -->

       </div>

          <!-- Row 1 end -->

 <!-- row 2 -->
         <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
             <div class="card growwell-table-card" >
               <div class="card-body">

                   <h2 class="card-title growwell-card-title">Assessor History </h2>


                 <div class="table-responsive">
                   <table class="table table-growwell">
                     <thead>
                       <tr>

                         <th>
                           Old Assessor
                         </th>
                         <th>
                           New Assessor
                         </th>
                         <th>
                           Date-Time
                         </th>
                       </tr>
                     </thead>
                     <tbody>
                        @if(!$assessment_history_details->isEmpty())
                       @foreach($assessment_history_details as $history)
                       <tr>

                         <td>
                           {{ $history->old_assessor_id }}
                         </td>

                         <td>
                             {{ $history->new_assessor_id }}

                         </td>
                         <td>
                            {{ $history->created_at }}
                         </td>
                       
                       </tr>
                       @endforeach
                       @else 

                        <td>
                          
                         </td>

                         <td>
                             No Data
                         </td>
                         <td>
                          
                         </td>
                       @endif

                     </tbody>

                   </table>
                   <nav aria-label="Page navigation example" >
                    {{ $assessment_history_details->render("pagination::bootstrap-4") }}
                 </nav>
                 </div>
               </div>
             </div>
           </div>
         <!-- row 1 ends -->

       </div>
				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->



@endsection
