@extends('admin.layouts.home')

@section('content') 



<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->
    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">
            @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
				<div class="content-wrapper">

	<div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card " >
                <div class="card-body">

                    <h2 class="card-title growwell-card-title">Create new question</h2>

            

                    <form action="/createquestion" method="POST">
              {{csrf_field()}}

                           
              <div id="myGroup">
              <a href="#eng" class="btn btn-info" id="engdiv" data-parent="#myGroup" data-toggle="collapse" onclick="show_english();">English</a>

               <a href="#hin" class="btn btn-info" data-parent="#myGroup"  data-toggle="collapse" onclick="show_hindi();">Hindi</a>

               <a href="#tl" class="btn btn-info" data-parent="#myGroup"  data-toggle="collapse" onclick="show_telugu();">Telugu</a>

               <a href="#ass" class="btn btn-info" data-parent="#myGroup"  data-toggle="collapse" onclick="show_assamese();">Assamese</a>

           

              <div id="eng" class="collapse">
                <br>
                        <div class="row">
                           <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Job Role</label>
                          <div class="col-sm-9">
                         @php
                               $jobRole = App\Http\Controllers\StaticValueProviderController::jobRoles();

                          @endphp
                            <select class="form-control" name="job_role" required>
                             @foreach($jobRole as $jr)
                              <option value="{{ $jr->name }}">{{ $jr->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                         <div class="col-md-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Sections</label>
                          <div class="col-sm-9">
                            @php
                               $sections = App\Http\Controllers\StaticValueProviderController::sections();

                            @endphp
                            <select class="form-control" name="section" required>
                             @foreach($sections as $section)
                              <option value="{{ $section->name }}">{{ $section->name }}</option>
                             @endforeach
                            </select>
                          </div>
                        </div>

                      </div>
                    </div>

                 <div class="form-group">
                  <label>Question id</label>
                  <input type="text" name="ques_id" class="form-control">
                 </div>
               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question" required="">
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1" required="">
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2" required="">
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3" required="">
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4" required="">
            </div>


              <div class="row">
                      <div class="col-md-12">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Right answer</label>
                          <div class="col-sm-9"> 
                            <select  name="right_answer" class="form-control" >
                  
                   <option value="option1" > Option 1</option>
                  
                   <option value="option2"  >Option 2</option>

                   <option value="option3"  >Option 3</option>

                   <option value="option4" >Option 4</option>                   


                </select>
                          </div>
                        </div>
                      </div>
                       
                    </div>
              


              


              </div>


             
              <div id="hin" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question_hindi" >
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1_hindi" >
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2_hindi" >
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3_hindi" >
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4_hindi" >
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_hindi" class="form-control" >


               	<option value="option1" > Option 1</option>
                  
                   <option value="option2"  >Option 2</option>

                   <option value="option3"  >Option 3</option>

                   <option value="option4" >Option 4</option>   




                </select>
              </div>
              


              

            


              </div>

               </div>


                <div id="tl"  class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question_telugu" >
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1_telugu" >
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2_telugu" >
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3_telugu" >
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4_telugu" >
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_telugu" class="form-control" >



               	<option value="option1_telugu" > Option 1</option>
                  
                   <option value="option2_telugu"  >Option 2</option>

                   <option value="option3_telugu"  >Option 3</option>

                   <option value="option4_telugu" >Option 4</option>   


                </select>
              </div>
             


              

             


              </div>

              <!-- assamase div start -->



               <div id="ass" class="collapse">

               <div class="form-group">
              <label >Question</label>
              <input type="text" class="form-control" name="question_assam" >
              </div>

              <div class="form-group">
              <label >Option1</label>
              <input type="text" class="form-control" name="option1_assam" >
            </div>

               <div class="form-group">
               <label >Option2</label>
              <input type="text" class="form-control" name="option2_assam" >
            </div>

            <div class="form-group">
               <label>option3</label>
              <input type="text" class="form-control" name="option3_assam" >
              </div>

               <div class="form-group">
                <label>option4</label>
              <input type="text" class="form-control" name="option4_assam" >
            </div>

               <div class="form-group">
              <label >Right answer</label>

               <select  name="right_ans_assam" class="form-control" >

               <option value="option1" > Option 1</option>
                  
                   <option value="option2"  >Option 2</option>

                   <option value="option3"  >Option 3</option>

                   <option value="option4" >Option 4</option>   


                </select>
              </div>          
              


             

           


             


              </div>

            </div>

            <div class="modal-footer">

             <button type="submit" class="btn btn-primary btn-growwell">Save</button>
              <a href="/questionmanagement" class="btn btn-danger" >Cancel</a>
              </form>

              





                </div>
            </div>
        </div>
    </div>




				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->

<script>
   document.getElementById("engdiv").click();
</script>

@endsection

<script>

  function show_english() {
 var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    hin.style.display = "none";
    tel.style.display = "none";
    ass.style.display = "none";
    eng.style.display="block";
  
}

function show_hindi() {
  var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    eng.style.display = "none";
    tel.style.display = "none";
    ass.style.display = "none";
    hin.style.display="block";
  
}

function show_telugu() {
  var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    hin.style.display = "none";
    eng.style.display = "none";
    ass.style.display = "none";
    tel.style.display = "block";
  
}

function show_assamese() {
  var eng = document.getElementById("eng");
  var hin = document.getElementById("hin");
  var tel = document.getElementById("tl");
  var ass = document.getElementById("ass");
  
    hin.style.display = "none";
    eng.style.display = "none";
    tel.style.display = "none";
    ass.style.display = "block";
  
}


  var $myGroup = $('#myGroup');
$myGroup.on('show.bs.collapse','.collapse', function() {
    $myGroup.find('.collapse.in').collapse('hide');
});
</script>

