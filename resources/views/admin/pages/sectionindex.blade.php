@extends('admin.layouts.home')

@section('content')




<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->
    <!-- Content Body -->
		<div class="container-fluid page-body-wrapper" style="font-family: lato;">
			<div class="main-panel">

            @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
				<div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Question Management</h2> -->

          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card " >
              <div class="card growwell-table-card" >
                <div class="card-body">

                    <h2 class="card-title growwell-card-title">Section List
                     <span style="float: right;margin-left: 2%;">
                        <a href="#"  data-toggle="modal" data-target="#createsectionmodal">
                          <button type="button" class="btn btn-primary btn-growwell" style="">Create Section</button>
                        </a>

                      </span>
                        @include('admin.include.createsectionmodal')
                    
                    </h2>
                      
               
                    

                
                  <div class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                          
                          <th>
                            Section Name
                          </th>
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>



                        @foreach($sections_details as $item)
                        <tr>

                    
                           
                              <td>
                                {{$item->name}}
                              </td>
                                
                          <td>

                              
                          <span style="float: right;margin-left: 2%;">
                        <a href="#"  data-toggle="modal" data-target="#editsectionmodal{{ $item->id }}">
                          <button type="button" class="btn btn-primary btn-growwell" style="">Edit Section</button>
                        </a>
                      </span>
                       @include('admin.include.editsectionmodal',['name'=>$item->name,'id'=>$item->id])

                                <a href="{{ route('delete_section',$item->name) }}"><button type="button" class="btn btn-danger btn-growwell-danger">DELETE</button></a>
                                

                          </td>
                        </tr>
                        @endforeach






                      </tbody>

                    </table>
                    <nav aria-label="Page navigation example" >
                  {{ $sections_details->render("pagination::bootstrap-4") }}
                  </nav>
                  </div>
                </div>
              </div>
            </div>
					<!-- row 1 ends -->

				</div>
				<!-- content-wrapper ends -->

				<!-- partial:partials/_footer.html -->
				  @include('admin.include.footer')
				<!-- partial footer end -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
</div>


@endsection
