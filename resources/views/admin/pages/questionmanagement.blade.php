@extends('admin.layouts.home')

@section('content')



<!-- Navbar start -->
 @include('admin.include.navbaradmin')
<!-- Navbar end -->
    <!-- Content Body -->
    <div class="container-fluid page-body-wrapper" style="font-family: lato;">
      <div class="main-panel">
            @if(Session::has('message'))
          <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
          @endif
        <div class="content-wrapper">

          <!-- <h2  style="font-size: 40px !important;color:Black;font-weight:bolder !important;">Question Management</h2> -->

          <!-- Row 1 -->


          <!-- Row 1 end -->
           <!-- row 2 -->
          <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card growwell-table-card" >
                <div class="card-body">

                    <h2 class="card-title growwell-card-title">Question List

                        <span class="headingbtnspan" style="float: right;margin-left: 2%;">
                        <a href="/questioncreate"  class="btn btn-primary btn-growwell">
                          Create Question
                        </a>
                      </span>

                      <span class="headingbtnspan" style="float: right;margin-left: 2%;">
                        <a href="#"  data-toggle="modal" data-target="#bulkquestionmodal">
                          <button type="button" class="btn btn-primary btn-growwell" style="">Bulk Upload</button>
                        </a>

                      </span>
                      <span class="headingbtnspan" style="float: right;margin-left: 2%;">
                        <a href="{{ route('sectionindex') }}" >
                          <button type="button" class="btn btn-primary btn-growwell" style="">Create Section</button>
                        </a>

                      </span>


                    </h2>

                     <!-- <a href="#"  data-toggle="modal" data-target="#createsectionmodal">  <button type="button" class="btn btn-primary btn-growwell" >Create Section</button></a> -->
                          {{-- @include('admin.include.createsectionmodal') --}}





                      <!-- <div class="text-center">
                        <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#orangeModalSubscription">Launch
                          modal Subscription</a>
                      </div> -->

                      @include('admin.include.bulkquestionmodal')
                      @include('admin.include.createquestionmodal')


                  <!-- <p class="card-description">
                    Add class <code>.table-striped</code>
                  </p> -->
                  <div class="table-responsive">
                    <table class="table table-growwell">
                      <thead>
                        <tr>
                          <th>
                            Question ID
                          </th>
                          <th>
                            Question
                          </th>
                          <th>
                            Option 1
                          </th>
                          <th>
                            Option 2
                          </th>
                          <th>
                            Option 3
                          </th>
                          <th>
                            Option 4
                          </th>

                          <th>
                            Right Answer
                          </th>
                          <th>
                            Job Role
                          </th>
                          <th>
                            Section
                          </th>
                          <th>
                            Action
                          </th>
                        </tr>
                      </thead>
                      <tbody>



                        @foreach($question_details as $item)
                        <tr>

                          <td>
                            {{ $item->id }}
                          </td>
                          <td>
                            {{$item->question}}/
                            {{$item->question_hindi}}/
                            {{$item->question_teleugu}}/
                            {{$item->question_assam}}
                          </td>
                          <td>
                            {{$item->option1}}/
                            {{$item->option1_hindi}}/
                            {{$item->option1_teleugu}}/
                            {{$item->option1_assam}}
                          </td>
                          <td>
                            {{$item->option2}}/
                            {{$item->option2_hindi}}/
                            {{$item->option2_teleugu}}/
                            {{$item->option2_assam}}
                          </td>
                          <td>
                           {{$item->option3}}/
                            {{$item->option3_hindi}}/
                            {{$item->option3_teleugu}}/
                            {{$item->option3_assam}}
                          </td>
                          <td>
                            {{$item->option4}}/
                            {{$item->option4_hindi}}/
                            {{$item->option4_teleugu}}/
                            {{$item->option4_assam}}
                          </td>
                          <td>
                            {{$item->right_answer}}/
                            {{$item->right_ans_hindi}}/
                            {{$item->right_ans_teleugu}}/
                            {{$item->right_ans_assam}}
                          </td>
                          <td>
                            {{$item->job_role}}
                          </td>

                              <td>
                                {{$item->section}}
                              </td>

                          <td nowrap="nowrap">

                            <a href="/question/edit/page/{{$item->id}}" type="button" style="COLOR:WHITE; margin-right:4%;" class="btn btn-primary btn-growwell-danger" >Edit</a>

                                <button type="button" class="btn btn-danger btn-growwell-danger" data-toggle="modal" data-target="#deleteQuestionmodal{{$item->id}}">DELETE</button>
                                 @include('admin.include.deleteQuestionModal')

                          </td>
                        </tr>
                        @endforeach






                      </tbody>

                    </table>
                    <nav aria-label="Page navigation example" >
                  {{ $question_details->render("pagination::bootstrap-4") }}
                  </nav>
                  </div>
                </div>
              </div>
            </div>
          <!-- row 1 ends -->

        </div>
        <!-- content-wrapper ends -->

        <!-- partial:partials/_footer.html -->
          @include('admin.include.footer')
        <!-- partial footer end -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->



@endsection
