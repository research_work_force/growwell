 <!DOCTYPE html> 
<html lang="en">
      @include('admin.include.headerLinks')
  <body>
    <div class="container-scroller">

    
    @yield('content')

    </div>
		<!-- container-scroller -->
      @include('admin.include.footerLinks')
  </body>
</html>