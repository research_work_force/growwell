<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin routes start

Route::get('/tal/{username}/{password}','AuthController@adminReg');

Route::get('/adminlogin', function () {
    return view('login.pages.adminlogin');
});
Route::get('/', function () {
    return view('login.pages.adminlogin');
});


Route::post('/admin/login/checking', 'AuthController@adminLoginService')
          ->name('admin.login.service');


Route::group(['middleware' => 'validateadminloginsession'],function(){   //Session validation in every pages


Route::get('/dashboard', function () {
    return view('admin.pages.home');
})->name('admin.pages.home');

Route::get('/logout','AuthController@adminLogout')
                ->name('admin.logout');

Route::get('/changepassword','AuthController@changePasswordAdminPage')
            ->name('admin.changepassword');

Route::post('/changepassword/checking','AuthController@changePasswordAdmin')
            ->name('admin.changepassword.save');
 

Route::get('/searchassessor', 'AssessorController@searchAssessor');
Route::get('/searchassessor/filters', 'AssessorController@searchAssessorByFilters')
                ->name('search.assessor.filters');


Route::get('/assessmentreq/{status}','BatchController@getAssessorAssignedBatchesAdmin');
Route::get('/assessmentreq','BatchController@getAssessorAssignedBatchesAdmin');

Route::get('/assessed/q/{status}','BatchController@getAssessorTestBatchesAdmin');

Route::get('/studentmanagement','StudentController@studentDetails');
Route::post('/student/insert','StudentController@studentDataInsert');
Route::post('/student/delete','StudentController@studentDelete');
Route::post('/student/edit','StudentController@studentDetailsUpdate');
Route::post('/student/bulk/upload','StudentController@BulkImportStudent')
            ->name('admin.student.bulk.upload');

Route::get('/viewbatchhistory/{batch_id}','BatchController@BatchDetailsSinglePage');

Route::get('/assessorprofileview/{assessor_id}','AssessorController@assessorProfileAdmin')
          ->name('admin.assessor.profile');


/*question*/
Route::get('/questionmanagement','QuestionController@QuestionList');
Route::post('/question/en/save','QuestionController@questionEnDataInsert')
            ->name('question.en.save');
Route::post('/question/hi/save','QuestionController@questionHiDataInsert')
            ->name('question.hi.save');
Route::post('/question/tl/save','QuestionController@questionTlDataInsert')
            ->name('question.tl.save');
Route::post('/question/as/save','QuestionController@questionAsDataInsert')
            ->name('question.as.save');

// Route::post('/question/delete','QuestionController@questionDelete');
// Route::post('/question/edit','QuestionController@QuestionDetailsUpdate');
// Route::post('/question/bulkupload','QuestionController@handleImportQuestion');

 Route::post('/question/delete','QuestionController@questionDelete');
Route::post('/questionsedit','QuestionController@questionDetailsEdit');
Route::get('/question/edit/page/{id}','QuestionController@questionEditPage');
Route::post('/question/bulkupload','QuestionController@handleImportQuestion');

Route::get('/questioncreate',function(){
    return view('admin.pages.questionCreate');
});
Route::post('/createquestion','QuestionController@questionCreateNew');


Route::get('/reportstudentwise', function () {
    return view('admin.pages.reportmanagementstudentwise');
});
Route::get('/reportsmarksview', function () {
    return view('admin.pages.reportmanagementmarksview');
});

// Assessed req url

Route::get('/assess/student/list/{batch_id}','StudentController@studentListAssessedTabAdmin')
            ->name('admin.assessreq.student.list');

/*assessor*/
Route::post('/assessor/create','AssessorController@assessorDataInsert');
Route::get('/assessormanagement','AssessorController@assessorList');
Route::post('/assessor/search','AssessorController@assessorListSearch');

Route::get('/assessor/list','StaticValueProviderController@getAssessors')
           ->name('assessor.list');

Route::post('/admin/assessor/delete','AssessorController@deleteAssessor')
            ->name('admin.assessor.delete');

Route::post('/admin/assessor/update','AssessorController@updateAssessor')
            ->name('admin.assessor.update');

Route::post('/assessor/bulk','AssessorController@bulkAssessorUpload')
            ->name('assessor.bulkupload');
// Batch Management

Route::post('/batch/save','BatchController@saveBatchDetails')
            ->name('admin.batch.save');

Route::get('/batchmanagement', 'BatchController@BatchDetails');

Route::get('/batchreq/search','BatchController@searchBatchAdmin')
                ->name('admin.batchreq.search');

Route::post('/batch/assign','BatchController@batchAssignToAssessor')
            ->name('assessor.batch.assign');

Route::post('/batch/assessor/reassign','BatchController@batchReAssignAssessor')
            ->name('assessor.batch.reassign');
            
Route::get('/batch/delete/{batch_id}','BatchController@batchDelete')
            ->name('admin.batch.delete');
Route::get('/batch/search','BatchController@searchBatchByFilters')
            ->name('admin.batch.search');

Route::get('/student/test/list/{batch_id}','StudentController@studentTestProgressPage')
            ->name('admin.student.list.progress');

Route::get('/batch/pending/request','BatchController@getAssessmentBatchReq')
->name('batch.pending.request');
});

Route::post('/change/assessmentdate','BatchController@changeAssessmentDate')
            ->name('admin.change.assessmentdate.save');

Route::post('/question/sections/create','QuestionController@createSection');

Route::post('/test/review','TestController@reviewTestbyAssessor')
           ->name('admin.assessor.review');

Route::get('/assessorreport','ReportController@batchReport')
            ->name('admin.batch.report');
Route::get('/admin/student/report/{batch_id}','ReportController@studentwiseReportAdmin')
            ->name('admin.student.report');

Route::get('/assessorreportsmarksview/{batch_id}/{student_id}','ReportController@adminReportsStudentMarksWise')
                ->name('admin.markswise.report');
Route::get('/batch/approve/{batch_id}','BatchController@adminApproveBatch')
                ->name('admin.assessreq.student.approve');

Route::get('/sectionindex','QuestionController@sectionindex')->name('sectionindex');
Route::post('/question/sections/edit','QuestionController@editSection');
Route::get('/delete_section/{name}','QuestionController@delete_section')->name('delete_section');

Route::post('/report/search','ReportController@batchReportSearch')
           ->name('admin.report.search');

// Admin Routes end




// Assessor routes

Route::get('/assessorlogin', function () {
    return view('login.pages.assessorlogin');
})->name('assessor.login');

Route::post('/assessor/login/service','AuthController@assessorLoginService')
           ->name('assessorlogin.service');

Route::group(['middleware' => 'validateassessorloginsession'],function(){   //Session validation in every pages

Route::prefix('assessor')->group(function () {

Route::get('/req/{status}', 'BatchController@getAssessorAssignedBatchs');

Route::get('/dashboard/{status}','BatchController@getAssessorDashboard')
            ->name('assessor.pages.assessorbatchmanagementa');


Route::get('/assessmentreq', function () {
    return view('assessor.pages.assessorassessmentreq');
});

Route::post('/assessor/req/accept',"BatchController@assessorReqAccept")
            ->name('assessor.req.accept');

Route::post('/assessor/req/reject',"BatchController@assessorReqReject")
            ->name('assessor.req.reject');

Route::get('/student/list/{batch_id}','BatchController@assessorStudentList')
            ->name('assessor.student.list');

Route::get('/logout','AuthController@assesorLogout')
                ->name('assessor.logout');

Route::get('/profile','AssessorController@assessorProfilePage')->name('assessor.profile');

Route::get('/changepassword','AuthController@changePasswordAssessorPage')
            ->name('assessor.changepassword');

Route::post('/changepassword/checking','AuthController@changePasswordAssessor')
            ->name('assessor.changepassword.save');

Route::post('/profile/update','AssessorController@updateAssessorProfileData')
            ->name('assessor.profile.update');

Route::post('/student/url/send','StudentController@urlSendToStudent')
            ->name('student.test.url');
Route::get('/assessor/batch/search','BatchController@searchBatchByFiltersAssessor')
                ->name('assessor.batch.search');

Route::get('/test/page/{username}/{batch_id}','StudentController@testWindow')->name('assessor.test.window');
Route::get('/test/url/{batch_id}/{std_id}','TestController@assessorTestURL')
        ->name('assessor.test.url');

Route::post('/test/url/validate','TestController@assessorTestURLValidate')
        ->name('assessor.test.auth');  //

Route::post('/image/save','imageandgeocontroller@assessorImgBeforeTest')
                ->name('assessor.img.save');
Route::get('/image/window','imageandgeocontroller@imageWindowForAssessor');

// Assessed section
Route::get('/assessed/{status}','BatchController@assessedBatchListAssessor');

Route::post('/assessed/req/accept',"BatchController@assessorAssessedReqAccept")
            ->name('assessor.assessed.req.accept');

Route::post('/assessed/req/reject',"BatchController@assessorAssessedReqReject")
            ->name('assessor.assessed.req.reject');

Route::get('/assessorreport','ReportController@assessorReport')
            ->name('assessor.report');

Route::get('/assessorreportstudentwise/{batch_id}','ReportController@assessorStudentWiseReport')
                ->name('assessor.student.report');

Route::get('/assessorreportsmarksview/{batch_id}/{student_id}','ReportController@assessorReportsStudentMarksWise')
                ->name('assessor.markswise.report');

Route::post('/updated/ques/ans','TestController@updateRightAnswerByAssessor');

Route::post('/review/batch','BatchController@reviewBatchToAdminByAssessor')
            ->name('assessor.review');

});
});




// Assessor routes End




// Student routes

Route::get('/studentlogin', function () {
    return view('login.pages.studentlogin');
});

Route::post('/student/login/check','AuthController@studentLoginCheck')
            ->name('student.login.check');

Route::get('/student/questiondashboard/','TestController@questionReloadWithBatch');
                // ->middleware('CheckValidExamURL'); //Questions reloading

Route::get('/test/page/{batch_id}','TestController@studentInfoCheckLoginPage');
Route::get('/test/page','TestController@studentWrongInfoCheckLoginPage');

Route::get('/test/window','TestController@questions')
            ->name('student.test.window');

Route::post('/qtrans','TestController@questions_transaction_insert')
              ->name('student.test.qtrans');

Route::get('/qtransdata/{qid}','TestController@questions_trans_data')
              ->name('student.test.qtransdata');
Route::get('/demodataqtrans','TestController@questions_transaction_insert_demodata')
              ->name('student.test.qtransdemodata');

Route::get('/demodataq','QuestionController@questions_insert_demodata')
                      ->name('student.test.qdemodata');

Route::get('/student/examend', function () {
                return view('student.pages.examend');
                      });

Route::get('/student/imageandgeocapture', function () {
                        return view('student.pages.imageandgeocapture');
                    });

Route::post('/imageandgeoinsert','imageandgeocontroller@makeimage')
                    ->name('student.test.imageandgeo');

Route::post('/imageandgeosave','imageandgeocontroller@saveimage')
                      ->name('student.test.imageandgeosave');

Route::post('/student/exam/finished','QuestionController@resultChecking')
                ->name('exam.result.checking');

// Route::get('/test/page/{username}/{batch_id}','StudentController@testWindow')->name('student.test.window');


// Student routes End

Route::get('/camrecorder', function () {
                        return view('student.pages.camrecorder');
                        });
Route::get('/camrecorder2', function () {
                      return view('student.pages.camrecorder2');
                    });
                    Route::get('/camrecorder3', function () {
                                          return view('student.pages.camrecorder3');
                                        });

Route::post('/camrecorder3/save','videoController@savevideo')
                        ->name('video.save');

Route::get('gen_pdf/{batch_id}','ReportController@gen_pdf')->name('gen_pdf');